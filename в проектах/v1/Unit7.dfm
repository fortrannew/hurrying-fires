object Form7: TForm7
  Left = 215
  Top = 338
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1092#1072#1081#1083#1086#1074
  ClientHeight = 103
  ClientWidth = 330
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object TabbedNotebook1: TTabbedNotebook
    Left = 0
    Top = 0
    Width = 331
    Height = 57
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'MS Sans Serif'
    TabFont.Style = []
    TabOrder = 0
    OnClick = TabbedNotebook1Click
    object TTabPage
      Left = 4
      Top = 24
      Caption = #1052#1080#1082#1088#1086#1089#1093#1077#1084#1072'1'
      object Label1: TLabel
        Left = 168
        Top = 8
        Width = 37
        Height = 13
        Caption = #1057#1090#1072#1090#1091#1089':'
      end
      object Label2: TLabel
        Left = 208
        Top = 8
        Width = 54
        Height = 13
        Caption = #1053#1077#1090' '#1092#1072#1081#1083#1072
      end
      object Button1: TButton
        Left = 8
        Top = 0
        Width = 75
        Height = 25
        Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100
        Default = True
        TabOrder = 0
        OnClick = Button1Click
      end
      object Button6: TButton
        Left = 88
        Top = 0
        Width = 75
        Height = 25
        Caption = #1054#1095#1080#1089#1090#1080#1090#1100
        Default = True
        TabOrder = 1
        OnClick = Button7Click
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = #1052#1080#1082#1088#1086#1089#1093#1077#1084#1072'2'
      object Label7: TLabel
        Left = 208
        Top = 8
        Width = 54
        Height = 13
        Caption = #1053#1077#1090' '#1092#1072#1081#1083#1072
      end
      object Label8: TLabel
        Left = 168
        Top = 8
        Width = 37
        Height = 13
        Caption = #1057#1090#1072#1090#1091#1089':'
      end
      object Button4: TButton
        Left = 8
        Top = 0
        Width = 75
        Height = 25
        Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100
        Default = True
        TabOrder = 0
        OnClick = Button1Click
      end
      object Button8: TButton
        Left = 88
        Top = 0
        Width = 75
        Height = 25
        Caption = #1054#1095#1080#1089#1090#1080#1090#1100
        Default = True
        TabOrder = 1
        OnClick = Button7Click
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = #1052#1080#1082#1088#1086#1089#1093#1077#1084#1072'3'
      object Label3: TLabel
        Left = 208
        Top = 8
        Width = 54
        Height = 13
        Caption = #1053#1077#1090' '#1092#1072#1081#1083#1072
      end
      object Label4: TLabel
        Left = 168
        Top = 8
        Width = 37
        Height = 13
        Caption = #1057#1090#1072#1090#1091#1089':'
      end
      object Button2: TButton
        Left = 8
        Top = 0
        Width = 75
        Height = 25
        Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100
        Default = True
        TabOrder = 0
        OnClick = Button1Click
      end
      object Button9: TButton
        Left = 88
        Top = 0
        Width = 75
        Height = 25
        Caption = #1054#1095#1080#1089#1090#1080#1090#1100
        Default = True
        TabOrder = 1
        OnClick = Button7Click
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = #1052#1080#1082#1088#1086#1089#1093#1077#1084#1072'4'
      object Label5: TLabel
        Left = 208
        Top = 8
        Width = 54
        Height = 13
        Caption = #1053#1077#1090' '#1092#1072#1081#1083#1072
      end
      object Label6: TLabel
        Left = 168
        Top = 8
        Width = 37
        Height = 13
        Caption = #1057#1090#1072#1090#1091#1089':'
      end
      object Button3: TButton
        Left = 8
        Top = 0
        Width = 75
        Height = 25
        Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100
        Default = True
        TabOrder = 0
        OnClick = Button1Click
      end
      object Button10: TButton
        Left = 88
        Top = 0
        Width = 75
        Height = 25
        Caption = #1054#1095#1080#1089#1090#1080#1090#1100
        Default = True
        TabOrder = 1
        OnClick = Button7Click
      end
    end
  end
  object MicroshemaFail1: TEdit
    Left = 8
    Top = 104
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object MicroshemaFail2: TEdit
    Left = 144
    Top = 104
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object MicroshemaFail3: TEdit
    Left = 272
    Top = 104
    Width = 121
    Height = 21
    TabOrder = 3
  end
  object MicroshemaFail4: TEdit
    Left = 400
    Top = 104
    Width = 121
    Height = 21
    TabOrder = 4
  end
  object Button5: TButton
    Left = 128
    Top = 72
    Width = 75
    Height = 25
    Caption = #1043#1086#1090#1086#1074#1086
    TabOrder = 5
    OnClick = Button5Click
  end
end
