object Form8: TForm8
  Left = 771
  Top = 194
  Width = 162
  Height = 148
  Caption = 'Form8'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 8
    Width = 103
    Height = 26
    Alignment = taCenter
    Caption = #1042#1074#1077#1076#1080#1090#1077' '#1082#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1084#1080#1082#1088#1086#1089#1093#1077#1084
    WordWrap = True
  end
  object CurrencyEdit1: TCurrencyEdit
    Left = 49
    Top = 40
    Width = 56
    Height = 21
    Alignment = taCenter
    AutoSize = False
    DisplayFormat = ',0'#39#39';-,4'#39#39
    MaxValue = 4.000000000000000000
    MinValue = 1.000000000000000000
    TabOrder = 0
    Value = 4.000000000000000000
    ZeroEmpty = False
  end
  object Button1: TButton
    Left = 16
    Top = 72
    Width = 33
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 64
    Top = 72
    Width = 81
    Height = 25
    Caption = #1055#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
    Default = True
    TabOrder = 2
    OnClick = Button2Click
  end
end
