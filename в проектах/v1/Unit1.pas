unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ActnList, StdCtrls, Menus, ExtCtrls, ComCtrls, IWControl,
  IWCompButton, Spin, Buttons, RxGrdCpt, RxCalc, SpeedBar, RXDice, Animate,
  GIFCtrl, ExtDlgs, ToolWin, Mask, ToolEdit, Grids, Registry;

type
  TForm1 = class(TForm)
    OpenDialog1: TOpenDialog;
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    GroupBox1: TGroupBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Memo1: TMemo;
    SaveDialog1: TSaveDialog;
    CheckBox1: TCheckBox;
    d: TLabel;
    Timer1: TTimer;
    Label4: TLabel;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    RxGIFAnimator1: TRxGIFAnimator;
    Label6: TLabel;
    RxGIFAnimator2: TRxGIFAnimator;
    Label7: TLabel;
    N4: TMenuItem;
    N5: TMenuItem;
    Label3: TLabel;
    GroupBox3: TGroupBox;
    Edit5: TEdit;
    Label8: TLabel;
    N6: TMenuItem;
    Label9: TLabel;
    N7: TMenuItem;
    N8: TMenuItem;
    SpinEdit1: TSpinEdit;
    Memo2: TMemo;
    OpenPictureDialog1: TOpenPictureDialog;
    N9: TMenuItem;
    GroupBox4: TGroupBox;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    Label10: TLabel;
    ScrollBox1: TScrollBox;
    N13: TMenuItem;
    Panel1: TPanel;
    Shape1: TShape;
    Shape2: TShape;
    Shape3: TShape;
    Shape4: TShape;
    Shape5: TShape;
    Shape6: TShape;
    Shape7: TShape;
    Shape8: TShape;
    Shape9: TShape;
    Shape10: TShape;
    Shape11: TShape;
    Shape12: TShape;
    Shape13: TShape;
    Shape14: TShape;
    Shape15: TShape;
    Shape16: TShape;
    Shape17: TShape;
    Shape18: TShape;
    Shape19: TShape;
    Shape20: TShape;
    Shape21: TShape;
    Shape22: TShape;
    Shape23: TShape;
    Shape24: TShape;
    Shape25: TShape;
    Shape26: TShape;
    Shape27: TShape;
    Shape29: TShape;
    Shape30: TShape;
    Shape31: TShape;
    Shape32: TShape;
    Shape33: TShape;
    Shape34: TShape;
    Shape35: TShape;
    Shape36: TShape;
    Shape37: TShape;
    Shape38: TShape;
    Shape39: TShape;
    Shape40: TShape;
    Shape41: TShape;
    Shape42: TShape;
    Shape43: TShape;
    Shape44: TShape;
    Shape45: TShape;
    Shape46: TShape;
    Shape47: TShape;
    Shape48: TShape;
    Shape49: TShape;
    Shape50: TShape;
    Shape51: TShape;
    Shape52: TShape;
    Shape53: TShape;
    Shape54: TShape;
    Shape55: TShape;
    Shape56: TShape;
    Shape58: TShape;
    Shape59: TShape;
    Shape60: TShape;
    Shape61: TShape;
    Shape62: TShape;
    Shape63: TShape;
    Shape64: TShape;
    Label12: TLabel;
    N14: TMenuItem;
    Label11: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    Label61: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label64: TLabel;
    Label65: TLabel;
    Label66: TLabel;
    Label67: TLabel;
    Label69: TLabel;
    Label70: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    Label73: TLabel;
    Label74: TLabel;
    Label75: TLabel;
    Button1: TButton;
    Button2: TButton;
    Shape66: TShape;
    Shape67: TShape;
    Shape68: TShape;
    Shape69: TShape;
    Shape70: TShape;
    Shape71: TShape;
    Shape72: TShape;
    Shape73: TShape;
    Shape74: TShape;
    Shape75: TShape;
    Shape76: TShape;
    Shape77: TShape;
    Shape78: TShape;
    Shape79: TShape;
    Shape80: TShape;
    Shape81: TShape;
    Shape82: TShape;
    Shape83: TShape;
    Shape84: TShape;
    Shape85: TShape;
    Shape86: TShape;
    Shape87: TShape;
    Shape88: TShape;
    Shape89: TShape;
    Shape90: TShape;
    Shape91: TShape;
    Shape92: TShape;
    Shape93: TShape;
    Shape94: TShape;
    Shape95: TShape;
    Shape96: TShape;
    Shape97: TShape;
    Shape98: TShape;
    Shape99: TShape;
    Shape100: TShape;
    Shape101: TShape;
    Shape102: TShape;
    Shape103: TShape;
    Shape104: TShape;
    Shape105: TShape;
    Shape106: TShape;
    Shape107: TShape;
    Shape108: TShape;
    Shape109: TShape;
    Shape110: TShape;
    Shape111: TShape;
    Shape112: TShape;
    Shape113: TShape;
    Shape114: TShape;
    Shape115: TShape;
    Shape116: TShape;
    Shape117: TShape;
    Shape118: TShape;
    Shape119: TShape;
    Shape120: TShape;
    Label77: TLabel;
    Label78: TLabel;
    Label79: TLabel;
    Label80: TLabel;
    Label81: TLabel;
    Label82: TLabel;
    Label83: TLabel;
    Label84: TLabel;
    Label85: TLabel;
    Label86: TLabel;
    Label87: TLabel;
    Label88: TLabel;
    Label89: TLabel;
    Label90: TLabel;
    Label91: TLabel;
    Label92: TLabel;
    Label93: TLabel;
    Label94: TLabel;
    Label95: TLabel;
    Label96: TLabel;
    Label97: TLabel;
    Label98: TLabel;
    Label99: TLabel;
    Label100: TLabel;
    Label101: TLabel;
    Label102: TLabel;
    Label103: TLabel;
    Label104: TLabel;
    Label105: TLabel;
    Label106: TLabel;
    Label107: TLabel;
    Label108: TLabel;
    Label109: TLabel;
    Label110: TLabel;
    Label111: TLabel;
    Label112: TLabel;
    Label113: TLabel;
    Label114: TLabel;
    Label115: TLabel;
    Label116: TLabel;
    Label117: TLabel;
    Label118: TLabel;
    Label119: TLabel;
    Label120: TLabel;
    Label121: TLabel;
    Label122: TLabel;
    Label123: TLabel;
    Label124: TLabel;
    Label125: TLabel;
    Label126: TLabel;
    Label127: TLabel;
    Label128: TLabel;
    Label129: TLabel;
    Label130: TLabel;
    Label131: TLabel;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    Memo3: TMemo;
    Timer2: TTimer;
    Shape121: TShape;
    Shape122: TShape;
    Shape123: TShape;
    Shape124: TShape;
    Shape125: TShape;
    Shape126: TShape;
    Shape127: TShape;
    Shape128: TShape;
    Shape129: TShape;
    Shape130: TShape;
    Shape131: TShape;
    Shape132: TShape;
    Shape133: TShape;
    Shape134: TShape;
    Shape135: TShape;
    Shape136: TShape;
    Shape137: TShape;
    Shape138: TShape;
    Shape139: TShape;
    Shape140: TShape;
    Shape141: TShape;
    Shape142: TShape;
    Shape143: TShape;
    Shape144: TShape;
    Shape145: TShape;
    Shape146: TShape;
    Shape147: TShape;
    Shape148: TShape;
    Shape149: TShape;
    Shape150: TShape;
    Shape151: TShape;
    Shape152: TShape;
    Shape153: TShape;
    Shape154: TShape;
    Shape155: TShape;
    Shape156: TShape;
    Shape157: TShape;
    Shape158: TShape;
    Shape159: TShape;
    Shape160: TShape;
    Shape161: TShape;
    Shape162: TShape;
    Shape163: TShape;
    Shape164: TShape;
    Shape165: TShape;
    Shape166: TShape;
    Shape167: TShape;
    Shape168: TShape;
    Shape169: TShape;
    Shape170: TShape;
    Shape171: TShape;
    Shape172: TShape;
    Shape173: TShape;
    Shape174: TShape;
    Shape175: TShape;
    Shape176: TShape;
    Shape177: TShape;
    Shape178: TShape;
    Shape179: TShape;
    Shape180: TShape;
    Shape181: TShape;
    Shape182: TShape;
    Shape183: TShape;
    Shape184: TShape;
    Shape185: TShape;
    Shape186: TShape;
    Shape187: TShape;
    Shape188: TShape;
    Shape189: TShape;
    Shape190: TShape;
    Shape191: TShape;
    Shape192: TShape;
    Shape193: TShape;
    Shape194: TShape;
    Shape195: TShape;
    Shape196: TShape;
    Shape197: TShape;
    Shape198: TShape;
    Shape199: TShape;
    Shape200: TShape;
    Shape201: TShape;
    Shape202: TShape;
    Shape203: TShape;
    Shape204: TShape;
    Shape205: TShape;
    Shape206: TShape;
    Shape207: TShape;
    Shape208: TShape;
    Shape209: TShape;
    Shape210: TShape;
    Shape211: TShape;
    Shape212: TShape;
    Shape213: TShape;
    Shape214: TShape;
    Shape215: TShape;
    Shape216: TShape;
    Shape217: TShape;
    Shape218: TShape;
    Shape219: TShape;
    Shape220: TShape;
    Shape221: TShape;
    Shape222: TShape;
    Shape223: TShape;
    Shape224: TShape;
    Shape225: TShape;
    Shape226: TShape;
    Shape227: TShape;
    Shape228: TShape;
    Shape229: TShape;
    Shape230: TShape;
    Shape231: TShape;
    Shape232: TShape;
    Shape233: TShape;
    Shape234: TShape;
    Shape235: TShape;
    Shape236: TShape;
    Shape237: TShape;
    Shape238: TShape;
    Shape239: TShape;
    Shape240: TShape;
    Timer3: TTimer;
    Label132: TLabel;
    Label133: TLabel;
    Label134: TLabel;
    Button3: TButton;
    Shape28: TShape;
    Label39: TLabel;
    Shape57: TShape;
    Label68: TLabel;
    Shape65: TShape;
    Label76: TLabel;
    Label135: TLabel;
    Button4: TButton;
    N19: TMenuItem;
    N20: TMenuItem;
    N21: TMenuItem;
    N22: TMenuItem;
    N23: TMenuItem;
    PopupMenu1: TPopupMenu;
    Cjplfnm1: TMenuItem;
    N24: TMenuItem;
    N25: TMenuItem;
    N26: TMenuItem;
    N27: TMenuItem;
    N28: TMenuItem;
    N29: TMenuItem;
    N30: TMenuItem;
    N31: TMenuItem;
    N32: TMenuItem;
    N33: TMenuItem;
    N34: TMenuItem;
    N35: TMenuItem;
    N36: TMenuItem;
    N37: TMenuItem;
    N38: TMenuItem;
    N39: TMenuItem;
    procedure N3Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Edit3KeyPress(Sender: TObject; var Key: Char);
    procedure RxGIFAnimator1Click(Sender: TObject);
    procedure Label5MouseEnter(Sender: TObject);
    procedure Label6MouseEnter(Sender: TObject);
    procedure Label5MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label5MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label7MouseEnter(Sender: TObject);
    procedure Label7MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Label7MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure N5Click(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure N8Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure SpinEdit1Change(Sender: TObject);
    procedure N13Click(Sender: TObject);
    procedure Panel1Resize(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure N10Click(Sender: TObject);
    procedure N14Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure N12Click(Sender: TObject);
    procedure Label11MouseEnter(Sender: TObject);
    procedure Label20MouseEnter(Sender: TObject);
    procedure Label28MouseEnter(Sender: TObject);
    procedure Label36MouseEnter(Sender: TObject);
    procedure Label44MouseEnter(Sender: TObject);
    procedure Label52MouseEnter(Sender: TObject);
    procedure Label60MouseEnter(Sender: TObject);
    procedure Label68MouseEnter(Sender: TObject);
    procedure Label76MouseEnter(Sender: TObject);
    procedure Label84MouseEnter(Sender: TObject);
    procedure Label92MouseEnter(Sender: TObject);
    procedure Label100MouseEnter(Sender: TObject);
    procedure Label108MouseEnter(Sender: TObject);
    procedure Label116MouseEnter(Sender: TObject);
    procedure Label124MouseEnter(Sender: TObject);
    procedure Label11Click(Sender: TObject);
    procedure Label13Click(Sender: TObject);
    procedure Label14Click(Sender: TObject);
    procedure Edit5KeyPress(Sender: TObject; var Key: Char);
    procedure SpinEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure Label15Click(Sender: TObject);
    procedure Label16Click(Sender: TObject);
    procedure Label17Click(Sender: TObject);
    procedure Label18Click(Sender: TObject);
    procedure Label19Click(Sender: TObject);
    procedure Label20Click(Sender: TObject);
    procedure Label28Click(Sender: TObject);
    procedure Label36Click(Sender: TObject);
    procedure Label44Click(Sender: TObject);
    procedure Label68Click(Sender: TObject);
    procedure Label60Click(Sender: TObject);
    procedure Label52Click(Sender: TObject);
    procedure Label76Click(Sender: TObject);
    procedure Label84Click(Sender: TObject);
    procedure Label92Click(Sender: TObject);
    procedure Label100Click(Sender: TObject);
    procedure Label108Click(Sender: TObject);
    procedure Label116Click(Sender: TObject);
    procedure Label124Click(Sender: TObject);
    procedure Label132Click(Sender: TObject);
    procedure Label140Click(Sender: TObject);
    procedure Label156Click(Sender: TObject);
    procedure Label172Click(Sender: TObject);
    procedure Label188Click(Sender: TObject);
    procedure Label204Click(Sender: TObject);
    procedure Label220Click(Sender: TObject);
    procedure Label236Click(Sender: TObject);
    procedure Label252Click(Sender: TObject);
    procedure Label148Click(Sender: TObject);
    procedure Label164Click(Sender: TObject);
    procedure Label180Click(Sender: TObject);
    procedure Label196Click(Sender: TObject);
    procedure Label212Click(Sender: TObject);
    procedure Label228Click(Sender: TObject);
    procedure Label244Click(Sender: TObject);
    procedure Label260Click(Sender: TObject);
    procedure Label268Click(Sender: TObject);
    procedure Label276Click(Sender: TObject);
    procedure Label284Click(Sender: TObject);
    procedure Label292Click(Sender: TObject);
    procedure Label300Click(Sender: TObject);
    procedure Label308Click(Sender: TObject);
    procedure Label316Click(Sender: TObject);
    procedure Label324Click(Sender: TObject);
    procedure Label332Click(Sender: TObject);
    procedure Label340Click(Sender: TObject);
    procedure Label348Click(Sender: TObject);
    procedure Label356Click(Sender: TObject);
    procedure Label364Click(Sender: TObject);
    procedure Label372Click(Sender: TObject);
    procedure Label380Click(Sender: TObject);
    procedure Label388Click(Sender: TObject);
    procedure Label396Click(Sender: TObject);
    procedure Label404Click(Sender: TObject);
    procedure Label21Click(Sender: TObject);
    procedure Label29Click(Sender: TObject);
    procedure Label37Click(Sender: TObject);
    procedure Label53Click(Sender: TObject);
    procedure Label61Click(Sender: TObject);
    procedure Label69Click(Sender: TObject);
    procedure Label77Click(Sender: TObject);
    procedure Label85Click(Sender: TObject);
    procedure Label133Click(Sender: TObject);
    procedure Label125Click(Sender: TObject);
    procedure Label117Click(Sender: TObject);
    procedure Label109Click(Sender: TObject);
    procedure Label101Click(Sender: TObject);
    procedure Label93Click(Sender: TObject);
    procedure Label141Click(Sender: TObject);
    procedure Label149Click(Sender: TObject);
    procedure Label157Click(Sender: TObject);
    procedure Label165Click(Sender: TObject);
    procedure Label173Click(Sender: TObject);
    procedure Label181Click(Sender: TObject);
    procedure Label189Click(Sender: TObject);
    procedure Label197Click(Sender: TObject);
    procedure Label205Click(Sender: TObject);
    procedure Label213Click(Sender: TObject);
    procedure Label221Click(Sender: TObject);
    procedure Label229Click(Sender: TObject);
    procedure Label237Click(Sender: TObject);
    procedure Label245Click(Sender: TObject);
    procedure Label253Click(Sender: TObject);
    procedure Label261Click(Sender: TObject);
    procedure Label269Click(Sender: TObject);
    procedure Label277Click(Sender: TObject);
    procedure Label285Click(Sender: TObject);
    procedure Label293Click(Sender: TObject);
    procedure Label301Click(Sender: TObject);
    procedure Label309Click(Sender: TObject);
    procedure Label317Click(Sender: TObject);
    procedure Label325Click(Sender: TObject);
    procedure Label333Click(Sender: TObject);
    procedure Label341Click(Sender: TObject);
    procedure Label349Click(Sender: TObject);
    procedure Label357Click(Sender: TObject);
    procedure Label365Click(Sender: TObject);
    procedure Label373Click(Sender: TObject);
    procedure Label381Click(Sender: TObject);
    procedure Label389Click(Sender: TObject);
    procedure Label397Click(Sender: TObject);
    procedure Label405Click(Sender: TObject);
    procedure Label22Click(Sender: TObject);
    procedure Label30Click(Sender: TObject);
    procedure Label38Click(Sender: TObject);
    procedure Label46Click(Sender: TObject);
    procedure Label54Click(Sender: TObject);
    procedure Label62Click(Sender: TObject);
    procedure Label70Click(Sender: TObject);
    procedure Label78Click(Sender: TObject);
    procedure Label86Click(Sender: TObject);
    procedure Label94Click(Sender: TObject);
    procedure Label102Click(Sender: TObject);
    procedure Label110Click(Sender: TObject);
    procedure Label118Click(Sender: TObject);
    procedure Label126Click(Sender: TObject);
    procedure Label134Click(Sender: TObject);
    procedure Label142Click(Sender: TObject);
    procedure Label150Click(Sender: TObject);
    procedure Label158Click(Sender: TObject);
    procedure Label166Click(Sender: TObject);
    procedure Label174Click(Sender: TObject);
    procedure Label182Click(Sender: TObject);
    procedure Label190Click(Sender: TObject);
    procedure Label198Click(Sender: TObject);
    procedure Label206Click(Sender: TObject);
    procedure Label214Click(Sender: TObject);
    procedure Label222Click(Sender: TObject);
    procedure Label230Click(Sender: TObject);
    procedure Label238Click(Sender: TObject);
    procedure Label246Click(Sender: TObject);
    procedure Label254Click(Sender: TObject);
    procedure Label262Click(Sender: TObject);
    procedure Label270Click(Sender: TObject);
    procedure Label278Click(Sender: TObject);
    procedure Label286Click(Sender: TObject);
    procedure Label294Click(Sender: TObject);
    procedure Label302Click(Sender: TObject);
    procedure Label310Click(Sender: TObject);
    procedure Label318Click(Sender: TObject);
    procedure Label326Click(Sender: TObject);
    procedure Label334Click(Sender: TObject);
    procedure Label342Click(Sender: TObject);
    procedure Label350Click(Sender: TObject);
    procedure Label358Click(Sender: TObject);
    procedure Label366Click(Sender: TObject);
    procedure Label374Click(Sender: TObject);
    procedure Label382Click(Sender: TObject);
    procedure Label390Click(Sender: TObject);
    procedure Label398Click(Sender: TObject);
    procedure Label406Click(Sender: TObject);
    procedure Label23Click(Sender: TObject);
    procedure Label31Click(Sender: TObject);
    procedure Label39Click(Sender: TObject);
    procedure Label47Click(Sender: TObject);
    procedure Label55Click(Sender: TObject);
    procedure Label63Click(Sender: TObject);
    procedure Label71Click(Sender: TObject);
    procedure Label79Click(Sender: TObject);
    procedure Label87Click(Sender: TObject);
    procedure Label95Click(Sender: TObject);
    procedure Label103Click(Sender: TObject);
    procedure Label111Click(Sender: TObject);
    procedure Label119Click(Sender: TObject);
    procedure Label127Click(Sender: TObject);
    procedure Label135Click(Sender: TObject);
    procedure Label143Click(Sender: TObject);
    procedure Label151Click(Sender: TObject);
    procedure Label159Click(Sender: TObject);
    procedure Label167Click(Sender: TObject);
    procedure Label175Click(Sender: TObject);
    procedure Label183Click(Sender: TObject);
    procedure Label191Click(Sender: TObject);
    procedure Label199Click(Sender: TObject);
    procedure Label207Click(Sender: TObject);
    procedure Label215Click(Sender: TObject);
    procedure Label223Click(Sender: TObject);
    procedure Label231Click(Sender: TObject);
    procedure Label239Click(Sender: TObject);
    procedure Label247Click(Sender: TObject);
    procedure Label255Click(Sender: TObject);
    procedure Label263Click(Sender: TObject);
    procedure Label271Click(Sender: TObject);
    procedure Label279Click(Sender: TObject);
    procedure Label287Click(Sender: TObject);
    procedure Label295Click(Sender: TObject);
    procedure Label303Click(Sender: TObject);
    procedure Label311Click(Sender: TObject);
    procedure Label319Click(Sender: TObject);
    procedure Label327Click(Sender: TObject);
    procedure Label335Click(Sender: TObject);
    procedure Label343Click(Sender: TObject);
    procedure Label351Click(Sender: TObject);
    procedure Label359Click(Sender: TObject);
    procedure Label367Click(Sender: TObject);
    procedure Label375Click(Sender: TObject);
    procedure Label383Click(Sender: TObject);
    procedure Label391Click(Sender: TObject);
    procedure Label399Click(Sender: TObject);
    procedure Label407Click(Sender: TObject);
    procedure Label24Click(Sender: TObject);
    procedure Label32Click(Sender: TObject);
    procedure Label40Click(Sender: TObject);
    procedure Label48Click(Sender: TObject);
    procedure Label56Click(Sender: TObject);
    procedure Label64Click(Sender: TObject);
    procedure Label72Click(Sender: TObject);
    procedure Label80Click(Sender: TObject);
    procedure Label88Click(Sender: TObject);
    procedure Label96Click(Sender: TObject);
    procedure Label104Click(Sender: TObject);
    procedure Label112Click(Sender: TObject);
    procedure Label120Click(Sender: TObject);
    procedure Label128Click(Sender: TObject);
    procedure Label136Click(Sender: TObject);
    procedure Label144Click(Sender: TObject);
    procedure Label152Click(Sender: TObject);
    procedure Label160Click(Sender: TObject);
    procedure Label168Click(Sender: TObject);
    procedure Label176Click(Sender: TObject);
    procedure Label184Click(Sender: TObject);
    procedure Label192Click(Sender: TObject);
    procedure Label200Click(Sender: TObject);
    procedure Label208Click(Sender: TObject);
    procedure Label216Click(Sender: TObject);
    procedure Label224Click(Sender: TObject);
    procedure Label232Click(Sender: TObject);
    procedure Label240Click(Sender: TObject);
    procedure Label248Click(Sender: TObject);
    procedure Label256Click(Sender: TObject);
    procedure Label264Click(Sender: TObject);
    procedure Label272Click(Sender: TObject);
    procedure Label280Click(Sender: TObject);
    procedure Label288Click(Sender: TObject);
    procedure Label296Click(Sender: TObject);
    procedure Label304Click(Sender: TObject);
    procedure Label312Click(Sender: TObject);
    procedure Label320Click(Sender: TObject);
    procedure Label328Click(Sender: TObject);
    procedure Label336Click(Sender: TObject);
    procedure Label344Click(Sender: TObject);
    procedure Label352Click(Sender: TObject);
    procedure Label360Click(Sender: TObject);
    procedure Label368Click(Sender: TObject);
    procedure Label376Click(Sender: TObject);
    procedure Label384Click(Sender: TObject);
    procedure Label392Click(Sender: TObject);
    procedure Label400Click(Sender: TObject);
    procedure Label408Click(Sender: TObject);
    procedure Label25Click(Sender: TObject);
    procedure Label33Click(Sender: TObject);
    procedure Label41Click(Sender: TObject);
    procedure Label49Click(Sender: TObject);
    procedure Label57Click(Sender: TObject);
    procedure Label65Click(Sender: TObject);
    procedure Label73Click(Sender: TObject);
    procedure Label81Click(Sender: TObject);
    procedure Label89Click(Sender: TObject);
    procedure Label97Click(Sender: TObject);
    procedure Label105Click(Sender: TObject);
    procedure Label113Click(Sender: TObject);
    procedure Label121Click(Sender: TObject);
    procedure Label129Click(Sender: TObject);
    procedure Label137Click(Sender: TObject);
    procedure Label145Click(Sender: TObject);
    procedure Label153Click(Sender: TObject);
    procedure Label161Click(Sender: TObject);
    procedure Label169Click(Sender: TObject);
    procedure Label177Click(Sender: TObject);
    procedure Label185Click(Sender: TObject);
    procedure Label193Click(Sender: TObject);
    procedure Label201Click(Sender: TObject);
    procedure Label209Click(Sender: TObject);
    procedure Label217Click(Sender: TObject);
    procedure Label225Click(Sender: TObject);
    procedure Label233Click(Sender: TObject);
    procedure Label241Click(Sender: TObject);
    procedure Label249Click(Sender: TObject);
    procedure Label257Click(Sender: TObject);
    procedure Label265Click(Sender: TObject);
    procedure Label273Click(Sender: TObject);
    procedure Label281Click(Sender: TObject);
    procedure Label289Click(Sender: TObject);
    procedure Label297Click(Sender: TObject);
    procedure Label305Click(Sender: TObject);
    procedure Label313Click(Sender: TObject);
    procedure Label321Click(Sender: TObject);
    procedure Label329Click(Sender: TObject);
    procedure Label337Click(Sender: TObject);
    procedure Label345Click(Sender: TObject);
    procedure Label353Click(Sender: TObject);
    procedure Label361Click(Sender: TObject);
    procedure Label369Click(Sender: TObject);
    procedure Label377Click(Sender: TObject);
    procedure Label385Click(Sender: TObject);
    procedure Label393Click(Sender: TObject);
    procedure Label401Click(Sender: TObject);
    procedure Label409Click(Sender: TObject);
    procedure Label26Click(Sender: TObject);
    procedure Label34Click(Sender: TObject);
    procedure Label42Click(Sender: TObject);
    procedure Label50Click(Sender: TObject);
    procedure Label58Click(Sender: TObject);
    procedure Label66Click(Sender: TObject);
    procedure Label74Click(Sender: TObject);
    procedure Label82Click(Sender: TObject);
    procedure Label90Click(Sender: TObject);
    procedure Label98Click(Sender: TObject);
    procedure Label106Click(Sender: TObject);
    procedure Label114Click(Sender: TObject);
    procedure Label122Click(Sender: TObject);
    procedure Label130Click(Sender: TObject);
    procedure Label138Click(Sender: TObject);
    procedure Label146Click(Sender: TObject);
    procedure Label154Click(Sender: TObject);
    procedure Label162Click(Sender: TObject);
    procedure Label170Click(Sender: TObject);
    procedure Label178Click(Sender: TObject);
    procedure Label186Click(Sender: TObject);
    procedure Label194Click(Sender: TObject);
    procedure Label202Click(Sender: TObject);
    procedure Label210Click(Sender: TObject);
    procedure Label218Click(Sender: TObject);
    procedure Label226Click(Sender: TObject);
    procedure Label234Click(Sender: TObject);
    procedure Label242Click(Sender: TObject);
    procedure Label250Click(Sender: TObject);
    procedure Label258Click(Sender: TObject);
    procedure Label266Click(Sender: TObject);
    procedure Label274Click(Sender: TObject);
    procedure Label282Click(Sender: TObject);
    procedure Label290Click(Sender: TObject);
    procedure Label298Click(Sender: TObject);
    procedure Label306Click(Sender: TObject);
    procedure Label314Click(Sender: TObject);
    procedure Label322Click(Sender: TObject);
    procedure Label330Click(Sender: TObject);
    procedure Label338Click(Sender: TObject);
    procedure Label346Click(Sender: TObject);
    procedure Label354Click(Sender: TObject);
    procedure Label362Click(Sender: TObject);
    procedure Label370Click(Sender: TObject);
    procedure Label378Click(Sender: TObject);
    procedure Label386Click(Sender: TObject);
    procedure Label394Click(Sender: TObject);
    procedure Label402Click(Sender: TObject);
    procedure Label410Click(Sender: TObject);
    procedure Label27Click(Sender: TObject);
    procedure Label35Click(Sender: TObject);
    procedure Label43Click(Sender: TObject);
    procedure Label51Click(Sender: TObject);
    procedure Label59Click(Sender: TObject);
    procedure Label67Click(Sender: TObject);
    procedure Label75Click(Sender: TObject);
    procedure Label83Click(Sender: TObject);
    procedure Label91Click(Sender: TObject);
    procedure Label99Click(Sender: TObject);
    procedure Label107Click(Sender: TObject);
    procedure Label115Click(Sender: TObject);
    procedure Label123Click(Sender: TObject);
    procedure Label131Click(Sender: TObject);
    procedure Label139Click(Sender: TObject);
    procedure Label147Click(Sender: TObject);
    procedure Label155Click(Sender: TObject);
    procedure Label163Click(Sender: TObject);
    procedure Label171Click(Sender: TObject);
    procedure Label179Click(Sender: TObject);
    procedure Label187Click(Sender: TObject);
    procedure Label195Click(Sender: TObject);
    procedure Label203Click(Sender: TObject);
    procedure Label211Click(Sender: TObject);
    procedure Label219Click(Sender: TObject);
    procedure Label227Click(Sender: TObject);
    procedure Label235Click(Sender: TObject);
    procedure Label243Click(Sender: TObject);
    procedure Label251Click(Sender: TObject);
    procedure Label259Click(Sender: TObject);
    procedure Label267Click(Sender: TObject);
    procedure Label275Click(Sender: TObject);
    procedure Label283Click(Sender: TObject);
    procedure Label291Click(Sender: TObject);
    procedure Label307Click(Sender: TObject);
    procedure Label323Click(Sender: TObject);
    procedure Label339Click(Sender: TObject);
    procedure Label355Click(Sender: TObject);
    procedure Label371Click(Sender: TObject);
    procedure Label387Click(Sender: TObject);
    procedure Label403Click(Sender: TObject);
    procedure Label299Click(Sender: TObject);
    procedure Label315Click(Sender: TObject);
    procedure Label331Click(Sender: TObject);
    procedure Label347Click(Sender: TObject);
    procedure Label363Click(Sender: TObject);
    procedure Label379Click(Sender: TObject);
    procedure Label395Click(Sender: TObject);
    procedure Label411Click(Sender: TObject);
    procedure ScrollBox1Click(Sender: TObject);
    procedure Label6Click(Sender: TObject);
    procedure izmcvgip(Sender: TObject);
    procedure Ydalvselampochki(Sender: TObject);
    procedure gsfgbhfghb(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure N16Click(Sender: TObject);
    procedure N17Click(Sender: TObject);
    procedure Closehg(Sender: TObject);
    procedure Label132MouseEnter(Sender: TObject);
    procedure GroupBox4MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Label133MouseEnter(Sender: TObject);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Timer3Timer(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Labe68licger(Sender: TObject);
    procedure Shape57ContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure Button4Click(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure N20Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure N22Click(Sender: TObject);
    procedure N23Click(Sender: TObject);
    procedure Cjplfnm1Click(Sender: TObject);
    procedure N25Click(Sender: TObject);
    procedure N26Click(Sender: TObject);
    procedure N28Click(Sender: TObject);
    procedure N29Click(Sender: TObject);
    procedure N30Click(Sender: TObject);
    procedure N31Click(Sender: TObject);
    procedure N33Click(Sender: TObject);
    procedure N35Click(Sender: TObject);
    procedure N36Click(Sender: TObject);
    procedure N37Click(Sender: TObject);
    procedure N39Click(Sender: TObject);
  private
    { Private declarations }
    procedure izmcvClick(Sender: TObject);
    procedure ShapeNevud(Sender: TObject);
    procedure Ydalenie(Sender: TObject);
    procedure WMSysCommand(var Msg: TWMSysCommand); message WM_SYSCOMMAND;
    procedure effectu(Sender: TObject);
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  Hit: boolean;
  _x,_y: integer;
implementation

uses Unit2, Unit5, Unit3, Unit6, Unit7, Unit8, Unit12, Unit13, Unit14,
  Unit10, Unit4, Unit9, Unit11, Unit15, Unit16, ShellApi;

{$R *.dfm}

procedure TForm1.N3Click(Sender: TObject);
begin
Form1.Close;
end;

procedure TForm1.N2Click(Sender: TObject);
Var i,n:integer;
Begin
Form13.Visible:=true;
n:=-14;
For i:=1 to 15 do
  Begin
  n:=n+24;
  case i of
  1 : Form13.Edit1.Top:=n;
  2 : Form13.Edit2.Top:=n;
  3 : Form13.Edit3.Top:=n;
  4 : Form13.Edit4.Top:=n;
  5 : Form13.Edit5.Top:=n;
  6 : Form13.Edit6.Top:=n;
  7 : Form13.Edit7.Top:=n;
  8 : Form13.Edit8.Top:=n;
  9 : Form13.Edit9.Top:=n;
  10 : Form13.Edit10.Top:=n;
  11 : Form13.Edit11.Top:=n;
  12 : Form13.Edit12.Top:=n;
  13 : Form13.Edit13.Top:=n;
  14 : Form13.Edit14.Top:=n;
  15 : Form13.Edit15.Top:=n;
  end;
  end;
n:=-16;
For i:=1 to 15 do
  Begin
  n:=n+24;
  case i of
  1 : Form13.Button1.Top:=n;
  2 : Form13.Button2.Top:=n;
  3 : Form13.Button3.Top:=n;
  4 : Form13.Button4.Top:=n;
  5 : Form13.Button5.Top:=n;
  6 : Form13.Button6.Top:=n;
  7 : Form13.Button7.Top:=n;
  8 : Form13.Button8.Top:=n;
  9 : Form13.Button9.Top:=n;
  10 : Form13.Button10.Top:=n;
  11 : Form13.Button11.Top:=n;
  12 : Form13.Button12.Top:=n;
  13 : Form13.Button13.Top:=n;
  14 : Form13.Button14.Top:=n;
  15 : Form13.Button15.Top:=n;
  end;
  end;
Form13.ScrollBar1.Position:=1;
end;

procedure TForm1.RadioButton3Click(Sender: TObject);
begin
Panel2.Visible:=true;
GroupBox1.Width:=209;
GroupBox1.Height:=153;
label3.Caption:='label3';
GroupBox4.Left:=520;
Form1.Width:=743;
end;

procedure TForm1.RadioButton2Click(Sender: TObject);
begin
Panel2.Visible:=false;
GroupBox1.Width:=105;
GroupBox1.Height:=81;
Edit1.Text:='0';
Edit2.Text:='1999';
GroupBox4.Left:=416;
Width:=639;
end;

procedure TForm1.RadioButton1Click(Sender: TObject);
begin
Panel2.Visible:=false;
GroupBox1.Width:=105;
GroupBox1.Height:=81;
Edit1.Text:='0';
Edit2.Text:='1999';
GroupBox4.Left:=416;
Width:=639;
end;

procedure TForm1.Button1Click(Sender: TObject);
Var i,l,c,a,m:integer;
p:string;
d:array[1..2000] of string;
begin
if Label4.Caption='Label4' then
Begin
Memo1.Lines.Delete(0);
Memo1.Lines.Delete(0);
For i:=0 to Memo1.Lines.Count do
Begin
l:=Length(Memo1.Lines[i]);
p:=copy(Memo1.Lines[i], 10, l) ;
Memo1.Lines[i]:=p;
end;
a:=9;
Memo1.Lines.Delete(Memo1.Lines.Count-1);
For i:=0 to Memo1.Lines.Count do
Begin
Form5.Memo1.Lines.Add(copy(Memo1.Lines[i], 1, 2));

Form5.Memo2.Lines.Add(copy(Memo1.Lines[i], 3, 2));

Form5.Memo3.Lines.Add(copy(Memo1.Lines[i], 5, 2));


Form5.Memo4.Lines.Add(copy(Memo1.Lines[i], 7, 2));

end;
Timer1.Enabled:=true;
end;
if Label4.Caption='����' then
Begin
timer1.Enabled:=true;
Label4.Caption:='Label4';

end;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var m,i:integer;
p1,p2,p3,p4:array[1..32] of string;
begin
If Form8.CurrencyEdit1.Value=4 then
  Begin
  Form1.d.Caption:=IntToStr(StrToInt(Form1.d.Caption)+1);
  i:=StrToInt(Form1.d.Caption);
  Edit5.Text:=IntToStr(i);
  if RadioButton2.Checked=true then
    Begin
    if form1.d.Caption='2000' then
    form1.d.Caption:='-1';
    end;
  if RadioButton1.Checked=true then
    Begin
    if form1.d.Caption='2000' then
    Form1.Label7.OnClick(Label7);
    end;
  if RadioButton3.Checked=true then
    Begin
    if label3.Caption='label3' then
    Begin
    form1.d.Caption:=IntToStr(StrToInt(Edit1.Text)-1);
    Label3.Caption:='��label3';
    end;
    if form1.d.Caption=Edit2.Text then
      Begin
      if CheckBox1.Checked=true then
      form1.d.Caption:=IntToStr(StrToInt(Edit1.Text)-1);
      if CheckBox1.Checked=false then
      Form1.Label7.OnClick(Label7);
      end;
    end;
For m:=1 to 8 do
Begin
p1[m]:=copy(Form5.Memo1.Lines[i],m,1);
if p1[m]='' then
p1[m]:='1';
end;
For m:=1 to 8 do
Begin
p2[m]:=copy(Form5.Memo2.Lines[i],m,1);
if p2[m]='' then
p2[m]:='1';
end;
For m:=1 to 8 do
Begin
p3[m]:=copy(Form5.Memo3.Lines[i],m,1);
if p3[m]='' then
p3[m]:='1';
end;

For m:=1 to 8 do
Begin
p4[m]:=copy(Form5.Memo4.Lines[i],m,1);
if p4[m]='' then
p4[m]:='1';
end;
if StrToInt(p1[1])=0 then
Form6.Shape1.Brush.Color:=clRed;
if StrToInt(p1[1])=1 then
Form6.Shape1.Brush.Color:=clWhite;
if StrToInt(p1[2])=0 then
Form6.Shape2.Brush.Color:=clRed;
if StrToInt(p1[2])=1 then
Form6.Shape2.Brush.Color:=clWhite;
if StrToInt(p1[3])=0 then
Form6.Shape3.Brush.Color:=clRed;
if StrToInt(p1[3])=1 then
Form6.Shape3.Brush.Color:=clWhite;
if StrToInt(p1[4])=0 then
Form6.Shape4.Brush.Color:=clRed;
if StrToInt(p1[4])=1 then
Form6.Shape4.Brush.Color:=clWhite;
if StrToInt(p1[5])=0 then
Form6.Shape5.Brush.Color:=clRed;
if StrToInt(p1[5])=1 then
Form6.Shape5.Brush.Color:=clWhite;
if StrToInt(p1[6])=0 then
Form6.Shape6.Brush.Color:=clRed;
if StrToInt(p1[6])=1 then
Form6.Shape6.Brush.Color:=clWhite;
if StrToInt(p1[7])=0 then
Form6.Shape7.Brush.Color:=clRed;
if StrToInt(p1[7])=1 then
Form6.Shape7.Brush.Color:=clWhite;
if StrToInt(p1[8])=0 then
Form6.Shape8.Brush.Color:=clRed;
if StrToInt(p1[8])=1 then
Form6.Shape8.Brush.Color:=clWhite;
if StrToInt(p2[1])=0 then
Form6.Shape9.Brush.Color:=clRed;
if StrToInt(p2[1])=1 then
Form6.Shape9.Brush.Color:=clWhite;
if StrToInt(p2[2])=0 then
Form6.Shape10.Brush.Color:=clRed;
if StrToInt(p2[2])=1 then
Form6.Shape10.Brush.Color:=clWhite;
if StrToInt(p2[3])=0 then
Form6.Shape11.Brush.Color:=clRed;
if StrToInt(p2[3])=1 then
Form6.Shape11.Brush.Color:=clWhite;
if StrToInt(p2[4])=0 then
Form6.Shape12.Brush.Color:=clRed;
if StrToInt(p2[4])=1 then
Form6.Shape12.Brush.Color:=clWhite;
if StrToInt(p2[5])=0 then
Form6.Shape13.Brush.Color:=clRed;
if StrToInt(p2[5])=1 then
Form6.Shape13.Brush.Color:=clWhite;
if StrToInt(p2[6])=0 then
Form6.Shape14.Brush.Color:=clRed;
if StrToInt(p2[6])=1 then
Form6.Shape14.Brush.Color:=clWhite;
if StrToInt(p2[7])=0 then
Form6.Shape15.Brush.Color:=clRed;
if StrToInt(p2[7])=1 then
Form6.Shape15.Brush.Color:=clWhite;
if StrToInt(p2[8])=0 then
Form6.Shape16.Brush.Color:=clRed;
if StrToInt(p2[8])=1 then
Form6.Shape16.Brush.Color:=clWhite;
if StrToInt(p3[1])=0 then
Form6.Shape17.Brush.Color:=clRed;
if StrToInt(p3[1])=1 then
Form6.Shape17.Brush.Color:=clWhite;
if StrToInt(p3[2])=0 then
Form6.Shape18.Brush.Color:=clRed;
if StrToInt(p3[2])=1 then
Form6.Shape18.Brush.Color:=clWhite;
if StrToInt(p3[3])=0 then
Form6.Shape19.Brush.Color:=clRed;
if StrToInt(p3[3])=1 then
Form6.Shape19.Brush.Color:=clWhite;
if StrToInt(p3[4])=0 then
Form6.Shape20.Brush.Color:=clRed;
if StrToInt(p3[4])=1 then
Form6.Shape20.Brush.Color:=clWhite;
if StrToInt(p3[5])=0 then
Form6.Shape21.Brush.Color:=clRed;
if StrToInt(p3[5])=1 then
Form6.Shape21.Brush.Color:=clWhite;
if StrToInt(p3[6])=0 then
Form6.Shape22.Brush.Color:=clRed;
if StrToInt(p3[6])=1 then
Form6.Shape22.Brush.Color:=clWhite;
if StrToInt(p3[7])=0 then
Form6.Shape23.Brush.Color:=clRed;
if StrToInt(p3[7])=1 then
Form6.Shape23.Brush.Color:=clWhite;
if StrToInt(p3[8])=0 then
Form6.Shape24.Brush.Color:=clRed;
if StrToInt(p3[8])=1 then
Form6.Shape24.Brush.Color:=clWhite;
if StrToInt(p4[1])=0 then
Form6.Shape25.Brush.Color:=clRed;
if StrToInt(p4[1])=1 then
Form6.Shape25.Brush.Color:=clWhite;
if StrToInt(p4[2])=0 then
Form6.Shape26.Brush.Color:=clRed;
if StrToInt(p4[2])=1 then
Form6.Shape26.Brush.Color:=clWhite;
if StrToInt(p4[3])=0 then
Form6.Shape27.Brush.Color:=clRed;
if StrToInt(p4[3])=1 then
Form6.Shape27.Brush.Color:=clWhite;
if StrToInt(p4[4])=0 then
Form6.Shape28.Brush.Color:=clRed;
if StrToInt(p4[4])=1 then
Form6.Shape28.Brush.Color:=clWhite;
if StrToInt(p4[5])=0 then
Form6.Shape29.Brush.Color:=clRed;
if StrToInt(p4[5])=1 then
Form6.Shape29.Brush.Color:=clWhite;
if StrToInt(p4[6])=0 then
Form6.Shape30.Brush.Color:=clRed;
if StrToInt(p4[6])=1 then
Form6.Shape30.Brush.Color:=clWhite;
if StrToInt(p4[7])=0 then
Form6.Shape31.Brush.Color:=clRed;
if StrToInt(p4[7])=1 then
Form6.Shape31.Brush.Color:=clWhite;
if StrToInt(p4[8])=0 then
Form6.Shape32.Brush.Color:=clRed;
if StrToInt(p4[8])=1 then
Form6.Shape32.Brush.Color:=clWhite;
end;
If Form8.CurrencyEdit1.Value=3 then
  Begin
  Form1.d.Caption:=IntToStr(StrToInt(Form1.d.Caption)+1);
  i:=StrToInt(Form1.d.Caption);
  Edit5.Text:=IntToStr(i);
  if RadioButton2.Checked=true then
    Begin
    if form1.d.Caption='2000' then
    form1.d.Caption:='-1';
    end;
  if RadioButton1.Checked=true then
    Begin
    if form1.d.Caption='2000' then
    Form1.Label7.OnClick(Label7);
    end;
  if RadioButton3.Checked=true then
    Begin
    if label3.Caption='label3' then
    Begin
    form1.d.Caption:=IntToStr(StrToInt(Edit1.Text)-1);
    Label3.Caption:='��label3';
    end;
    if form1.d.Caption=Edit2.Text then
      Begin
      if CheckBox1.Checked=true then
      form1.d.Caption:=IntToStr(StrToInt(Edit1.Text)-1);
      if CheckBox1.Checked=false then
      Form1.Label7.OnClick(Label7);
      end;
    end;
For m:=1 to 8 do
Begin
p1[m]:=copy(Form5.Memo1.Lines[i],m,1);
if p1[m]='' then
p1[m]:='1';
end;
For m:=1 to 8 do
Begin
p2[m]:=copy(Form5.Memo2.Lines[i],m,1);
if p2[m]='' then
p2[m]:='1';
end;
For m:=1 to 8 do
Begin
p3[m]:=copy(Form5.Memo3.Lines[i],m,1);
if p3[m]='' then
p3[m]:='1';
end;

For m:=1 to 8 do
Begin
p4[m]:=copy(Form5.Memo4.Lines[i],m,1);
if p4[m]='' then
p4[m]:='1';
end;
if StrToInt(p1[1])=0 then
Form6.Shape1.Brush.Color:=clRed;
if StrToInt(p1[1])=1 then
Form6.Shape1.Brush.Color:=clWhite;
if StrToInt(p1[2])=0 then
Form6.Shape2.Brush.Color:=clRed;
if StrToInt(p1[2])=1 then
Form6.Shape2.Brush.Color:=clWhite;
if StrToInt(p1[3])=0 then
Form6.Shape3.Brush.Color:=clRed;
if StrToInt(p1[3])=1 then
Form6.Shape3.Brush.Color:=clWhite;
if StrToInt(p1[4])=0 then
Form6.Shape4.Brush.Color:=clRed;
if StrToInt(p1[4])=1 then
Form6.Shape4.Brush.Color:=clWhite;
if StrToInt(p1[5])=0 then
Form6.Shape5.Brush.Color:=clRed;
if StrToInt(p1[5])=1 then
Form6.Shape5.Brush.Color:=clWhite;
if StrToInt(p1[6])=0 then
Form6.Shape6.Brush.Color:=clRed;
if StrToInt(p1[6])=1 then
Form6.Shape6.Brush.Color:=clWhite;
if StrToInt(p1[7])=0 then
Form6.Shape7.Brush.Color:=clRed;
if StrToInt(p1[7])=1 then
Form6.Shape7.Brush.Color:=clWhite;
if StrToInt(p1[8])=0 then
Form6.Shape8.Brush.Color:=clRed;
if StrToInt(p1[8])=1 then
Form6.Shape8.Brush.Color:=clWhite;
if StrToInt(p2[1])=0 then
Form6.Shape9.Brush.Color:=clRed;
if StrToInt(p2[1])=1 then
Form6.Shape9.Brush.Color:=clWhite;
if StrToInt(p2[2])=0 then
Form6.Shape10.Brush.Color:=clRed;
if StrToInt(p2[2])=1 then
Form6.Shape10.Brush.Color:=clWhite;
if StrToInt(p2[3])=0 then
Form6.Shape11.Brush.Color:=clRed;
if StrToInt(p2[3])=1 then
Form6.Shape11.Brush.Color:=clWhite;
if StrToInt(p2[4])=0 then
Form6.Shape12.Brush.Color:=clRed;
if StrToInt(p2[4])=1 then
Form6.Shape12.Brush.Color:=clWhite;
if StrToInt(p2[5])=0 then
Form6.Shape13.Brush.Color:=clRed;
if StrToInt(p2[5])=1 then
Form6.Shape13.Brush.Color:=clWhite;
if StrToInt(p2[6])=0 then
Form6.Shape14.Brush.Color:=clRed;
if StrToInt(p2[6])=1 then
Form6.Shape14.Brush.Color:=clWhite;
if StrToInt(p2[7])=0 then
Form6.Shape15.Brush.Color:=clRed;
if StrToInt(p2[7])=1 then
Form6.Shape15.Brush.Color:=clWhite;
if StrToInt(p2[8])=0 then
Form6.Shape16.Brush.Color:=clRed;
if StrToInt(p2[8])=1 then
Form6.Shape16.Brush.Color:=clWhite;
if StrToInt(p3[1])=0 then
Form6.Shape17.Brush.Color:=clRed;
if StrToInt(p3[1])=1 then
Form6.Shape17.Brush.Color:=clWhite;
if StrToInt(p3[2])=0 then
Form6.Shape18.Brush.Color:=clRed;
if StrToInt(p3[2])=1 then
Form6.Shape18.Brush.Color:=clWhite;
if StrToInt(p3[3])=0 then
Form6.Shape19.Brush.Color:=clRed;
if StrToInt(p3[3])=1 then
Form6.Shape19.Brush.Color:=clWhite;
if StrToInt(p3[4])=0 then
Form6.Shape20.Brush.Color:=clRed;
if StrToInt(p3[4])=1 then
Form6.Shape20.Brush.Color:=clWhite;
if StrToInt(p3[5])=0 then
Form6.Shape21.Brush.Color:=clRed;
if StrToInt(p3[5])=1 then
Form6.Shape21.Brush.Color:=clWhite;
if StrToInt(p3[6])=0 then
Form6.Shape22.Brush.Color:=clRed;
if StrToInt(p3[6])=1 then
Form6.Shape22.Brush.Color:=clWhite;
if StrToInt(p3[7])=0 then
Form6.Shape23.Brush.Color:=clRed;
if StrToInt(p3[7])=1 then
Form6.Shape23.Brush.Color:=clWhite;
if StrToInt(p3[8])=0 then
Form6.Shape24.Brush.Color:=clRed;
if StrToInt(p3[8])=1 then
Form6.Shape24.Brush.Color:=clWhite;
if StrToInt(p4[1])=0 then
Form6.Shape25.Brush.Color:=clRed;
if StrToInt(p4[1])=1 then
Form6.Shape25.Brush.Color:=clWhite;
if StrToInt(p4[2])=0 then
Form6.Shape26.Brush.Color:=clRed;
if StrToInt(p4[2])=1 then
Form6.Shape26.Brush.Color:=clWhite;
if StrToInt(p4[3])=0 then
Form6.Shape27.Brush.Color:=clRed;
if StrToInt(p4[3])=1 then
Form6.Shape27.Brush.Color:=clWhite;
if StrToInt(p4[4])=0 then
Form6.Shape28.Brush.Color:=clRed;
if StrToInt(p4[4])=1 then
Form6.Shape28.Brush.Color:=clWhite;
if StrToInt(p4[5])=0 then
Form6.Shape29.Brush.Color:=clRed;
if StrToInt(p4[5])=1 then
Form6.Shape29.Brush.Color:=clWhite;
if StrToInt(p4[6])=0 then
Form6.Shape30.Brush.Color:=clRed;
if StrToInt(p4[6])=1 then
Form6.Shape30.Brush.Color:=clWhite;
if StrToInt(p4[7])=0 then
Form6.Shape31.Brush.Color:=clRed;
if StrToInt(p4[7])=1 then
Form6.Shape31.Brush.Color:=clWhite;
if StrToInt(p4[8])=0 then
Form6.Shape32.Brush.Color:=clRed;
if StrToInt(p4[8])=1 then
Form6.Shape32.Brush.Color:=clWhite;
end;
If Form8.CurrencyEdit1.Value=2 then
  Begin
  Form1.d.Caption:=IntToStr(StrToInt(Form1.d.Caption)+1);
  i:=StrToInt(Form1.d.Caption);
  Edit5.Text:=IntToStr(i);
  if RadioButton2.Checked=true then
    Begin
    if form1.d.Caption='2000' then
    form1.d.Caption:='-1';
    end;
  if RadioButton1.Checked=true then
    Begin
    if form1.d.Caption='2000' then
    Form1.Label7.OnClick(Label7);
    end;
  if RadioButton3.Checked=true then
    Begin
    if label3.Caption='label3' then
    Begin
    form1.d.Caption:=IntToStr(StrToInt(Edit1.Text)-1);
    Label3.Caption:='��label3';
    end;
    if form1.d.Caption=Edit2.Text then
      Begin
      if CheckBox1.Checked=true then
      form1.d.Caption:=IntToStr(StrToInt(Edit1.Text)-1);
      if CheckBox1.Checked=false then
      Form1.Label7.OnClick(Label7);
      end;
    end;
For m:=1 to 8 do
Begin
p1[m]:=copy(Form5.Memo1.Lines[i],m,1);
if p1[m]='' then
p1[m]:='1';
end;
For m:=1 to 8 do
Begin
p2[m]:=copy(Form5.Memo2.Lines[i],m,1);
if p2[m]='' then
p2[m]:='1';
end;
For m:=1 to 8 do
Begin
p3[m]:=copy(Form5.Memo3.Lines[i],m,1);
if p3[m]='' then
p3[m]:='1';
end;

For m:=1 to 8 do
Begin
p4[m]:=copy(Form5.Memo4.Lines[i],m,1);
if p4[m]='' then
p4[m]:='1';
end;
if StrToInt(p1[1])=0 then
Form6.Shape1.Brush.Color:=clRed;
if StrToInt(p1[1])=1 then
Form6.Shape1.Brush.Color:=clWhite;
if StrToInt(p1[2])=0 then
Form6.Shape2.Brush.Color:=clRed;
if StrToInt(p1[2])=1 then
Form6.Shape2.Brush.Color:=clWhite;
if StrToInt(p1[3])=0 then
Form6.Shape3.Brush.Color:=clRed;
if StrToInt(p1[3])=1 then
Form6.Shape3.Brush.Color:=clWhite;
if StrToInt(p1[4])=0 then
Form6.Shape4.Brush.Color:=clRed;
if StrToInt(p1[4])=1 then
Form6.Shape4.Brush.Color:=clWhite;
if StrToInt(p1[5])=0 then
Form6.Shape5.Brush.Color:=clRed;
if StrToInt(p1[5])=1 then
Form6.Shape5.Brush.Color:=clWhite;
if StrToInt(p1[6])=0 then
Form6.Shape6.Brush.Color:=clRed;
if StrToInt(p1[6])=1 then
Form6.Shape6.Brush.Color:=clWhite;
if StrToInt(p1[7])=0 then
Form6.Shape7.Brush.Color:=clRed;
if StrToInt(p1[7])=1 then
Form6.Shape7.Brush.Color:=clWhite;
if StrToInt(p1[8])=0 then
Form6.Shape8.Brush.Color:=clRed;
if StrToInt(p1[8])=1 then
Form6.Shape8.Brush.Color:=clWhite;
if StrToInt(p2[1])=0 then
Form6.Shape9.Brush.Color:=clRed;
if StrToInt(p2[1])=1 then
Form6.Shape9.Brush.Color:=clWhite;
if StrToInt(p2[2])=0 then
Form6.Shape10.Brush.Color:=clRed;
if StrToInt(p2[2])=1 then
Form6.Shape10.Brush.Color:=clWhite;
if StrToInt(p2[3])=0 then
Form6.Shape11.Brush.Color:=clRed;
if StrToInt(p2[3])=1 then
Form6.Shape11.Brush.Color:=clWhite;
if StrToInt(p2[4])=0 then
Form6.Shape12.Brush.Color:=clRed;
if StrToInt(p2[4])=1 then
Form6.Shape12.Brush.Color:=clWhite;
if StrToInt(p2[5])=0 then
Form6.Shape13.Brush.Color:=clRed;
if StrToInt(p2[5])=1 then
Form6.Shape13.Brush.Color:=clWhite;
if StrToInt(p2[6])=0 then
Form6.Shape14.Brush.Color:=clRed;
if StrToInt(p2[6])=1 then
Form6.Shape14.Brush.Color:=clWhite;
if StrToInt(p2[7])=0 then
Form6.Shape15.Brush.Color:=clRed;
if StrToInt(p2[7])=1 then
Form6.Shape15.Brush.Color:=clWhite;
if StrToInt(p2[8])=0 then
Form6.Shape16.Brush.Color:=clRed;
if StrToInt(p2[8])=1 then
Form6.Shape16.Brush.Color:=clWhite;
if StrToInt(p3[1])=0 then
Form6.Shape17.Brush.Color:=clRed;
if StrToInt(p3[1])=1 then
Form6.Shape17.Brush.Color:=clWhite;
if StrToInt(p3[2])=0 then
Form6.Shape18.Brush.Color:=clRed;
if StrToInt(p3[2])=1 then
Form6.Shape18.Brush.Color:=clWhite;
if StrToInt(p3[3])=0 then
Form6.Shape19.Brush.Color:=clRed;
if StrToInt(p3[3])=1 then
Form6.Shape19.Brush.Color:=clWhite;
if StrToInt(p3[4])=0 then
Form6.Shape20.Brush.Color:=clRed;
if StrToInt(p3[4])=1 then
Form6.Shape20.Brush.Color:=clWhite;
if StrToInt(p3[5])=0 then
Form6.Shape21.Brush.Color:=clRed;
if StrToInt(p3[5])=1 then
Form6.Shape21.Brush.Color:=clWhite;
if StrToInt(p3[6])=0 then
Form6.Shape22.Brush.Color:=clRed;
if StrToInt(p3[6])=1 then
Form6.Shape22.Brush.Color:=clWhite;
if StrToInt(p3[7])=0 then
Form6.Shape23.Brush.Color:=clRed;
if StrToInt(p3[7])=1 then
Form6.Shape23.Brush.Color:=clWhite;
if StrToInt(p3[8])=0 then
Form6.Shape24.Brush.Color:=clRed;
if StrToInt(p3[8])=1 then
Form6.Shape24.Brush.Color:=clWhite;
if StrToInt(p4[1])=0 then
Form6.Shape25.Brush.Color:=clRed;
if StrToInt(p4[1])=1 then
Form6.Shape25.Brush.Color:=clWhite;
if StrToInt(p4[2])=0 then
Form6.Shape26.Brush.Color:=clRed;
if StrToInt(p4[2])=1 then
Form6.Shape26.Brush.Color:=clWhite;
if StrToInt(p4[3])=0 then
Form6.Shape27.Brush.Color:=clRed;
if StrToInt(p4[3])=1 then
Form6.Shape27.Brush.Color:=clWhite;
if StrToInt(p4[4])=0 then
Form6.Shape28.Brush.Color:=clRed;
if StrToInt(p4[4])=1 then
Form6.Shape28.Brush.Color:=clWhite;
if StrToInt(p4[5])=0 then
Form6.Shape29.Brush.Color:=clRed;
if StrToInt(p4[5])=1 then
Form6.Shape29.Brush.Color:=clWhite;
if StrToInt(p4[6])=0 then
Form6.Shape30.Brush.Color:=clRed;
if StrToInt(p4[6])=1 then
Form6.Shape30.Brush.Color:=clWhite;
if StrToInt(p4[7])=0 then
Form6.Shape31.Brush.Color:=clRed;
if StrToInt(p4[7])=1 then
Form6.Shape31.Brush.Color:=clWhite;
if StrToInt(p4[8])=0 then
Form6.Shape32.Brush.Color:=clRed;
if StrToInt(p4[8])=1 then
Form6.Shape32.Brush.Color:=clWhite;
end;
If Form8.CurrencyEdit1.Value=1 then
  Begin
  Form1.d.Caption:=IntToStr(StrToInt(Form1.d.Caption)+1);
  i:=StrToInt(Form1.d.Caption);
  Edit5.Text:=IntToStr(i);
  if RadioButton2.Checked=true then
    Begin
    if form1.d.Caption='2000' then
    form1.d.Caption:='-1';
    end;
  if RadioButton1.Checked=true then
    Begin
    if form1.d.Caption='2000' then
    Form1.Label7.OnClick(Label7);
    end;
  if RadioButton3.Checked=true then
    Begin
    if label3.Caption='label3' then
    Begin
    form1.d.Caption:=IntToStr(StrToInt(Edit1.Text)-1);
    Label3.Caption:='��label3';
    end;
    if form1.d.Caption=Edit2.Text then
      Begin
      if CheckBox1.Checked=true then
      form1.d.Caption:=IntToStr(StrToInt(Edit1.Text)-1);
      if CheckBox1.Checked=false then
      Form1.Label7.OnClick(Label7);
      end;
    end;
For m:=1 to 8 do
Begin
p1[m]:=copy(Form5.Memo1.Lines[i],m,1);
if p1[m]='' then
p1[m]:='1';
end;
For m:=1 to 8 do
Begin
p2[m]:=copy(Form5.Memo2.Lines[i],m,1);
if p2[m]='' then
p2[m]:='1';
end;
For m:=1 to 8 do
Begin
p3[m]:=copy(Form5.Memo3.Lines[i],m,1);
if p3[m]='' then
p3[m]:='1';
end;

For m:=1 to 8 do
Begin
p4[m]:=copy(Form5.Memo4.Lines[i],m,1);
if p4[m]='' then
p4[m]:='1';
end;
if StrToInt(p1[1])=0 then
Form6.Shape1.Brush.Color:=clRed;
if StrToInt(p1[1])=1 then
Form6.Shape1.Brush.Color:=clWhite;
if StrToInt(p1[2])=0 then
Form6.Shape2.Brush.Color:=clRed;
if StrToInt(p1[2])=1 then
Form6.Shape2.Brush.Color:=clWhite;
if StrToInt(p1[3])=0 then
Form6.Shape3.Brush.Color:=clRed;
if StrToInt(p1[3])=1 then
Form6.Shape3.Brush.Color:=clWhite;
if StrToInt(p1[4])=0 then
Form6.Shape4.Brush.Color:=clRed;
if StrToInt(p1[4])=1 then
Form6.Shape4.Brush.Color:=clWhite;
if StrToInt(p1[5])=0 then
Form6.Shape5.Brush.Color:=clRed;
if StrToInt(p1[5])=1 then
Form6.Shape5.Brush.Color:=clWhite;
if StrToInt(p1[6])=0 then
Form6.Shape6.Brush.Color:=clRed;
if StrToInt(p1[6])=1 then
Form6.Shape6.Brush.Color:=clWhite;
if StrToInt(p1[7])=0 then
Form6.Shape7.Brush.Color:=clRed;
if StrToInt(p1[7])=1 then
Form6.Shape7.Brush.Color:=clWhite;
if StrToInt(p1[8])=0 then
Form6.Shape8.Brush.Color:=clRed;
if StrToInt(p1[8])=1 then
Form6.Shape8.Brush.Color:=clWhite;
if StrToInt(p2[1])=0 then
Form6.Shape9.Brush.Color:=clRed;
if StrToInt(p2[1])=1 then
Form6.Shape9.Brush.Color:=clWhite;
if StrToInt(p2[2])=0 then
Form6.Shape10.Brush.Color:=clRed;
if StrToInt(p2[2])=1 then
Form6.Shape10.Brush.Color:=clWhite;
if StrToInt(p2[3])=0 then
Form6.Shape11.Brush.Color:=clRed;
if StrToInt(p2[3])=1 then
Form6.Shape11.Brush.Color:=clWhite;
if StrToInt(p2[4])=0 then
Form6.Shape12.Brush.Color:=clRed;
if StrToInt(p2[4])=1 then
Form6.Shape12.Brush.Color:=clWhite;
if StrToInt(p2[5])=0 then
Form6.Shape13.Brush.Color:=clRed;
if StrToInt(p2[5])=1 then
Form6.Shape13.Brush.Color:=clWhite;
if StrToInt(p2[6])=0 then
Form6.Shape14.Brush.Color:=clRed;
if StrToInt(p2[6])=1 then
Form6.Shape14.Brush.Color:=clWhite;
if StrToInt(p2[7])=0 then
Form6.Shape15.Brush.Color:=clRed;
if StrToInt(p2[7])=1 then
Form6.Shape15.Brush.Color:=clWhite;
if StrToInt(p2[8])=0 then
Form6.Shape16.Brush.Color:=clRed;
if StrToInt(p2[8])=1 then
Form6.Shape16.Brush.Color:=clWhite;
if StrToInt(p3[1])=0 then
Form6.Shape17.Brush.Color:=clRed;
if StrToInt(p3[1])=1 then
Form6.Shape17.Brush.Color:=clWhite;
if StrToInt(p3[2])=0 then
Form6.Shape18.Brush.Color:=clRed;
if StrToInt(p3[2])=1 then
Form6.Shape18.Brush.Color:=clWhite;
if StrToInt(p3[3])=0 then
Form6.Shape19.Brush.Color:=clRed;
if StrToInt(p3[3])=1 then
Form6.Shape19.Brush.Color:=clWhite;
if StrToInt(p3[4])=0 then
Form6.Shape20.Brush.Color:=clRed;
if StrToInt(p3[4])=1 then
Form6.Shape20.Brush.Color:=clWhite;
if StrToInt(p3[5])=0 then
Form6.Shape21.Brush.Color:=clRed;
if StrToInt(p3[5])=1 then
Form6.Shape21.Brush.Color:=clWhite;
if StrToInt(p3[6])=0 then
Form6.Shape22.Brush.Color:=clRed;
if StrToInt(p3[6])=1 then
Form6.Shape22.Brush.Color:=clWhite;
if StrToInt(p3[7])=0 then
Form6.Shape23.Brush.Color:=clRed;
if StrToInt(p3[7])=1 then
Form6.Shape23.Brush.Color:=clWhite;
if StrToInt(p3[8])=0 then
Form6.Shape24.Brush.Color:=clRed;
if StrToInt(p3[8])=1 then
Form6.Shape24.Brush.Color:=clWhite;
if StrToInt(p4[1])=0 then
Form6.Shape25.Brush.Color:=clRed;
if StrToInt(p4[1])=1 then
Form6.Shape25.Brush.Color:=clWhite;
if StrToInt(p4[2])=0 then
Form6.Shape26.Brush.Color:=clRed;
if StrToInt(p4[2])=1 then
Form6.Shape26.Brush.Color:=clWhite;
if StrToInt(p4[3])=0 then
Form6.Shape27.Brush.Color:=clRed;
if StrToInt(p4[3])=1 then
Form6.Shape27.Brush.Color:=clWhite;
if StrToInt(p4[4])=0 then
Form6.Shape28.Brush.Color:=clRed;
if StrToInt(p4[4])=1 then
Form6.Shape28.Brush.Color:=clWhite;
if StrToInt(p4[5])=0 then
Form6.Shape29.Brush.Color:=clRed;
if StrToInt(p4[5])=1 then
Form6.Shape29.Brush.Color:=clWhite;
if StrToInt(p4[6])=0 then
Form6.Shape30.Brush.Color:=clRed;
if StrToInt(p4[6])=1 then
Form6.Shape30.Brush.Color:=clWhite;
if StrToInt(p4[7])=0 then
Form6.Shape31.Brush.Color:=clRed;
if StrToInt(p4[7])=1 then
Form6.Shape31.Brush.Color:=clWhite;
if StrToInt(p4[8])=0 then
Form6.Shape32.Brush.Color:=clRed;
if StrToInt(p4[8])=1 then
Form6.Shape32.Brush.Color:=clWhite;
end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
Label7.Enabled:=false;
Label5.Enabled:=true;
RxGIFAnimator2.Image.LoadFromFile(Label9.Caption+'\������\���������������.gif');
RxGIFAnimator1.Image.LoadFromFile(Label9.Caption+'\������\�������������.gif');
Form1.d.Caption:='-1';
if Label134.Caption='0' then
Begin
Edit5.Text:='0';
Label134.Caption:='50';
end;
Label4.Caption:='����';
timer3.Enabled:=false;
Shape121.Brush.Color:=clWhite;
Shape122.Brush.Color:=clWhite;
Shape123.Brush.Color:=clWhite;
Shape124.Brush.Color:=clWhite;
Shape125.Brush.Color:=clWhite;
Shape126.Brush.Color:=clWhite;
Shape127.Brush.Color:=clWhite;
Shape128.Brush.Color:=clWhite;
Shape129.Brush.Color:=clWhite;
Shape130.Brush.Color:=clWhite;
Shape131.Brush.Color:=clWhite;
Shape132.Brush.Color:=clWhite;
Shape133.Brush.Color:=clWhite;
Shape134.Brush.Color:=clWhite;
Shape135.Brush.Color:=clWhite;
Shape136.Brush.Color:=clWhite;
Shape137.Brush.Color:=clWhite;
Shape138.Brush.Color:=clWhite;
Shape139.Brush.Color:=clWhite;
Shape140.Brush.Color:=clWhite;
Shape141.Brush.Color:=clWhite;
Shape142.Brush.Color:=clWhite;
Shape143.Brush.Color:=clWhite;
Shape144.Brush.Color:=clWhite;
Shape145.Brush.Color:=clWhite;
Shape146.Brush.Color:=clWhite;
Shape147.Brush.Color:=clWhite;
Shape148.Brush.Color:=clWhite;
Shape149.Brush.Color:=clWhite;
Shape150.Brush.Color:=clWhite;
Shape151.Brush.Color:=clWhite;
Shape152.Brush.Color:=clWhite;
Shape153.Brush.Color:=clWhite;
Shape154.Brush.Color:=clWhite;
Shape155.Brush.Color:=clWhite;
Shape156.Brush.Color:=clWhite;
Shape157.Brush.Color:=clWhite;
Shape158.Brush.Color:=clWhite;
Shape159.Brush.Color:=clWhite;
Shape160.Brush.Color:=clWhite;
Shape161.Brush.Color:=clWhite;
Shape162.Brush.Color:=clWhite;
Shape163.Brush.Color:=clWhite;
Shape164.Brush.Color:=clWhite;
Shape165.Brush.Color:=clWhite;
Shape166.Brush.Color:=clWhite;
Shape167.Brush.Color:=clWhite;
Shape168.Brush.Color:=clWhite;
Shape169.Brush.Color:=clWhite;
Shape170.Brush.Color:=clWhite;
Shape171.Brush.Color:=clWhite;
Shape172.Brush.Color:=clWhite;
Shape173.Brush.Color:=clWhite;
Shape174.Brush.Color:=clWhite;
Shape175.Brush.Color:=clWhite;
Shape176.Brush.Color:=clWhite;
Shape177.Brush.Color:=clWhite;
Shape178.Brush.Color:=clWhite;
Shape179.Brush.Color:=clWhite;
Shape180.Brush.Color:=clWhite;
Shape181.Brush.Color:=clWhite;
Shape182.Brush.Color:=clWhite;
Shape183.Brush.Color:=clWhite;
Shape184.Brush.Color:=clWhite;
Shape185.Brush.Color:=clWhite;
Shape186.Brush.Color:=clWhite;
Shape187.Brush.Color:=clWhite;
Shape188.Brush.Color:=clWhite;
Shape189.Brush.Color:=clWhite;
Shape190.Brush.Color:=clWhite;
Shape191.Brush.Color:=clWhite;
Shape192.Brush.Color:=clWhite;
Shape193.Brush.Color:=clWhite;
Shape194.Brush.Color:=clWhite;
Shape195.Brush.Color:=clWhite;
Shape196.Brush.Color:=clWhite;
Shape197.Brush.Color:=clWhite;
Shape198.Brush.Color:=clWhite;
Shape199.Brush.Color:=clWhite;
Shape200.Brush.Color:=clWhite;
Shape201.Brush.Color:=clWhite;
Shape202.Brush.Color:=clWhite;
Shape203.Brush.Color:=clWhite;
Shape204.Brush.Color:=clWhite;
Shape205.Brush.Color:=clWhite;
Shape206.Brush.Color:=clWhite;
Shape207.Brush.Color:=clWhite;
Shape208.Brush.Color:=clWhite;
Shape209.Brush.Color:=clWhite;
Shape210.Brush.Color:=clWhite;
Shape211.Brush.Color:=clWhite;
Shape212.Brush.Color:=clWhite;
Shape213.Brush.Color:=clWhite;
Shape214.Brush.Color:=clWhite;
Shape215.Brush.Color:=clWhite;
Shape216.Brush.Color:=clWhite;
Shape217.Brush.Color:=clWhite;
Shape218.Brush.Color:=clWhite;
Shape219.Brush.Color:=clWhite;
Shape220.Brush.Color:=clWhite;
Shape221.Brush.Color:=clWhite;
Shape222.Brush.Color:=clWhite;
Shape223.Brush.Color:=clWhite;
Shape224.Brush.Color:=clWhite;
Shape225.Brush.Color:=clWhite;
Shape226.Brush.Color:=clWhite;
Shape227.Brush.Color:=clWhite;
Shape228.Brush.Color:=clWhite;
Shape229.Brush.Color:=clWhite;
Shape230.Brush.Color:=clWhite;
Shape231.Brush.Color:=clWhite;
Shape232.Brush.Color:=clWhite;
Shape233.Brush.Color:=clWhite;
Shape234.Brush.Color:=clWhite;
Shape235.Brush.Color:=clWhite;
Shape236.Brush.Color:=clWhite;
Shape237.Brush.Color:=clWhite;
Shape238.Brush.Color:=clWhite;
Shape239.Brush.Color:=clWhite;
Shape240.Brush.Color:=clWhite;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
AnimateWindow(Form1.Handle, 2000, aw_ver_positive);
Form1.Show;
label9.Caption:=ExtractFilePath(Application.ExeName);
RxGIFAnimator1.Image.LoadFromFile(Label9.Caption+'\������\������������������.gif');
Hit:=false;
end;

procedure TForm1.Edit3KeyPress(Sender: TObject; var Key: Char);
begin
case Key of
#32 : ;
else
Key :=Chr(0); // ������ �� ����������
end;
end;

procedure TForm1.RxGIFAnimator1Click(Sender: TObject);
begin
if Form3.Label1.Caption<>'2' then
Form3.N1.Click;
Timer3.Enabled:=true;
Label7.Enabled:=true;
Label5.Enabled:=false;
RxGIFAnimator2.Image.LoadFromFile(Label9.Caption+'\������\����������.gif');
RxGIFAnimator1.Image.LoadFromFile(Label9.Caption+'\������\������������������.gif');
end;

procedure TForm1.Label5MouseEnter(Sender: TObject);
begin
if Label5.Enabled=true then
RxGIFAnimator1.Image.LoadFromFile(Label9.Caption+'\������\������������� �����.gif');
end;

procedure TForm1.Label6MouseEnter(Sender: TObject);
begin
if Label5.Enabled=true then
RxGIFAnimator1.Image.LoadFromFile(Label9.Caption+'\������\�������������.gif');
if Label7.Enabled=true then
RxGIFAnimator2.Image.LoadFromFile(Label9.Caption+'\������\����������.gif');
Label132.Font.Color:=clBlack;
end;

procedure TForm1.Label5MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
if Label5.Enabled=true then
RxGIFAnimator1.Image.LoadFromFile(Label9.Caption+'\������\�����������������.gif');
end;

procedure TForm1.Label5MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
if Label5.Enabled=true then
RxGIFAnimator1.Image.LoadFromFile(Label9.Caption+'\������\������������� �����.gif');
end;

procedure TForm1.Label7MouseEnter(Sender: TObject);
begin
if Label7.Enabled=true then
RxGIFAnimator2.Image.LoadFromFile(Label9.Caption+'\������\���������� �����.gif');
end;

procedure TForm1.Label7MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
if Label7.Enabled=true then
RxGIFAnimator2.Image.LoadFromFile(Label9.Caption+'\������\���������� �����.gif');
end;

procedure TForm1.Label7MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
if Label7.Enabled=true then
RxGIFAnimator2.Image.LoadFromFile(Label9.Caption+'\������\��������������.gif');
end;

procedure TForm1.N5Click(Sender: TObject);
begin
if N5.Checked=true then
Form1.Label132.OnClick(Label132)
else
Begin
Form1.GroupBox4.Visible:=true;
N5.Checked:=true;
end;
end;

procedure TForm1.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
case Key of
'0'..'9': Label3.Caption:='label3';
else Key :=Chr(0); // ������ �� ����������
end;
end;

procedure TForm1.N8Click(Sender: TObject);
begin
Form12.Visible:=true;
end;

procedure TForm1.N9Click(Sender: TObject);
begin
Form14.Visible:=true;
end;

procedure TForm1.SpinEdit1Change(Sender: TObject);
begin
if SpinEdit1.Value=1 then
Timer3.Interval:=600;
if SpinEdit1.Value=2 then
Timer3.Interval:=550;
if SpinEdit1.Value=3 then
Timer3.Interval:=500;
if SpinEdit1.Value=4 then
Timer3.Interval:=450;
if SpinEdit1.Value=5 then
Timer3.Interval:=400;
if SpinEdit1.Value=6 then
Timer3.Interval:=350;
if SpinEdit1.Value=7 then
Timer3.Interval:=300;
if SpinEdit1.Value=8 then
Timer3.Interval:=250;
if SpinEdit1.Value=9 then
Timer3.Interval:=200;
if SpinEdit1.Value=10 then
Timer3.Interval:=150;
if SpinEdit1.Value=11 then
Timer3.Interval:=125;
if SpinEdit1.Value=12 then
Timer3.Interval:=100;
if SpinEdit1.Value=13 then
Timer3.Interval:=75;
if SpinEdit1.Value=14 then
Timer3.Interval:=50;
if SpinEdit1.Value=15 then
Timer3.Interval:=25;
end;


procedure TForm1.N13Click(Sender: TObject);
begin
Form1.Cursor:=crArrow;
end;

procedure TForm1.Panel1Resize(Sender: TObject);
begin
Label12.Left:=Round(Panel1.ClientWidth/2)-Round(Label12.Width/2);
Label12.Top:=Panel1.Height-17;
end;

procedure TForm1.FormResize(Sender: TObject);
begin
ScrollBox1.Height:=Form1.Height-ScrollBox1.Top;
ScrollBox1.Width:=Form1.Width-ScrollBox1.Left;
end;

procedure TForm1.N10Click(Sender: TObject);
Var i,i1,i2:integer;
begin
Edit5.Text:='0';
Form3.Label1.Caption:='2';
if (Form2.Visible<>true) and (Form13.Visible<>true) then
Begin
Form13.Button53.Click;
end;
Shape1.Visible:=true;
Shape2.Visible:=true;
Shape3.Visible:=true;
Shape4.Visible:=true;
Shape5.Visible:=true;
Shape6.Visible:=true;
Shape7.Visible:=true;
Shape8.Visible:=true;
Shape9.Visible:=false;
Shape10.Visible:=false;
Shape11.Visible:=false;
Shape12.Visible:=false;
Shape13.Visible:=false;
Shape14.Visible:=false;
Shape15.Visible:=false;
Shape16.Visible:=false;
Shape17.Visible:=false;
Shape18.Visible:=false;
Shape19.Visible:=false;
Shape20.Visible:=false;
Shape21.Visible:=false;
Shape22.Visible:=false;
Shape23.Visible:=false;
Shape24.Visible:=false;
Shape25.Visible:=false;
Shape26.Visible:=false;
Shape27.Visible:=false;
Shape28.Visible:=false;
Shape29.Visible:=false;
Shape30.Visible:=false;
Shape31.Visible:=false;
Shape32.Visible:=false;
Shape33.Visible:=false;
Shape34.Visible:=false;
Shape35.Visible:=false;
Shape36.Visible:=false;
Shape37.Visible:=false;
Shape38.Visible:=false;
Shape39.Visible:=false;
Shape40.Visible:=false;
Shape41.Visible:=false;
Shape42.Visible:=false;
Shape43.Visible:=false;
Shape44.Visible:=false;
Shape45.Visible:=false;
Shape46.Visible:=false;
Shape47.Visible:=false;
Shape48.Visible:=false;
Shape49.Visible:=false;
Shape50.Visible:=false;
Shape51.Visible:=false;
Shape52.Visible:=false;
Shape53.Visible:=false;
Shape54.Visible:=false;
Shape55.Visible:=false;
Shape56.Visible:=false;
Shape57.Visible:=false;
Shape58.Visible:=false;
Shape59.Visible:=false;
Shape60.Visible:=false;
Shape61.Visible:=false;
Shape62.Visible:=false;
Shape63.Visible:=false;
Shape64.Visible:=false;
Shape65.Visible:=false;
Shape66.Visible:=false;
Shape67.Visible:=false;
Shape68.Visible:=false;
Shape69.Visible:=false;
Shape70.Visible:=false;
Shape71.Visible:=false;
Shape72.Visible:=false;
Shape73.Visible:=false;
Shape74.Visible:=false;
Shape75.Visible:=false;
Shape76.Visible:=false;
Shape77.Visible:=false;
Shape78.Visible:=false;
Shape79.Visible:=false;
Shape80.Visible:=false;
Shape81.Visible:=false;
Shape82.Visible:=false;
Shape83.Visible:=false;
Shape84.Visible:=false;
Shape85.Visible:=false;
Shape86.Visible:=false;
Shape87.Visible:=false;
Shape88.Visible:=false;
Shape89.Visible:=false;
Shape90.Visible:=false;
Shape91.Visible:=false;
Shape92.Visible:=false;
Shape93.Visible:=false;
Shape94.Visible:=false;
Shape95.Visible:=false;
Shape96.Visible:=false;
Shape97.Visible:=false;
Shape98.Visible:=false;
Shape99.Visible:=false;
Shape100.Visible:=false;
Shape101.Visible:=false;
Shape102.Visible:=false;
Shape103.Visible:=false;
Shape104.Visible:=false;
Shape105.Visible:=false;
Shape106.Visible:=false;
Shape107.Visible:=false;
Shape108.Visible:=false;
Shape109.Visible:=false;
Shape110.Visible:=false;
Shape111.Visible:=false;
Shape112.Visible:=false;
Shape113.Visible:=false;
Shape114.Visible:=false;
Shape115.Visible:=false;
Shape116.Visible:=false;
Shape117.Visible:=false;
Shape118.Visible:=false;
Shape119.Visible:=false;
Shape120.Visible:=false;
Shape121.Visible:=true;
Shape122.Visible:=true;
Shape123.Visible:=true;
Shape124.Visible:=true;
Shape125.Visible:=true;
Shape126.Visible:=true;
Shape127.Visible:=true;
Shape128.Visible:=true;
Shape129.Visible:=false;
Shape130.Visible:=false;
Shape131.Visible:=false;
Shape132.Visible:=false;
Shape133.Visible:=false;
Shape134.Visible:=false;
Shape135.Visible:=false;
Shape136.Visible:=false;
Shape137.Visible:=false;
Shape138.Visible:=false;
Shape139.Visible:=false;
Shape140.Visible:=false;
Shape141.Visible:=false;
Shape142.Visible:=false;
Shape143.Visible:=false;
Shape144.Visible:=false;
Shape145.Visible:=false;
Shape146.Visible:=false;
Shape147.Visible:=false;
Shape148.Visible:=false;
Shape149.Visible:=false;
Shape150.Visible:=false;
Shape51.Visible:=false;
Shape152.Visible:=false;
Shape153.Visible:=false;
Shape154.Visible:=false;
Shape55.Visible:=false;
Shape156.Visible:=false;
Shape157.Visible:=false;
Shape158.Visible:=false;
Shape159.Visible:=false;
Shape160.Visible:=false;
Shape161.Visible:=false;
Shape162.Visible:=false;
Shape163.Visible:=false;
Shape164.Visible:=false;
Shape165.Visible:=false;
Shape166.Visible:=false;
Shape167.Visible:=false;
Shape168.Visible:=false;
Shape169.Visible:=false;
Shape170.Visible:=false;
Shape171.Visible:=false;
Shape172.Visible:=false;
Shape173.Visible:=false;
Shape174.Visible:=false;
Shape175.Visible:=false;
Shape176.Visible:=false;
Shape177.Visible:=false;
Shape178.Visible:=false;
Shape179.Visible:=false;
Shape180.Visible:=false;
Shape181.Visible:=false;
Shape182.Visible:=false;
Shape183.Visible:=false;
Shape184.Visible:=false;
Shape185.Visible:=false;
Shape186.Visible:=false;
Shape187.Visible:=false;
Shape188.Visible:=false;
Shape189.Visible:=false;
Shape190.Visible:=false;
Shape191.Visible:=false;
Shape192.Visible:=false;
Shape193.Visible:=false;
Shape194.Visible:=false;
Shape195.Visible:=false;
Shape196.Visible:=false;
Shape197.Visible:=false;
Shape198.Visible:=false;
Shape199.Visible:=false;
Shape200.Visible:=false;
Shape201.Visible:=false;
Shape202.Visible:=false;
Shape203.Visible:=false;
Shape204.Visible:=false;
Shape205.Visible:=false;
Shape206.Visible:=false;
Shape207.Visible:=false;
Shape208.Visible:=false;
Shape209.Visible:=false;
Shape210.Visible:=false;
Shape211.Visible:=false;
Shape212.Visible:=false;
Shape213.Visible:=false;
Shape214.Visible:=false;
Shape215.Visible:=false;
Shape216.Visible:=false;
Shape217.Visible:=false;
Shape218.Visible:=false;
Shape219.Visible:=false;
Shape220.Visible:=false;
Shape221.Visible:=false;
Shape222.Visible:=false;
Shape223.Visible:=false;
Shape224.Visible:=false;
Shape225.Visible:=false;
Shape226.Visible:=false;
Shape227.Visible:=false;
Shape228.Visible:=false;
Shape229.Visible:=false;
Shape230.Visible:=false;
Shape231.Visible:=false;
Shape232.Visible:=false;
Shape233.Visible:=false;
Shape234.Visible:=false;
Shape235.Visible:=false;
Shape236.Visible:=false;
Shape237.Visible:=false;
Shape238.Visible:=false;
Shape239.Visible:=false;
Shape240.Visible:=false;
Label11.Visible:=true;
Label13.Visible:=true;
Label14.Visible:=true;
Label15.Visible:=true;
Label16.Visible:=true;
Label17.Visible:=true;
Label18.Visible:=true;
Label19.Visible:=true;
Label20.Visible:=false;
Label21.Visible:=false;
Label22.Visible:=false;
Label23.Visible:=false;
Label24.Visible:=false;
Label25.Visible:=false;
Label26.Visible:=false;
Label27.Visible:=false;
Label28.Visible:=false;
Label29.Visible:=false;
Label30.Visible:=false;
Label31.Visible:=false;
Label32.Visible:=false;
Label33.Visible:=false;
Label34.Visible:=false;
Label35.Visible:=false;
Label36.Visible:=false;
Label37.Visible:=false;
Label38.Visible:=false;
Label39.Visible:=false;
Label40.Visible:=false;
Label41.Visible:=false;
Label42.Visible:=false;
Label43.Visible:=false;
Label44.Visible:=false;
Label45.Visible:=false;
Label46.Visible:=false;
Label47.Visible:=false;
Label48.Visible:=false;
Label49.Visible:=false;
Label50.Visible:=false;
Label51.Visible:=false;
Label52.Visible:=false;
Label53.Visible:=false;
Label54.Visible:=false;
Label55.Visible:=false;
Label56.Visible:=false;
Label57.Visible:=false;
Label58.Visible:=false;
Label59.Visible:=false;
Label60.Visible:=false;
Label61.Visible:=false;
Label62.Visible:=false;
Label63.Visible:=false;
Label64.Visible:=false;
Label65.Visible:=false;
Label66.Visible:=false;
Label67.Visible:=false;
Label68.Visible:=false;
Label69.Visible:=false;
Label70.Visible:=false;
Label71.Visible:=false;
Label72.Visible:=false;
Label73.Visible:=false;
Label74.Visible:=false;
Label75.Visible:=false;
Label76.Visible:=false;
Label77.Visible:=false;
Label78.Visible:=false;
Label79.Visible:=false;
Label80.Visible:=false;
Label81.Visible:=false;
Label82.Visible:=false;
Label83.Visible:=false;
Label84.Visible:=false;
Label85.Visible:=false;
Label86.Visible:=false;
Label87.Visible:=false;
Label88.Visible:=false;
Label89.Visible:=false;
Label90.Visible:=false;
Label91.Visible:=false;
Label92.Visible:=false;
Label93.Visible:=false;
Label94.Visible:=false;
Label95.Visible:=false;
Label96.Visible:=false;
Label97.Visible:=false;
Label98.Visible:=false;
Label99.Visible:=false;
Label100.Visible:=false;
Label101.Visible:=false;
Label102.Visible:=false;
Label103.Visible:=false;
Label104.Visible:=false;
Label105.Visible:=false;
Label106.Visible:=false;
Label107.Visible:=false;
Label108.Visible:=false;
Label109.Visible:=false;
Label110.Visible:=false;
Label111.Visible:=false;
Label112.Visible:=false;
Label113.Visible:=false;
Label114.Visible:=false;
Label115.Visible:=false;
Label116.Visible:=false;
Label117.Visible:=false;
Label118.Visible:=false;
Label119.Visible:=false;
Label120.Visible:=false;
Label121.Visible:=false;
Label122.Visible:=false;
Label123.Visible:=false;
Label124.Visible:=false;
Label125.Visible:=false;
Label126.Visible:=false;
Label127.Visible:=false;
Label128.Visible:=false;
Label129.Visible:=false;
Label130.Visible:=false;
Label131.Visible:=false;
Form3.StringGrid1.ColCount:=2;
Form3.StringGrid1.RowCount:=2;
For i1:=1 to Form3.StringGrid1.ColCount do
Begin
For i2:=1 to Form3.StringGrid1.RowCount do
Begin
Form3.StringGrid1.Cells[i1,i2]:='';
end;
end;
Label10.Caption:='1';
GroupBox1.Visible:=true;
GroupBox2.Visible:=true;
GroupBox3.Visible:=true;
GroupBox4.Visible:=true;
ScrollBox1.Visible:=true;
RxGIFAnimator1.Visible:=true;
Label5.Visible:=true;
RxGIFAnimator2.Visible:=true;
Label7.Visible:=true;
N8.Enabled:=true;
N9.Enabled:=true;
Form3.StringGrid1.Cells[0,0]:='�������';
Form3.StringGrid1.Cells[1,0]:='����������1';
For i:=1 to 2000 do
Begin
Form3.StringGrid1.Cells[0,i]:=IntToStr(i-1);
Form3.StringGrid1.RowCount:=i+1;
end;
For i:=1 to 2000 do
Form3.StringGrid1.Cells[1,i]:='11111111';
GroupBox4.Height:=49;
GroupBox4.Width:=209;
Panel1.Height:=41;
Panel1.Width:=145;
N14.Enabled:=true;
N5.Enabled:=true;
N5.Checked:=true;
N12.Enabled:=true;
N13.Enabled:=true;
Form1.izmcvClick(Form1);
end;

procedure TForm1.izmcvClick(Sender: TObject);
var i1,i2:integer;
begin
Panel1.Color:=Form14.Shape1.Brush.Color;
GroupBox4.Color:=Form12.Shape1.Brush.Color;
i1:=StrToInt(Edit5.Text)+1;
i2:=1;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape1.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape2.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape3.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape4.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape5.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape6.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape7.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape8.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=2;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape9.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape10.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape11.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape12.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape13.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape14.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape15.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape16.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=3;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape17.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape18.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape19.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape20.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape21.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape22.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape23.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape24.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=4;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape25.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape26.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape27.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape28.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape29.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape30.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape31.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape32.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=5;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape33.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape34.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape35.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape36.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape37.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape38.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape39.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape40.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=6;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape41.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape42.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape43.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape44.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape45.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape46.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape47.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape48.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=7;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape49.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape50.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape51.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1'then
Shape52.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape53.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape54.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape55.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape56.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=8;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape57.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape58.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape59.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape60.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape61.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape62.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape63.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape64.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=9;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Form1.Shape65.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Form1.Shape66.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Form1.Shape67.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape68.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape69.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape70.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape71.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape72.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=10;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape73.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape74.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape75.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape76.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape77.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape78.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape79.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape80.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=11;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape81.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape82.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape83.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape84.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape85.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape86.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape87.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape88.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=12;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape89.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape90.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape91.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape92.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape93.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape94.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape95.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape96.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=13;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape97.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape98.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape99.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape100.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape101.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape102.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape103.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape104.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=14;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape105.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape106.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape107.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape108.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape109.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape110.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape111.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape112.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=15;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape113.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape114.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape115.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape116.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape117.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape118.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape119.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape120.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=1;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape1.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape2.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape3.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape4.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape5.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape6.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape7.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape8.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=2;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape9.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape10.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape11.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape12.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape13.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape14.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape15.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape16.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=3;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape17.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape18.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape19.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape20.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape21.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape22.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape23.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape24.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=4;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape25.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape26.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape27.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape28.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape29.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape30.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape31.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape32.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=5;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape33.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape34.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape35.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape36.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape37.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape38.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape39.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape40.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=6;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape41.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape42.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape43.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape44.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape45.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape46.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape47.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape48.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=7;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape49.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape50.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape51.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape52.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape53.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape54.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape55.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape56.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=8;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape57.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape58.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape59.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape60.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape61.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape62.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape63.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape64.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=9;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape65.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape66.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape67.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape68.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape69.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape70.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape71.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape72.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=10;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape73.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape74.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape75.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape76.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape77.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape78.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape79.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape80.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=11;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape81.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape82.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape83.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape84.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape85.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape86.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape87.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape88.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=12;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape89.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape90.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape91.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape92.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape93.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape94.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape95.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape96.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=13;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape97.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape98.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape99.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape100.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape101.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape102.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape103.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape104.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=14;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape105.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape106.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape107.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape108.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape109.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape110.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape111.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape112.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=15;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape113.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape114.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape115.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape116.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape117.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape118.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape119.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape120.Brush.Color:=Form14.Shape2.Brush.Color;
end;

procedure TForm1.N14Click(Sender: TObject);
begin
if N14.Checked=true then
Form3.Close
else
Form3.Visible:=true;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
Shape1.Free;
Shape2.Free;
Shape3.Free;
Shape4.Free;
Shape5.Free;
Shape6.Free;
Shape7.Free;
Shape8.Free;
Shape9.Free;
Shape10.Free;
Shape11.Free;
Shape12.Free;
Shape13.Free;
Shape14.Free;
Shape15.Free;
Shape16.Free;
Shape17.Free;
Shape18.Free;
Shape19.Free;
Shape20.Free;
Shape21.Free;
Shape22.Free;
Shape23.Free;
Shape24.Free;
Shape25.Free;
Shape26.Free;
Shape27.Free;
Shape28.Free;
Shape29.Free;
Shape30.Free;
Shape31.Free;
Shape32.Free;
Shape33.Free;
Shape34.Free;
Shape35.Free;
Shape36.Free;
Shape37.Free;
Shape38.Free;
Shape39.Free;
Shape40.Free;
Shape41.Free;
Shape42.Free;
Shape43.Free;
Shape44.Free;
Shape45.Free;
Shape46.Free;
Shape47.Free;
Shape48.Free;
Shape49.Free;
Shape50.Free;
Shape51.Free;
Shape52.Free;
Shape53.Free;
Shape54.Free;
Shape55.Free;
Shape56.Free;
Shape57.Free;
Shape58.Free;
Shape59.Free;
Shape60.Free;
Shape61.Free;
Shape62.Free;
Shape63.Free;
Shape64.Free;
Shape65.Free;
Shape66.Free;
Shape67.Free;
Shape68.Free;
Shape69.Free;
Shape70.Free;
Shape71.Free;
Shape72.Free;
Shape73.Free;
Shape74.Free;
Shape75.Free;
Shape76.Free;
Shape77.Free;
Shape78.Free;
Shape79.Free;
Shape80.Free;
Shape81.Free;
Shape82.Free;
Shape83.Free;
Shape84.Free;
Shape85.Free;
Shape86.Free;
Shape87.Free;
Shape88.Free;
Shape89.Free;
Shape90.Free;
Shape91.Free;
Shape92.Free;
Shape93.Free;
Shape94.Free;
Shape95.Free;
Shape96.Free;
Shape97.Free;
Shape98.Free;
Shape99.Free;
Shape100.Free;
Shape101.Free;
Shape102.Free;
Shape103.Free;
Shape104.Free;
Shape105.Free;
Shape106.Free;
Shape107.Free;
Shape108.Free;
Shape109.Free;
Shape110.Free;
Shape111.Free;
Shape112.Free;
Shape113.Free;
Shape114.Free;
Shape115.Free;
Shape116.Free;
Shape117.Free;
Shape118.Free;
Shape119.Free;
Shape120.Free;
end;

procedure TForm1.N12Click(Sender: TObject);
var b,i:integer;
begin
b:=0;
Repeat
Begin
if Label10.Caption='15' then
Begin
b:=1;
Break
end;
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount+1;
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,0]:='����������'+IntToStr(StrToInt(Label10.Caption)+1);
Form3.StringGrid1.ColWidths[Form3.StringGrid1.ColCount-1]:=80;
GroupBox4.Height:=GroupBox4.Height+24;
Panel1.Height:=Panel1.Height+16;
For i:=1 to 2000 do
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i]:='11111111';
Form1.izmcvClick(Form1);
if Label10.Caption='0' then
Begin
Shape1.Visible:=true;
Shape2.Visible:=true;
Shape3.Visible:=true;
Shape4.Visible:=true;
Shape5.Visible:=true;
Shape6.Visible:=true;
Shape7.Visible:=true;
Shape8.Visible:=true;
Shape121.Visible:=true;
Shape122.Visible:=true;
Shape123.Visible:=true;
Shape124.Visible:=true;
Shape125.Visible:=true;
Shape126.Visible:=true;
Shape127.Visible:=true;
Shape128.Visible:=true;
Label11.Visible:=true;
Label13.Visible:=true;
Label14.Visible:=true;
Label15.Visible:=true;
Label16.Visible:=true;
Label17.Visible:=true;
Label18.Visible:=true;
Label19.Visible:=true;
Label10.Caption:='1';
b:=1;
Break
end;
if Label10.Caption='1' then
Begin
Shape9.Visible:=true;
Shape10.Visible:=true;
Shape11.Visible:=true;
Shape12.Visible:=true;
Shape13.Visible:=true;
Shape14.Visible:=true;
Shape15.Visible:=true;
Shape16.Visible:=true;
Shape129.Visible:=true;
Shape130.Visible:=true;
Shape131.Visible:=true;
Shape132.Visible:=true;
Shape133.Visible:=true;
Shape134.Visible:=true;
Shape135.Visible:=true;
Shape136.Visible:=true;
Label20.Visible:=true;
Label21.Visible:=true;
Label22.Visible:=true;
Label23.Visible:=true;
Label24.Visible:=true;
Label25.Visible:=true;
Label26.Visible:=true;
Label27.Visible:=true;
Label10.Caption:='2';
b:=1;
Break
end;
if Label10.Caption='2' then
Begin
Shape17.Visible:=true;
Shape18.Visible:=true;
Shape19.Visible:=true;
Shape20.Visible:=true;
Shape21.Visible:=true;
Shape22.Visible:=true;
Shape23.Visible:=true;
Shape24.Visible:=true;
Shape137.Visible:=true;
Shape138.Visible:=true;
Shape139.Visible:=true;
Shape140.Visible:=true;
Shape141.Visible:=true;
Shape142.Visible:=true;
Shape143.Visible:=true;
Shape144.Visible:=true;
Label28.Visible:=true;
Label29.Visible:=true;
Label30.Visible:=true;
Label31.Visible:=true;
Label32.Visible:=true;
Label33.Visible:=true;
Label34.Visible:=true;
Label35.Visible:=true;
Label10.Caption:='3';
b:=1;
Break
end;
if Label10.Caption='3' then
Begin
Shape25.Visible:=true;
Shape26.Visible:=true;
Shape27.Visible:=true;
Shape28.Visible:=true;
Shape29.Visible:=true;
Shape30.Visible:=true;
Shape31.Visible:=true;
Shape32.Visible:=true;
Shape145.Visible:=true;
Shape146.Visible:=true;
Shape147.Visible:=true;
Shape148.Visible:=true;
Shape149.Visible:=true;
Shape150.Visible:=true;
Shape151.Visible:=true;
Shape152.Visible:=true;
Label36.Visible:=true;
Label37.Visible:=true;
Label38.Visible:=true;
Label39.Visible:=true;
Label40.Visible:=true;
Label41.Visible:=true;
Label42.Visible:=true;
Label43.Visible:=true;
Label10.Caption:='4';
b:=1;
Break
end;
if Label10.Caption='4' then
Begin
Shape33.Visible:=true;
Shape34.Visible:=true;
Shape35.Visible:=true;
Shape36.Visible:=true;
Shape37.Visible:=true;
Shape38.Visible:=true;
Shape39.Visible:=true;
Shape40.Visible:=true;
Shape153.Visible:=true;
Shape154.Visible:=true;
Shape155.Visible:=true;
Shape156.Visible:=true;
Shape157.Visible:=true;
Shape158.Visible:=true;
Shape159.Visible:=true;
Shape160.Visible:=true;
Label44.Visible:=true;
Label45.Visible:=true;
Label46.Visible:=true;
Label47.Visible:=true;
Label48.Visible:=true;
Label49.Visible:=true;
Label50.Visible:=true;
Label51.Visible:=true;
Label10.Caption:='5';
b:=1;
Break
end;
if Label10.Caption='5' then
Begin
Shape41.Visible:=true;
Shape42.Visible:=true;
Shape43.Visible:=true;
Shape44.Visible:=true;
Shape45.Visible:=true;
Shape46.Visible:=true;
Shape47.Visible:=true;
Shape48.Visible:=true;
Shape161.Visible:=true;
Shape162.Visible:=true;
Shape163.Visible:=true;
Shape164.Visible:=true;
Shape165.Visible:=true;
Shape166.Visible:=true;
Shape167.Visible:=true;
Shape168.Visible:=true;
Label52.Visible:=true;
Label53.Visible:=true;
Label54.Visible:=true;
Label55.Visible:=true;
Label56.Visible:=true;
Label57.Visible:=true;
Label58.Visible:=true;
Label59.Visible:=true;
Label10.Caption:='6';
b:=1;
Break
end;
if Label10.Caption='6' then
Begin
Shape49.Visible:=true;
Shape50.Visible:=true;
Shape51.Visible:=true;
Shape52.Visible:=true;
Shape53.Visible:=true;
Shape54.Visible:=true;
Shape55.Visible:=true;
Shape56.Visible:=true;
Shape169.Visible:=true;
Shape170.Visible:=true;
Shape171.Visible:=true;
Shape172.Visible:=true;
Shape173.Visible:=true;
Shape174.Visible:=true;
Shape175.Visible:=true;
Shape176.Visible:=true;
Label60.Visible:=true;
Label61.Visible:=true;
Label62.Visible:=true;
Label63.Visible:=true;
Label64.Visible:=true;
Label65.Visible:=true;
Label66.Visible:=true;
Label67.Visible:=true;
Label10.Caption:='7';
b:=1;
Break
end;
if Label10.Caption='7' then
Begin
Shape57.Visible:=true;
Shape58.Visible:=true;
Shape59.Visible:=true;
Shape60.Visible:=true;
Shape61.Visible:=true;
Shape62.Visible:=true;
Shape63.Visible:=true;
Shape64.Visible:=true;
Shape177.Visible:=true;
Shape178.Visible:=true;
Shape179.Visible:=true;
Shape180.Visible:=true;
Shape181.Visible:=true;
Shape182.Visible:=true;
Shape183.Visible:=true;
Shape184.Visible:=true;
Form1.Label68.Visible:=true;
Form1.Label69.Visible:=true;
Label70.Visible:=true;
Label71.Visible:=true;
Label72.Visible:=true;
Label73.Visible:=true;
Label74.Visible:=true;
Label75.Visible:=true;
Label10.Caption:='8';
b:=1;
Break
end;
if Label10.Caption='8' then
Begin
Shape65.Visible:=true;
Shape66.Visible:=true;
Shape67.Visible:=true;
Shape68.Visible:=true;
Shape69.Visible:=true;
Shape70.Visible:=true;
Shape71.Visible:=true;
Shape72.Visible:=true;
Shape185.Visible:=true;
Shape186.Visible:=true;
Shape187.Visible:=true;
Shape188.Visible:=true;
Shape189.Visible:=true;
Shape190.Visible:=true;
Shape191.Visible:=true;
Shape192.Visible:=true;
Label76.Visible:=true;
Label77.Visible:=true;
Label78.Visible:=true;
Label79.Visible:=true;
Label80.Visible:=true;
Label81.Visible:=true;
Label82.Visible:=true;
Label83.Visible:=true;
Label10.Caption:='9';
b:=1;
Break
end;
if Label10.Caption='9' then
Begin
Shape73.Visible:=true;
Shape74.Visible:=true;
Shape75.Visible:=true;
Shape76.Visible:=true;
Shape77.Visible:=true;
Shape78.Visible:=true;
Shape79.Visible:=true;
Shape80.Visible:=true;
Shape193.Visible:=true;
Shape194.Visible:=true;
Shape195.Visible:=true;
Shape196.Visible:=true;
Shape197.Visible:=true;
Shape198.Visible:=true;
Shape199.Visible:=true;
Shape200.Visible:=true;
Label84.Visible:=true;
Label85.Visible:=true;
Label86.Visible:=true;
Label87.Visible:=true;
Label88.Visible:=true;
Label89.Visible:=true;
Label90.Visible:=true;
Label91.Visible:=true;
Label10.Caption:='10';
b:=1;
Break
end;
if Label10.Caption='10' then
Begin
Shape81.Visible:=true;
Shape82.Visible:=true;
Shape83.Visible:=true;
Shape84.Visible:=true;
Shape85.Visible:=true;
Shape86.Visible:=true;
Shape87.Visible:=true;
Shape88.Visible:=true;
Shape201.Visible:=true;
Shape202.Visible:=true;
Shape203.Visible:=true;
Shape204.Visible:=true;
Shape205.Visible:=true;
Shape206.Visible:=true;
Shape207.Visible:=true;
Shape208.Visible:=true;
Label92.Visible:=true;
Label113.Visible:=true;
Label114.Visible:=true;
Label119.Visible:=true;
Label120.Visible:=true;
Label123.Visible:=true;
Label128.Visible:=true;
Label129.Visible:=true;
Label10.Caption:='11';
b:=1;
Break
end;
if Label10.Caption='11' then
Begin
Shape89.Visible:=true;
Shape90.Visible:=true;
Shape91.Visible:=true;
Shape92.Visible:=true;
Shape93.Visible:=true;
Shape94.Visible:=true;
Shape95.Visible:=true;
Shape96.Visible:=true;
Shape209.Visible:=true;
Shape210.Visible:=true;
Shape211.Visible:=true;
Shape212.Visible:=true;
Shape213.Visible:=true;
Shape214.Visible:=true;
Shape215.Visible:=true;
Shape216.Visible:=true;
Label93.Visible:=true;
Label112.Visible:=true;
Label115.Visible:=true;
Label118.Visible:=true;
Label121.Visible:=true;
Label124.Visible:=true;
Label127.Visible:=true;
Label130.Visible:=true;
Label10.Caption:='12';
b:=1;
Break
end;
if Label10.Caption='12' then
Begin
Shape97.Visible:=true;
Shape98.Visible:=true;
Shape99.Visible:=true;
Shape100.Visible:=true;
Shape101.Visible:=true;
Shape102.Visible:=true;
Shape103.Visible:=true;
Shape104.Visible:=true;
Shape217.Visible:=true;
Shape218.Visible:=true;
Shape219.Visible:=true;
Shape220.Visible:=true;
Shape221.Visible:=true;
Shape222.Visible:=true;
Shape223.Visible:=true;
Shape224.Visible:=true;
Label94.Visible:=true;
Label111.Visible:=true;
Label116.Visible:=true;
Label117.Visible:=true;
Label122.Visible:=true;
Label125.Visible:=true;
Label126.Visible:=true;
Label131.Visible:=true;
Label10.Caption:='13';
b:=1;
Break
end;
if Label10.Caption='13' then
Begin
Shape105.Visible:=true;
Shape106.Visible:=true;
Shape107.Visible:=true;
Shape108.Visible:=true;
Shape109.Visible:=true;
Shape110.Visible:=true;
Shape111.Visible:=true;
Shape112.Visible:=true;
Shape225.Visible:=true;
Shape226.Visible:=true;
Shape227.Visible:=true;
Shape228.Visible:=true;
Shape229.Visible:=true;
Shape230.Visible:=true;
Shape231.Visible:=true;
Shape232.Visible:=true;

Label95.Visible:=true;
Label110.Visible:=true;
Label109.Visible:=true;
Label108.Visible:=true;
Label107.Visible:=true;
Label106.Visible:=true;
Label105.Visible:=true;
Label104.Visible:=true;
Label10.Caption:='14';
b:=1;
Break
end;
if Label10.Caption='14' then
Begin
Shape113.Visible:=true;
Shape114.Visible:=true;
Shape115.Visible:=true;
Shape116.Visible:=true;
Shape117.Visible:=true;
Shape118.Visible:=true;
Shape119.Visible:=true;
Shape120.Visible:=true;
Shape233.Visible:=true;
Shape234.Visible:=true;
Shape235.Visible:=true;
Shape236.Visible:=true;
Shape237.Visible:=true;
Shape238.Visible:=true;
Shape239.Visible:=true;
Shape240.Visible:=true;
Label96.Visible:=true;
Label97.Visible:=true;
Label98.Visible:=true;
Label99.Visible:=true;
Label100.Visible:=true;
Label101.Visible:=true;
Label102.Visible:=true;
Label103.Visible:=true;
Label10.Caption:='15';
b:=1;
Break
end;
end;
until b=1;
end;

procedure TForm1.ShapeNevud(Sender: TObject);
begin
Shape1.Pen.Width:=1;
Shape2.Pen.Width:=1;
Shape3.Pen.Width:=1;
Shape4.Pen.Width:=1;
Shape5.Pen.Width:=1;
Shape6.Pen.Width:=1;
Shape7.Pen.Width:=1;
Shape8.Pen.Width:=1;
Shape9.Pen.Width:=1;
Shape10.Pen.Width:=1;
Shape11.Pen.Width:=1;
Shape12.Pen.Width:=1;
Shape13.Pen.Width:=1;
Shape14.Pen.Width:=1;
Shape15.Pen.Width:=1;
Shape16.Pen.Width:=1;
Shape17.Pen.Width:=1;
Shape18.Pen.Width:=1;
Shape19.Pen.Width:=1;
Shape20.Pen.Width:=1;
Shape21.Pen.Width:=1;
Shape22.Pen.Width:=1;
Shape23.Pen.Width:=1;
Shape24.Pen.Width:=1;
Shape25.Pen.Width:=1;
Shape26.Pen.Width:=1;
Shape27.Pen.Width:=1;
Shape28.Pen.Width:=1;
Shape29.Pen.Width:=1;
Shape30.Pen.Width:=1;
Shape31.Pen.Width:=1;
Shape32.Pen.Width:=1;
Shape33.Pen.Width:=1;
Shape34.Pen.Width:=1;
Shape35.Pen.Width:=1;
Shape36.Pen.Width:=1;
Shape37.Pen.Width:=1;
Shape38.Pen.Width:=1;
Shape39.Pen.Width:=1;
Shape40.Pen.Width:=1;
Shape41.Pen.Width:=1;
Shape42.Pen.Width:=1;
Shape43.Pen.Width:=1;
Shape44.Pen.Width:=1;
Shape45.Pen.Width:=1;
Shape46.Pen.Width:=1;
Shape47.Pen.Width:=1;
Shape48.Pen.Width:=1;
Shape49.Pen.Width:=1;
Shape50.Pen.Width:=1;
Shape51.Pen.Width:=1;
Shape52.Pen.Width:=1;
Shape53.Pen.Width:=1;
Shape54.Pen.Width:=1;
Shape55.Pen.Width:=1;
Shape56.Pen.Width:=1;
Shape57.Pen.Width:=1;
Shape58.Pen.Width:=1;
Shape59.Pen.Width:=1;
Shape60.Pen.Width:=1;
Shape61.Pen.Width:=1;
Shape62.Pen.Width:=1;
Shape63.Pen.Width:=1;
Shape64.Pen.Width:=1;
Shape65.Pen.Width:=1;
Shape66.Pen.Width:=1;
Shape67.Pen.Width:=1;
Shape68.Pen.Width:=1;
Shape69.Pen.Width:=1;
Shape70.Pen.Width:=1;
Shape71.Pen.Width:=1;
Shape72.Pen.Width:=1;
Shape73.Pen.Width:=1;
Shape74.Pen.Width:=1;
Shape75.Pen.Width:=1;
Shape76.Pen.Width:=1;
Shape77.Pen.Width:=1;
Shape78.Pen.Width:=1;
Shape79.Pen.Width:=1;
Shape80.Pen.Width:=1;
Shape81.Pen.Width:=1;
Shape82.Pen.Width:=1;
Shape83.Pen.Width:=1;
Shape84.Pen.Width:=1;
Shape85.Pen.Width:=1;
Shape86.Pen.Width:=1;
Shape87.Pen.Width:=1;
Shape88.Pen.Width:=1;
Shape89.Pen.Width:=1;
Shape90.Pen.Width:=1;
Shape91.Pen.Width:=1;
Shape92.Pen.Width:=1;
Shape93.Pen.Width:=1;
Shape94.Pen.Width:=1;
Shape95.Pen.Width:=1;
Shape96.Pen.Width:=1;
Shape97.Pen.Width:=1;
Shape98.Pen.Width:=1;
Shape99.Pen.Width:=1;
Shape100.Pen.Width:=1;
Shape101.Pen.Width:=1;
Shape102.Pen.Width:=1;
Shape103.Pen.Width:=1;
Shape104.Pen.Width:=1;
Shape105.Pen.Width:=1;
Shape106.Pen.Width:=1;
Shape107.Pen.Width:=1;
Shape108.Pen.Width:=1;
Shape109.Pen.Width:=1;
Shape110.Pen.Width:=1;
Shape111.Pen.Width:=1;
Shape112.Pen.Width:=1;
Shape113.Pen.Width:=1;
Shape114.Pen.Width:=1;
Shape115.Pen.Width:=1;
Shape116.Pen.Width:=1;
Shape117.Pen.Width:=1;
Shape118.Pen.Width:=1;
Shape119.Pen.Width:=1;
Shape120.Pen.Width:=1;
end;

procedure TForm1.Label11MouseEnter(Sender: TObject);
begin
if Form1.Cursor=crArrow then
Begin
Form1.ShapeNevud(Form1);
Shape1.Pen.Width:=3;
Shape2.Pen.Width:=3;
Shape3.Pen.Width:=3;
Shape4.Pen.Width:=3;
Shape5.Pen.Width:=3;
Shape6.Pen.Width:=3;
Shape7.Pen.Width:=3;
Shape8.Pen.Width:=3;
end;
end;

procedure TForm1.Label20MouseEnter(Sender: TObject);
begin
if Form1.Cursor=crArrow then
Begin
Form1.ShapeNevud(Form1);
Shape9.Pen.Width:=3;
Shape10.Pen.Width:=3;
Shape11.Pen.Width:=3;
Shape12.Pen.Width:=3;
Shape13.Pen.Width:=3;
Shape14.Pen.Width:=3;
Shape15.Pen.Width:=3;
Shape16.Pen.Width:=3;
end;
end;

procedure TForm1.Label28MouseEnter(Sender: TObject);
begin
if Form1.Cursor=crArrow then
Begin
Form1.ShapeNevud(Form1);
Shape17.Pen.Width:=3;
Shape18.Pen.Width:=3;
Shape19.Pen.Width:=3;
Shape20.Pen.Width:=3;
Shape21.Pen.Width:=3;
Shape22.Pen.Width:=3;
Shape23.Pen.Width:=3;
Shape24.Pen.Width:=3;
end;
end;

procedure TForm1.Label36MouseEnter(Sender: TObject);
begin
if Form1.Cursor=crArrow then
Begin
Form1.ShapeNevud(Form1);
Shape25.Pen.Width:=3;
Shape26.Pen.Width:=3;
Shape27.Pen.Width:=3;
Shape28.Pen.Width:=3;
Shape29.Pen.Width:=3;
Shape30.Pen.Width:=3;
Shape31.Pen.Width:=3;
Shape32.Pen.Width:=3;
end;
end;

procedure TForm1.Label44MouseEnter(Sender: TObject);
begin
if Form1.Cursor=crArrow then
Begin
Form1.ShapeNevud(Form1);
Shape33.Pen.Width:=3;
Shape34.Pen.Width:=3;
Shape35.Pen.Width:=3;
Shape36.Pen.Width:=3;
Shape37.Pen.Width:=3;
Shape38.Pen.Width:=3;
Shape39.Pen.Width:=3;
Shape40.Pen.Width:=3;
end;
end;

procedure TForm1.Label52MouseEnter(Sender: TObject);
begin
if Form1.Cursor=crArrow then
Begin
Form1.ShapeNevud(Form1);
Shape41.Pen.Width:=3;
Shape42.Pen.Width:=3;
Shape43.Pen.Width:=3;
Shape44.Pen.Width:=3;
Shape45.Pen.Width:=3;
Shape46.Pen.Width:=3;
Shape47.Pen.Width:=3;
Shape48.Pen.Width:=3;
end;
end;

procedure TForm1.Label60MouseEnter(Sender: TObject);
begin
if Form1.Cursor=crArrow then
Begin
Form1.ShapeNevud(Form1);
Shape49.Pen.Width:=3;
Shape50.Pen.Width:=3;
Shape51.Pen.Width:=3;
Shape52.Pen.Width:=3;
Shape53.Pen.Width:=3;
Shape54.Pen.Width:=3;
Shape55.Pen.Width:=3;
Shape56.Pen.Width:=3;
end;
end;

procedure TForm1.Label68MouseEnter(Sender: TObject);
begin
if Form1.Cursor=crArrow then
Begin
Form1.ShapeNevud(Form1);
Shape57.Pen.Width:=3;
Shape58.Pen.Width:=3;
Shape59.Pen.Width:=3;
Shape60.Pen.Width:=3;
Shape61.Pen.Width:=3;
Shape62.Pen.Width:=3;
Shape63.Pen.Width:=3;
Shape64.Pen.Width:=3;
end;
end;

procedure TForm1.Label76MouseEnter(Sender: TObject);
begin
if Form1.Cursor=crArrow then
Begin
Form1.ShapeNevud(Form1);
Shape65.Pen.Width:=3;
Shape66.Pen.Width:=3;
Shape67.Pen.Width:=3;
Shape68.Pen.Width:=3;
Shape69.Pen.Width:=3;
Shape70.Pen.Width:=3;
Shape71.Pen.Width:=3;
Shape72.Pen.Width:=3;
end;
end;

procedure TForm1.Label84MouseEnter(Sender: TObject);
begin
if Form1.Cursor=crArrow then
Begin
Form1.ShapeNevud(Form1);
Shape73.Pen.Width:=3;
Shape74.Pen.Width:=3;
Shape75.Pen.Width:=3;
Shape76.Pen.Width:=3;
Shape77.Pen.Width:=3;
Shape78.Pen.Width:=3;
Shape79.Pen.Width:=3;
Shape80.Pen.Width:=3;
end;
end;

procedure TForm1.Label92MouseEnter(Sender: TObject);
begin
if Form1.Cursor=crArrow then
Begin
Form1.ShapeNevud(Form1);
Shape81.Pen.Width:=3;
Shape82.Pen.Width:=3;
Shape83.Pen.Width:=3;
Shape84.Pen.Width:=3;
Shape85.Pen.Width:=3;
Shape86.Pen.Width:=3;
Shape87.Pen.Width:=3;
Shape88.Pen.Width:=3;
end;
end;

procedure TForm1.Label100MouseEnter(Sender: TObject);
begin
if Form1.Cursor=crArrow then
Begin
Form1.ShapeNevud(Form1);
Shape89.Pen.Width:=3;
Shape90.Pen.Width:=3;
Shape91.Pen.Width:=3;
Shape92.Pen.Width:=3;
Shape93.Pen.Width:=3;
Shape94.Pen.Width:=3;
Shape95.Pen.Width:=3;
Shape96.Pen.Width:=3;
end;
end;

procedure TForm1.Label108MouseEnter(Sender: TObject);
begin
if Form1.Cursor=crArrow then
Begin
Form1.ShapeNevud(Form1);
Shape97.Pen.Width:=3;
Shape98.Pen.Width:=3;
Shape99.Pen.Width:=3;
Shape100.Pen.Width:=3;
Shape101.Pen.Width:=3;
Shape102.Pen.Width:=3;
Shape103.Pen.Width:=3;
Shape104.Pen.Width:=3;
end;
end;

procedure TForm1.Label116MouseEnter(Sender: TObject);
begin
if Form1.Cursor=crArrow then
Begin
Form1.ShapeNevud(Form1);
Shape105.Pen.Width:=3;
Shape106.Pen.Width:=3;
Shape107.Pen.Width:=3;
Shape108.Pen.Width:=3;
Shape109.Pen.Width:=3;
Shape110.Pen.Width:=3;
Shape111.Pen.Width:=3;
Shape112.Pen.Width:=3;
end;
end;

procedure TForm1.Label124MouseEnter(Sender: TObject);
begin
if Form1.Cursor=crArrow then
Begin
Form1.ShapeNevud(Form1);
Shape113.Pen.Width:=3;
Shape114.Pen.Width:=3;
Shape115.Pen.Width:=3;
Shape116.Pen.Width:=3;
Shape117.Pen.Width:=3;
Shape118.Pen.Width:=3;
Shape119.Pen.Width:=3;
Shape120.Pen.Width:=3;
end;
end;

procedure TForm1.Label11Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Begin
For a:=1 to Form3.StringGrid1.ColCount do
Begin
For i:=1 to Form3.StringGrid1.RowCount do
Form3.StringGrid1.Cells[a,i]:=Form3.StringGrid1.Cells[a+1,i];
end;
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
Form1.izmcvClick(Form1);
Form1.ShapeNevud(Form1);
Form1.Cursor:=crDefault;
Form1.Ydalenie(Form1);
end
else
Begin
i:=1;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label13Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label11Click(Form1)
else
Begin
i:=1;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label14Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label11Click(Form1)
else
Begin
i:=1;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Edit5KeyPress(Sender: TObject; var Key: Char);
begin
case Key of
'0'..'9': ; // �����
#13:
Begin
Form1.izmcvClick(Form1);
if Form3.Label1.Caption<>'2' then
Form3.N1.Click;
end  // ������� <Enter>
else
Key :=Chr(0); // ������ �� ����������
end;
end;

procedure TForm1.SpinEdit1KeyPress(Sender: TObject; var Key: Char);
begin
Key :=Chr(0); // ������ �� ����������
end;

procedure TForm1.Label15Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label11Click(Form1)
else
Begin
i:=1;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label16Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label11Click(Form1)
else
Begin
i:=1;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label17Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label11Click(Form1)
else
Begin
i:=1;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label18Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label11Click(Form1)
else
Begin
i:=1;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label19Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label11Click(Form1)
else
Begin
i:=1;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label20Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Begin
For a:=2 to Form3.StringGrid1.ColCount do
Begin
For i:=1 to Form3.StringGrid1.RowCount do
Form3.StringGrid1.Cells[a,i]:=Form3.StringGrid1.Cells[a+1,i];
end;
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
Form1.izmcvClick(Form1);
Form1.ShapeNevud(Form1);
Form1.Cursor:=crDefault;
Form1.Ydalenie(Form1);
end
else
Begin
i:=2;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label28Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Begin
For a:=3 to Form3.StringGrid1.ColCount do
Begin
For i:=1 to Form3.StringGrid1.RowCount do
Form3.StringGrid1.Cells[a,i]:=Form3.StringGrid1.Cells[a+1,i];
end;
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
Form1.izmcvClick(Form1);
Form1.ShapeNevud(Form1);
Form1.Cursor:=crDefault;
Form1.Ydalenie(Form1);
end
else
Begin
i:=3;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label36Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Begin
For a:=4 to Form3.StringGrid1.ColCount do
Begin
For i:=1 to Form3.StringGrid1.RowCount do
Form3.StringGrid1.Cells[a,i]:=Form3.StringGrid1.Cells[a+1,i];
end;
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
Form1.izmcvClick(Form1);
Form1.ShapeNevud(Form1);
Form1.Cursor:=crDefault;
Form1.Ydalenie(Form1);
end
else
Begin
i:=4;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label44Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Begin
For a:=5 to Form3.StringGrid1.ColCount do
Begin
For i:=1 to Form3.StringGrid1.RowCount do
Form3.StringGrid1.Cells[a,i]:=Form3.StringGrid1.Cells[a+1,i];
end;
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
Form1.izmcvClick(Form1);
Form1.ShapeNevud(Form1);
Form1.Cursor:=crDefault;
Form1.Ydalenie(Form1);
end
else
Begin
i:=5;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label68Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Begin
For a:=8 to Form3.StringGrid1.ColCount do
Begin
For i:=1 to Form3.StringGrid1.RowCount do
Form3.StringGrid1.Cells[a,i]:=Form3.StringGrid1.Cells[a+1,i];
end;
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
Form1.izmcvClick(Form1);
Form1.ShapeNevud(Form1);
Form1.Cursor:=crDefault;
Form1.Ydalenie(Form1);
end
else
Begin
i:=8;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label60Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Begin
For a:=7 to Form3.StringGrid1.ColCount do
Begin
For i:=1 to Form3.StringGrid1.RowCount do
Form3.StringGrid1.Cells[a,i]:=Form3.StringGrid1.Cells[a+1,i];
end;
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
Form1.izmcvClick(Form1);
Form1.ShapeNevud(Form1);
Form1.Cursor:=crDefault;
Form1.Ydalenie(Form1);
end
else
Begin
i:=7;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label52Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Begin
For a:=6 to Form3.StringGrid1.ColCount do
Begin
For i:=1 to Form3.StringGrid1.RowCount do
Form3.StringGrid1.Cells[a,i]:=Form3.StringGrid1.Cells[a+1,i];
end;
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
Form1.izmcvClick(Form1);
Form1.ShapeNevud(Form1);
Form1.Cursor:=crDefault;
Form1.Ydalenie(Form1);
end
else
Begin
i:=6;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label76Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Begin
For a:=9 to Form3.StringGrid1.ColCount do
Begin
For i:=1 to Form3.StringGrid1.RowCount do
Form3.StringGrid1.Cells[a,i]:=Form3.StringGrid1.Cells[a+1,i];
end;
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
Form1.izmcvClick(Form1);
Form1.ShapeNevud(Form1);
Form1.Cursor:=crDefault;
Form1.Ydalenie(Form1);
end
else
Begin
i:=9;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;end;

procedure TForm1.Label84Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label91Click(Form1)
else
Begin
i:=10;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label92Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Begin
For a:=11 to Form3.StringGrid1.ColCount do
Begin
For i:=1 to Form3.StringGrid1.RowCount do
Form3.StringGrid1.Cells[a,i]:=Form3.StringGrid1.Cells[a+1,i];
end;
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
Form1.izmcvClick(Form1);
Form1.ShapeNevud(Form1);
Form1.Cursor:=crDefault;
Form1.Ydalenie(Form1);
end
else
Begin
i:=11;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label100Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label96Click(Form1)
else
Begin
i:=15;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label108Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label95Click(Form1)
else
Begin
i:=14;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label116Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label94Click(Form1)
else
Begin
i:=13;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label124Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label93Click(Form1)
else
Begin
i:=12;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label132Click(Sender: TObject);
Var i:integer;
begin
i:=16;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label140Click(Sender: TObject);
Var i:integer;
begin
i:=17;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label156Click(Sender: TObject);
Var i:integer;
begin
i:=19;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label172Click(Sender: TObject);
Var i:integer;
begin
i:=21;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label188Click(Sender: TObject);
Var i:integer;
begin
i:=23;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label204Click(Sender: TObject);
Var i:integer;
begin
i:=25;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label220Click(Sender: TObject);
Var i:integer;
begin
i:=27;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label236Click(Sender: TObject);
Var i:integer;
begin
i:=29;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label252Click(Sender: TObject);
Var i:integer;
begin
i:=31;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label148Click(Sender: TObject);
Var i:integer;
begin
i:=18;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label164Click(Sender: TObject);
Var i:integer;
begin
i:=20;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label180Click(Sender: TObject);
Var i:integer;
begin
i:=22;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label196Click(Sender: TObject);
Var i:integer;
begin
i:=24;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label212Click(Sender: TObject);
Var i:integer;
begin
i:=26;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label228Click(Sender: TObject);
Var i:integer;
begin
i:=28;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label244Click(Sender: TObject);
Var i:integer;
begin
i:=30;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label260Click(Sender: TObject);
Var i:integer;
begin
i:=32;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label268Click(Sender: TObject);
Var i:integer;
begin
i:=33;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label276Click(Sender: TObject);
Var i:integer;
begin
i:=34;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label284Click(Sender: TObject);
Var i:integer;
begin
i:=35;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label292Click(Sender: TObject);
Var i:integer;
begin
i:=36;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label300Click(Sender: TObject);
Var i:integer;
begin
i:=37;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label308Click(Sender: TObject);
Var i:integer;
begin
i:=38;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label316Click(Sender: TObject);
Var i:integer;
begin
i:=39;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label324Click(Sender: TObject);
Var i:integer;
begin
i:=40;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label332Click(Sender: TObject);
Var i:integer;
begin
i:=41;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label340Click(Sender: TObject);
Var i:integer;
begin
i:=42;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label348Click(Sender: TObject);
Var i:integer;
begin
i:=43;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label356Click(Sender: TObject);
Var i:integer;
begin
i:=44;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label364Click(Sender: TObject);
Var i:integer;
begin
i:=45;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label372Click(Sender: TObject);
Var i:integer;
begin
i:=46;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label380Click(Sender: TObject);
Var i:integer;
begin
i:=47;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label388Click(Sender: TObject);
Var i:integer;
begin
i:=48;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label396Click(Sender: TObject);
Var i:integer;
begin
i:=49;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label404Click(Sender: TObject);
Var i:integer;
begin
i:=50;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label21Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label20Click(Form1)
else
Begin
i:=2;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label29Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label28Click(Form1)
else
Begin
i:=3;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label37Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label36Click(Form1)
else
Begin
i:=4;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label53Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label52Click(Form1)
else
Begin
i:=6;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label61Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label60Click(Form1)
else
Begin
i:=7;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label69Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label68Click(Form1)
else
Begin
i:=8;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label77Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label76Click(Form1)
else
Begin
i:=9;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label85Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label91Click(Form1)
else
Begin
i:=10;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label133Click(Sender: TObject);
var i:integer;
begin
i:=16;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label125Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label94Click(Form1)
else
Begin
i:=13;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label117Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label94Click(Form1)
else
Begin
i:=13;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label109Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label95Click(Form1)
else
Begin
i:=14;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label101Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label96Click(Form1)
else
Begin
i:=15;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label93Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Begin
For a:=12 to Form3.StringGrid1.ColCount do
Begin
For i:=1 to Form3.StringGrid1.RowCount do
Form3.StringGrid1.Cells[a,i]:=Form3.StringGrid1.Cells[a+1,i];
end;
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
Form1.izmcvClick(Form1);
Form1.ShapeNevud(Form1);
Form1.Cursor:=crDefault;
Form1.Ydalenie(Form1);
end
else
Begin
i:=12;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label141Click(Sender: TObject);
var i:integer;
begin
i:=17;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label149Click(Sender: TObject);
var i:integer;
begin
i:=18;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label157Click(Sender: TObject);
var i:integer;
begin
i:=19;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label165Click(Sender: TObject);
var i:integer;
begin
i:=20;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label173Click(Sender: TObject);
var i:integer;
begin
i:=21;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label181Click(Sender: TObject);
var i:integer;
begin
i:=22;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label189Click(Sender: TObject);
var i:integer;
begin
i:=23;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label197Click(Sender: TObject);
var i:integer;
begin
i:=24;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label205Click(Sender: TObject);
var i:integer;
begin
i:=25;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label213Click(Sender: TObject);
var i:integer;
begin
i:=26;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label221Click(Sender: TObject);
var i:integer;
begin
i:=27;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label229Click(Sender: TObject);
var i:integer;
begin
i:=28;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label237Click(Sender: TObject);
var i:integer;
begin
i:=29;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label245Click(Sender: TObject);
var i:integer;
begin
i:=30;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label253Click(Sender: TObject);
var i:integer;
begin
i:=31;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label261Click(Sender: TObject);
var i:integer;
begin
i:=32;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label269Click(Sender: TObject);
var i:integer;
begin
i:=33;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label277Click(Sender: TObject);
var i:integer;
begin
i:=34;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label285Click(Sender: TObject);
var i:integer;
begin
i:=35;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label293Click(Sender: TObject);
var i:integer;
begin
i:=36;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label301Click(Sender: TObject);
var i:integer;
begin
i:=37;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label309Click(Sender: TObject);
var i:integer;
begin
i:=38;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label317Click(Sender: TObject);
var i:integer;
begin
i:=39;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label325Click(Sender: TObject);
var i:integer;
begin
i:=40;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label333Click(Sender: TObject);
var i:integer;
begin
i:=41;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label341Click(Sender: TObject);
var i:integer;
begin
i:=42;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label349Click(Sender: TObject);
var i:integer;
begin
i:=43;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label357Click(Sender: TObject);
var i:integer;
begin
i:=44;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label365Click(Sender: TObject);
var i:integer;
begin
i:=45;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label373Click(Sender: TObject);
var i:integer;
begin
i:=46;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label381Click(Sender: TObject);
var i:integer;
begin
i:=47;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label389Click(Sender: TObject);
var i:integer;
begin
i:=48;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label397Click(Sender: TObject);
var i:integer;
begin
i:=49;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label405Click(Sender: TObject);
var i:integer;
begin
i:=50;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label22Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label20Click(Form1)
else
Begin
i:=2;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label30Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label28Click(Form1)
else
Begin
i:=3;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label38Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label36Click(Form1)
else
Begin
i:=4;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label46Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label44Click(Form1)
else
Begin
i:=5;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label54Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label52Click(Form1)
else
Begin
i:=6;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label62Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label60Click(Form1)
else
Begin
i:=7;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label70Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label68Click(Form1)
else
Begin
i:=8;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label78Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label76Click(Form1)
else
Begin
i:=9;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label86Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label91Click(Form1)
else
Begin
i:=10;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label94Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Begin
For a:=13 to Form3.StringGrid1.ColCount do
Begin
For i:=1 to Form3.StringGrid1.RowCount do
Form3.StringGrid1.Cells[a,i]:=Form3.StringGrid1.Cells[a+1,i];
end;
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
Form1.izmcvClick(Form1);
Form1.ShapeNevud(Form1);
Form1.Cursor:=crDefault;
Form1.Ydalenie(Form1);
end
else
Begin
i:=13;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label102Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label96Click(Form1)
else
Begin
i:=15;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label110Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label95Click(Form1)
else
Begin
i:=14;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label118Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label93Click(Form1)
else
Begin
i:=12;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label126Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label94Click(Form1)
else
Begin
i:=13;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label134Click(Sender: TObject);
var i:integer;
begin
i:=16;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label142Click(Sender: TObject);
var i:integer;
begin
i:=17;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label150Click(Sender: TObject);
var i:integer;
begin
i:=18;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label158Click(Sender: TObject);
var i:integer;
begin
i:=19;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label166Click(Sender: TObject);
var i:integer;
begin
i:=20;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label174Click(Sender: TObject);
var i:integer;
begin
i:=21;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label182Click(Sender: TObject);
var i:integer;
begin
i:=22;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label190Click(Sender: TObject);
var i:integer;
begin
i:=23;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label198Click(Sender: TObject);
var i:integer;
begin
i:=24;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label206Click(Sender: TObject);
var i:integer;
begin
i:=25;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label214Click(Sender: TObject);
var i:integer;
begin
i:=26;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label222Click(Sender: TObject);
var i:integer;
begin
i:=27;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label230Click(Sender: TObject);
var i:integer;
begin
i:=28;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label238Click(Sender: TObject);
var i:integer;
begin
i:=29;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label246Click(Sender: TObject);
var i:integer;
begin
i:=30;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label254Click(Sender: TObject);
var i:integer;
begin
i:=31;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label262Click(Sender: TObject);
var i:integer;
begin
i:=32;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label270Click(Sender: TObject);
var i:integer;
begin
i:=33;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label278Click(Sender: TObject);
var i:integer;
begin
i:=34;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label286Click(Sender: TObject);
var i:integer;
begin
i:=35;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label294Click(Sender: TObject);
var i:integer;
begin
i:=36;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label302Click(Sender: TObject);
var i:integer;
begin
i:=37;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label310Click(Sender: TObject);
var i:integer;
begin
i:=38;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label318Click(Sender: TObject);
var i:integer;
begin
i:=39;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label326Click(Sender: TObject);
var i:integer;
begin
i:=40;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label334Click(Sender: TObject);
var i:integer;
begin
i:=41;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label342Click(Sender: TObject);
var i:integer;
begin
i:=42;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label350Click(Sender: TObject);
var i:integer;
begin
i:=43;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label358Click(Sender: TObject);
var i:integer;
begin
i:=44;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label366Click(Sender: TObject);
var i:integer;
begin
i:=45;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label374Click(Sender: TObject);
var i:integer;
begin
i:=46;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label382Click(Sender: TObject);
var i:integer;
begin
i:=47;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label390Click(Sender: TObject);
var i:integer;
begin
i:=48;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label398Click(Sender: TObject);
var i:integer;
begin
i:=49;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label406Click(Sender: TObject);
var i:integer;
begin
i:=50;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label23Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label20Click(Form1)
else
Begin
i:=2;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label31Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label28Click(Form1)
else
Begin
i:=3;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label39Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label28Click(Form1)
else
Begin
i:=4;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label47Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label44Click(Form1)
else
Begin
i:=5;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label55Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label52Click(Form1)
else
Begin
i:=6;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label63Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label60Click(Form1)
else
Begin
i:=7;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label71Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label68Click(Form1)
else
Begin
i:=8;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label79Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label76Click(Form1)
else
Begin
i:=9;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label87Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label91Click(Form1)
else
Begin
i:=10;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label95Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Begin
For a:=14 to Form3.StringGrid1.ColCount do
Begin
For i:=1 to Form3.StringGrid1.RowCount do
Form3.StringGrid1.Cells[a,i]:=Form3.StringGrid1.Cells[a+1,i];
end;
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
Form1.izmcvClick(Form1);
Form1.ShapeNevud(Form1);
Form1.Cursor:=crDefault;
Form1.Ydalenie(Form1);
end
else
Begin
i:=14;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label103Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label96Click(Form1)
else
Begin
i:=15;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label111Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label94Click(Form1)
else
Begin
i:=13;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label119Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label92Click(Form1)
else
Begin
i:=11;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label127Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label93Click(Form1)
else
Begin
i:=12;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label135Click(Sender: TObject);
var i:integer;
begin
i:=16;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label143Click(Sender: TObject);
var i:integer;
begin
i:=17;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label151Click(Sender: TObject);
var i:integer;
begin
i:=18;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label159Click(Sender: TObject);
var i:integer;
begin
i:=19;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label167Click(Sender: TObject);
var i:integer;
begin
i:=20;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label175Click(Sender: TObject);
var i:integer;
begin
i:=21;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label183Click(Sender: TObject);
var i:integer;
begin
i:=22;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label191Click(Sender: TObject);
var i:integer;
begin
i:=23;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label199Click(Sender: TObject);
var i:integer;
begin
i:=24;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label207Click(Sender: TObject);
var i:integer;
begin
i:=25;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label215Click(Sender: TObject);
var i:integer;
begin
i:=26;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label223Click(Sender: TObject);
var i:integer;
begin
i:=27;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label231Click(Sender: TObject);
var i:integer;
begin
i:=28;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label239Click(Sender: TObject);
var i:integer;
begin
i:=29;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label247Click(Sender: TObject);
var i:integer;
begin
i:=30;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label255Click(Sender: TObject);
var i:integer;
begin
i:=31;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label263Click(Sender: TObject);
var i:integer;
begin
i:=32;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label271Click(Sender: TObject);
var i:integer;
begin
i:=33;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label279Click(Sender: TObject);
var i:integer;
begin
i:=34;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label287Click(Sender: TObject);
var i:integer;
begin
i:=35;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label295Click(Sender: TObject);
var i:integer;
begin
i:=36;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label303Click(Sender: TObject);
var i:integer;
begin
i:=37;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label311Click(Sender: TObject);
var i:integer;
begin
i:=38;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label319Click(Sender: TObject);
var i:integer;
begin
i:=39;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label327Click(Sender: TObject);
var i:integer;
begin
i:=40;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label335Click(Sender: TObject);
var i:integer;
begin
i:=41;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label343Click(Sender: TObject);
var i:integer;
begin
i:=42;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label351Click(Sender: TObject);
var i:integer;
begin
i:=43;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label359Click(Sender: TObject);
var i:integer;
begin
i:=44;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label367Click(Sender: TObject);
var i:integer;
begin
i:=45;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label375Click(Sender: TObject);
var i:integer;
begin
i:=46;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label383Click(Sender: TObject);
var i:integer;
begin
i:=47;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label391Click(Sender: TObject);
var i:integer;
begin
i:=48;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label399Click(Sender: TObject);
var i:integer;
begin
i:=49;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label407Click(Sender: TObject);
var i:integer;
begin
i:=50;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label24Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label20Click(Form1)
else
Begin
i:=2;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label32Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label28Click(Form1)
else
Begin
i:=3;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label40Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label36Click(Form1)
else
Begin
i:=4;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label48Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label44Click(Form1)
else
Begin
i:=5;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label56Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label52Click(Form1)
else
Begin
i:=6;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label64Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label60Click(Form1)
else
Begin
i:=7;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label72Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label68Click(Form1)
else
Begin
i:=8;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label80Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label76Click(Form1)
else
Begin
i:=9;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label88Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label91Click(Form1)
else
Begin
i:=10;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label96Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Begin
For a:=15 to Form3.StringGrid1.ColCount do
Begin
For i:=1 to Form3.StringGrid1.RowCount do
Form3.StringGrid1.Cells[a,i]:=Form3.StringGrid1.Cells[a+1,i];
end;
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
Form1.izmcvClick(Form1);
Form1.ShapeNevud(Form1);
Form1.Cursor:=crDefault;
Form1.Ydalenie(Form1);
end
else
Begin
i:=15;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label104Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label95Click(Form1)
else
Begin
i:=14;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label112Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label93Click(Form1)
else
Begin
i:=12;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label120Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label92Click(Form1)
else
Begin
i:=11;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label128Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label92Click(Form1)
else
Begin
i:=11;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label136Click(Sender: TObject);
var i:integer;
begin
i:=16;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label144Click(Sender: TObject);
var i:integer;
begin
i:=17;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label152Click(Sender: TObject);
var i:integer;
begin
i:=18;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label160Click(Sender: TObject);
var i:integer;
begin
i:=19;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label168Click(Sender: TObject);
var i:integer;
begin
i:=20;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label176Click(Sender: TObject);
var i:integer;
begin
i:=21;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label184Click(Sender: TObject);
var i:integer;
begin
i:=22;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label192Click(Sender: TObject);
var i:integer;
begin
i:=23;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label200Click(Sender: TObject);
var i:integer;
begin
i:=24;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label208Click(Sender: TObject);
var i:integer;
begin
i:=25;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label216Click(Sender: TObject);
var i:integer;
begin
i:=26;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label224Click(Sender: TObject);
var i:integer;
begin
i:=27;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label232Click(Sender: TObject);
var i:integer;
begin
i:=28;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label240Click(Sender: TObject);
var i:integer;
begin
i:=29;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label248Click(Sender: TObject);
var i:integer;
begin
i:=30;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label256Click(Sender: TObject);
var i:integer;
begin
i:=31;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label264Click(Sender: TObject);
var i:integer;
begin
i:=32;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label272Click(Sender: TObject);
var i:integer;
begin
i:=33;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label280Click(Sender: TObject);
var i:integer;
begin
i:=34;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label288Click(Sender: TObject);
var i:integer;
begin
i:=35;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label296Click(Sender: TObject);
var i:integer;
begin
i:=36;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label304Click(Sender: TObject);
var i:integer;
begin
i:=37;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label312Click(Sender: TObject);
var i:integer;
begin
i:=38;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label320Click(Sender: TObject);
var i:integer;
begin
i:=39;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label328Click(Sender: TObject);
var i:integer;
begin
i:=40;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label336Click(Sender: TObject);
var i:integer;
begin
i:=41;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label344Click(Sender: TObject);
var i:integer;
begin
i:=42;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label352Click(Sender: TObject);
var i:integer;
begin
i:=43;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label360Click(Sender: TObject);
var i:integer;
begin
i:=44;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label368Click(Sender: TObject);
var i:integer;
begin
i:=45;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label376Click(Sender: TObject);
var i:integer;
begin
i:=46;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label384Click(Sender: TObject);
var i:integer;
begin
i:=47;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label392Click(Sender: TObject);
var i:integer;
begin
i:=48;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label400Click(Sender: TObject);
var i:integer;
begin
i:=49;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label408Click(Sender: TObject);
var i:integer;
begin
i:=50;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label25Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label20Click(Form1)
else
Begin
i:=2;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label33Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label28Click(Form1)
else
Begin
i:=3;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label41Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label36Click(Form1)
else
Begin
i:=4;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label49Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label44Click(Form1)
else
Begin
i:=5;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label57Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label52Click(Form1)
else
Begin
i:=6;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label65Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label60Click(Form1)
else
Begin
i:=7;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label73Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label68Click(Form1)
else
Begin
i:=8;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label81Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label76Click(Form1)
else
Begin
i:=9;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label89Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label91Click(Form1)
else
Begin
i:=10;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label97Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label96Click(Form1)
else
Begin
i:=15;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label105Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label95Click(Form1)
else
Begin
i:=14;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label113Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label92Click(Form1)
else
Begin
i:=11;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label121Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label93Click(Form1)
else
Begin
i:=12;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label129Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label92Click(Form1)
else
Begin
i:=11;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label137Click(Sender: TObject);
var i:integer;
begin
i:=16;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label145Click(Sender: TObject);
var i:integer;
begin
i:=17;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label153Click(Sender: TObject);
var i:integer;
begin
i:=18;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label161Click(Sender: TObject);
var i:integer;
begin
i:=19;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label169Click(Sender: TObject);
var i:integer;
begin
i:=20;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label177Click(Sender: TObject);
var i:integer;
begin
i:=21;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label185Click(Sender: TObject);
var i:integer;
begin
i:=22;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label193Click(Sender: TObject);
var i:integer;
begin
i:=23;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label201Click(Sender: TObject);
var i:integer;
begin
i:=24;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label209Click(Sender: TObject);
var i:integer;
begin
i:=25;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label217Click(Sender: TObject);
var i:integer;
begin
i:=26;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label225Click(Sender: TObject);
var i:integer;
begin
i:=27;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label233Click(Sender: TObject);
var i:integer;
begin
i:=28;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label241Click(Sender: TObject);
var i:integer;
begin
i:=29;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label249Click(Sender: TObject);
var i:integer;
begin
i:=30;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label257Click(Sender: TObject);
var i:integer;
begin
i:=31;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label265Click(Sender: TObject);
var i:integer;
begin
i:=32;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label273Click(Sender: TObject);
var i:integer;
begin
i:=33;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label281Click(Sender: TObject);
var i:integer;
begin
i:=34;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label289Click(Sender: TObject);
var i:integer;
begin
i:=35;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label297Click(Sender: TObject);
var i:integer;
begin
i:=36;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label305Click(Sender: TObject);
var i:integer;
begin
i:=37;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label313Click(Sender: TObject);
var i:integer;
begin
i:=38;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label321Click(Sender: TObject);
var i:integer;
begin
i:=39;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label329Click(Sender: TObject);
var i:integer;
begin
i:=40;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label337Click(Sender: TObject);
var i:integer;
begin
i:=41;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label345Click(Sender: TObject);
var i:integer;
begin
i:=42;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label353Click(Sender: TObject);
var i:integer;
begin
i:=43;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label361Click(Sender: TObject);
var i:integer;
begin
i:=44;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label369Click(Sender: TObject);
var i:integer;
begin
i:=45;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label377Click(Sender: TObject);
var i:integer;
begin
i:=46;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label385Click(Sender: TObject);
var i:integer;
begin
i:=47;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label393Click(Sender: TObject);
var i:integer;
begin
i:=48;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label401Click(Sender: TObject);
var i:integer;
begin
i:=49;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label409Click(Sender: TObject);
var i:integer;
begin
i:=50;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label26Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label20Click(Form1)
else
Begin
i:=2;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label34Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label28Click(Form1)
else
Begin
i:=3;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label42Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label36Click(Form1)
else
Begin
i:=4;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label50Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label44Click(Form1)
else
Begin
i:=5;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label58Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label52Click(Form1)
else
Begin
i:=6;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label66Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label60Click(Form1)
else
Begin
i:=7;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label74Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label68Click(Form1)
else
Begin
i:=8;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label82Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label76Click(Form1)
else
Begin
i:=9;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label90Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label91Click(Form1)
else
Begin
i:=10;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label98Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label96Click(Form1)
else
Begin
i:=15;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label106Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label95Click(Form1)
else
Begin
i:=14;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label114Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label92Click(Form1)
else
Begin
i:=11;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label122Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label94Click(Form1)
else
Begin
i:=13;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label130Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label93Click(Form1)
else
Begin
i:=12;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label138Click(Sender: TObject);
var i:integer;
begin
i:=16;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label146Click(Sender: TObject);
var i:integer;
begin
i:=17;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label154Click(Sender: TObject);
var i:integer;
begin
i:=18;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label162Click(Sender: TObject);
var i:integer;
begin
i:=19;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label170Click(Sender: TObject);
var i:integer;
begin
i:=20;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label178Click(Sender: TObject);
var i:integer;
begin
i:=21;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label186Click(Sender: TObject);
var i:integer;
begin
i:=22;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label194Click(Sender: TObject);
var i:integer;
begin
i:=23;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label202Click(Sender: TObject);
var i:integer;
begin
i:=24;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label210Click(Sender: TObject);
var i:integer;
begin
i:=25;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label218Click(Sender: TObject);
var i:integer;
begin
i:=26;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label226Click(Sender: TObject);
var i:integer;
begin
i:=27;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label234Click(Sender: TObject);
var i:integer;
begin
i:=28;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label242Click(Sender: TObject);
var i:integer;
begin
i:=29;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label250Click(Sender: TObject);
var i:integer;
begin
i:=30;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label258Click(Sender: TObject);
var i:integer;
begin
i:=31;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label266Click(Sender: TObject);
var i:integer;
begin
i:=32;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label274Click(Sender: TObject);
var i:integer;
begin
i:=33;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label282Click(Sender: TObject);
var i:integer;
begin
i:=34;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label290Click(Sender: TObject);
var i:integer;
begin
i:=35;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label298Click(Sender: TObject);
var i:integer;
begin
i:=36;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label306Click(Sender: TObject);
var i:integer;
begin
i:=37;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label314Click(Sender: TObject);
var i:integer;
begin
i:=38;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label322Click(Sender: TObject);
var i:integer;
begin
i:=39;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label330Click(Sender: TObject);
var i:integer;
begin
i:=40;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label338Click(Sender: TObject);
var i:integer;
begin
i:=41;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label346Click(Sender: TObject);
var i:integer;
begin
i:=42;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label354Click(Sender: TObject);
var i:integer;
begin
i:=43;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label362Click(Sender: TObject);
var i:integer;
begin
i:=44;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label370Click(Sender: TObject);
var i:integer;
begin
i:=45;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label378Click(Sender: TObject);
var i:integer;
begin
i:=46;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label386Click(Sender: TObject);
var i:integer;
begin
i:=47;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label394Click(Sender: TObject);
var i:integer;
begin
i:=48;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label402Click(Sender: TObject);
var i:integer;
begin
i:=49;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label410Click(Sender: TObject);
var i:integer;
begin
i:=50;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,6)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1);
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label27Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label20Click(Form1)
else
Begin
i:=2;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label35Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label28Click(Form1)
else
Begin
i:=3;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label43Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label36Click(Form1)
else
Begin
i:=4;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label51Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label44Click(Form1)
else
Begin
i:=5;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label59Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label52Click(Form1)
else
Begin
i:=6;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label67Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label60Click(Form1)
else
Begin
i:=7;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label75Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label68Click(Form1)
else
Begin
i:=8;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label83Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label76Click(Form1)
else
Begin
i:=9;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label91Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Begin
For a:=10 to Form3.StringGrid1.ColCount do
Begin
For i:=1 to Form3.StringGrid1.RowCount do
Form3.StringGrid1.Cells[a,i]:=Form3.StringGrid1.Cells[a+1,i];
end;
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
Form1.izmcvClick(Form1);
Form1.ShapeNevud(Form1);
Form1.Cursor:=crDefault;
Form1.Ydalenie(Form1);
end
else
Begin
i:=10;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label99Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label96Click(Form1)
else
Begin
i:=15;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,3)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,4);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label107Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label95Click(Form1)
else
Begin
i:=14;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],5,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,4)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,3);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label115Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label93Click(Form1)
else
Begin
i:=12;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,2)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],4,5);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label123Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label92Click(Form1)
else
Begin
i:=11;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],6,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,5)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],7,2);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label131Click(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label94Click(Form1)
else
Begin
i:=13;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Label139Click(Sender: TObject);
var i:integer;
begin
i:=16;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label147Click(Sender: TObject);
var i:integer;
begin
i:=17;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label155Click(Sender: TObject);
var i:integer;
begin
i:=18;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label163Click(Sender: TObject);
var i:integer;
begin
i:=19;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label171Click(Sender: TObject);
var i:integer;
begin
i:=20;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label179Click(Sender: TObject);
var i:integer;
begin
i:=21;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label187Click(Sender: TObject);
var i:integer;
begin
i:=22;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label195Click(Sender: TObject);
var i:integer;
begin
i:=23;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label203Click(Sender: TObject);
var i:integer;
begin
i:=24;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label211Click(Sender: TObject);
var i:integer;
begin
i:=25;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label219Click(Sender: TObject);
var i:integer;
begin
i:=26;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label227Click(Sender: TObject);
var i:integer;
begin
i:=27;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label235Click(Sender: TObject);
var i:integer;
begin
i:=28;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label243Click(Sender: TObject);
var i:integer;
begin
i:=29;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label251Click(Sender: TObject);
var i:integer;
begin
i:=30;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label259Click(Sender: TObject);
var i:integer;
begin
i:=31;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label267Click(Sender: TObject);
var i:integer;
begin
i:=32;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label275Click(Sender: TObject);
var i:integer;
begin
i:=33;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label283Click(Sender: TObject);
var i:integer;
begin
i:=34;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label291Click(Sender: TObject);
var i:integer;
begin
i:=35;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label307Click(Sender: TObject);
var i:integer;
begin
i:=37;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label323Click(Sender: TObject);
var i:integer;
begin
i:=39;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label339Click(Sender: TObject);
var i:integer;
begin
i:=41;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label355Click(Sender: TObject);
var i:integer;
begin
i:=43;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label371Click(Sender: TObject);
var i:integer;
begin
i:=45;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label387Click(Sender: TObject);
var i:integer;
begin
i:=47;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label403Click(Sender: TObject);
var i:integer;
begin
i:=49;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label299Click(Sender: TObject);
var i:integer;
begin
i:=36;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label315Click(Sender: TObject);
var i:integer;
begin
i:=38;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label331Click(Sender: TObject);
var i:integer;
begin
i:=40;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label347Click(Sender: TObject);
var i:integer;
begin
i:=42;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label363Click(Sender: TObject);
var i:integer;
begin
i:=44;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label379Click(Sender: TObject);
var i:integer;
begin
i:=46;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label395Click(Sender: TObject);
var i:integer;
begin
i:=48;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.Label411Click(Sender: TObject);
var i:integer;
begin
i:=50;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],8,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'0'
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,7)+'1';
Form1.izmcvClick(Form1);
end;

procedure TForm1.ScrollBox1Click(Sender: TObject);
begin
Form1.ShapeNevud(Form1);
Form1.Cursor:=crDefault;
end;

procedure TForm1.Label6Click(Sender: TObject);
begin
Form1.ShapeNevud(Form1);
Form1.Cursor:=crDefault;
end;

procedure TForm1.Ydalenie(Sender: TObject);
var b:integer;
begin
Form1.ShapeNevud(Form1);
Form1.Cursor:=crDefault;
GroupBox4.Height:=GroupBox4.Height-24;
Panel1.Height:=Panel1.Height-16;
Repeat
if Label10.Caption='0' then
Begin
b:=1;
Break
end;
if Label10.Caption='1' then
Begin
Shape1.Visible:=false;
Shape2.Visible:=false;
Shape3.Visible:=false;
Shape4.Visible:=false;
Shape5.Visible:=false;
Shape6.Visible:=false;
Shape7.Visible:=false;
Shape8.Visible:=false;
Shape121.Visible:=false;
Shape122.Visible:=false;
Shape123.Visible:=false;
Shape124.Visible:=false;
Shape125.Visible:=false;
Shape126.Visible:=false;
Shape127.Visible:=false;
Shape128.Visible:=false;
Label11.Visible:=false;
Label13.Visible:=false;
Label14.Visible:=false;
Label15.Visible:=false;
Label16.Visible:=false;
Label17.Visible:=false;
Label18.Visible:=false;
Label19.Visible:=false;
Label10.Caption:='0';
b:=1;
Break
end;
if Label10.Caption='2' then
Begin
Shape9.Visible:=false;
Shape10.Visible:=false;
Shape11.Visible:=false;
Shape12.Visible:=false;
Shape13.Visible:=false;
Shape14.Visible:=false;
Shape15.Visible:=false;
Shape16.Visible:=false;
Shape129.Visible:=false;
Shape130.Visible:=false;
Shape131.Visible:=false;
Shape132.Visible:=false;
Shape133.Visible:=false;
Shape134.Visible:=false;
Shape135.Visible:=false;
Shape136.Visible:=false;
Label20.Visible:=false;
Label21.Visible:=false;
Label22.Visible:=false;
Label23.Visible:=false;
Label24.Visible:=false;
Label25.Visible:=false;
Label26.Visible:=false;
Label27.Visible:=false;
Label10.Caption:='1';
b:=1;
Break
end;
if Label10.Caption='3' then
Begin
Shape17.Visible:=false;
Shape18.Visible:=false;
Shape19.Visible:=false;
Shape20.Visible:=false;
Shape21.Visible:=false;
Shape22.Visible:=false;
Shape23.Visible:=false;
Shape24.Visible:=false;
Shape137.Visible:=false;
Shape138.Visible:=false;
Shape139.Visible:=false;
Shape140.Visible:=false;
Shape141.Visible:=false;
Shape142.Visible:=false;
Shape143.Visible:=false;
Shape144.Visible:=false;
Label28.Visible:=false;
Label29.Visible:=false;
Label30.Visible:=false;
Label31.Visible:=false;
Label32.Visible:=false;
Label33.Visible:=false;
Label34.Visible:=false;
Label35.Visible:=false;
Label10.Caption:='2';
b:=1;
Break
end;
if Label10.Caption='4' then
Begin
Shape25.Visible:=false;
Shape26.Visible:=false;
Shape27.Visible:=false;
Shape28.Visible:=false;
Shape29.Visible:=false;
Shape30.Visible:=false;
Shape31.Visible:=false;
Shape32.Visible:=false;
Shape145.Visible:=false;
Shape146.Visible:=false;
Shape147.Visible:=false;
Shape148.Visible:=false;
Shape149.Visible:=false;
Shape150.Visible:=false;
Shape151.Visible:=false;
Shape152.Visible:=false;
Label36.Visible:=false;
Label37.Visible:=false;
Label38.Visible:=false;
Label49.Visible:=false;
Label40.Visible:=false;
Label41.Visible:=false;
Label42.Visible:=false;
Label43.Visible:=false;
Label10.Caption:='3';
b:=1;
Break
end;
if Label10.Caption='5' then
Begin
Shape33.Visible:=false;
Shape34.Visible:=false;
Shape35.Visible:=false;;
Shape36.Visible:=false;
Shape37.Visible:=false;
Shape38.Visible:=false;
Shape39.Visible:=false;
Shape40.Visible:=false;
Shape153.Visible:=false;
Shape154.Visible:=false;
Shape155.Visible:=false;;
Shape156.Visible:=false;
Shape157.Visible:=false;
Shape158.Visible:=false;
Shape159.Visible:=false;
Shape160.Visible:=false;
Label44.Visible:=false;
Label45.Visible:=false;
Label46.Visible:=false;
Label47.Visible:=false;
Label48.Visible:=false;
Label49.Visible:=false;
Label50.Visible:=false;
Label51.Visible:=false;
Label10.Caption:='4';
b:=1;
Break
end;
if Label10.Caption='6' then
Begin
Shape41.Visible:=false;
Shape42.Visible:=false;
Shape43.Visible:=false;
Shape44.Visible:=false;
Shape45.Visible:=false;
Shape46.Visible:=false;
Shape47.Visible:=false;
Shape48.Visible:=false;
Shape161.Visible:=false;
Shape162.Visible:=false;
Shape163.Visible:=false;
Shape164.Visible:=false;
Shape165.Visible:=false;
Shape166.Visible:=false;
Shape167.Visible:=false;
Shape168.Visible:=false;
Label52.Visible:=false;
Label53.Visible:=false;
Label54.Visible:=false;
Label55.Visible:=false;
Label56.Visible:=false;
Label57.Visible:=false;
Label58.Visible:=false;
Label59.Visible:=false;
Label10.Caption:='5';
b:=1;
Break
end;
if Label10.Caption='7' then
Begin
Shape49.Visible:=false;
Shape50.Visible:=false;
Shape51.Visible:=false;
Shape52.Visible:=false;
Shape53.Visible:=false;
Shape54.Visible:=false;
Shape55.Visible:=false;
Shape56.Visible:=false;
Shape169.Visible:=false;
Shape170.Visible:=false;
Shape171.Visible:=false;
Shape172.Visible:=false;
Shape173.Visible:=false;
Shape174.Visible:=false;
Shape175.Visible:=false;
Shape176.Visible:=false;
Label60.Visible:=false;
Label61.Visible:=false;
Label62.Visible:=false;
Label63.Visible:=false;
Label64.Visible:=false;
Label65.Visible:=false;
Label66.Visible:=false;
Label67.Visible:=false;
Label10.Caption:='6';
b:=1;
Break
end;
if Label10.Caption='8' then
Begin
Shape57.Visible:=false;
Shape58.Visible:=false;
Shape59.Visible:=false;
Shape60.Visible:=false;
Shape61.Visible:=false;
Shape62.Visible:=false;
Shape63.Visible:=false;
Shape64.Visible:=false;
Shape177.Visible:=false;
Shape178.Visible:=false;
Shape179.Visible:=false;
Shape180.Visible:=false;
Shape181.Visible:=false;
Shape182.Visible:=false;
Shape183.Visible:=false;
Shape184.Visible:=false;
Label68.Visible:=false;
Label69.Visible:=false;
Label70.Visible:=false;
Label71.Visible:=false;
Label72.Visible:=false;
Label73.Visible:=false;
Label74.Visible:=false;
Label75.Visible:=false;
Label10.Caption:='7';
b:=1;
Break
end;
if Label10.Caption='9' then
Begin
Shape65.Visible:=false;
Shape66.Visible:=false;
Shape67.Visible:=false;
Shape68.Visible:=false;
Shape69.Visible:=false;
Shape70.Visible:=false;
Shape71.Visible:=false;
Shape72.Visible:=false;
Shape185.Visible:=false;
Shape186.Visible:=false;
Shape187.Visible:=false;
Shape188.Visible:=false;
Shape189.Visible:=false;
Shape190.Visible:=false;
Shape191.Visible:=false;
Shape192.Visible:=false;
Label76.Visible:=false;
Label77.Visible:=false;
Label78.Visible:=false;
Label79.Visible:=false;
Label80.Visible:=false;
Label81.Visible:=false;
Label82.Visible:=false;
Label83.Visible:=false;
Label10.Caption:='8';
b:=1;
Break
end;
if Label10.Caption='10' then
Begin
Shape73.Visible:=false;
Shape74.Visible:=false;
Shape75.Visible:=false;
Shape76.Visible:=false;
Shape77.Visible:=false;
Shape78.Visible:=false;
Shape79.Visible:=false;
Shape80.Visible:=false;
Shape193.Visible:=false;
Shape194.Visible:=false;
Shape195.Visible:=false;
Shape196.Visible:=false;
Shape197.Visible:=false;
Shape198.Visible:=false;
Shape199.Visible:=false;
Shape200.Visible:=false;
Label84.Visible:=false;
Label85.Visible:=false;
Label86.Visible:=false;
Label87.Visible:=false;
Label88.Visible:=false;
Label89.Visible:=false;
Label90.Visible:=false;
Label91.Visible:=false;
Label10.Caption:='9';
b:=1;
Break
end;
if Label10.Caption='11' then
Begin
Shape81.Visible:=false;
Shape82.Visible:=false;
Shape83.Visible:=false;
Shape84.Visible:=false;
Shape85.Visible:=false;
Shape86.Visible:=false;
Shape87.Visible:=false;
Shape88.Visible:=false;
Shape201.Visible:=false;
Shape202.Visible:=false;
Shape203.Visible:=false;
Shape204.Visible:=false;
Shape205.Visible:=false;
Shape206.Visible:=false;
Shape207.Visible:=false;
Shape208.Visible:=false;
Label92.Visible:=false;
Label113.Visible:=false;
Label114.Visible:=false;
Label119.Visible:=false;
Label120.Visible:=false;
Label123.Visible:=false;
Label128.Visible:=false;
Label129.Visible:=false;
Label10.Caption:='10';
b:=1;
Break
end;
if Label10.Caption='12' then
Begin
Shape89.Visible:=false;
Shape90.Visible:=false;
Shape91.Visible:=false;
Shape92.Visible:=false;
Shape93.Visible:=false;
Shape94.Visible:=false;
Shape95.Visible:=false;
Shape96.Visible:=false;
Shape209.Visible:=false;
Shape210.Visible:=false;
Shape211.Visible:=false;
Shape212.Visible:=false;
Shape213.Visible:=false;
Shape214.Visible:=false;
Shape215.Visible:=false;
Shape216.Visible:=false;
Label93.Visible:=false;
Label112.Visible:=false;
Label115.Visible:=false;
Label118.Visible:=false;
Label121.Visible:=false;
Label124.Visible:=false;
Label127.Visible:=false;
Label130.Visible:=false;
Label10.Caption:='11';
b:=1;
Break
end;
if Label10.Caption='13' then
Begin
Shape97.Visible:=false;
Shape98.Visible:=false;
Shape99.Visible:=false;
Shape100.Visible:=false;
Shape101.Visible:=false;
Shape102.Visible:=false;
Shape103.Visible:=false;
Shape104.Visible:=false;
Shape217.Visible:=false;
Shape218.Visible:=false;
Shape219.Visible:=false;
Shape220.Visible:=false;
Shape221.Visible:=false;
Shape222.Visible:=false;
Shape223.Visible:=false;
Shape224.Visible:=false;
Label94.Visible:=false;
Label111.Visible:=false;
Label116.Visible:=false;
Label117.Visible:=false;
Label122.Visible:=false;
Label125.Visible:=false;
Label126.Visible:=false;
Label131.Visible:=false;
Label10.Caption:='12';
b:=1;
Break
end;
if Label10.Caption='14' then
Begin
Shape105.Visible:=false;
Shape106.Visible:=false;;
Shape107.Visible:=false;
Shape108.Visible:=false;
Shape109.Visible:=false;
Shape110.Visible:=false;
Shape111.Visible:=false;
Shape112.Visible:=false;
Shape225.Visible:=false;
Shape226.Visible:=false;;
Shape227.Visible:=false;
Shape228.Visible:=false;
Shape229.Visible:=false;
Shape230.Visible:=false;
Shape231.Visible:=false;
Shape232.Visible:=false;
Label95.Visible:=false;
Label110.Visible:=false;
Label109.Visible:=false;
Label108.Visible:=false;
Label107.Visible:=false;
Label106.Visible:=false;
Label105.Visible:=false;
Label104.Visible:=false;
Label10.Caption:='13';
b:=1;
Break
end;
if Label10.Caption='15' then
Begin
Shape113.Visible:=false;
Shape114.Visible:=false;
Shape115.Visible:=false;
Shape116.Visible:=false;
Shape117.Visible:=false;
Shape118.Visible:=false;
Shape119.Visible:=false;
Shape120.Visible:=false;
Shape233.Visible:=false;
Shape234.Visible:=false;
Shape235.Visible:=false;
Shape236.Visible:=false;
Shape237.Visible:=false;
Shape238.Visible:=false;
Shape239.Visible:=false;
Shape240.Visible:=false;
Label96.Visible:=false;
Label97.Visible:=false;
Label98.Visible:=false;
Label99.Visible:=false;
Label100.Visible:=false;
Label101.Visible:=false;
Label102.Visible:=false;
Label103.Visible:=false;
Label10.Caption:='14';
b:=1;
Break
end;
until b=1;
end;

procedure TForm1.izmcvgip(Sender: TObject);
begin
Form1.izmcvgip(Form1);
end;

procedure TForm1.Ydalvselampochki(Sender: TObject);
begin
Form1.izmcvClick(Form1);
end;

procedure TForm1.gsfgbhfghb(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Form1.Label44Click(Form1)
else
Begin
i:=5;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:=Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1)+'1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],3,6);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Timer2Timer(Sender: TObject);
begin
if StrToInt(Edit5.Text)>1999 then
Begin
Edit5.Text:='1999';
end;
end;

procedure TForm1.N16Click(Sender: TObject);
begin
Form15.Left:=Round(Form1.ClientWidth/2)-Round(Form15.Width/2)+Form1.Left;
Form15.Top:=Round(Form1.ClientHeight/2)-Round(Form15.Height/2)+Form1.Top;
AnimateWindow(Form15.Handle, 2000, aw_center);
Form15.Show;
end;

procedure TForm1.N17Click(Sender: TObject);
begin
Form16.Visible:=true;
end;

procedure TForm1.WMSysCommand(var Msg: TWMSysCommand);
var i:integer;
 begin
 For i:=1 to 2 do
 Begin
   if ((Msg.CmdType and $FFF0) = SC_MOVE) or
     ((Msg.CmdType and $FFF0) = SC_SIZE) then
   begin
   Form15.Left:=Round(ClientWidth/2)-Round(Form15.Width/2)+Left;
   Form15.Top:=Round(ClientHeight/2)-Round(Form15.Height/2)+Top;
   end;
   inherited;
   end;
 end;

procedure TForm1.Closehg(Sender: TObject);
begin
Form1.N5.Checked:=false;
Form1.GroupBox4.Visible:=false;
end;

procedure TForm1.Label132MouseEnter(Sender: TObject);
begin
Form1.Label132.Font.Color:=clRed;
end;

procedure TForm1.GroupBox4MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
Label132.Color:=clBlack;
end;

procedure TForm1.Label133MouseEnter(Sender: TObject);
begin
Label132.Color:=clBlack;
end;

procedure TForm1.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
Label132.Color:=clBlack;
end;

procedure TForm1.effectu(Sender: TObject);
var i1,i2:integer;
Begin
i1:=StrToInt(Edit5.Text)+1;
i2:=1;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape121.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape122.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape123.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape124.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape125.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape126.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape127.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape128.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=2;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape129.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape130.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape131.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape132.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape133.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape134.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape135.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape136.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=3;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape137.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape138.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape139.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape140.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape141.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape142.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape143.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape144.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=4;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape145.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape146.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape147.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape148.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape149.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape150.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape151.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape152.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=5;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape153.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape154.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape155.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape156.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape157.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape158.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape159.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape160.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=6;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape161.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape162.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape163.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape164.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape165.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape166.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape167.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape168.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=7;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape169.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape170.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape171.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1'then
Shape172.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape173.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape174.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape175.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape176.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=8;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape177.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape178.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape179.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape180.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape181.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape182.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape183.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape184.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=9;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape185.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape186.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape187.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape188.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape189.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape190.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape191.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape192.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=10;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape193.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape194.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape195.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape196.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape197.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape198.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape199.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape200.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=11;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape201.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape202.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape203.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape204.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape205.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape206.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape207.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape208.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=12;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape209.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape210.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape211.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape212.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape213.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape214.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape215.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape216.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=13;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape217.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape218.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape219.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape220.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape221.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape222.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape223.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape224.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=14;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape225.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape226.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape227.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape228.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape229.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape230.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape231.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape232.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=15;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='1' then
Shape233.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='1' then
Shape234.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='1' then
Shape235.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='1' then
Shape236.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='1' then
Shape237.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='1' then
Shape238.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='1' then
Shape239.Brush.Color:=Form14.Shape3.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='1' then
Shape240.Brush.Color:=Form14.Shape3.Brush.Color;
i2:=1;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape121.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape122.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape123.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape124.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape125.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape126.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape127.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape128.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=2;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape129.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape130.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape131.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape132.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape133.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape134.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape135.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape136.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=3;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape137.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape138.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape139.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape140.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape141.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape142.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape143.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape144.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=4;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape145.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape146.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape147.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape148.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape149.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape150.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape151.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape152.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=5;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape153.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape154.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape155.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape156.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape157.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape158.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape159.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape160.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=6;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape161.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape162.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape163.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape164.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape165.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape166.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape167.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape168.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=7;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape169.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape170.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape171.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape172.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape173.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape174.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape175.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape176.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=8;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape177.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape178.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape179.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape180.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape181.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape182.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape183.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape184.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=9;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape185.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape186.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape187.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape188.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape189.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape190.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape191.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape192.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=10;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape193.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape194.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape195.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape196.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape197.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape198.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape199.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape200.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=11;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape201.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape202.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape203.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape204.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape205.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape206.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape207.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape208.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=12;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape209.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape210.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape211.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape212.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape213.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape214.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape215.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape216.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=13;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape217.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape218.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape219.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape220.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape221.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape222.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape223.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape224.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=14;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape225.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape226.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape227.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape228.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape229.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape230.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape231.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape232.Brush.Color:=Form14.Shape2.Brush.Color;
i2:=15;
if Copy((Form3.StringGrid1.Cells[i2,i1]),1,1)='0' then
Shape233.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),2,1)='0' then
Shape234.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),3,1)='0' then
Shape235.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),4,1)='0' then
Shape236.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),5,1)='0' then
Shape237.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),6,1)='0' then
Shape238.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),7,1)='0' then
Shape239.Brush.Color:=Form14.Shape2.Brush.Color;
if Copy((Form3.StringGrid1.Cells[i2,i1]),8,1)='0' then
Shape240.Brush.Color:=Form14.Shape2.Brush.Color;
end;

procedure TForm1.Timer3Timer(Sender: TObject);
begin
if Label134.Caption='50' then
Begin
Edit5.Text:='-1';
Label134.Caption:='0';
end;
if RadioButton1.Checked=true then
Begin
if Edit5.Text='1999' then
Label7.OnClick(Label7);
end;
if RadioButton2.Checked=true then
Begin
if Edit5.Text='2000' then
Edit5.Text:='-1';
end;
if RadioButton3.Checked=true then
Begin
if Edit5.Text=Edit2.Text then
Begin
if CheckBox1.Checked=true then
Edit5.Text:=IntToStr(StrToInt(Edit1.Text)-1)
else
Begin
Edit5.Text:=Edit1.Text;
if (StrToInt(Edit5.Text)<StrToInt(Edit1.Text)) or (StrToInt(Edit5.Text)>StrToInt(Edit2.Text)) then
Edit5.Text:=Edit1.Text;
Timer3.Enabled:=false;
end;
end;
end;
Edit5.Text:=IntToStr(StrToInt(Edit5.Text)+1);
Form1.effectu(Form1);
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
Form1.effectu(Form1);
end;

procedure TForm1.Labe68licger(Sender: TObject);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Begin
For a:=7 to Form3.StringGrid1.ColCount do
Begin
For i:=1 to Form3.StringGrid1.RowCount do
Form3.StringGrid1.Cells[a,i]:=Form3.StringGrid1.Cells[a+1,i];
end;
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
Form1.izmcvClick(Form1);
Form1.ShapeNevud(Form1);
Form1.Cursor:=crDefault;
Form1.Ydalenie(Form1);
end
else
Begin
i:=8;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Shape57ContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
Var i,a:integer;
begin
if Form1.Cursor=crArrow then
Begin
For a:=7 to Form3.StringGrid1.ColCount do
Begin
For i:=1 to Form3.StringGrid1.RowCount do
Form3.StringGrid1.Cells[a,i]:=Form3.StringGrid1.Cells[a+1,i];
end;
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
Form1.izmcvClick(Form1);
Form1.ShapeNevud(Form1);
Form1.Cursor:=crDefault;
Form1.Ydalenie(Form1);
end
else
Begin
i:=9;
if StrToInt(Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],1,1))=1 then
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='0'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7)
else
Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1]:='1'+Copy(Form3.StringGrid1.Cells[i,StrToInt(Edit5.Text)+1],2,7);
Form1.izmcvClick(Form1);
end;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
Label135.Visible:=false;
Button4.Visible:=false;
N10.Enabled:=true;
N2.Enabled:=true;
N17.Enabled:=true;
Form1.PopupMenu:=PopupMenu1;
end;

procedure TForm1.FormPaint(Sender: TObject);
begin
Button4.Left:=Round(ClientWidth/2)-Round(Button4.Width/2);
Label135.Left:=Round(ClientWidth/2)-Round(Label135.Width/2);
end;

procedure TForm1.N20Click(Sender: TObject);
begin
Label5.OnClick(Label5);
end;

procedure TForm1.N21Click(Sender: TObject);
begin
Label7.OnClick(Label7);
end;

procedure TForm1.N22Click(Sender: TObject);
var f:char;
begin
Edit5.Text:=IntTOStr(StrToInt(Edit5.Text)-1);
if StrToInt(Edit5.Text)<0 then
Edit5.Text:='0';
f:=#13;
Edit5.OnKeyPress(Edit5,f);
end;

procedure TForm1.N23Click(Sender: TObject);
var f:char;
begin
Edit5.Text:=IntTOStr(StrToInt(Edit5.Text)+1);
if StrToInt(Edit5.Text)>1999 then
Edit5.Text:='1999';
f:=#13;
Edit5.OnKeyPress(Edit5,f);
end;

procedure TForm1.Cjplfnm1Click(Sender: TObject);
begin
N10.Click;

end;

procedure TForm1.N25Click(Sender: TObject);
begin
N2.Click;
end;

procedure TForm1.N26Click(Sender: TObject);
begin
N17.Click;
end;

procedure TForm1.N28Click(Sender: TObject);
begin
N20.Click;
end;

procedure TForm1.N29Click(Sender: TObject);
begin
N21.Click;
end;

procedure TForm1.N30Click(Sender: TObject);
begin
N23.Click;
end;

procedure TForm1.N31Click(Sender: TObject);
begin
N22.Click;
end;

procedure TForm1.N33Click(Sender: TObject);
begin
N16.Click;
end;

procedure TForm1.N35Click(Sender: TObject);
begin
N3.Click;
end;

procedure TForm1.N36Click(Sender: TObject);
begin
N12.Click;
end;

procedure TForm1.N37Click(Sender: TObject);
begin
N13.Click;
end;

procedure TForm1.N39Click(Sender: TObject);
begin
ShellExecute(Handle, 'open', '/������� �� ��������� Hurrying fires.chm', nil, nil, SW_SHOWNORMAL);
end;

end.
