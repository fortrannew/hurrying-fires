unit Unit17;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ShellCtrls, StdCtrls;

type
  TForm17 = class(TForm)
    ShellTreeView1: TShellTreeView;
    Label1: TLabel;
    Button1: TButton;
    Button2: TButton;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ShellTreeView1Click(Sender: TObject);
    procedure ShellTreeView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form17: TForm17;

implementation

uses Unit16, Unit1;

{$R *.dfm}

procedure TForm17.Button2Click(Sender: TObject);
begin
Form17.Close;
Form16.Edit1.Text:=Form16.LabeledEdit1.Text;
end;

procedure TForm17.Button1Click(Sender: TObject);
begin
Form17.Close;
Form16.LabeledEdit1.Text:=Form16.Edit1.Text;
end;

procedure TForm17.ShellTreeView1Click(Sender: TObject);
begin
Form16.Edit1.Text:=ShellTreeView1.Path;
end;

procedure TForm17.ShellTreeView1DblClick(Sender: TObject);
begin
Form16.Edit1.Text:=ShellTreeView1.Path;
Form17.Button1.Click;
end;

end.
