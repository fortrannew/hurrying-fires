object Form16: TForm16
  Left = 390
  Top = 265
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1069#1082#1089#1087#1086#1088#1090' '#1092#1072#1081#1083#1086#1074
  ClientHeight = 98
  ClientWidth = 236
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object LabeledEdit1: TLabeledEdit
    Left = 8
    Top = 24
    Width = 137
    Height = 21
    EditLabel.Width = 129
    EditLabel.Height = 13
    EditLabel.Caption = #1042#1099#1073#1088#1072#1090#1100' '#1087#1072#1087#1082#1091' '#1101#1082#1089#1087#1086#1088#1090#1072':'
    TabOrder = 0
    OnKeyPress = LabeledEdit1KeyPress
  end
  object Button1: TButton
    Left = 152
    Top = 24
    Width = 75
    Height = 25
    Caption = '&'#1054#1073#1079#1086#1088
    TabOrder = 1
    OnClick = Button1Click
  end
  object Edit1: TEdit
    Left = 264
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object Button2: TButton
    Left = 24
    Top = 64
    Width = 75
    Height = 25
    Caption = '&'#1069#1082#1089#1087#1086#1088#1090
    TabOrder = 3
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 104
    Top = 64
    Width = 75
    Height = 25
    Caption = '&'#1054#1090#1084#1077#1085#1072
    TabOrder = 4
    OnClick = Button3Click
  end
  object Edit2: TEdit
    Left = 112
    Top = 56
    Width = 121
    Height = 21
    TabOrder = 5
    Visible = False
  end
  object SaveDialog1: TSaveDialog
    Left = 8
    Top = 64
  end
end
