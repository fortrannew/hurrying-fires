unit Unit9;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, TabNotBk;

type
  TForm9 = class(TForm)
    TabbedNotebook1: TTabbedNotebook;
    Label1: TLabel;
    Label2: TLabel;
    Button1: TButton;
    Button6: TButton;
    Label7: TLabel;
    Label8: TLabel;
    Button4: TButton;
    Button8: TButton;
    Label3: TLabel;
    Label4: TLabel;
    Button2: TButton;
    Button9: TButton;
    Button5: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure TabbedNotebook1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form9: TForm9;

implementation

uses Unit7, Unit10, Unit11, Unit6;

{$R *.dfm}

procedure TForm9.Button1Click(Sender: TObject);
begin
Form7.Button1.Click;
end;

procedure TForm9.Button6Click(Sender: TObject);
begin
Form7.Button6.Click;
end;

procedure TForm9.Button5Click(Sender: TObject);
begin
Form7.Button5.Click;
end;

procedure TForm9.Button4Click(Sender: TObject);
begin
Form7.Button4.Click;
end;

procedure TForm9.Button8Click(Sender: TObject);
begin
Form7.Button8.Click;
end;

procedure TForm9.Button2Click(Sender: TObject);
begin
Form7.Button2.Click;
end;

procedure TForm9.Button9Click(Sender: TObject);
begin
Form7.Button9.Click;
end;

procedure TForm9.TabbedNotebook1Click(Sender: TObject);
begin
if TabbedNotebook1.ActivePage='����������1' then
Begin
Form7.TabbedNotebook1.ActivePage:='����������1';
Form9.TabbedNotebook1.ActivePage:='����������1';
Form10.TabbedNotebook1.ActivePage:='����������1';
Form11.TabbedNotebook1.ActivePage:='����������1';
end;
if TabbedNotebook1.ActivePage='����������2' then
Begin
Form7.TabbedNotebook1.ActivePage:='����������2';
Form9.TabbedNotebook1.ActivePage:='����������2';
Form10.TabbedNotebook1.ActivePage:='����������2';
Form11.TabbedNotebook1.ActivePage:='����������2';
end;
if TabbedNotebook1.ActivePage='����������3' then
Begin
Form7.TabbedNotebook1.ActivePage:='����������3';
Form9.TabbedNotebook1.ActivePage:='����������3';
Form10.TabbedNotebook1.ActivePage:='����������3';
Form11.TabbedNotebook1.ActivePage:='����������3';
end;
if TabbedNotebook1.ActivePage='����������4' then
Begin
Form7.TabbedNotebook1.ActivePage:='����������4';
Form9.TabbedNotebook1.ActivePage:='����������4';
Form10.TabbedNotebook1.ActivePage:='����������4';
Form11.TabbedNotebook1.ActivePage:='����������4';
end;
end;

end.
