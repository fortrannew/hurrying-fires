unit Unit15;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, ToolWin;

type
  TForm15 = class(TForm)
    Timer1: TTimer;
    Label1: TLabel;
    Image1: TImage;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Label1Click(Sender: TObject);
    procedure Label1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form15: TForm15;
  x,y:Integer;

implementation

{$R *.dfm}

procedure TForm15.FormCreate(Sender: TObject);
begin
x:=Round(Image1.Height/2);//����� ����� ���������� ����� ��������
y:=Height+50;//����� ����� ��������� �� �����
Image1.Canvas.Font.Name:='Times New Roman';//������ ����� Times New Roman
Image1.Canvas.Brush.Color:=clYellow;//����� ���� ���� bitmap
Image1.Canvas.FillRect(Image1.Canvas.ClipRect);//������, bitmap ���� ���� �����
end;

procedure TForm15.Timer1Timer(Sender: TObject);
begin
y:=y-1;
if y=-200 then y:=height+10;
with Image1 do begin
Canvas.Font.Color:=$000576DC;//������ ���� ���������
Canvas.TextOut(x-10,y,'��� ���������: Hurrying fires');
Canvas.Font.Color:=clBlack;
Canvas.TextOut(x,y+15, '�����: �������� ����');
Canvas.Font.Color:=$000576DC;//������ ���� ���������
Canvas.TextOut(x-10,y+30,'������� �� ������ ����������������� ������');
Canvas.Font.Color:=clRed;
Canvas.TextOut(x,y+60,'������� � ���������������� ���������!');
Canvas.Font.Color:=$000576DC;//������ ���� ���������
Canvas.TextOut(x-10,y+75,'�������� ����.2010');
Canvas.Font.Color:=$000576DC;//������ ���� ���������
Canvas.TextOut(x-10,y+45,'������� �� ������������� ������ ���������');
Canvas.Font.Color:=clWhite;
Canvas.TextOut(x-10,y+105,'          ');
end;
Image1.Canvas.Draw(0,0,Image1.Picture.Bitmap);//������������ �� ����� ������ Bitmap

end;

procedure TForm15.Label1Click(Sender: TObject);
begin
Form15.Close;
Form15.Timer1.Enabled:=false;
y:=Height+50;//����� ����� ��������� �� �����
Image1.Canvas.Brush.Color:=clYellow;//����� ���� ���� bitmap
Image1.Canvas.FillRect(Image1.Canvas.ClipRect);//������, bitmap ���� ���� �����
end;

procedure TForm15.Label1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
Label1.Font.Style:=[fsBold];
end;

procedure TForm15.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
Label1.Font.Style:=[];
end;

procedure TForm15.FormActivate(Sender: TObject);
begin
timer1.Enabled:=True;//�������� ������
end;

end.
