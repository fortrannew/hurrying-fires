unit Unit13;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls;

type
  TForm13 = class(TForm)
    OpenDialog1: TOpenDialog;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GroupBox1: TGroupBox;
    TabSheet2: TTabSheet;
    RadioGroup1: TRadioGroup;
    GroupBox2: TGroupBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    Edit51: TEdit;
    ScrollBar1: TScrollBar;
    Panel1: TPanel;
    Edit9: TEdit;
    Edit8: TEdit;
    Edit7: TEdit;
    Edit6: TEdit;
    Edit5: TEdit;
    Edit4: TEdit;
    Edit3: TEdit;
    Edit2: TEdit;
    Edit15: TEdit;
    Edit14: TEdit;
    Edit13: TEdit;
    Edit12: TEdit;
    Edit11: TEdit;
    Edit10: TEdit;
    Edit1: TEdit;
    Panel2: TPanel;
    Button9: TButton;
    Button8: TButton;
    Button7: TButton;
    Button6: TButton;
    Button5: TButton;
    Button4: TButton;
    Button3: TButton;
    Button2: TButton;
    Button15: TButton;
    Button14: TButton;
    Button13: TButton;
    Button12: TButton;
    Button11: TButton;
    Button10: TButton;
    Button1: TButton;
    Button51: TButton;
    Button52: TButton;
    Button53: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Button16: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure Click1(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure Edit2KeyPress(Sender: TObject; var Key: Char);
    procedure Edit3KeyPress(Sender: TObject; var Key: Char);
    procedure Edit4KeyPress(Sender: TObject; var Key: Char);
    procedure Edit5KeyPress(Sender: TObject; var Key: Char);
    procedure Edit6KeyPress(Sender: TObject; var Key: Char);
    procedure Edit7KeyPress(Sender: TObject; var Key: Char);
    procedure Edit8KeyPress(Sender: TObject; var Key: Char);
    procedure Edit9KeyPress(Sender: TObject; var Key: Char);
    procedure Edit10KeyPress(Sender: TObject; var Key: Char);
    procedure Edit11KeyPress(Sender: TObject; var Key: Char);
    procedure Edit12KeyPress(Sender: TObject; var Key: Char);
    procedure Edit14KeyPress(Sender: TObject; var Key: Char);
    procedure Edit15KeyPress(Sender: TObject; var Key: Char);
    procedure Button11Click(Sender: TObject);
    procedure Button12Click(Sender: TObject);
    procedure Button13Click(Sender: TObject);
    procedure Button14Click(Sender: TObject);
    procedure Button15Click(Sender: TObject);
    procedure Edit13KeyPress(Sender: TObject; var Key: Char);
    procedure ScrollBar1Change(Sender: TObject);
    procedure Button51Click(Sender: TObject);
    procedure RadioButton3Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure Button52Click(Sender: TObject);
    procedure Button53Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form13: TForm13;

implementation

uses Unit1, Unit2, Unit5, Unit3, Unit16;

{$R *.dfm}

procedure TForm13.Button1Click(Sender: TObject);
var l,i:integer;
begin
For i:=0 to RadioGroup1.Items.Count do
Begin
if RadioGroup1.ItemIndex=i then
Begin
l:=Length(RadioGroup1.Items[i]);
OpenDialog1.Filter:=RadioGroup1.Items[i]+'|'+Copy(RadioGroup1.Items[i],(l-5),5)+'|��� ����� (*.*)|*.*';
end;
end;
if OpenDialog1.Execute then
 begin
 if Edit51.Text='1' then
   Begin
   Edit1.Text:=OpenDialog1.FileName;
   Edit2.Visible:=true;
   Button2.Visible:=true;
   if GroupBox1.Height=65 then
   GroupBox1.Height:=89;
   end;
     if Edit51.Text='2' then
     Begin
     Edit2.Text:=OpenDialog1.FileName;
     Edit3.Visible:=true;
     Button3.Visible:=true;
     if GroupBox1.Height=89 then
     GroupBox1.Height:=113;
     end;
     if Edit51.Text='3' then
       Begin
       Edit3.Text:=OpenDialog1.FileName;
       Edit4.Visible:=true;
       Button4.Visible:=true;
       if GroupBox1.Height=113 then
       GroupBox1.Height:=137;
       end;
       if Edit51.Text='4' then
         Begin
         Edit4.Text:=OpenDialog1.FileName;
         Edit5.Visible:=true;
         Button5.Visible:=true;
         if GroupBox1.Height=137 then
         GroupBox1.Height:=161;
         end;
         if Edit51.Text='5' then
           Begin
           Edit5.Text:=OpenDialog1.FileName;
           Edit6.Visible:=true;
           Button6.Visible:=true;
           if GroupBox1.Height=161 then
           GroupBox1.Height:=185;
           end;
           if Edit51.Text='6' then
             Begin
             Edit6.Text:=OpenDialog1.FileName;
             Edit7.Visible:=true;
             Button7.Visible:=true;
             if GroupBox1.Height=185 then
             GroupBox1.Height:=209;
             end;
             if Edit51.Text='7' then
               Begin
               Edit7.Text:=OpenDialog1.FileName;
               Edit8.Visible:=true;
               Button8.Visible:=true;
               if GroupBox1.Height=209 then
               GroupBox1.Height:=233;
               end;
               if Edit51.Text='8' then
                 Begin
                 Edit8.Text:=OpenDialog1.FileName;
                 Edit9.Visible:=true;
                 Button9.Visible:=true;
                 if GroupBox1.Height=233 then
                 GroupBox1.Height:=257;
                 end;
                 if Edit51.Text='9' then
                   Begin
                   Edit9.Text:=OpenDialog1.FileName;
                   Edit10.Visible:=true;
                   Button10.Visible:=true;
                   if GroupBox1.Height=257 then
                   GroupBox1.Height:=281;
                   end;
                   if Edit51.Text='10' then
                     Begin
                     Edit10.Text:=OpenDialog1.FileName;
                     Edit11.Visible:=true;
                     Button11.Visible:=true;
                     if GroupBox1.Height=281 then
                     GroupBox1.Height:=305;
                     end;
                     if Edit51.Text='11' then
                       Begin
                       Edit11.Text:=OpenDialog1.FileName;
                       Edit12.Visible:=true;
                       Button12.Visible:=true;
                       if GroupBox1.Height=305 then
                       GroupBox1.Height:=329;
                       end;
                       if Edit51.Text='12' then
                         Begin
                         Edit12.Text:=OpenDialog1.FileName;
                         Edit13.Visible:=true;
                         Button13.Visible:=true;
                         if GroupBox1.Height=329 then
                         GroupBox1.Height:=353;
                         end;
                         if Edit51.Text='13' then
                           Begin
                           Edit13.Text:=OpenDialog1.FileName;
                           Edit14.Visible:=true;
                           Button14.Visible:=true;
                           if GroupBox1.Height=353 then
                           GroupBox1.Height:=377;
                           end;
                           if Edit51.Text='14' then
                             Begin
                             Edit14.Text:=OpenDialog1.FileName;
                             Edit15.Visible:=true;
                             Button15.Visible:=true;
                             if GroupBox1.Height=377 then
                             GroupBox1.Height:=393;
                             end;
                             if Edit51.Text='15' then
                               Begin
                               Edit15.Text:=OpenDialog1.FileName;
                               end;

 if Edit14.Visible=true then
 Begin

 end;
 end;
end;

procedure TForm13.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
case Key of
#8 : Edit1.Text:='';
else
Key :=Chr(0); // ������ �� ����������
end;
end;

procedure TForm13.Click1(Sender: TObject);
begin
Edit51.Text:='1';
OpenDialog1.FileName:=Edit1.Text;
Form13.Button1Click(Sender);
end;

procedure TForm13.Button2Click(Sender: TObject);
begin
Edit51.Text:='2';
OpenDialog1.FileName:=Edit2.Text;
Form13.Button1Click(Sender);
end;

procedure TForm13.Button3Click(Sender: TObject);
begin
Edit51.Text:='3';
OpenDialog1.FileName:=Edit3.Text;
Form13.Button1Click(Sender);
end;

procedure TForm13.Button4Click(Sender: TObject);
begin
Edit51.Text:='4';
OpenDialog1.FileName:=Edit4.Text;
Form13.Button1Click(Sender);
end;

procedure TForm13.Button5Click(Sender: TObject);
begin
Edit51.Text:='5';
OpenDialog1.FileName:=Edit5.Text;
Form13.Button1Click(Sender);
end;

procedure TForm13.Button6Click(Sender: TObject);
begin
Edit51.Text:='6';
OpenDialog1.FileName:=Edit6.Text;
Form13.Button1Click(Sender);
end;

procedure TForm13.Button7Click(Sender: TObject);
begin
Edit51.Text:='7';
OpenDialog1.FileName:=Edit7.Text;
Form13.Button1Click(Sender);
end;

procedure TForm13.Button8Click(Sender: TObject);
begin
Edit51.Text:='8';
OpenDialog1.FileName:=Edit8.Text;
Form13.Button1Click(Sender);
end;

procedure TForm13.Button9Click(Sender: TObject);
begin
Edit51.Text:='9';
OpenDialog1.FileName:=Edit9.Text;
Form13.Button1Click(Sender);
end;

procedure TForm13.Button10Click(Sender: TObject);
begin
Edit51.Text:='10';
OpenDialog1.FileName:=Edit10.Text;
Form13.Button1Click(Sender);
end;

procedure TForm13.Edit2KeyPress(Sender: TObject; var Key: Char);
begin
case Key of
#8 : Edit2.Text:='';
else
Key :=Chr(0); // ������ �� ����������
end;
end;

procedure TForm13.Edit3KeyPress(Sender: TObject; var Key: Char);
begin
case Key of
#8 : Edit3.Text:='';
else
Key :=Chr(0); // ������ �� ����������
end;
end;

procedure TForm13.Edit4KeyPress(Sender: TObject; var Key: Char);
begin
case Key of
#8 : Edit4.Text:='';
else
Key :=Chr(0); // ������ �� ����������
end;
end;

procedure TForm13.Edit5KeyPress(Sender: TObject; var Key: Char);
begin
case Key of
#8 : Edit5.Text:='';
else
Key :=Chr(0); // ������ �� ����������
end;
end;

procedure TForm13.Edit6KeyPress(Sender: TObject; var Key: Char);
begin
case Key of
#8 : Edit6.Text:='';
else
Key :=Chr(0); // ������ �� ����������
end;
end;

procedure TForm13.Edit7KeyPress(Sender: TObject; var Key: Char);
begin
case Key of
#8 : Edit7.Text:='';
else
Key :=Chr(0); // ������ �� ����������
end;
end;

procedure TForm13.Edit8KeyPress(Sender: TObject; var Key: Char);
begin
case Key of
#8 : Edit8.Text:='';
else
Key :=Chr(0); // ������ �� ����������
end;
end;

procedure TForm13.Edit9KeyPress(Sender: TObject; var Key: Char);
begin
case Key of
#8 : Edit9.Text:='';
else
Key :=Chr(0); // ������ �� ����������
end;
end;

procedure TForm13.Edit10KeyPress(Sender: TObject; var Key: Char);
begin
case Key of
#8 : Edit10.Text:='';
else
Key :=Chr(0); // ������ �� ����������
end;
end;

procedure TForm13.Edit12KeyPress(Sender: TObject; var Key: Char);
begin
case Key of
#8 : Edit12.Text:='';
else
Key :=Chr(0); // ������ �� ����������
end;
end;

procedure TForm13.Edit14KeyPress(Sender: TObject; var Key: Char);
begin
case Key of
#8 : Edit14.Text:='';
else
Key :=Chr(0); // ������ �� ����������
end;
end;

procedure TForm13.Edit15KeyPress(Sender: TObject; var Key: Char);
begin
case Key of
#8 : Edit15.Text:='';
else
Key :=Chr(0); // ������ �� ����������
end;
end;

procedure TForm13.Edit11KeyPress(Sender: TObject; var Key: Char);
begin
case Key of
#8 : Edit11.Text:='';
else
Key :=Chr(0); // ������ �� ����������
end;
end;


procedure TForm13.Button11Click(Sender: TObject);
begin
Edit51.Text:='11';
OpenDialog1.FileName:=Edit11.Text;
Form13.Button1Click(Sender);
end;

procedure TForm13.Button12Click(Sender: TObject);
begin
Edit51.Text:='12';
OpenDialog1.FileName:=Edit12.Text;
Form13.Button1Click(Sender);
end;

procedure TForm13.Button13Click(Sender: TObject);
begin
Edit51.Text:='13';
OpenDialog1.FileName:=Edit13.Text;
Form13.Button1Click(Sender);
end;

procedure TForm13.Button14Click(Sender: TObject);
begin
Edit51.Text:='14';
OpenDialog1.FileName:=Edit14.Text;
Form13.Button1Click(Sender);
end;

procedure TForm13.Button15Click(Sender: TObject);
begin
Edit51.Text:='15';
OpenDialog1.FileName:=Edit15.Text;
Form13.Button1Click(Sender);
end;

procedure TForm13.Edit13KeyPress(Sender: TObject; var Key: Char);
begin
case Key of
#8 : Edit13.Text:='';
else
Key :=Chr(0); // ������ �� ����������
end;
end;

procedure TForm13.ScrollBar1Change(Sender: TObject);
var i,n,b:integer;
begin
if ScrollBar1.Position=1 then
  Begin
  n:=-14;
  b:=-16;
  end;
if ScrollBar1.Position=2 then
  Begin
  n:=-38;
  b:=-40;
  end;
if ScrollBar1.Position=3 then
  Begin
  n:=-62;
  b:=-64;
  end;
if ScrollBar1.Position=4 then
  Begin
  n:=-86;
  b:=-88;
  end;
if ScrollBar1.Position=5 then
  Begin
  n:=-110;
  b:=-112;
  end;
if ScrollBar1.Position=6 then
  Begin
  n:=-134;
  b:=-136;
  end;
if ScrollBar1.Position=7 then
  Begin
  n:=-158;
  b:=-160;
  end;
if ScrollBar1.Position=8 then
  Begin
  n:=-182;
  b:=-184;
  end;
if ScrollBar1.Position=9 then
  Begin
  n:=-206;
  b:=-208;
  end;
if ScrollBar1.Position=10 then
  Begin
  n:=-230;
  b:=-232;
  end;
if ScrollBar1.Position=11 then
  Begin
  n:=-254;
  b:=-256;
  end;
if ScrollBar1.Position=12 then
  Begin
  n:=-278;
  b:=-280;
  end;
if ScrollBar1.Position=13 then
  Begin
  n:=-302;
  b:=-304;
  end;
if ScrollBar1.Position=14 then
  Begin
  n:=-326;
  b:=-328;
  end;
if ScrollBar1.Position=15 then
  Begin
  n:=-350;
  b:=-352;
  end;
if ScrollBar1.Position=16 then
  Begin
  n:=-374;
  b:=-376;
  end;
if ScrollBar1.Position=17 then
  Begin
  n:=-398;
  b:=-400;
  end;
if ScrollBar1.Position=18 then
  Begin
  n:=-422;
  b:=-424;
  end;
if ScrollBar1.Position=19 then
  Begin
  n:=-446;
  b:=-448;
  end;
if ScrollBar1.Position=20 then
  Begin
  n:=-470;
  b:=-472;
  end;
if ScrollBar1.Position=21 then
  Begin
  n:=-494;
  b:=-496;
  end;
if ScrollBar1.Position=22 then
  Begin
  n:=-518;
  b:=-520;
  end;
if ScrollBar1.Position=23 then
  Begin
  n:=-542;
  b:=-544;
  end;
if ScrollBar1.Position=24 then
  Begin
  n:=-566;
  b:=-568;
  end;
if ScrollBar1.Position=25 then
  Begin
  n:=-590;
  b:=-592;
  end;
if ScrollBar1.Position=26 then
  Begin
  n:=-614;
  b:=-616;
  end;
if ScrollBar1.Position=27 then
  Begin
  n:=-638;
  b:=-640;
  end;
if ScrollBar1.Position=28 then
  Begin
  n:=-662;
  b:=-664;
  end;
if ScrollBar1.Position=29 then
  Begin
  n:=-686;
  b:=-688;
  end;
if ScrollBar1.Position=30 then
  Begin
  n:=-710;
  b:=-712;
  end;
if ScrollBar1.Position=31 then
  Begin
  n:=-734;
  b:=-736;
  end;
if ScrollBar1.Position=32 then
  Begin
  n:=-758;
  b:=-760;
  end;
if ScrollBar1.Position=33 then
  Begin
  n:=-782;
  b:=-784;
  end;
if ScrollBar1.Position=34 then
  Begin
  n:=-806;
  b:=-808;
  end;
if ScrollBar1.Position=35 then
  Begin
  n:=-830;
  b:=-832;
  end;
if ScrollBar1.Position=36 then
  Begin
  n:=-854;
  b:=-856;
  end;
if ScrollBar1.Position=37 then
  Begin
  n:=-878;
  b:=-880;
  end;
if ScrollBar1.Position=38 then
  Begin
  n:=-902;
  b:=-904;
  end;
if ScrollBar1.Position=39 then
  Begin
  n:=-926;
  b:=-928;
  end;
if ScrollBar1.Position=40 then
  Begin
  n:=-950;
  b:=-952;
  end;
if ScrollBar1.Position=41 then
  Begin
  n:=-974;
  b:=-976;
  end;
if ScrollBar1.Position=42 then
  Begin
  n:=-998;
  b:=-1000;
  end;
if ScrollBar1.Position=43 then
  Begin
  n:=-1022;
  b:=-1024;
  end;
if ScrollBar1.Position=44 then
  Begin
  n:=-1046;
  b:=-1048;
  end;
if ScrollBar1.Position=45 then
  Begin
  n:=-1070;
  b:=-1072;
  end;
if ScrollBar1.Position=46 then
  Begin
  n:=-1094;
  b:=-1096;
  end;
if ScrollBar1.Position=47 then
  Begin
  n:=-1118;
  b:=-1120;
  end;
if ScrollBar1.Position=48 then
  Begin
  n:=-1142;
  b:=-1144;
  end;
if ScrollBar1.Position=49 then
  Begin
  n:=-1166;
  b:=-1168;
  end;
if ScrollBar1.Position=50 then
  Begin
  n:=-1190;
  b:=-1192;
  end;
For i:=1 to 15 do
  Begin
  n:=n+24;
  b:=b+24;
  case i of
  1 : Begin Edit1.Top:=n; Button1.Top:=b; end;
  2 : Begin Edit2.Top:=n; Button2.Top:=b; end;
  3 : Begin Edit3.Top:=n; Button3.Top:=b; end;
  4 : Begin Edit4.Top:=n; Button4.Top:=b; end;
  5 : Begin Edit5.Top:=n; Button5.Top:=b; end;
  6 : Begin Edit6.Top:=n; Button6.Top:=b; end;
  7 : Begin Edit7.Top:=n; Button7.Top:=b; end;
  8 : Begin Edit8.Top:=n; Button8.Top:=b; end;
  9 : Begin Edit9.Top:=n; Button9.Top:=b; end;
  10 : Begin Edit10.Top:=n; Button10.Top:=b; end;
  11 : Begin Edit11.Top:=n; Button11.Top:=b; end;
  12 : Begin Edit12.Top:=n; Button12.Top:=b; end;
  13 : Begin Edit13.Top:=n; Button13.Top:=b; end;
  14 : Begin Edit14.Top:=n; Button14.Top:=b; end;
  15 : Begin Edit15.Top:=n; Button15.Top:=b; end;
  end;
  end;
end;

function to16do10(a:String):integer;
    external 'ShestToDesyat.dll';
function to10do2(d:String):integer;
    external 'DesyatToDvoich.dll';

procedure TForm13.Button51Click(Sender: TObject);
var i,l,a,c,m,por,moya,i1,i2,ha,n:integer;
p,prob:string;
begin
if Form13.Edit1.Text<>'' then
Begin
Form1.N10.Click;
Form1.Shape1.Visible:=false;
Form1.Shape2.Visible:=false;
Form1.Shape3.Visible:=false;
Form1.Shape4.Visible:=false;
Form1.Shape5.Visible:=false;
Form1.Shape6.Visible:=false;
Form1.Shape7.Visible:=false;
Form1.Shape8.Visible:=false;
Form2.ProgressBar1.Position:=0;
Form2.ProgressBar2.Position:=0;
Form2.Label2.Caption:='��������� 0%';
Form3.StringGrid1.ColCount:=2;
Form3.StringGrid1.RowCount:=2;
For i1:=1 to Form3.StringGrid1.ColCount do
Begin
For i2:=1 to Form3.StringGrid1.RowCount do
Begin
Form3.StringGrid1.Cells[i1,i2]:='';
end;
end;
Form1.Label10.Caption:='0';
if Edit1.Text<>'' then
Begin
Label1.Caption:=IntToStr(StrToInt(Label1.Caption)+1);
Form1.N12Click(Form1);
end;
if Edit2.Text<>'' then
Begin
Label1.Caption:=IntToStr(StrToInt(Label1.Caption)+1);
Form1.N12Click(Form1);
end;
if Edit3.Text<>'' then
Begin
Label1.Caption:=IntToStr(StrToInt(Label1.Caption)+1);
Form1.N12Click(Form1);
end;
if Edit4.Text<>'' then
Begin
Label1.Caption:=IntToStr(StrToInt(Label1.Caption)+1);
Form1.N12Click(Form1);
end;
if Edit5.Text<>'' then
Begin
Label1.Caption:=IntToStr(StrToInt(Label1.Caption)+1);
Form1.N12Click(Form1);
end;
if Edit6.Text<>'' then
Begin
Label1.Caption:=IntToStr(StrToInt(Label1.Caption)+1);
Form1.N12Click(Form1);
end;
if Edit7.Text<>'' then
Begin
Label1.Caption:=IntToStr(StrToInt(Label1.Caption)+1);
Form1.N12Click(Form1);
end;
if Edit8.Text<>'' then
Begin
Label1.Caption:=IntToStr(StrToInt(Label1.Caption)+1);
Form1.N12Click(Form1);
end;
if Edit9.Text<>'' then
Begin
Label1.Caption:=IntToStr(StrToInt(Label1.Caption)+1);
Form1.N12Click(Form1);
end;
if Edit10.Text<>'' then
Begin
Label1.Caption:=IntToStr(StrToInt(Label1.Caption)+1);
Form1.N12Click(Form1);
end;
if Edit11.Text<>'' then
Begin
Label1.Caption:=IntToStr(StrToInt(Label1.Caption)+1);
Form1.N12Click(Form1);
end;
if Edit12.Text<>'' then
Begin
Label1.Caption:=IntToStr(StrToInt(Label1.Caption)+1);
Form1.N12Click(Form1);
end;
if Edit13.Text<>'' then
Begin
Label1.Caption:=IntToStr(StrToInt(Label1.Caption)+1);
Form1.N12Click(Form1);
end;
if Edit14.Text<>'' then
Begin
Label1.Caption:=IntToStr(StrToInt(Label1.Caption)+1);
Form1.N12Click(Form1);
end;
if Edit15.Text<>'' then
Begin
Label1.Caption:=IntToStr(StrToInt(Label1.Caption)+1);
Form1.N12Click(Form1);
end;

////////


if Label1.Caption='0' then
Begin
Form13.Visible:=false;
Form2.Visible:=false;
end;
Form2.ProgressBar2.Max:=StrToInt(Label1.Caption)*4000;
if Edit1.Text<>'' then
Begin
Form3.StringGrid1.ColCount:=2;
Form3.StringGrid1.Cells[0,0]:='�������';
Form3.StringGrid1.Cells[1,0]:='����������1';
Form3.StringGrid1.ColWidths[1]:=80;
Form13.Visible:=false;
Form1.Enabled:=false;
Form2.Visible:=true;
Form2.Label1.Caption:='�������� �����: '+ExtractFileName(Edit1.Text);
Form2.Label1.Update;
Form2.ProgressBar1.Update;
Form2.Label2.Update;
Form2.ProgressBar2.Update;
Form1.Memo1.Lines.Clear;
Form1.Memo1.Lines.LoadFromFile(Edit1.Text);
if RadioGroup1.ItemIndex=0 then
Begin
Repeat
Begin
ha:=Form1.Memo1.Lines.Count;
if ha<16391 then
Form1.Memo1.Lines.Add(':1000F000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF10');
end;
until ha=16391
end;
if RadioGroup1.ItemIndex=4 then
Begin
Form1.Memo1.Lines.Delete(0);
Form1.Memo1.Lines.Delete(0);
end;
i:=Form1.Memo1.Lines.Count;
if RadioGroup1.ItemIndex=4 then
Begin
if i>134 then
Begin
For i:=134 to Form1.Memo1.Lines.Count do
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
end;
For i:=0 to (Form1.Memo1.Lines.Count) do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 10,l) ;
Form1.Memo1.Lines[i]:=p;
end;
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
For i:=0 to (Form1.Memo1.Lines.Count) or 124 do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 1,l-2) ;
Form1.Memo1.Lines[i]:=p;
end;
end;
if RadioGroup1.ItemIndex=7 then
Begin






For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
if (Form1.Memo1.Lines[i]='$A00,') or (Form1.Memo1.Lines[i]='$A80,') or (Form1.Memo1.Lines[i]='$A100,') or (Form1.Memo1.Lines[i]='$A180,') or (Form1.Memo1.Lines[i]='$A200,') or (Form1.Memo1.Lines[i]='$A280,') or (Form1.Memo1.Lines[i]='$A300,') or (Form1.Memo1.Lines[i]='$A380,') or (Form1.Memo1.Lines[i]='$A400,') or (Form1.Memo1.Lines[i]='$A480,') or (Form1.Memo1.Lines[i]='$A500,') or (Form1.Memo1.Lines[i]='$A580,') or (Form1.Memo1.Lines[i]='$A600,') or (Form1.Memo1.Lines[i]='$A680,') or (Form1.Memo1.Lines[i]='$A700,') or (Form1.Memo1.Lines[i]='$A780,') or (Form1.Memo1.Lines[i]='$A800,') or (Form1.Memo1.Lines[i]='$A880,') or (Form1.Memo1.Lines[i]='$A900,') or (Form1.Memo1.Lines[i]='$A980,') or (Form1.Memo1.Lines[i]='$AA00,') or (Form1.Memo1.Lines[i]='$AA80,') or (Form1.Memo1.Lines[i]='$AB00,') or (Form1.Memo1.Lines[i]='$AB80,') or (Form1.Memo1.Lines[i]='$AC00,') or (Form1.Memo1.Lines[i]='$AC80,') or (Form1.Memo1.Lines[i]='$AD00,') or (Form1.Memo1.Lines[i]='$AD80,') or (Form1.Memo1.Lines[i]='$AE00,') or
(Form1.Memo1.Lines[i]='$AE80,') or (Form1.Memo1.Lines[i]='$AF00,') or (Form1.Memo1.Lines[i]='$AF80,') then
Form1.Memo1.Lines.Delete(i);
end;
m:=0;
For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
p:=Form1.Memo1.Lines[i];
For n:=1 to Length(p) do
Begin
if p[n]=' ' then
Begin
Delete(p,n,1);
Form1.Memo1.Lines[i]:=p;
end;
end;
end;












end;
i:=-1;
For a:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
For c:=1 to 16 do
Begin
i:=i+1;
m:=Length(Form1.Memo1.Lines[a]);
Form3.StringGrid1.Cells[1,i+1]:=copy(Form1.Memo1.Lines[a],1,2);
Form1.Memo1.Lines[a]:=copy(Form1.Memo1.Lines[a],3,m-2);
Form2.ProgressBar1.Position:=Form2.ProgressBar1.Position+1;
Form2.ProgressBar1.Update;
Form2.ProgressBar2.Position:=Form2.ProgressBar2.Position+1;
Form2.ProgressBar2.Update;
Form2.Label2.Caption:='��������� '+IntToStr(Round(100/Form2.ProgressBar2.Max*Form2.ProgressBar2.Position))+'%';
Form2.Label2.Update;
Form3.StringGrid1.Cells[0,i+1]:=IntToStr(i);
Form3.StringGrid1.RowCount:=i+2;
if RadioButton3.Checked=true then
Begin
Form2.Label1.Caption:='�������������� ����� � ����������������� ���';
Form2.Label1.Update;
end;
if RadioButton2.Checked=true then
  Begin
  Form2.Label1.Caption:='�������������� ����� � ���������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]));
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
if RadioButton1.Checked=true then
  Begin
  Form2.Label1.Caption:='�������������� ����� � �������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to10do2(IntToStr(to16do10(Form3.StringGrid1.Cells[1,i+1]))));
    por:=Length(prob);
    if por<>8 then
      Begin
      Repeat
        Begin
        prob:='0'+prob;
        por:=Length(prob);
        end;
      until por=8;
      end;
    Form3.StringGrid1.Cells[1,i+1]:=prob
    end;
end;
end;
Repeat
Begin
c:=1;
if Form3.StringGrid1.Cells[1,Form3.StringGrid1.ColCount]='' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
c:=0;
end;
if Form3.StringGrid1.Cells[1,Form3.StringGrid1.ColCount]<>'' then
Begin
c:=1;
end;
end;
until c=1;
end;
if Edit2.Text<>'' then
Begin
Form2.Label2.Update;
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount+1;
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,0]:='����������2';
Form3.StringGrid1.ColWidths[2]:=80;
Form2.Label1.Caption:='�������� �����: '+ExtractFileName(Edit2.Text);
Form2.Label1.Update;
Form2.ProgressBar1.Position:=0;
Form2.ProgressBar1.Update;
Form1.Memo1.Lines.Clear;
Form1.Memo1.Lines.LoadFromFile(Edit2.Text);
if RadioGroup1.ItemIndex=0 then
Begin
Repeat
Begin
ha:=Form1.Memo1.Lines.Count;
if ha<16391 then
Form1.Memo1.Lines.Add(':1000F000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF10');
end;
until ha=16391
end;
if RadioGroup1.ItemIndex=4 then
Begin
Form1.Memo1.Lines.Delete(0);
Form1.Memo1.Lines.Delete(0);
end;
i:=Form1.Memo1.Lines.Count;
if RadioGroup1.ItemIndex=4 then
Begin
if i>134 then
Begin
For i:=134 to Form1.Memo1.Lines.Count do
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
end;
For i:=0 to (Form1.Memo1.Lines.Count) do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 10,l) ;
Form1.Memo1.Lines[i]:=p;
end;
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
For i:=0 to (Form1.Memo1.Lines.Count) or 124 do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 1,l-2) ;
Form1.Memo1.Lines[i]:=p;
end;
end;
if RadioGroup1.ItemIndex=7 then
Begin






For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
if (Form1.Memo1.Lines[i]='$A00,') or (Form1.Memo1.Lines[i]='$A80,') or (Form1.Memo1.Lines[i]='$A100,') or (Form1.Memo1.Lines[i]='$A180,') or (Form1.Memo1.Lines[i]='$A200,') or (Form1.Memo1.Lines[i]='$A280,') or (Form1.Memo1.Lines[i]='$A300,') or (Form1.Memo1.Lines[i]='$A380,') or (Form1.Memo1.Lines[i]='$A400,') or (Form1.Memo1.Lines[i]='$A480,') or (Form1.Memo1.Lines[i]='$A500,') or (Form1.Memo1.Lines[i]='$A580,') or (Form1.Memo1.Lines[i]='$A600,') or (Form1.Memo1.Lines[i]='$A680,') or (Form1.Memo1.Lines[i]='$A700,') or (Form1.Memo1.Lines[i]='$A780,') or (Form1.Memo1.Lines[i]='$A800,') or (Form1.Memo1.Lines[i]='$A880,') or (Form1.Memo1.Lines[i]='$A900,') or (Form1.Memo1.Lines[i]='$A980,') or (Form1.Memo1.Lines[i]='$AA00,') or (Form1.Memo1.Lines[i]='$AA80,') or (Form1.Memo1.Lines[i]='$AB00,') or (Form1.Memo1.Lines[i]='$AB80,') or (Form1.Memo1.Lines[i]='$AC00,') or (Form1.Memo1.Lines[i]='$AC80,') or (Form1.Memo1.Lines[i]='$AD00,') or (Form1.Memo1.Lines[i]='$AD80,') or (Form1.Memo1.Lines[i]='$AE00,') or
(Form1.Memo1.Lines[i]='$AE80,') or (Form1.Memo1.Lines[i]='$AF00,') or (Form1.Memo1.Lines[i]='$AF80,') then
Form1.Memo1.Lines.Delete(i);
end;
m:=0;
For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
p:=Form1.Memo1.Lines[i];
For n:=1 to Length(p) do
Begin
if p[n]=' ' then
Begin
Delete(p,n,1);
Form1.Memo1.Lines[i]:=p;
end;
end;
end;












end;
i:=-1;
For a:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
For c:=1 to 16 do
Begin
i:=i+1;
m:=Length(Form1.Memo1.Lines[a]);
Form3.StringGrid1.Cells[2,i+1]:=copy(Form1.Memo1.Lines[a],1,2);
Form1.Memo1.Lines[a]:=copy(Form1.Memo1.Lines[a],3,m-2);
Form2.ProgressBar1.Position:=Form2.ProgressBar1.Position+1;
Form2.ProgressBar1.Update;
Form2.ProgressBar2.Position:=Form2.ProgressBar2.Position+1;
Form2.ProgressBar2.Update;
Form2.Label2.Caption:='��������� '+IntToStr(Round(100/Form2.ProgressBar2.Max*Form2.ProgressBar2.Position))+'%';
Form2.Label2.Update;
if RadioButton3.Checked=true then
  Begin
  Form2.Label1.Caption:='�������������� ����� � ����������������� ���';
Form2.Label1.Update;
    prob:=Form3.StringGrid1.Cells[2,i+1];
    Form3.StringGrid1.Cells[2,i+1]:=prob;
  end;
if RadioButton2.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ���������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to16do10(Form3.StringGrid1.Cells[2,i+1]));
    Form3.StringGrid1.Cells[2,i+1]:=prob;
    end;
if RadioButton1.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � �������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to10do2(IntToStr(to16do10(Form3.StringGrid1.Cells[2,i+1]))));
    por:=Length(prob);
    if por<>8 then
      Begin
      Repeat
        Begin
        prob:='0'+prob;
        por:=Length(prob);
        end;
      until por=8;
      end;
    Form3.StringGrid1.Cells[2,i+1]:=prob;
    end;
end;
end;
Repeat
Begin
c:=1;
if Form3.StringGrid1.Cells[2,Form3.StringGrid1.ColCount]='' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
c:=0;
end;
if Form3.StringGrid1.Cells[2,Form3.StringGrid1.ColCount]<>'' then
Begin
c:=1;
end;
end;
until c=1;
end;
if Edit3.Text<>'' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount+1;
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,0]:='����������3';
Form3.StringGrid1.ColWidths[Form3.StringGrid1.ColCount-1]:=80;
Form2.Label1.Caption:='�������� �����: '+ExtractFileName(Edit3.Text);
Form2.Label1.Update;
Form2.ProgressBar1.Update;
Form2.Label2.Update;
Form2.ProgressBar1.Position:=0;
Form1.Memo1.Lines.Clear;
Form1.Memo1.Lines.LoadFromFile(Edit3.Text);
if RadioGroup1.ItemIndex=0 then
Begin
Repeat
Begin
ha:=Form1.Memo1.Lines.Count;
if ha<16391 then
Form1.Memo1.Lines.Add(':1000F000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF10');
end;
until ha=16391
end;
if RadioGroup1.ItemIndex=4 then
Begin
Form1.Memo1.Lines.Delete(0);
Form1.Memo1.Lines.Delete(0);
end;
i:=Form1.Memo1.Lines.Count;
if RadioGroup1.ItemIndex=4 then
Begin
if i>134 then
Begin
For i:=134 to Form1.Memo1.Lines.Count do
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
end;
For i:=0 to (Form1.Memo1.Lines.Count) do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 10,l) ;
Form1.Memo1.Lines[i]:=p;
end;
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
For i:=0 to (Form1.Memo1.Lines.Count) or 124 do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 1,l-2) ;
Form1.Memo1.Lines[i]:=p;
end;
end;
if RadioGroup1.ItemIndex=7 then
Begin






For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
if (Form1.Memo1.Lines[i]='$A00,') or (Form1.Memo1.Lines[i]='$A80,') or (Form1.Memo1.Lines[i]='$A100,') or (Form1.Memo1.Lines[i]='$A180,') or (Form1.Memo1.Lines[i]='$A200,') or (Form1.Memo1.Lines[i]='$A280,') or (Form1.Memo1.Lines[i]='$A300,') or (Form1.Memo1.Lines[i]='$A380,') or (Form1.Memo1.Lines[i]='$A400,') or (Form1.Memo1.Lines[i]='$A480,') or (Form1.Memo1.Lines[i]='$A500,') or (Form1.Memo1.Lines[i]='$A580,') or (Form1.Memo1.Lines[i]='$A600,') or (Form1.Memo1.Lines[i]='$A680,') or (Form1.Memo1.Lines[i]='$A700,') or (Form1.Memo1.Lines[i]='$A780,') or (Form1.Memo1.Lines[i]='$A800,') or (Form1.Memo1.Lines[i]='$A880,') or (Form1.Memo1.Lines[i]='$A900,') or (Form1.Memo1.Lines[i]='$A980,') or (Form1.Memo1.Lines[i]='$AA00,') or (Form1.Memo1.Lines[i]='$AA80,') or (Form1.Memo1.Lines[i]='$AB00,') or (Form1.Memo1.Lines[i]='$AB80,') or (Form1.Memo1.Lines[i]='$AC00,') or (Form1.Memo1.Lines[i]='$AC80,') or (Form1.Memo1.Lines[i]='$AD00,') or (Form1.Memo1.Lines[i]='$AD80,') or (Form1.Memo1.Lines[i]='$AE00,') or
(Form1.Memo1.Lines[i]='$AE80,') or (Form1.Memo1.Lines[i]='$AF00,') or (Form1.Memo1.Lines[i]='$AF80,') then
Form1.Memo1.Lines.Delete(i);
end;
m:=0;
For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
p:=Form1.Memo1.Lines[i];
For n:=1 to Length(p) do
Begin
if p[n]=' ' then
Begin
Delete(p,n,1);
Form1.Memo1.Lines[i]:=p;
end;
end;
end;












end;
i:=-1;
For a:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
For c:=1 to 16 do
Begin
i:=i+1;
m:=Length(Form1.Memo1.Lines[a]);
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=copy(Form1.Memo1.Lines[a],1,2);
Form1.Memo1.Lines[a]:=copy(Form1.Memo1.Lines[a],3,m-2);
Form2.ProgressBar1.Position:=Form2.ProgressBar1.Position+1;
Form2.ProgressBar1.Update;
Form2.ProgressBar2.Position:=Form2.ProgressBar2.Position+1;
Form2.ProgressBar2.Update;
Form2.Label2.Caption:='��������� '+IntToStr(Round(100/Form2.ProgressBar2.Max*Form2.ProgressBar2.Position))+'%';
Form2.Label2.Update;
if RadioButton3.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ����������������� ���';
Form2.Label1.Update;
    prob:=Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1];
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
  end;
if RadioButton2.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ���������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]));
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
if RadioButton1.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � �������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to10do2(IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]))));
    por:=Length(prob);
    if por<>8 then
      Begin
      Repeat
        Begin
        prob:='0'+prob;
        por:=Length(prob);
        end;
      until por=8;
      end;
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
end;
end;
Repeat
Begin
c:=1;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]='' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
c:=0;
end;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]<>'' then
Begin
c:=1;
end;
end;
until c=1;
end;
if Edit4.Text<>'' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount+1;
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,0]:='����������'+IntToStr(Form3.StringGrid1.ColCount-1);
Form3.StringGrid1.ColWidths[Form3.StringGrid1.ColCount-1]:=80;
Form2.Label1.Caption:='�������� �����: '+ExtractFileName(Edit4.Text);
Form2.Label1.Update;
Form2.ProgressBar1.Update;
Form2.Label2.Update;
Form2.ProgressBar1.Position:=0;
Form1.Memo1.Lines.Clear;
Form1.Memo1.Lines.LoadFromFile(Edit4.Text);
if RadioGroup1.ItemIndex=0 then
Begin
Repeat
Begin
ha:=Form1.Memo1.Lines.Count;
if ha<16391 then
Form1.Memo1.Lines.Add(':1000F000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF10');
end;
until ha=16391
end;
if RadioGroup1.ItemIndex=4 then
Begin
Form1.Memo1.Lines.Delete(0);
Form1.Memo1.Lines.Delete(0);
end;
i:=Form1.Memo1.Lines.Count;
if RadioGroup1.ItemIndex=4 then
Begin
if i>134 then
Begin
For i:=134 to Form1.Memo1.Lines.Count do
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
end;
For i:=0 to (Form1.Memo1.Lines.Count) do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 10,l) ;
Form1.Memo1.Lines[i]:=p;
end;
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
For i:=0 to (Form1.Memo1.Lines.Count) or 124 do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 1,l-2) ;
Form1.Memo1.Lines[i]:=p;
end;
end;
if RadioGroup1.ItemIndex=7 then
Begin






For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
if (Form1.Memo1.Lines[i]='$A00,') or (Form1.Memo1.Lines[i]='$A80,') or (Form1.Memo1.Lines[i]='$A100,') or (Form1.Memo1.Lines[i]='$A180,') or (Form1.Memo1.Lines[i]='$A200,') or (Form1.Memo1.Lines[i]='$A280,') or (Form1.Memo1.Lines[i]='$A300,') or (Form1.Memo1.Lines[i]='$A380,') or (Form1.Memo1.Lines[i]='$A400,') or (Form1.Memo1.Lines[i]='$A480,') or (Form1.Memo1.Lines[i]='$A500,') or (Form1.Memo1.Lines[i]='$A580,') or (Form1.Memo1.Lines[i]='$A600,') or (Form1.Memo1.Lines[i]='$A680,') or (Form1.Memo1.Lines[i]='$A700,') or (Form1.Memo1.Lines[i]='$A780,') or (Form1.Memo1.Lines[i]='$A800,') or (Form1.Memo1.Lines[i]='$A880,') or (Form1.Memo1.Lines[i]='$A900,') or (Form1.Memo1.Lines[i]='$A980,') or (Form1.Memo1.Lines[i]='$AA00,') or (Form1.Memo1.Lines[i]='$AA80,') or (Form1.Memo1.Lines[i]='$AB00,') or (Form1.Memo1.Lines[i]='$AB80,') or (Form1.Memo1.Lines[i]='$AC00,') or (Form1.Memo1.Lines[i]='$AC80,') or (Form1.Memo1.Lines[i]='$AD00,') or (Form1.Memo1.Lines[i]='$AD80,') or (Form1.Memo1.Lines[i]='$AE00,') or
(Form1.Memo1.Lines[i]='$AE80,') or (Form1.Memo1.Lines[i]='$AF00,') or (Form1.Memo1.Lines[i]='$AF80,') then
Form1.Memo1.Lines.Delete(i);
end;
m:=0;
For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
p:=Form1.Memo1.Lines[i];
For n:=1 to Length(p) do
Begin
if p[n]=' ' then
Begin
Delete(p,n,1);
Form1.Memo1.Lines[i]:=p;
end;
end;
end;












end;
i:=-1;
For a:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
For c:=1 to 16 do
Begin
i:=i+1;
m:=Length(Form1.Memo1.Lines[a]);
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=copy(Form1.Memo1.Lines[a],1,2);
Form1.Memo1.Lines[a]:=copy(Form1.Memo1.Lines[a],3,m-2);
Form2.ProgressBar1.Position:=Form2.ProgressBar1.Position+1;
Form2.ProgressBar1.Update;
Form2.ProgressBar2.Position:=Form2.ProgressBar2.Position+1;
Form2.ProgressBar2.Update;
Form2.Label2.Caption:='��������� '+IntToStr(Round(100/Form2.ProgressBar2.Max*Form2.ProgressBar2.Position))+'%';
Form2.Label2.Update;
if RadioButton3.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ����������������� ���';
Form2.Label1.Update;
    prob:=Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1];
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
  end;
if RadioButton2.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ���������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]));
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
if RadioButton1.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � �������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to10do2(IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]))));
    por:=Length(prob);
    if por<>8 then
      Begin
      Repeat
        Begin
        prob:='0'+prob;
        por:=Length(prob);
        end;
      until por=8;
      end;
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
end;
end;
Repeat
Begin
c:=1;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]='' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
c:=0;
end;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]<>'' then
Begin
c:=1;
end;
end;
until c=1;
end;
if Edit5.Text<>'' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount+1;
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,0]:='����������'+IntToStr(Form3.StringGrid1.ColCount-1);
Form3.StringGrid1.ColWidths[Form3.StringGrid1.ColCount-1]:=80;
Form2.Label1.Caption:='�������� �����: '+ExtractFileName(Edit5.Text);
Form2.Label1.Update;
Form2.ProgressBar1.Update;
Form2.Label2.Update;
Form2.ProgressBar1.Position:=0;
Form1.Memo1.Lines.Clear;
Form1.Memo1.Lines.LoadFromFile(Edit5.Text);
if RadioGroup1.ItemIndex=0 then
Begin
Repeat
Begin
ha:=Form1.Memo1.Lines.Count;
if ha<16391 then
Form1.Memo1.Lines.Add(':1000F000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF10');
end;
until ha=16391
end;
if RadioGroup1.ItemIndex=4 then
Begin
Form1.Memo1.Lines.Delete(0);
Form1.Memo1.Lines.Delete(0);
end;
i:=Form1.Memo1.Lines.Count;
if RadioGroup1.ItemIndex=4 then
Begin
if i>134 then
Begin
For i:=134 to Form1.Memo1.Lines.Count do
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
end;
For i:=0 to (Form1.Memo1.Lines.Count) do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 10,l) ;
Form1.Memo1.Lines[i]:=p;
end;
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
For i:=0 to (Form1.Memo1.Lines.Count) or 124 do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 1,l-2) ;
Form1.Memo1.Lines[i]:=p;
end;
end;
if RadioGroup1.ItemIndex=7 then
Begin






For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
if (Form1.Memo1.Lines[i]='$A00,') or (Form1.Memo1.Lines[i]='$A80,') or (Form1.Memo1.Lines[i]='$A100,') or (Form1.Memo1.Lines[i]='$A180,') or (Form1.Memo1.Lines[i]='$A200,') or (Form1.Memo1.Lines[i]='$A280,') or (Form1.Memo1.Lines[i]='$A300,') or (Form1.Memo1.Lines[i]='$A380,') or (Form1.Memo1.Lines[i]='$A400,') or (Form1.Memo1.Lines[i]='$A480,') or (Form1.Memo1.Lines[i]='$A500,') or (Form1.Memo1.Lines[i]='$A580,') or (Form1.Memo1.Lines[i]='$A600,') or (Form1.Memo1.Lines[i]='$A680,') or (Form1.Memo1.Lines[i]='$A700,') or (Form1.Memo1.Lines[i]='$A780,') or (Form1.Memo1.Lines[i]='$A800,') or (Form1.Memo1.Lines[i]='$A880,') or (Form1.Memo1.Lines[i]='$A900,') or (Form1.Memo1.Lines[i]='$A980,') or (Form1.Memo1.Lines[i]='$AA00,') or (Form1.Memo1.Lines[i]='$AA80,') or (Form1.Memo1.Lines[i]='$AB00,') or (Form1.Memo1.Lines[i]='$AB80,') or (Form1.Memo1.Lines[i]='$AC00,') or (Form1.Memo1.Lines[i]='$AC80,') or (Form1.Memo1.Lines[i]='$AD00,') or (Form1.Memo1.Lines[i]='$AD80,') or (Form1.Memo1.Lines[i]='$AE00,') or
(Form1.Memo1.Lines[i]='$AE80,') or (Form1.Memo1.Lines[i]='$AF00,') or (Form1.Memo1.Lines[i]='$AF80,') then
Form1.Memo1.Lines.Delete(i);
end;
m:=0;
For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
p:=Form1.Memo1.Lines[i];
For n:=1 to Length(p) do
Begin
if p[n]=' ' then
Begin
Delete(p,n,1);
Form1.Memo1.Lines[i]:=p;
end;
end;
end;












end;
i:=-1;
For a:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
For c:=1 to 16 do
Begin
i:=i+1;
m:=Length(Form1.Memo1.Lines[a]);
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=copy(Form1.Memo1.Lines[a],1,2);
Form1.Memo1.Lines[a]:=copy(Form1.Memo1.Lines[a],3,m-2);
Form2.ProgressBar1.Position:=Form2.ProgressBar1.Position+1;
Form2.ProgressBar1.Update;
Form2.ProgressBar2.Position:=Form2.ProgressBar2.Position+1;
Form2.ProgressBar2.Update;
Form2.Label2.Caption:='��������� '+IntToStr(Round(100/Form2.ProgressBar2.Max*Form2.ProgressBar2.Position))+'%';
Form2.Label2.Update;
if RadioButton3.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ����������������� ���';
Form2.Label1.Update;
    prob:=Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1];
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
  end;
if RadioButton2.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ���������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]));
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
if RadioButton1.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � �������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to10do2(IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]))));
    por:=Length(prob);
    if por<>8 then
      Begin
      Repeat
        Begin
        prob:='0'+prob;
        por:=Length(prob);
        end;
      until por=8;
      end;
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
end;
end;
Repeat
Begin
c:=1;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]='' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
c:=0;
end;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]<>'' then
Begin
c:=1;
end;
end;
until c=1;
end;
if Edit6.Text<>'' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount+1;
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,0]:='����������'+IntToStr(Form3.StringGrid1.ColCount-1);
Form3.StringGrid1.ColWidths[Form3.StringGrid1.ColCount-1]:=80;
Form2.Label1.Caption:='�������� �����: '+ExtractFileName(Edit6.Text);
Form2.Label1.Update;
Form2.ProgressBar1.Update;
Form2.Label2.Update;
Form2.ProgressBar1.Position:=0;
Form1.Memo1.Lines.Clear;
Form1.Memo1.Lines.LoadFromFile(Edit6.Text);
if RadioGroup1.ItemIndex=0 then
Begin
Repeat
Begin
ha:=Form1.Memo1.Lines.Count;
if ha<16391 then
Form1.Memo1.Lines.Add(':1000F000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF10');
end;
until ha=16391
end;
if RadioGroup1.ItemIndex=4 then
Begin
Form1.Memo1.Lines.Delete(0);
Form1.Memo1.Lines.Delete(0);
end;
i:=Form1.Memo1.Lines.Count;
if RadioGroup1.ItemIndex=4 then
Begin
if i>134 then
Begin
For i:=134 to Form1.Memo1.Lines.Count do
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
end;
For i:=0 to (Form1.Memo1.Lines.Count) do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 10,l) ;
Form1.Memo1.Lines[i]:=p;
end;
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
For i:=0 to (Form1.Memo1.Lines.Count) or 124 do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 1,l-2) ;
Form1.Memo1.Lines[i]:=p;
end;
end;
if RadioGroup1.ItemIndex=7 then
Begin






For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
if (Form1.Memo1.Lines[i]='$A00,') or (Form1.Memo1.Lines[i]='$A80,') or (Form1.Memo1.Lines[i]='$A100,') or (Form1.Memo1.Lines[i]='$A180,') or (Form1.Memo1.Lines[i]='$A200,') or (Form1.Memo1.Lines[i]='$A280,') or (Form1.Memo1.Lines[i]='$A300,') or (Form1.Memo1.Lines[i]='$A380,') or (Form1.Memo1.Lines[i]='$A400,') or (Form1.Memo1.Lines[i]='$A480,') or (Form1.Memo1.Lines[i]='$A500,') or (Form1.Memo1.Lines[i]='$A580,') or (Form1.Memo1.Lines[i]='$A600,') or (Form1.Memo1.Lines[i]='$A680,') or (Form1.Memo1.Lines[i]='$A700,') or (Form1.Memo1.Lines[i]='$A780,') or (Form1.Memo1.Lines[i]='$A800,') or (Form1.Memo1.Lines[i]='$A880,') or (Form1.Memo1.Lines[i]='$A900,') or (Form1.Memo1.Lines[i]='$A980,') or (Form1.Memo1.Lines[i]='$AA00,') or (Form1.Memo1.Lines[i]='$AA80,') or (Form1.Memo1.Lines[i]='$AB00,') or (Form1.Memo1.Lines[i]='$AB80,') or (Form1.Memo1.Lines[i]='$AC00,') or (Form1.Memo1.Lines[i]='$AC80,') or (Form1.Memo1.Lines[i]='$AD00,') or (Form1.Memo1.Lines[i]='$AD80,') or (Form1.Memo1.Lines[i]='$AE00,') or
(Form1.Memo1.Lines[i]='$AE80,') or (Form1.Memo1.Lines[i]='$AF00,') or (Form1.Memo1.Lines[i]='$AF80,') then
Form1.Memo1.Lines.Delete(i);
end;
m:=0;
For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
p:=Form1.Memo1.Lines[i];
For n:=1 to Length(p) do
Begin
if p[n]=' ' then
Begin
Delete(p,n,1);
Form1.Memo1.Lines[i]:=p;
end;
end;
end;












end;

i:=-1;
For a:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
For c:=1 to 16 do
Begin
i:=i+1;
m:=Length(Form1.Memo1.Lines[a]);
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=copy(Form1.Memo1.Lines[a],1,2);
Form1.Memo1.Lines[a]:=copy(Form1.Memo1.Lines[a],3,m-2);
Form2.ProgressBar1.Position:=Form2.ProgressBar1.Position+1;
Form2.ProgressBar1.Update;
Form2.ProgressBar2.Position:=Form2.ProgressBar2.Position+1;
Form2.ProgressBar2.Update;
Form2.Label2.Caption:='��������� '+IntToStr(Round(100/Form2.ProgressBar2.Max*Form2.ProgressBar2.Position))+'%';
Form2.Label2.Update;
if RadioButton3.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ����������������� ���';
Form2.Label1.Update;
    prob:=Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1];
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
  end;
if RadioButton2.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ���������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]));
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
if RadioButton1.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � �������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to10do2(IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]))));
    por:=Length(prob);
    if por<>8 then
      Begin
      Repeat
        Begin
        prob:='0'+prob;
        por:=Length(prob);
        end;
      until por=8;
      end;
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
end;
end;
Repeat
Begin
c:=1;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]='' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
c:=0;
end;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]<>'' then
Begin
c:=1;
end;
end;
until c=1;
end;
if Edit7.Text<>'' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount+1;
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,0]:='����������'+IntToStr(Form3.StringGrid1.ColCount-1);
Form3.StringGrid1.ColWidths[Form3.StringGrid1.ColCount-1]:=80;
Form2.Label1.Caption:='�������� �����: '+ExtractFileName(Edit7.Text);
Form2.Label1.Update;
Form2.ProgressBar1.Update;
Form2.Label2.Update;
Form2.ProgressBar1.Position:=0;
Form1.Memo1.Lines.Clear;
Form1.Memo1.Lines.LoadFromFile(Edit7.Text);
if RadioGroup1.ItemIndex=0 then
Begin
Repeat
Begin
ha:=Form1.Memo1.Lines.Count;
if ha<16391 then
Form1.Memo1.Lines.Add(':1000F000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF10');
end;
until ha=16391
end;
if RadioGroup1.ItemIndex=4 then
Begin
Form1.Memo1.Lines.Delete(0);
Form1.Memo1.Lines.Delete(0);
end;
i:=Form1.Memo1.Lines.Count;
if RadioGroup1.ItemIndex=4 then
Begin
if i>134 then
Begin
For i:=134 to Form1.Memo1.Lines.Count do
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
end;
For i:=0 to (Form1.Memo1.Lines.Count) do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 10,l) ;
Form1.Memo1.Lines[i]:=p;
end;
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
For i:=0 to (Form1.Memo1.Lines.Count) or 124 do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 1,l-2) ;
Form1.Memo1.Lines[i]:=p;
end;
end;
if RadioGroup1.ItemIndex=7 then
Begin






For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
if (Form1.Memo1.Lines[i]='$A00,') or (Form1.Memo1.Lines[i]='$A80,') or (Form1.Memo1.Lines[i]='$A100,') or (Form1.Memo1.Lines[i]='$A180,') or (Form1.Memo1.Lines[i]='$A200,') or (Form1.Memo1.Lines[i]='$A280,') or (Form1.Memo1.Lines[i]='$A300,') or (Form1.Memo1.Lines[i]='$A380,') or (Form1.Memo1.Lines[i]='$A400,') or (Form1.Memo1.Lines[i]='$A480,') or (Form1.Memo1.Lines[i]='$A500,') or (Form1.Memo1.Lines[i]='$A580,') or (Form1.Memo1.Lines[i]='$A600,') or (Form1.Memo1.Lines[i]='$A680,') or (Form1.Memo1.Lines[i]='$A700,') or (Form1.Memo1.Lines[i]='$A780,') or (Form1.Memo1.Lines[i]='$A800,') or (Form1.Memo1.Lines[i]='$A880,') or (Form1.Memo1.Lines[i]='$A900,') or (Form1.Memo1.Lines[i]='$A980,') or (Form1.Memo1.Lines[i]='$AA00,') or (Form1.Memo1.Lines[i]='$AA80,') or (Form1.Memo1.Lines[i]='$AB00,') or (Form1.Memo1.Lines[i]='$AB80,') or (Form1.Memo1.Lines[i]='$AC00,') or (Form1.Memo1.Lines[i]='$AC80,') or (Form1.Memo1.Lines[i]='$AD00,') or (Form1.Memo1.Lines[i]='$AD80,') or (Form1.Memo1.Lines[i]='$AE00,') or
(Form1.Memo1.Lines[i]='$AE80,') or (Form1.Memo1.Lines[i]='$AF00,') or (Form1.Memo1.Lines[i]='$AF80,') then
Form1.Memo1.Lines.Delete(i);
end;
m:=0;
For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
p:=Form1.Memo1.Lines[i];
For n:=1 to Length(p) do
Begin
if p[n]=' ' then
Begin
Delete(p,n,1);
Form1.Memo1.Lines[i]:=p;
end;
end;
end;












end;

i:=-1;
For a:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
For c:=1 to 16 do
Begin
i:=i+1;
m:=Length(Form1.Memo1.Lines[a]);
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=copy(Form1.Memo1.Lines[a],1,2);
Form1.Memo1.Lines[a]:=copy(Form1.Memo1.Lines[a],3,m-2);
Form2.ProgressBar1.Position:=Form2.ProgressBar1.Position+1;
Form2.ProgressBar1.Update;
Form2.ProgressBar2.Position:=Form2.ProgressBar2.Position+1;
Form2.ProgressBar2.Update;
Form2.Label2.Caption:='��������� '+IntToStr(Round(100/Form2.ProgressBar2.Max*Form2.ProgressBar2.Position))+'%';
Form2.Label2.Update;
if RadioButton3.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ����������������� ���';
Form2.Label1.Update;
    prob:=Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1];
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
  end;
if RadioButton2.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ���������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]));
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
if RadioButton1.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � �������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to10do2(IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]))));
    por:=Length(prob);
    if por<>8 then
      Begin
      Repeat
        Begin
        prob:='0'+prob;
        por:=Length(prob);
        end;
      until por=8;
      end;
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
end;
end;
Repeat
Begin
c:=1;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]='' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
c:=0;
end;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]<>'' then
Begin
c:=1;
end;
end;
until c=1;
end;
if Edit8.Text<>'' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount+1;
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,0]:='����������'+IntToStr(Form3.StringGrid1.ColCount-1);
Form3.StringGrid1.ColWidths[Form3.StringGrid1.ColCount-1]:=80;
Form2.Label1.Caption:='�������� �����: '+ExtractFileName(Edit8.Text);
Form2.Label1.Update;
Form2.ProgressBar1.Update;
Form2.Label2.Update;
Form2.ProgressBar1.Position:=0;
Form1.Memo1.Lines.Clear;
Form1.Memo1.Lines.LoadFromFile(Edit8.Text);
if RadioGroup1.ItemIndex=0 then
Begin
Repeat
Begin
ha:=Form1.Memo1.Lines.Count;
if ha<16391 then
Form1.Memo1.Lines.Add(':1000F000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF10');
end;
until ha=16391
end;
if RadioGroup1.ItemIndex=4 then
Begin
Form1.Memo1.Lines.Delete(0);
Form1.Memo1.Lines.Delete(0);
end;
i:=Form1.Memo1.Lines.Count;
if RadioGroup1.ItemIndex=4 then
Begin
if i>134 then
Begin
For i:=134 to Form1.Memo1.Lines.Count do
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
end;
For i:=0 to (Form1.Memo1.Lines.Count) do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 10,l) ;
Form1.Memo1.Lines[i]:=p;
end;
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
For i:=0 to (Form1.Memo1.Lines.Count) or 124 do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 1,l-2) ;
Form1.Memo1.Lines[i]:=p;
end;
end;
if RadioGroup1.ItemIndex=7 then
Begin






For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
if (Form1.Memo1.Lines[i]='$A00,') or (Form1.Memo1.Lines[i]='$A80,') or (Form1.Memo1.Lines[i]='$A100,') or (Form1.Memo1.Lines[i]='$A180,') or (Form1.Memo1.Lines[i]='$A200,') or (Form1.Memo1.Lines[i]='$A280,') or (Form1.Memo1.Lines[i]='$A300,') or (Form1.Memo1.Lines[i]='$A380,') or (Form1.Memo1.Lines[i]='$A400,') or (Form1.Memo1.Lines[i]='$A480,') or (Form1.Memo1.Lines[i]='$A500,') or (Form1.Memo1.Lines[i]='$A580,') or (Form1.Memo1.Lines[i]='$A600,') or (Form1.Memo1.Lines[i]='$A680,') or (Form1.Memo1.Lines[i]='$A700,') or (Form1.Memo1.Lines[i]='$A780,') or (Form1.Memo1.Lines[i]='$A800,') or (Form1.Memo1.Lines[i]='$A880,') or (Form1.Memo1.Lines[i]='$A900,') or (Form1.Memo1.Lines[i]='$A980,') or (Form1.Memo1.Lines[i]='$AA00,') or (Form1.Memo1.Lines[i]='$AA80,') or (Form1.Memo1.Lines[i]='$AB00,') or (Form1.Memo1.Lines[i]='$AB80,') or (Form1.Memo1.Lines[i]='$AC00,') or (Form1.Memo1.Lines[i]='$AC80,') or (Form1.Memo1.Lines[i]='$AD00,') or (Form1.Memo1.Lines[i]='$AD80,') or (Form1.Memo1.Lines[i]='$AE00,') or
(Form1.Memo1.Lines[i]='$AE80,') or (Form1.Memo1.Lines[i]='$AF00,') or (Form1.Memo1.Lines[i]='$AF80,') then
Form1.Memo1.Lines.Delete(i);
end;
m:=0;
For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
p:=Form1.Memo1.Lines[i];
For n:=1 to Length(p) do
Begin
if p[n]=' ' then
Begin
Delete(p,n,1);
Form1.Memo1.Lines[i]:=p;
end;
end;
end;












end;

i:=-1;
For a:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
For c:=1 to 16 do
Begin
i:=i+1;
m:=Length(Form1.Memo1.Lines[a]);
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=copy(Form1.Memo1.Lines[a],1,2);
Form1.Memo1.Lines[a]:=copy(Form1.Memo1.Lines[a],3,m-2);
Form2.ProgressBar1.Position:=Form2.ProgressBar1.Position+1;
Form2.ProgressBar1.Update;
Form2.ProgressBar2.Position:=Form2.ProgressBar2.Position+1;
Form2.ProgressBar2.Update;
Form2.Label2.Caption:='��������� '+IntToStr(Round(100/Form2.ProgressBar2.Max*Form2.ProgressBar2.Position))+'%';
Form2.Label2.Update;
if RadioButton3.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ����������������� ���';
Form2.Label1.Update;
    prob:=Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1];
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
  end;
if RadioButton2.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ���������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]));
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
if RadioButton1.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � �������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to10do2(IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]))));
    por:=Length(prob);
    if por<>8 then
      Begin
      Repeat
        Begin
        prob:='0'+prob;
        por:=Length(prob);
        end;
      until por=8;
      end;
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
end;
end;
Repeat
Begin
c:=1;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]='' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
c:=0;
end;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]<>'' then
Begin
c:=1;
end;
end;
until c=1;
end;
if Edit9.Text<>'' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount+1;
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,0]:='����������'+IntToStr(Form3.StringGrid1.ColCount-1);
Form3.StringGrid1.ColWidths[Form3.StringGrid1.ColCount-1]:=80;
Form2.Label1.Caption:='�������� �����: '+ExtractFileName(Edit9.Text);
Form2.Label1.Update;
Form2.ProgressBar1.Update;
Form2.Label2.Update;
Form2.ProgressBar1.Position:=0;
Form1.Memo1.Lines.Clear;
Form1.Memo1.Lines.LoadFromFile(Edit9.Text);
if RadioGroup1.ItemIndex=0 then
Begin
Repeat
Begin
ha:=Form1.Memo1.Lines.Count;
if ha<16391 then
Form1.Memo1.Lines.Add(':1000F000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF10');
end;
until ha=16391
end;
if RadioGroup1.ItemIndex=4 then
Begin
Form1.Memo1.Lines.Delete(0);
Form1.Memo1.Lines.Delete(0);
end;
i:=Form1.Memo1.Lines.Count;
if RadioGroup1.ItemIndex=4 then
Begin
if i>134 then
Begin
For i:=134 to Form1.Memo1.Lines.Count do
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
end;
For i:=0 to (Form1.Memo1.Lines.Count) do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 10,l) ;
Form1.Memo1.Lines[i]:=p;
end;
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
For i:=0 to (Form1.Memo1.Lines.Count) or 124 do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 1,l-2) ;
Form1.Memo1.Lines[i]:=p;
end;
end;
if RadioGroup1.ItemIndex=7 then
Begin






For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
if (Form1.Memo1.Lines[i]='$A00,') or (Form1.Memo1.Lines[i]='$A80,') or (Form1.Memo1.Lines[i]='$A100,') or (Form1.Memo1.Lines[i]='$A180,') or (Form1.Memo1.Lines[i]='$A200,') or (Form1.Memo1.Lines[i]='$A280,') or (Form1.Memo1.Lines[i]='$A300,') or (Form1.Memo1.Lines[i]='$A380,') or (Form1.Memo1.Lines[i]='$A400,') or (Form1.Memo1.Lines[i]='$A480,') or (Form1.Memo1.Lines[i]='$A500,') or (Form1.Memo1.Lines[i]='$A580,') or (Form1.Memo1.Lines[i]='$A600,') or (Form1.Memo1.Lines[i]='$A680,') or (Form1.Memo1.Lines[i]='$A700,') or (Form1.Memo1.Lines[i]='$A780,') or (Form1.Memo1.Lines[i]='$A800,') or (Form1.Memo1.Lines[i]='$A880,') or (Form1.Memo1.Lines[i]='$A900,') or (Form1.Memo1.Lines[i]='$A980,') or (Form1.Memo1.Lines[i]='$AA00,') or (Form1.Memo1.Lines[i]='$AA80,') or (Form1.Memo1.Lines[i]='$AB00,') or (Form1.Memo1.Lines[i]='$AB80,') or (Form1.Memo1.Lines[i]='$AC00,') or (Form1.Memo1.Lines[i]='$AC80,') or (Form1.Memo1.Lines[i]='$AD00,') or (Form1.Memo1.Lines[i]='$AD80,') or (Form1.Memo1.Lines[i]='$AE00,') or
(Form1.Memo1.Lines[i]='$AE80,') or (Form1.Memo1.Lines[i]='$AF00,') or (Form1.Memo1.Lines[i]='$AF80,') then
Form1.Memo1.Lines.Delete(i);
end;
m:=0;
For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
p:=Form1.Memo1.Lines[i];
For n:=1 to Length(p) do
Begin
if p[n]=' ' then
Begin
Delete(p,n,1);
Form1.Memo1.Lines[i]:=p;
end;
end;
end;












end;
i:=-1;
For a:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
For c:=1 to 16 do
Begin
i:=i+1;
m:=Length(Form1.Memo1.Lines[a]);
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=copy(Form1.Memo1.Lines[a],1,2);
Form1.Memo1.Lines[a]:=copy(Form1.Memo1.Lines[a],3,m-2);
Form2.ProgressBar1.Position:=Form2.ProgressBar1.Position+1;
Form2.ProgressBar1.Update;
Form2.ProgressBar2.Position:=Form2.ProgressBar2.Position+1;
Form2.ProgressBar2.Update;
Form2.Label2.Caption:='��������� '+IntToStr(Round(100/Form2.ProgressBar2.Max*Form2.ProgressBar2.Position))+'%';
Form2.Label2.Update;
if RadioButton3.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ����������������� ���';
Form2.Label1.Update;
    prob:=Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1];
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
  end;
if RadioButton2.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ���������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]));
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
if RadioButton1.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � �������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to10do2(IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]))));
    por:=Length(prob);
    if por<>8 then
      Begin
      Repeat
        Begin
        prob:='0'+prob;
        por:=Length(prob);
        end;
      until por=8;
      end;
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
end;
end;
Repeat
Begin
c:=1;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]='' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
c:=0;
end;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]<>'' then
Begin
c:=1;
end;
end;
until c=1;
end;
if Edit10.Text<>'' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount+1;
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,0]:='����������'+IntToStr(Form3.StringGrid1.ColCount-1);
Form3.StringGrid1.ColWidths[Form3.StringGrid1.ColCount-1]:=80;
Form2.Label1.Caption:='�������� �����: '+ExtractFileName(Edit10.Text);
Form2.Label1.Update;
Form2.ProgressBar1.Update;
Form2.Label2.Update;
Form2.ProgressBar1.Position:=0;
Form1.Memo1.Lines.Clear;
Form1.Memo1.Lines.LoadFromFile(Edit10.Text);
if RadioGroup1.ItemIndex=0 then
Begin
Repeat
Begin
ha:=Form1.Memo1.Lines.Count;
if ha<16391 then
Form1.Memo1.Lines.Add(':1000F000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF10');
end;
until ha=16391
end;
if RadioGroup1.ItemIndex=4 then
Begin
Form1.Memo1.Lines.Delete(0);
Form1.Memo1.Lines.Delete(0);
end;
i:=Form1.Memo1.Lines.Count;
if RadioGroup1.ItemIndex=4 then
Begin
if i>134 then
Begin
For i:=134 to Form1.Memo1.Lines.Count do
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
end;
For i:=0 to (Form1.Memo1.Lines.Count) do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 10,l) ;
Form1.Memo1.Lines[i]:=p;
end;
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
For i:=0 to (Form1.Memo1.Lines.Count) or 124 do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 1,l-2) ;
Form1.Memo1.Lines[i]:=p;
end;
end;
if RadioGroup1.ItemIndex=7 then
Begin






For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
if (Form1.Memo1.Lines[i]='$A00,') or (Form1.Memo1.Lines[i]='$A80,') or (Form1.Memo1.Lines[i]='$A100,') or (Form1.Memo1.Lines[i]='$A180,') or (Form1.Memo1.Lines[i]='$A200,') or (Form1.Memo1.Lines[i]='$A280,') or (Form1.Memo1.Lines[i]='$A300,') or (Form1.Memo1.Lines[i]='$A380,') or (Form1.Memo1.Lines[i]='$A400,') or (Form1.Memo1.Lines[i]='$A480,') or (Form1.Memo1.Lines[i]='$A500,') or (Form1.Memo1.Lines[i]='$A580,') or (Form1.Memo1.Lines[i]='$A600,') or (Form1.Memo1.Lines[i]='$A680,') or (Form1.Memo1.Lines[i]='$A700,') or (Form1.Memo1.Lines[i]='$A780,') or (Form1.Memo1.Lines[i]='$A800,') or (Form1.Memo1.Lines[i]='$A880,') or (Form1.Memo1.Lines[i]='$A900,') or (Form1.Memo1.Lines[i]='$A980,') or (Form1.Memo1.Lines[i]='$AA00,') or (Form1.Memo1.Lines[i]='$AA80,') or (Form1.Memo1.Lines[i]='$AB00,') or (Form1.Memo1.Lines[i]='$AB80,') or (Form1.Memo1.Lines[i]='$AC00,') or (Form1.Memo1.Lines[i]='$AC80,') or (Form1.Memo1.Lines[i]='$AD00,') or (Form1.Memo1.Lines[i]='$AD80,') or (Form1.Memo1.Lines[i]='$AE00,') or
(Form1.Memo1.Lines[i]='$AE80,') or (Form1.Memo1.Lines[i]='$AF00,') or (Form1.Memo1.Lines[i]='$AF80,') then
Form1.Memo1.Lines.Delete(i);
end;
m:=0;
For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
p:=Form1.Memo1.Lines[i];
For n:=1 to Length(p) do
Begin
if p[n]=' ' then
Begin
Delete(p,n,1);
Form1.Memo1.Lines[i]:=p;
end;
end;
end;












end;

i:=-1;
For a:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
For c:=1 to 16 do
Begin
i:=i+1;
m:=Length(Form1.Memo1.Lines[a]);
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=copy(Form1.Memo1.Lines[a],1,2);
Form1.Memo1.Lines[a]:=copy(Form1.Memo1.Lines[a],3,m-2);
Form2.ProgressBar1.Position:=Form2.ProgressBar1.Position+1;
Form2.ProgressBar1.Update;
Form2.ProgressBar2.Position:=Form2.ProgressBar2.Position+1;
Form2.ProgressBar2.Update;
Form2.Label2.Caption:='��������� '+IntToStr(Round(100/Form2.ProgressBar2.Max*Form2.ProgressBar2.Position))+'%';
Form2.Label2.Update;
if RadioButton3.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ����������������� ���';
Form2.Label1.Update;
    prob:=Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1];
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
  end;
if RadioButton2.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ���������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]));
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
if RadioButton1.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � �������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to10do2(IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]))));
    por:=Length(prob);
    if por<>8 then
      Begin
      Repeat
        Begin
        prob:='0'+prob;
        por:=Length(prob);
        end;
      until por=8;
      end;
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
end;
end;
Repeat
Begin
c:=1;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]='' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
c:=0;
end;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]<>'' then
Begin
c:=1;
end;
end;
until c=1;
end;
if Edit11.Text<>'' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount+1;
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,0]:='����������'+IntToStr(Form3.StringGrid1.ColCount-1);
Form3.StringGrid1.ColWidths[Form3.StringGrid1.ColCount-1]:=80;
Form2.Label1.Caption:='�������� �����: '+ExtractFileName(Edit11.Text);
Form2.Label1.Update;
Form2.ProgressBar1.Update;
Form2.Label2.Update;
Form2.ProgressBar1.Position:=0;
Form1.Memo1.Lines.Clear;
Form1.Memo1.Lines.LoadFromFile(Edit11.Text);
if RadioGroup1.ItemIndex=0 then
Begin
Repeat
Begin
ha:=Form1.Memo1.Lines.Count;
if ha<16391 then
Form1.Memo1.Lines.Add(':1000F000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF10');
end;
until ha=16391
end;
if RadioGroup1.ItemIndex=4 then
Begin
Form1.Memo1.Lines.Delete(0);
Form1.Memo1.Lines.Delete(0);
end;
i:=Form1.Memo1.Lines.Count;
if RadioGroup1.ItemIndex=4 then
Begin
if i>134 then
Begin
For i:=134 to Form1.Memo1.Lines.Count do
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
end;
For i:=0 to (Form1.Memo1.Lines.Count) do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 10,l) ;
Form1.Memo1.Lines[i]:=p;
end;
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
For i:=0 to (Form1.Memo1.Lines.Count) or 124 do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 1,l-2) ;
Form1.Memo1.Lines[i]:=p;
end;
end;
if RadioGroup1.ItemIndex=7 then
Begin






For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
if (Form1.Memo1.Lines[i]='$A00,') or (Form1.Memo1.Lines[i]='$A80,') or (Form1.Memo1.Lines[i]='$A100,') or (Form1.Memo1.Lines[i]='$A180,') or (Form1.Memo1.Lines[i]='$A200,') or (Form1.Memo1.Lines[i]='$A280,') or (Form1.Memo1.Lines[i]='$A300,') or (Form1.Memo1.Lines[i]='$A380,') or (Form1.Memo1.Lines[i]='$A400,') or (Form1.Memo1.Lines[i]='$A480,') or (Form1.Memo1.Lines[i]='$A500,') or (Form1.Memo1.Lines[i]='$A580,') or (Form1.Memo1.Lines[i]='$A600,') or (Form1.Memo1.Lines[i]='$A680,') or (Form1.Memo1.Lines[i]='$A700,') or (Form1.Memo1.Lines[i]='$A780,') or (Form1.Memo1.Lines[i]='$A800,') or (Form1.Memo1.Lines[i]='$A880,') or (Form1.Memo1.Lines[i]='$A900,') or (Form1.Memo1.Lines[i]='$A980,') or (Form1.Memo1.Lines[i]='$AA00,') or (Form1.Memo1.Lines[i]='$AA80,') or (Form1.Memo1.Lines[i]='$AB00,') or (Form1.Memo1.Lines[i]='$AB80,') or (Form1.Memo1.Lines[i]='$AC00,') or (Form1.Memo1.Lines[i]='$AC80,') or (Form1.Memo1.Lines[i]='$AD00,') or (Form1.Memo1.Lines[i]='$AD80,') or (Form1.Memo1.Lines[i]='$AE00,') or
(Form1.Memo1.Lines[i]='$AE80,') or (Form1.Memo1.Lines[i]='$AF00,') or (Form1.Memo1.Lines[i]='$AF80,') then
Form1.Memo1.Lines.Delete(i);
end;
m:=0;
For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
p:=Form1.Memo1.Lines[i];
For n:=1 to Length(p) do
Begin
if p[n]=' ' then
Begin
Delete(p,n,1);
Form1.Memo1.Lines[i]:=p;
end;
end;
end;












end;

i:=-1;
For a:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
For c:=1 to 16 do
Begin
i:=i+1;
m:=Length(Form1.Memo1.Lines[a]);
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=copy(Form1.Memo1.Lines[a],1,2);
Form1.Memo1.Lines[a]:=copy(Form1.Memo1.Lines[a],3,m-2);
Form2.ProgressBar1.Position:=Form2.ProgressBar1.Position+1;
Form2.ProgressBar1.Update;
Form2.ProgressBar2.Position:=Form2.ProgressBar2.Position+1;
Form2.ProgressBar2.Update;
Form2.Label2.Caption:='��������� '+IntToStr(Round(100/Form2.ProgressBar2.Max*Form2.ProgressBar2.Position))+'%';
Form2.Label2.Update;
if RadioButton3.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ����������������� ���';
Form2.Label1.Update;
    prob:=Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1];
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
  end;
if RadioButton2.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ���������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]));
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
if RadioButton1.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � �������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to10do2(IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]))));
    por:=Length(prob);
    if por<>8 then
      Begin
      Repeat
        Begin
        prob:='0'+prob;
        por:=Length(prob);
        end;
      until por=8;
      end;
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
end;
end;
Repeat
Begin
c:=1;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]='' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
c:=0;
end;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]<>'' then
Begin
c:=1;
end;
end;
until c=1;
end;
if Edit12.Text<>'' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount+1;
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,0]:='����������'+IntToStr(Form3.StringGrid1.ColCount-1);
Form3.StringGrid1.ColWidths[Form3.StringGrid1.ColCount-1]:=80;
Form2.Label1.Caption:='�������� �����: '+ExtractFileName(Edit12.Text);
Form2.Label1.Update;
Form2.ProgressBar1.Update;
Form2.Label2.Update;
Form2.ProgressBar1.Position:=0;
Form1.Memo1.Lines.Clear;
Form1.Memo1.Lines.LoadFromFile(Edit12.Text);
if RadioGroup1.ItemIndex=0 then
Begin
Repeat
Begin
ha:=Form1.Memo1.Lines.Count;
if ha<16391 then
Form1.Memo1.Lines.Add(':1000F000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF10');
end;
until ha=16391
end;
if RadioGroup1.ItemIndex=4 then
Begin
Form1.Memo1.Lines.Delete(0);
Form1.Memo1.Lines.Delete(0);
end;
i:=Form1.Memo1.Lines.Count;
if RadioGroup1.ItemIndex=4 then
Begin
if i>134 then
Begin
For i:=134 to Form1.Memo1.Lines.Count do
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
end;
For i:=0 to (Form1.Memo1.Lines.Count) do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 10,l) ;
Form1.Memo1.Lines[i]:=p;
end;
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
For i:=0 to (Form1.Memo1.Lines.Count) or 124 do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 1,l-2) ;
Form1.Memo1.Lines[i]:=p;
end;
end;
if RadioGroup1.ItemIndex=7 then
Begin






For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
if (Form1.Memo1.Lines[i]='$A00,') or (Form1.Memo1.Lines[i]='$A80,') or (Form1.Memo1.Lines[i]='$A100,') or (Form1.Memo1.Lines[i]='$A180,') or (Form1.Memo1.Lines[i]='$A200,') or (Form1.Memo1.Lines[i]='$A280,') or (Form1.Memo1.Lines[i]='$A300,') or (Form1.Memo1.Lines[i]='$A380,') or (Form1.Memo1.Lines[i]='$A400,') or (Form1.Memo1.Lines[i]='$A480,') or (Form1.Memo1.Lines[i]='$A500,') or (Form1.Memo1.Lines[i]='$A580,') or (Form1.Memo1.Lines[i]='$A600,') or (Form1.Memo1.Lines[i]='$A680,') or (Form1.Memo1.Lines[i]='$A700,') or (Form1.Memo1.Lines[i]='$A780,') or (Form1.Memo1.Lines[i]='$A800,') or (Form1.Memo1.Lines[i]='$A880,') or (Form1.Memo1.Lines[i]='$A900,') or (Form1.Memo1.Lines[i]='$A980,') or (Form1.Memo1.Lines[i]='$AA00,') or (Form1.Memo1.Lines[i]='$AA80,') or (Form1.Memo1.Lines[i]='$AB00,') or (Form1.Memo1.Lines[i]='$AB80,') or (Form1.Memo1.Lines[i]='$AC00,') or (Form1.Memo1.Lines[i]='$AC80,') or (Form1.Memo1.Lines[i]='$AD00,') or (Form1.Memo1.Lines[i]='$AD80,') or (Form1.Memo1.Lines[i]='$AE00,') or
(Form1.Memo1.Lines[i]='$AE80,') or (Form1.Memo1.Lines[i]='$AF00,') or (Form1.Memo1.Lines[i]='$AF80,') then
Form1.Memo1.Lines.Delete(i);
end;
m:=0;
For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
p:=Form1.Memo1.Lines[i];
For n:=1 to Length(p) do
Begin
if p[n]=' ' then
Begin
Delete(p,n,1);
Form1.Memo1.Lines[i]:=p;
end;
end;
end;












end;
i:=-1;
For a:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
For c:=1 to 16 do
Begin
i:=i+1;
m:=Length(Form1.Memo1.Lines[a]);
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=copy(Form1.Memo1.Lines[a],1,2);
Form1.Memo1.Lines[a]:=copy(Form1.Memo1.Lines[a],3,m-2);
Form2.ProgressBar1.Position:=Form2.ProgressBar1.Position+1;
Form2.ProgressBar1.Update;
Form2.ProgressBar2.Position:=Form2.ProgressBar2.Position+1;
Form2.ProgressBar2.Update;
Form2.Label2.Caption:='��������� '+IntToStr(Round(100/Form2.ProgressBar2.Max*Form2.ProgressBar2.Position))+'%';
Form2.Label2.Update;
if RadioButton3.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ����������������� ���';
Form2.Label1.Update;
    prob:=Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1];
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
  end;
if RadioButton2.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ���������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]));
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
if RadioButton1.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � �������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to10do2(IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]))));
    por:=Length(prob);
    if por<>8 then
      Begin
      Repeat
        Begin
        prob:='0'+prob;
        por:=Length(prob);
        end;
      until por=8;
      end;
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
end;
end;
Repeat
Begin
c:=1;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]='' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
c:=0;
end;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]<>'' then
Begin
c:=1;
end;
end;
until c=1;
end;
if Edit13.Text<>'' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount+1;
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,0]:='����������'+IntToStr(Form3.StringGrid1.ColCount-1);
Form3.StringGrid1.ColWidths[Form3.StringGrid1.ColCount-1]:=80;
Form2.Label1.Caption:='�������� �����: '+ExtractFileName(Edit13.Text);
Form2.Label1.Update;
Form2.ProgressBar1.Update;
Form2.Label2.Update;
Form2.ProgressBar1.Position:=0;
Form1.Memo1.Lines.Clear;
Form1.Memo1.Lines.LoadFromFile(Edit13.Text);
if RadioGroup1.ItemIndex=0 then
Begin
Repeat
Begin
ha:=Form1.Memo1.Lines.Count;
if ha<16391 then
Form1.Memo1.Lines.Add(':1000F000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF10');
end;
until ha=16391
end;
if RadioGroup1.ItemIndex=4 then
Begin
Form1.Memo1.Lines.Delete(0);
Form1.Memo1.Lines.Delete(0);
end;
i:=Form1.Memo1.Lines.Count;
if RadioGroup1.ItemIndex=4 then
Begin
if i>134 then
Begin
For i:=134 to Form1.Memo1.Lines.Count do
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
end;
For i:=0 to (Form1.Memo1.Lines.Count) do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 10,l) ;
Form1.Memo1.Lines[i]:=p;
end;
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
For i:=0 to (Form1.Memo1.Lines.Count) or 124 do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 1,l-2) ;
Form1.Memo1.Lines[i]:=p;
end;
end;
if RadioGroup1.ItemIndex=7 then
Begin






For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
if (Form1.Memo1.Lines[i]='$A00,') or (Form1.Memo1.Lines[i]='$A80,') or (Form1.Memo1.Lines[i]='$A100,') or (Form1.Memo1.Lines[i]='$A180,') or (Form1.Memo1.Lines[i]='$A200,') or (Form1.Memo1.Lines[i]='$A280,') or (Form1.Memo1.Lines[i]='$A300,') or (Form1.Memo1.Lines[i]='$A380,') or (Form1.Memo1.Lines[i]='$A400,') or (Form1.Memo1.Lines[i]='$A480,') or (Form1.Memo1.Lines[i]='$A500,') or (Form1.Memo1.Lines[i]='$A580,') or (Form1.Memo1.Lines[i]='$A600,') or (Form1.Memo1.Lines[i]='$A680,') or (Form1.Memo1.Lines[i]='$A700,') or (Form1.Memo1.Lines[i]='$A780,') or (Form1.Memo1.Lines[i]='$A800,') or (Form1.Memo1.Lines[i]='$A880,') or (Form1.Memo1.Lines[i]='$A900,') or (Form1.Memo1.Lines[i]='$A980,') or (Form1.Memo1.Lines[i]='$AA00,') or (Form1.Memo1.Lines[i]='$AA80,') or (Form1.Memo1.Lines[i]='$AB00,') or (Form1.Memo1.Lines[i]='$AB80,') or (Form1.Memo1.Lines[i]='$AC00,') or (Form1.Memo1.Lines[i]='$AC80,') or (Form1.Memo1.Lines[i]='$AD00,') or (Form1.Memo1.Lines[i]='$AD80,') or (Form1.Memo1.Lines[i]='$AE00,') or
(Form1.Memo1.Lines[i]='$AE80,') or (Form1.Memo1.Lines[i]='$AF00,') or (Form1.Memo1.Lines[i]='$AF80,') then
Form1.Memo1.Lines.Delete(i);
end;
m:=0;
For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
p:=Form1.Memo1.Lines[i];
For n:=1 to Length(p) do
Begin
if p[n]=' ' then
Begin
Delete(p,n,1);
Form1.Memo1.Lines[i]:=p;
end;
end;
end;












end;
i:=-1;
For a:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
For c:=1 to 16 do
Begin
i:=i+1;
m:=Length(Form1.Memo1.Lines[a]);
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=copy(Form1.Memo1.Lines[a],1,2);
Form1.Memo1.Lines[a]:=copy(Form1.Memo1.Lines[a],3,m-2);
Form2.ProgressBar1.Position:=Form2.ProgressBar1.Position+1;
Form2.ProgressBar1.Update;
Form2.ProgressBar2.Position:=Form2.ProgressBar2.Position+1;
Form2.ProgressBar2.Update;
Form2.Label2.Caption:='��������� '+IntToStr(Round(100/Form2.ProgressBar2.Max*Form2.ProgressBar2.Position))+'%';
Form2.Label2.Update;
if RadioButton3.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ����������������� ���';
Form2.Label1.Update;
    prob:=Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1];
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
  end;
if RadioButton2.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ���������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]));
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
if RadioButton1.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � �������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to10do2(IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]))));
    por:=Length(prob);
    if por<>8 then
      Begin
      Repeat
        Begin
        prob:='0'+prob;
        por:=Length(prob);
        end;
      until por=8;
      end;
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
end;
end;
Repeat
Begin
c:=1;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]='' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
c:=0;
end;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]<>'' then
Begin
c:=1;
end;
end;
until c=1;
end;











if Edit14.Text<>'' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount+1;
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,0]:='����������'+IntToStr(Form3.StringGrid1.ColCount-1);
Form3.StringGrid1.ColWidths[Form3.StringGrid1.ColCount-1]:=80;
Form2.Label1.Caption:='�������� �����: '+ExtractFileName(Edit14.Text);
Form2.Label1.Update;
Form2.ProgressBar1.Update;
Form2.Label2.Update;
Form2.ProgressBar1.Position:=0;
Form1.Memo1.Lines.Clear;
Form1.Memo1.Lines.LoadFromFile(Edit14.Text);
if RadioGroup1.ItemIndex=0 then
Begin
Repeat
Begin
ha:=Form1.Memo1.Lines.Count;
if ha<16391 then
Form1.Memo1.Lines.Add(':1000F000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF10');
end;
until ha=16391
end;
if RadioGroup1.ItemIndex=4 then
Begin
Form1.Memo1.Lines.Delete(0);
Form1.Memo1.Lines.Delete(0);
end;
i:=Form1.Memo1.Lines.Count;
if RadioGroup1.ItemIndex=4 then
Begin
if i>134 then
Begin
For i:=134 to Form1.Memo1.Lines.Count do
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
end;
For i:=0 to (Form1.Memo1.Lines.Count) do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 10,l) ;
Form1.Memo1.Lines[i]:=p;
end;
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
For i:=0 to (Form1.Memo1.Lines.Count) or 124 do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 1,l-2) ;
Form1.Memo1.Lines[i]:=p;
end;
end;
if RadioGroup1.ItemIndex=7 then
Begin






For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
if (Form1.Memo1.Lines[i]='$A00,') or (Form1.Memo1.Lines[i]='$A80,') or (Form1.Memo1.Lines[i]='$A100,') or (Form1.Memo1.Lines[i]='$A180,') or (Form1.Memo1.Lines[i]='$A200,') or (Form1.Memo1.Lines[i]='$A280,') or (Form1.Memo1.Lines[i]='$A300,') or (Form1.Memo1.Lines[i]='$A380,') or (Form1.Memo1.Lines[i]='$A400,') or (Form1.Memo1.Lines[i]='$A480,') or (Form1.Memo1.Lines[i]='$A500,') or (Form1.Memo1.Lines[i]='$A580,') or (Form1.Memo1.Lines[i]='$A600,') or (Form1.Memo1.Lines[i]='$A680,') or (Form1.Memo1.Lines[i]='$A700,') or (Form1.Memo1.Lines[i]='$A780,') or (Form1.Memo1.Lines[i]='$A800,') or (Form1.Memo1.Lines[i]='$A880,') or (Form1.Memo1.Lines[i]='$A900,') or (Form1.Memo1.Lines[i]='$A980,') or (Form1.Memo1.Lines[i]='$AA00,') or (Form1.Memo1.Lines[i]='$AA80,') or (Form1.Memo1.Lines[i]='$AB00,') or (Form1.Memo1.Lines[i]='$AB80,') or (Form1.Memo1.Lines[i]='$AC00,') or (Form1.Memo1.Lines[i]='$AC80,') or (Form1.Memo1.Lines[i]='$AD00,') or (Form1.Memo1.Lines[i]='$AD80,') or (Form1.Memo1.Lines[i]='$AE00,') or
(Form1.Memo1.Lines[i]='$AE80,') or (Form1.Memo1.Lines[i]='$AF00,') or (Form1.Memo1.Lines[i]='$AF80,') then
Form1.Memo1.Lines.Delete(i);
end;
m:=0;
For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
p:=Form1.Memo1.Lines[i];
For n:=1 to Length(p) do
Begin
if p[n]=' ' then
Begin
Delete(p,n,1);
Form1.Memo1.Lines[i]:=p;
end;
end;
end;












end;
i:=-1;
For a:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
For c:=1 to 16 do
Begin
i:=i+1;
m:=Length(Form1.Memo1.Lines[a]);
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=copy(Form1.Memo1.Lines[a],1,2);
Form1.Memo1.Lines[a]:=copy(Form1.Memo1.Lines[a],3,m-2);
Form2.ProgressBar1.Position:=Form2.ProgressBar1.Position+1;
Form2.ProgressBar1.Update;
Form2.ProgressBar2.Position:=Form2.ProgressBar2.Position+1;
Form2.ProgressBar2.Update;
Form2.Label2.Caption:='��������� '+IntToStr(Round(100/Form2.ProgressBar2.Max*Form2.ProgressBar2.Position))+'%';
Form2.Label2.Update;
if RadioButton3.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ����������������� ���';
Form2.Label1.Update;
    prob:=Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1];
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
  end;
if RadioButton2.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ���������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]));
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
if RadioButton1.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � �������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to10do2(IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]))));
    por:=Length(prob);
    if por<>8 then
      Begin
      Repeat
        Begin
        prob:='0'+prob;
        por:=Length(prob);
        end;
      until por=8;
      end;
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
end;
end;
Repeat
Begin
c:=1;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]='' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
c:=0;
end;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]<>'' then
Begin
c:=1;
end;
end;
until c=1;
end;
if Edit15.Text<>'' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount+1;
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,0]:='����������'+IntToStr(Form3.StringGrid1.ColCount-1);
Form3.StringGrid1.ColWidths[Form3.StringGrid1.ColCount-1]:=80;
Form2.Label1.Caption:='�������� �����: '+ExtractFileName(Edit15.Text);
Form2.Label1.Update;
Form2.ProgressBar1.Update;
Form2.Label2.Update;
Form2.ProgressBar1.Position:=0;
Form1.Memo1.Lines.Clear;
Form1.Memo1.Lines.LoadFromFile(Edit15.Text);
if RadioGroup1.ItemIndex=0 then
Begin
Repeat
Begin
ha:=Form1.Memo1.Lines.Count;
if ha<16391 then
Form1.Memo1.Lines.Add(':1000F000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF10');
end;
until ha=16391
end;
if RadioGroup1.ItemIndex=4 then
Begin
Form1.Memo1.Lines.Delete(0);
Form1.Memo1.Lines.Delete(0);
end;
i:=Form1.Memo1.Lines.Count;
if RadioGroup1.ItemIndex=4 then
Begin
if i>134 then
Begin
For i:=134 to Form1.Memo1.Lines.Count do
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
end;
For i:=0 to (Form1.Memo1.Lines.Count) do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 10,l) ;
Form1.Memo1.Lines[i]:=p;
end;
Form1.Memo1.Lines.Delete(Form1.Memo1.Lines.Count-1);
For i:=0 to (Form1.Memo1.Lines.Count) or 124 do
Begin
l:=Length(Form1.Memo1.Lines[i]);
p:=copy(Form1.Memo1.Lines[i], 1,l-2) ;
Form1.Memo1.Lines[i]:=p;
end;
end;
if RadioGroup1.ItemIndex=7 then
Begin






For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
if (Form1.Memo1.Lines[i]='$A00,') or (Form1.Memo1.Lines[i]='$A80,') or (Form1.Memo1.Lines[i]='$A100,') or (Form1.Memo1.Lines[i]='$A180,') or (Form1.Memo1.Lines[i]='$A200,') or (Form1.Memo1.Lines[i]='$A280,') or (Form1.Memo1.Lines[i]='$A300,') or (Form1.Memo1.Lines[i]='$A380,') or (Form1.Memo1.Lines[i]='$A400,') or (Form1.Memo1.Lines[i]='$A480,') or (Form1.Memo1.Lines[i]='$A500,') or (Form1.Memo1.Lines[i]='$A580,') or (Form1.Memo1.Lines[i]='$A600,') or (Form1.Memo1.Lines[i]='$A680,') or (Form1.Memo1.Lines[i]='$A700,') or (Form1.Memo1.Lines[i]='$A780,') or (Form1.Memo1.Lines[i]='$A800,') or (Form1.Memo1.Lines[i]='$A880,') or (Form1.Memo1.Lines[i]='$A900,') or (Form1.Memo1.Lines[i]='$A980,') or (Form1.Memo1.Lines[i]='$AA00,') or (Form1.Memo1.Lines[i]='$AA80,') or (Form1.Memo1.Lines[i]='$AB00,') or (Form1.Memo1.Lines[i]='$AB80,') or (Form1.Memo1.Lines[i]='$AC00,') or (Form1.Memo1.Lines[i]='$AC80,') or (Form1.Memo1.Lines[i]='$AD00,') or (Form1.Memo1.Lines[i]='$AD80,') or (Form1.Memo1.Lines[i]='$AE00,') or
(Form1.Memo1.Lines[i]='$AE80,') or (Form1.Memo1.Lines[i]='$AF00,') or (Form1.Memo1.Lines[i]='$AF80,') then
Form1.Memo1.Lines.Delete(i);
end;
m:=0;
For i:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
p:=Form1.Memo1.Lines[i];
For n:=1 to Length(p) do
Begin
if p[n]=' ' then
Begin
Delete(p,n,1);
Form1.Memo1.Lines[i]:=p;
end;
end;
end;












end;
i:=-1;
For a:=0 to Form1.Memo1.Lines.Count or 124 do
Begin
For c:=1 to 16 do
Begin
i:=i+1;
m:=Length(Form1.Memo1.Lines[a]);
Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=copy(Form1.Memo1.Lines[a],1,2);
Form1.Memo1.Lines[a]:=copy(Form1.Memo1.Lines[a],3,m-2);
Form2.ProgressBar1.Position:=Form2.ProgressBar1.Position+1;
Form2.ProgressBar1.Update;
Form2.ProgressBar2.Position:=Form2.ProgressBar2.Position+1;
Form2.ProgressBar2.Update;
Form2.Label2.Caption:='��������� '+IntToStr(Round(100/Form2.ProgressBar2.Max*Form2.ProgressBar2.Position))+'%';
Form2.Label2.Update;
if RadioButton3.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ����������������� ���';
Form2.Label1.Update;
    prob:=Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1];
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
  end;
if RadioButton2.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � ���������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]));
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
if RadioButton1.Checked=true then
  Begin
    Form2.Label1.Caption:='�������������� ����� � �������� ���';
Form2.Label1.Update;
    prob:=IntToStr(to10do2(IntToStr(to16do10(Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]))));
    por:=Length(prob);
    if por<>8 then
      Begin
      Repeat
        Begin
        prob:='0'+prob;
        por:=Length(prob);
        end;
      until por=8;
      end;
    Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,i+1]:=prob;
    end;
end;
end;
Repeat
Begin
c:=1;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]='' then
Begin
Form3.StringGrid1.ColCount:=Form3.StringGrid1.ColCount-1;
c:=0;
end;
if Form3.StringGrid1.Cells[Form3.StringGrid1.ColCount-1,Form3.StringGrid1.ColCount]<>'' then
Begin
c:=1;
end;
end;
until c=1;
end;
if Label1.Caption<>'0' then
Begin
Form1.Enabled:=true;
Form2.Visible:=false;
if RadioGroup1.ItemIndex=4 then ////////
Begin
Form2.Label2.Caption:='���������� ��������';
Form2.Label2.Update;
Form3.StringGrid1.RowCount:=2001;
Form2.ProgressBar2.Max:=Form3.StringGrid1.ColCount;
For a:=1 to Form3.StringGrid1.ColCount do
  Begin
Form2.ProgressBar2.Position:=Form2.ProgressBar2.Position+1;
Form2.ProgressBar2.Update;
For i:=1 to Form3.StringGrid1.RowCount do
  Begin
Form3.StringGrid1.Cells[a,i]:=Form3.StringGrid1.Cells[a,i+16];
  end;
  end;
end;
if RadioGroup1.ItemIndex=7 then ////////
Begin
Form2.Label2.Caption:='���������� ��������';
Form2.Label2.Update;
Form3.StringGrid1.RowCount:=2001;
Form2.ProgressBar2.Max:=Form3.StringGrid1.ColCount;
For a:=1 to Form3.StringGrid1.ColCount do
  Begin
Form2.ProgressBar2.Position:=Form2.ProgressBar2.Position+1;
Form2.ProgressBar2.Update;
For i:=1 to Form3.StringGrid1.RowCount do
  Begin
Form3.StringGrid1.Cells[a,i]:=Form3.StringGrid1.Cells[a,i];
  end;
  end;
  end;
end;
end;
if RadioButton1.Checked=true then
Form3.Label1.Caption:='2';
if RadioButton2.Checked=true then
Form3.Label1.Caption:='10';
if RadioButton3.Checked=true then
Form3.Label1.Caption:='16';
end;


procedure TForm13.RadioButton3Click(Sender: TObject);
begin
Form3.Label1.Caption:='16';
end;

procedure TForm13.RadioButton2Click(Sender: TObject);
begin
Form3.Label1.Caption:='10';
end;

procedure TForm13.RadioButton1Click(Sender: TObject);
begin
Form3.Label1.Caption:='2';
end;

procedure TForm13.Button52Click(Sender: TObject);
begin
Form13.Close;
end;

procedure TForm13.Button53Click(Sender: TObject);
begin
Form13.Edit15.Visible:=false;
Form13.Edit14.Visible:=false;
Form13.Edit13.Visible:=false;
Form13.Edit12.Visible:=false;
Form13.Edit11.Visible:=false;
Form13.Edit10.Visible:=false;
Form13.Edit9.Visible:=false;
Form13.Edit8.Visible:=false;
Form13.Edit7.Visible:=false;
Form13.Edit6.Visible:=false;
Form13.Edit5.Visible:=false;
Form13.Edit4.Visible:=false;
Form13.Edit3.Visible:=false;
Form13.Edit2.Visible:=false;
Form13.Edit1.Text:='';
Form13.Edit2.Text:='';
Form13.Edit3.Text:='';
Form13.Edit4.Text:='';
Form13.Edit5.Text:='';
Form13.Edit6.Text:='';
Form13.Edit7.Text:='';
Form13.Edit8.Text:='';
Form13.Edit9.Text:='';
Form13.Edit10.Text:='';
Form13.Edit11.Text:='';
Form13.Edit12.Text:='';
Form13.Edit13.Text:='';
Form13.Edit14.Text:='';
Form13.Edit15.Text:='';
Form13.Label1.Caption:='0';
Form13.Label2.Caption:='Label2';
Form13.Edit51.Text:='Edit51';
Form13.Button15.Visible:=false;
Form13.Button14.Visible:=false;
Form13.Button13.Visible:=false;
Form13.Button12.Visible:=false;
Form13.Button11.Visible:=false;
Form13.Button10.Visible:=false;
Form13.Button9.Visible:=false;
Form13.Button8.Visible:=false;
Form13.Button7.Visible:=false;
Form13.Button6.Visible:=false;
Form13.Button5.Visible:=false;
Form13.Button4.Visible:=false;
Form13.Button3.Visible:=false;
Form13.Button2.Visible:=false;
end;

end.
