object Form15: TForm15
  Left = 288
  Top = 202
  BorderStyle = bsNone
  Caption = 'Form15'
  ClientHeight = 150
  ClientWidth = 300
  Color = clYellow
  TransparentColorValue = clNavy
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnMouseMove = FormMouseMove
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 288
    Top = 0
    Width = 9
    Height = 13
    Alignment = taCenter
    Caption = 'X'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = False
    OnClick = Label1Click
    OnMouseMove = Label1MouseMove
  end
  object Image1: TImage
    Left = 0
    Top = 16
    Width = 305
    Height = 137
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 20
    OnTimer = Timer1Timer
    Left = 96
    Top = 56
  end
end
