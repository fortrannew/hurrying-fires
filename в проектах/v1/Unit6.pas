unit Unit6;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TForm6 = class(TForm)
    Panel1: TPanel;
    Shape32: TShape;
    Shape24: TShape;
    Shape16: TShape;
    Shape8: TShape;
    Shape31: TShape;
    Shape23: TShape;
    Shape15: TShape;
    Shape7: TShape;
    Shape30: TShape;
    Shape22: TShape;
    Shape14: TShape;
    Shape6: TShape;
    Shape29: TShape;
    Shape21: TShape;
    Shape13: TShape;
    Shape5: TShape;
    Shape28: TShape;
    Shape20: TShape;
    Shape12: TShape;
    Shape4: TShape;
    Shape27: TShape;
    Shape19: TShape;
    Shape11: TShape;
    Shape3: TShape;
    Shape26: TShape;
    Shape18: TShape;
    Shape10: TShape;
    Shape2: TShape;
    Shape25: TShape;
    Shape17: TShape;
    Shape9: TShape;
    Shape1: TShape;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormPaint(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form6: TForm6;

implementation

uses Unit1;

{$R *.dfm}

procedure TForm6.FormClose(Sender: TObject; var Action: TCloseAction);
begin
Form1.N5.Checked:=false;
end;

procedure TForm6.FormPaint(Sender: TObject);
begin
Form1.N5.Checked:=true;
end;

procedure TForm6.FormResize(Sender: TObject);
begin
if Form6.ClientHeight<Form6.ClientWidth then
Begin
Panel1.Height:=Form6.ClientWidth;;
Panel1.Width:=Form6.ClientWidth;;
end
else
Begin
Panel1.Height:=Form6.ClientHeight;
Panel1.Width:=Form6.ClientHeight;
end;
Panel1.Left:=Round(Form6.ClientWidth/2)-Round(Panel1.Width/2);
Panel1.Top:=Round(Form6.ClientHeight/2)-Round(Panel1.Height/2);
Shape1.Height:=round(Panel1.Height/8);
Shape1.Width:=round(Panel1.Width/8);
Shape2.Left:=Shape1.Height+Shape1.Left;
Shape2.Height:=round(Panel1.Height/8);
Shape2.Width:=round(Panel1.Width/8);
Shape3.Left:=Shape1.Height+Shape2.Height+Shape1.Left;
Shape3.Height:=round(Panel1.Height/8);
Shape3.Width:=round(Panel1.Width/8);
Shape4.Left:=Shape1.Left+Shape1.Height+Shape2.Height+Shape3.Height;
Shape4.Height:=round(Panel1.Height/8);
Shape4.Width:=round(Panel1.Width/8);
Shape5.Left:=Shape1.Left+Shape1.Height+Shape2.Height+Shape3.Height+Shape4.Height;
Shape5.Height:=round(Panel1.Height/8);
Shape5.Width:=round(Panel1.Width/8);
Shape6.Left:=Shape1.Left+Shape1.Height+Shape2.Height+Shape3.Height+Shape4.Height+Shape5.Height;Shape6.Height:=round(Panel1.Height/8);
Shape6.Width:=round(Panel1.Width/8);
Shape7.Left:=Shape1.Left+Shape1.Height+Shape2.Height+Shape3.Height+Shape4.Height+Shape5.Height+Shape6.Height;Shape7.Height:=round(Panel1.Height/8);
Shape7.Width:=round(Panel1.Width/8);
Shape8.Left:=Shape1.Left+Shape1.Height+Shape2.Height+Shape3.Height+Shape4.Height+Shape5.Height+Shape6.Height+Shape7.Height;Shape8.Height:=round(Panel1.Height/8);
Shape8.Width:=round(Panel1.Width/8);
Shape9.Top:=Shape1.Height;
Shape9.Height:=round(Panel1.Height/8);
Shape9.Width:=round(Panel1.Width/8);
Shape10.Top:=Shape2.Height;
Shape10.Left:=Shape1.Height+Shape1.Left;
Shape10.Height:=round(Panel1.Height/8);
Shape10.Width:=round(Panel1.Width/8);
Shape11.Top:=Shape3.Height;
Shape11.Left:=Shape1.Height+Shape2.Height+Shape1.Left;
Shape11.Height:=round(Panel1.Height/8);
Shape11.Width:=round(Panel1.Width/8);
Shape12.Top:=Shape4.Height;
Shape12.Left:=Shape1.Left+Shape1.Height+Shape2.Height+Shape3.Height;
Shape12.Height:=round(Panel1.Height/8);
Shape12.Width:=round(Panel1.Width/8);
Shape13.Top:=Shape5.Height;
Shape13.Left:=Shape1.Left+Shape1.Height+Shape2.Height+Shape3.Height+Shape4.Height;
Shape13.Height:=round(Panel1.Height/8);
Shape13.Width:=round(Panel1.Width/8);
Shape14.Top:=Shape6.Height;
Shape14.Left:=Shape1.Left+Shape1.Height+Shape2.Height+Shape3.Height+Shape4.Height+Shape5.Height;Shape14.Height:=round(Panel1.Height/8);
Shape14.Width:=round(Panel1.Width/8);
Shape15.Top:=Shape7.Height;
Shape15.Left:=Shape1.Left+Shape1.Height+Shape2.Height+Shape3.Height+Shape4.Height+Shape5.Height+Shape6.Height;Shape15.Height:=round(Panel1.Height/8);
Shape15.Width:=round(Panel1.Width/8);
Shape16.Top:=Shape8.Height;
Shape16.Left:=Shape1.Left+Shape1.Height+Shape2.Height+Shape3.Height+Shape4.Height+Shape5.Height+Shape6.Height+Shape7.Height;Shape16.Height:=round(Panel1.Height/8);
Shape16.Width:=round(Panel1.Width/8);
Shape17.Top:=Shape1.Height+Shape9.Height;
Shape17.Height:=round(Panel1.Height/8);
Shape17.Width:=round(Panel1.Width/8);
Shape18.Top:=Shape2.Height+Shape10.Height;
Shape18.Left:=Shape1.Height+Shape1.Left;
Shape18.Height:=round(Panel1.Height/8);
Shape18.Width:=round(Panel1.Width/8);
Shape19.Top:=Shape3.Height+Shape11.Height;
Shape19.Left:=Shape1.Height+Shape2.Height+Shape1.Left;
Shape19.Height:=round(Panel1.Height/8);
Shape19.Width:=round(Panel1.Width/8);
Shape20.Top:=Shape4.Height+Shape12.Height;
Shape20.Left:=Shape1.Left+Shape1.Height+Shape2.Height+Shape3.Height;
Shape20.Height:=round(Panel1.Height/8);
Shape20.Width:=round(Panel1.Width/8);
Shape21.Top:=Shape5.Height+Shape13.Height;
Shape21.Left:=Shape1.Left+Shape1.Height+Shape2.Height+Shape3.Height+Shape4.Height;
Shape21.Height:=round(Panel1.Height/8);
Shape21.Width:=round(Panel1.Width/8);
Shape22.Top:=Shape6.Height+Shape14.Height;
Shape22.Left:=Shape1.Left+Shape1.Height+Shape2.Height+Shape3.Height+Shape4.Height+Shape5.Height;Shape22.Height:=round(Panel1.Height/8);
Shape22.Width:=round(Panel1.Width/8);
Shape23.Top:=Shape7.Height+Shape15.Height;
Shape23.Left:=Shape1.Left+Shape1.Height+Shape2.Height+Shape3.Height+Shape4.Height+Shape5.Height+Shape6.Height;Shape23.Height:=round(Panel1.Height/8);
Shape23.Width:=round(Panel1.Width/8);
Shape24.Top:=Shape8.Height+Shape16.Height;
Shape24.Left:=Shape1.Left+Shape1.Height+Shape2.Height+Shape3.Height+Shape4.Height+Shape5.Height+Shape6.Height+Shape7.Height;Shape24.Height:=round(Panel1.Height/8);
Shape24.Width:=round(Panel1.Width/8);
Shape25.Top:=Shape1.Height+Shape9.Height+Shape17.Height;
Shape25.Height:=round(Panel1.Height/8);
Shape25.Width:=round(Panel1.Width/8);
Shape26.Top:=Shape2.Height+Shape10.Height+Shape18.Height;
Shape26.Left:=Shape1.Height+Shape1.Left;
Shape26.Height:=round(Panel1.Height/8);
Shape26.Width:=round(Panel1.Width/8);
Shape27.Top:=Shape3.Height+Shape11.Height+Shape19.Height;
Shape27.Left:=Shape1.Height+Shape2.Height+Shape1.Left;
Shape27.Height:=round(Panel1.Height/8);
Shape27.Width:=round(Panel1.Width/8);
Shape28.Top:=Shape4.Height+Shape12.Height+Shape20.Height;
Shape28.Left:=Shape1.Left+Shape1.Height+Shape2.Height+Shape3.Height;
Shape28.Height:=round(Panel1.Height/8);
Shape28.Width:=round(Panel1.Width/8);
Shape29.Top:=Shape5.Height+Shape13.Height+Shape21.Height;
Shape29.Left:=Shape1.Left+Shape1.Height+Shape2.Height+Shape3.Height+Shape4.Height;
Shape29.Height:=round(Panel1.Height/8);
Shape29.Width:=round(Panel1.Width/8);
Shape30.Top:=Shape6.Height+Shape14.Height+Shape22.Height;
Shape30.Left:=Shape1.Left+Shape1.Height+Shape2.Height+Shape3.Height+Shape4.Height+Shape5.Height;Shape30.Height:=round(Panel1.Height/8);
Shape30.Width:=round(Panel1.Width/8);
Shape31.Top:=Shape7.Height+Shape15.Height+Shape23.Height;
Shape31.Left:=Shape1.Left+Shape1.Height+Shape2.Height+Shape3.Height+Shape4.Height+Shape5.Height+Shape6.Height;Shape31.Height:=round(Panel1.Height/8);
Shape31.Width:=round(Panel1.Width/8);
Shape32.Top:=Shape8.Height+Shape16.Height+Shape24.Height;
Shape32.Left:=Shape1.Left+Shape1.Height+Shape2.Height+Shape3.Height+Shape4.Height+Shape5.Height+Shape6.Height+Shape7.Height;Shape32.Height:=round(Panel1.Height/8);
Shape32.Width:=round(Panel1.Width/8);
Shape1.Top:=round(Panel1.Height/2)-Shape9.Height-Shape1.Height;
Shape2.Top:=round(Panel1.Height/2)-Shape9.Height-Shape1.Height;
Shape3.Top:=round(Panel1.Height/2)-Shape9.Height-Shape1.Height;
Shape4.Top:=round(Panel1.Height/2)-Shape9.Height-Shape1.Height;
Shape5.Top:=round(Panel1.Height/2)-Shape9.Height-Shape1.Height;
Shape6.Top:=round(Panel1.Height/2)-Shape9.Height-Shape1.Height;
Shape7.Top:=round(Panel1.Height/2)-Shape9.Height-Shape1.Height;
Shape8.Top:=round(Panel1.Height/2)-Shape9.Height-Shape1.Height;
Shape9.Top:=round(Panel1.Height/2)-Shape9.Height;
Shape10.Top:=round(Panel1.Height/2)-Shape9.Height;
Shape11.Top:=round(Panel1.Height/2)-Shape9.Height;
Shape12.Top:=round(Panel1.Height/2)-Shape9.Height;
Shape13.Top:=round(Panel1.Height/2)-Shape9.Height;
Shape14.Top:=round(Panel1.Height/2)-Shape9.Height;
Shape15.Top:=round(Panel1.Height/2)-Shape9.Height;
Shape16.Top:=round(Panel1.Height/2)-Shape9.Height;
Shape17.Top:=round(Panel1.Height/2);
Shape18.Top:=round(Panel1.Height/2);
Shape19.Top:=round(Panel1.Height/2);
Shape20.Top:=round(Panel1.Height/2);
Shape21.Top:=round(Panel1.Height/2);
Shape22.Top:=round(Panel1.Height/2);
Shape23.Top:=round(Panel1.Height/2);
Shape24.Top:=round(Panel1.Height/2);
Shape25.Top:=round(Panel1.Height/2)+Shape17.Height;
Shape26.Top:=round(Panel1.Height/2)+Shape17.Height;
Shape27.Top:=round(Panel1.Height/2)+Shape17.Height;
Shape28.Top:=round(Panel1.Height/2)+Shape17.Height;
Shape29.Top:=round(Panel1.Height/2)+Shape17.Height;
Shape30.Top:=round(Panel1.Height/2)+Shape17.Height;
Shape31.Top:=round(Panel1.Height/2)+Shape17.Height;
Shape32.Top:=round(Panel1.Height/2)+Shape17.Height;
end;

end.
