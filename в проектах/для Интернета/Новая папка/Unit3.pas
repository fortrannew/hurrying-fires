unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Menus, Grids;

type
  TForm3 = class(TForm)
    StringGrid1: TStringGrid;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    Label1: TLabel;
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
  private
  procedure to16(Sender: TObject);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form3: TForm3;
  kad:   string;

implementation

{$R *.dfm}

function to16do10(a:String):integer;
    external 'ShestToDesyat.dll';
function to10do2(d:String):integer;
    external 'DesyatToDvoich.dll';
function to2do10(a:String):integer;
    external 'DvoichToDesyat.dll';

procedure TForm3.N1Click(Sender: TObject);
var i1,i2:integer;
begin
For i1:=1 to Form3.StringGrid1.ColCount-1 do
Begin
For i2:=1 to Form3.StringGrid1.RowCount-1 do
Begin
if Label1.Caption='10' then
Form3.StringGrid1.Cells[i1,i2]:=IntToStr(to10do2(Form3.StringGrid1.Cells[i1,i2]));
if Label1.Caption='16' then
Form3.StringGrid1.Cells[i1,i2]:=IntToStr(to10do2(IntToStr(to16do10(Form3.StringGrid1.Cells[i1,i2]))));
Repeat
Begin
if Length(Form3.StringGrid1.Cells[i1,i2])<8 then
Form3.StringGrid1.Cells[i1,i2]:='0'+Form3.StringGrid1.Cells[i1,i2];
end;
until Length(Form3.StringGrid1.Cells[i1,i2])>=8;
end;
end;
Label1.Caption:='2';
end;

procedure TForm3.N2Click(Sender: TObject);
var i1,i2:integer;
begin
For i1:=1 to Form3.StringGrid1.ColCount-1 do
Begin
For i2:=1 to Form3.StringGrid1.RowCount-1 do
Begin
if Label1.Caption='2' then
Form3.StringGrid1.Cells[i1,i2]:=IntToStr(to2do10(Form3.StringGrid1.Cells[i1,i2]));
if Label1.Caption='16' then
Form3.StringGrid1.Cells[i1,i2]:=IntToStr(to16do10(Form3.StringGrid1.Cells[i1,i2]));
Repeat
Begin
if Length(Form3.StringGrid1.Cells[i1,i2])<3 then
Form3.StringGrid1.Cells[i1,i2]:='0'+Form3.StringGrid1.Cells[i1,i2];
end;
until Length(Form3.StringGrid1.Cells[i1,i2])>=3;
end;
end;
Label1.Caption:='10';
end;

procedure TForm3.N3Click(Sender: TObject);
var i1,i2:integer;
begin
For i1:=1 to Form3.StringGrid1.ColCount-1 do
Begin
For i2:=1 to Form3.StringGrid1.RowCount-1 do
Begin
if Label1.Caption='2' then
Begin
kad:=IntToStr(to2do10(Form3.StringGrid1.Cells[i1,i2]));
Form3.to16(Form3);
Form3.StringGrid1.Cells[i1,i2]:=kad;
end;
if Label1.Caption='10' then
Begin
kad:=Form3.StringGrid1.Cells[i1,i2];
Form3.to16(Form3);
Form3.StringGrid1.Cells[i1,i2]:=kad;
end;
Repeat
Begin
if Length(Form3.StringGrid1.Cells[i1,i2])<2 then
Form3.StringGrid1.Cells[i1,i2]:='0'+Form3.StringGrid1.Cells[i1,i2];
end;
until Length(Form3.StringGrid1.Cells[i1,i2])>=2;
end;
end;
Label1.Caption:='16';
end;

procedure TForm3.to16(Sender: TObject);
var c,e,a,i,i1,i2:integer;
m:array [1..100] of string;
v,dop,k:string;
begin
a:=StrToInt(kad);
i:=0;
Repeat
Begin
i:=i+1;
m[i]:=IntToStr(a mod 16);
if m[i]='10' then
m[i]:='A';
if m[i]='11' then
m[i]:='B';
if m[i]='12' then
m[i]:='C';
if m[i]='13' then
m[i]:='D';
if m[i]='14' then
m[i]:='E';
if m[i]='15' then
m[i]:='F';
a:=a div 16;
end;
until (a=1) or (a=0);
For e:=1 to i do
Begin
c:=i-e+1;
v:=v+(m[c]);
end;
if a=0 then
dop:=v
else
dop:=IntToStr(a)+v;
kad:=dop;
end;
end.
