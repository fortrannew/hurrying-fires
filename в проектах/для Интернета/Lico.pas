unit Lico;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, IdAntiFreezeBase, IdAntiFreeze, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdHTTP, ExtCtrls, StdCtrls, sButton,
  sGauge, sLabel, SRGrad, sSkinManager, Registry, DateUtils;

type
  TForm45 = class(TForm)
    sWebLabel1: TsWebLabel;
    sSkinManager1: TsSkinManager;
    SRGradient1: TSRGradient;
    sLabelFX4: TsLabelFX;
    sLabelFX1: TsLabelFX;
    sLabel2: TsLabelFX;
    sLabel1: TsLabelFX;
    sGauge1: TsGauge;
    sButton1: TsButton;
    Image1: TImage;
    procedure FormCreate(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
  k:string;
  weq:boolean;
  otk:boolean;
  za:boolean;
    { Public declarations }
  end;

var
  Form45: TForm45;

implementation

uses Unit21, Unit1;

{$R *.dfm}

procedure TForm45.FormCreate(Sender: TObject);
var Registry,reg:TRegistry;
i:integer;
b,c,d,b1,c1,d1:Word;
s,FileOnNet, LocalFileName,buf,buf1,pyt,lang:string;
ka:boolean;
f:TextFile;
slovar:array[1..1000] of string;
begin
otk:=false;
weq:=false;
za:=false;
k:='�����';
ka:=false;
Registry := TRegistry.Create;
Registry.RootKey := HKEY_CURRENT_USER;
Registry.OpenKey('Software\Geteroides',true);
if (Registry.ValueExists('fatewerq')=true) then
ka:=true;
Registry.CloseKey;
Registry.Free;
Begin
Registry := TRegistry.Create;
Registry.RootKey := hkey_local_machine;
Registry.OpenKey('Software\texas',true);
if (Registry.ValueExists('system1')=false) or (Registry.ValueExists('sys')=false) then
Begin
Registry.WriteString('system1',DateToStr(Date));
Registry.WriteString('sys','true');
end
else
Begin
if Registry.ReadString('sys')='true' then
Begin
s:=Registry.ReadString('system1');
DecodeDate(strtodate(s), b,c,d);
DecodeDate(Date,b1,c1,d1);
sLabel2.Caption:=IntToStr(30-DaysBetween(EncodeDateTime(b1, c1, d1, 0, 0, 0, 0), EncodeDateTime(b, c, d,  0, 0, 0, 0)));
if sLabel2.Caption<='0' then
Begin
Zaschita.Label1:='������';
k:='������';
Registry.WriteString('sys','false');
end;
end;
end;
if Registry.ReadString('sys')<>'true' then
Begin
Zaschita.Label1:='������';
k:='������';
sLabel2.Caption:='0';
end;
Registry.CloseKey;
Registry.Free;
sGauge1.Progress:=(30-StrToInt(sLabel2.Caption));
end;
pyt:=ExtractFilePath(Application.ExeName);
Registry := TRegistry.Create;
Registry.RootKey := hkey_local_machine;
Registry.OpenKey('system\getinominget',true);
lang:=Registry.ReadString('lang');
Registry.CloseKey;
Registry.Free;
i:=0;
if lang='ru' then
assignfile(f,pyt+'\lang\�������.lng');
if lang='en' then
assignfile(f,pyt+'\lang\English.lng');
if lang='lv' then
assignfile(f,pyt+'\lang\Latvian.lng');
if lang='d' then
assignfile(f,pyt+'\lang\German.lng');
if lang='pl' then
assignfile(f,pyt+'\lang\Polish.lng');
if lang='ua' then
assignfile(f,pyt+'\lang\Ukrainian.lng');
if lang='f' then
assignfile(f,pyt+'\lang\French.lng');
{$I-}
Reset(f);
{$I+}
While not EOF(f) do
begin
i:=i+1;
readln(f,s); // ��������� ������ �� �����
slovar[i]:=s;
end;
d:=i;
closefile(f);
sLabelFX1.Caption:=slovar[121];
sWebLabel1.Caption:=slovar[122];
sLabel1.Caption:=slovar[123];
sButton1.Caption:=slovar[124];
sLabel1.top:=Form45.sLabel2.top+2;
end;

procedure TForm45.sButton1Click(Sender: TObject);
begin
if weq=true then
Form45.Visible:=false
else
Begin
if k='������' then
Begin
Form45.Close;
end
else
Begin
Form1.closeded:=true;
Form1.Close;
end;
end;
end;

procedure TForm45.FormClose(Sender: TObject; var Action: TCloseAction);
begin
Action:=caFree;
end;

end.
