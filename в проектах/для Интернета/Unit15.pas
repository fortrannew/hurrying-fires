unit Unit15;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, ToolWin, Text3D, sButton, sBevel,
  SRGrad, sLabel, Registry;

type
  TForm15 = class(TForm)
    Image2: TImage;
    sLabelFX1: TsLabelFX;
    SRGradient1: TSRGradient;
    sWebLabel1: TsWebLabel;
    sLabelFX4: TsLabelFX;
    sWebLabel2: TsWebLabel;
    sLabelFX2: TsLabelFX;
    sLabelFX3: TsLabelFX;
    procedure FormClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form15: TForm15;
  x,y:Integer;

implementation

uses Unit1;

{$R *.dfm}

procedure TForm15.FormClick(Sender: TObject);
begin
Form15.Close;
Form1.Enabled:=true;
end;

end.
