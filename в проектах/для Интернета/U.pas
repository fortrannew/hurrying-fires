unit U;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sButton, sGroupBox;

type
  TForm145 = class(TForm)
    GroupBox1: TsGroupBox;
    OpenDialog1: TOpenDialog;
    Button51: TsButton;
    Button52: TsButton;
    Button53: TsButton;
    procedure Button51Click(Sender: TObject);
    procedure Button52Click(Sender: TObject);
    procedure Button53Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
  Button:array[1..15] of TButton;
  procedure EditKeyPress(Sender: TObject; var Key: Char);
  procedure otkrut(Sender: TObject);
  procedure dobavit(Sender: TObject);
  procedure ydalit(Sender: TObject);
  procedure otkrutbinformat(Sender: TObject);
    { Public declarations }
  end;

var
  Form145: TForm145;
  Edit:array[1..15] of TEdit;
  Kolichestvo:integer;

implementation

uses Unit1, Unit3;

{$R *.dfm}

function to16do10(a:String):integer;
    external 'ShestToDesyat.dll';
function to10do2(d:String):integer;
    external 'DesyatToDvoich.dll';
    
procedure TForm145.Button51Click(Sender: TObject);
var i:integer;
begin
if Edit[1].Text='' then
Close
else
Begin
Form1.N10.Click;
Visible:=false;
Kolichestvo:=0;
For i:=1 to 15 do
Begin
if Edit[i].Text<>'' then
Begin
Kolichestvo:=Kolichestvo+1;
if i<>1 then
Form1.N12.Click;
end;
end;
Form145.otkrutbinformat(Form145);
Close;
Form1.IzmcvShapanede(Form1);
end;
Form1.SaveDialog1.FileName:='';
Form1.OpenDialog1.FileName:='';
Form1.Caption:='Hurrying fires';
end;

procedure TForm145.Button52Click(Sender: TObject);
begin
Close;
end;

procedure TForm145.Button53Click(Sender: TObject);
var i:integer;
begin
For i:=1 to 15 do
Begin
Edit[i].Text:='';
Form145.ydalit(Form145);
end;
end;

procedure TForm145.dobavit(Sender: TObject);
var i,m:integer;
begin
m:=16;
For i:=1 to 15 do
Begin
m:=m-1;
if Edit[m].Visible then
Begin
if Edit[m].Text<>'' then
Begin
if m<>15 then
Begin
Edit[m+1].Visible:=true;
Button[m+1].Visible:=true;
end;
end;
Break;
end;
end;
end;

procedure TForm145.EditKeyPress(Sender: TObject; var Key: Char);
var i,m:integer;
begin
(Sender as TEdit).Tag:=1;
For i:=1 to 15 do
Begin
if Edit[i].Tag=1 then
Begin
Edit[i].Tag:=0;
Break;
end;
end;
case Key of
#8:
Begin
Edit[i].Text:='';
For m:=i to 14 do
Edit[m].Text:=Edit[m+1].Text;
Form145.ydalit(Form145);
end;
else
Key :=Chr(0); // ������ �� ����������
end;
end;

procedure TForm145.otkrut(Sender: TObject);
var i,i1:integer;
begin
(Sender as TButton).Tag:=1;
For i:=1 to 15 do
Begin
if Button[i].Tag=1 then
Begin
Button[i].Tag:=0;
Break;
end;
end;
if i=1 then
OpenDialog1.Options:=[ofHideReadOnly,ofAllowMultiSelect,ofEnableSizing]
else
OpenDialog1.Options:=[ofHideReadOnly,ofEnableSizing];
OpenDialog1.FileName:=Edit[i].Text;
if OpenDialog1.Execute then
Begin
if (OpenDialog1.Files.Count>1) and (i=1) then
Begin
For i1:=i-1 to OpenDialog1.Files.Count-1 do
Begin
Edit[i1+1].Text:=OpenDialog1.Files.Strings[i1];
Form145.dobavit(Form145);
end;
end
else
Edit[i].Text:=OpenDialog1.FileName;
Form145.dobavit(Form145);
end;
end;

procedure TForm145.otkrutbinformat(Sender: TObject);
var
  myFile: File;
  oneByte: byte;
  i,i1,i2,ka,por: Integer;
  prob:string;
begin
For i1:=1 to Kolichestvo do
Begin
AssignFile(myFile, Edit[i1].Text);
Reset(myFile, 1);
For i2:=1 to 2049 do
Begin
BlockRead(myFile, oneByte, 1);
prob:=IntToStr(to10do2(IntToStr(oneByte)));
por:=Length(prob);
if por<>8 then
Begin
Repeat
Begin
prob:='0'+prob;
por:=Length(prob);
end;
until por=8;
end;
Form3.StringGrid1.Cells[i1,i2]:=prob;
Form1.StringGrid1.Cells[i1,i2]:=prob;
end;
CloseFile(myFile);
end;
end;

procedure TForm145.ydalit(Sender: TObject);
var i,m:integer;
begin
m:=16;
For i:=2 to 15 do
Begin
m:=m-1;
if (Edit[m].Visible=true) and (Edit[m-1].Text='') then
Begin
Edit[m].Visible:=false;
Button[m].Visible:=false;
Break;
end;
end;
end;

procedure TForm145.FormCreate(Sender: TObject);
var i,x1,x2,y1,y2:integer;
begin
x1:=8;
x2:=550;
y1:=21;
y2:=19;
For i:=1 to 15 do
Begin
Edit[i]:=TEdit.Create(GroupBox1);
Edit[i].Parent:=GroupBox1;
Edit[i].Height:=21;
Edit[i].Width:=529;
Edit[i].Left:=x1;
Edit[i].Top:=y1;
Edit[i].OnKeyPress:=EditKeyPress;
Button[i]:=TButton.Create(GroupBox1);
Button[i].Parent:=GroupBox1;
Button[i].Height:=25;
Button[i].Width:=75;
Button[i].Left:=x2;
Button[i].Top:=y2;
Button[i].Caption:=Form1.slovar[130];
Button[i].Default:=false;
Button[i].OnClick:=otkrut;
if i<>1 then
Begin
Edit[i].Visible:=false;
Button[i].Visible:=false;
end;
y1:=y1+Edit[i].Height+3;
y2:=y2+Button[i].Height-1;
end;
end;

procedure TForm145.FormClose(Sender: TObject; var Action: TCloseAction);
begin
Button53.Click;
Form1.Enabled:=true;
end;

end.
