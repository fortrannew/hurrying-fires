object Form145: TForm145
  Left = 302
  Top = 363
  BorderStyle = bsDialog
  Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1092#1072#1081#1083#1086#1074
  ClientHeight = 449
  ClientWidth = 666
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TsGroupBox
    Left = 8
    Top = 8
    Width = 649
    Height = 393
    Caption = #1048#1084#1103' '#1092#1072#1081#1083#1072
    TabOrder = 0
    SkinData.SkinSection = 'GROUPBOX'
  end
  object Button51: TsButton
    Left = 224
    Top = 416
    Width = 75
    Height = 25
    Caption = #1054#1050
    TabOrder = 1
    OnClick = Button51Click
    SkinData.SkinSection = 'BUTTON'
  end
  object Button52: TsButton
    Left = 304
    Top = 416
    Width = 75
    Height = 25
    Caption = '&'#1054#1090#1084#1077#1085#1080#1090#1100
    TabOrder = 2
    OnClick = Button52Click
    SkinData.SkinSection = 'BUTTON'
  end
  object Button53: TsButton
    Left = 384
    Top = 416
    Width = 75
    Height = 25
    Caption = '&'#1042#1086#1089#1089#1090#1072#1085#1086#1074#1080#1090#1100
    TabOrder = 3
    OnClick = Button53Click
    SkinData.SkinSection = 'BUTTON'
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Binary image (*.bin)|*.bin'
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofEnableSizing]
    Left = 80
    Top = 408
  end
end
