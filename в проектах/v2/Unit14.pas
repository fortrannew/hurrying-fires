unit Unit14;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, Spin, Registry, sRadioButton;

type
  TForm14 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    ColorDialog1: TColorDialog;
    TabSheet: TPageControl;
    Monitor: TTabSheet;
    Fon: TTabSheet;
    Shape1: TShape;
    Label1: TLabel;
    Label2: TLabel;
    ComboBox1: TComboBox;
    Button5: TButton;
    Panel1: TPanel;
    Panel2: TPanel;
    ListBox1: TListBox;
    ListBox2: TListBox;
    ListBox3: TListBox;
    Lampochki: TTabSheet;
    Label5: TLabel;
    SpinEdit1: TSpinEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Label4: TLabel;
    ComboBox2: TComboBox;
    Panel4: TPanel;
    Label3: TLabel;
    ListBox4: TListBox;
    Shape2: TShape;
    Button4: TButton;
    Label6: TLabel;
    Panel5: TPanel;
    ComboBox3: TComboBox;
    Label7: TLabel;
    Panel6: TPanel;
    ListBox5: TListBox;
    Shape3: TShape;
    Button6: TButton;
    Label8: TLabel;
    Panel7: TPanel;
    SpinEdit2: TSpinEdit;
    sRadioButton1: TsRadioButton;
    sRadioButton2: TsRadioButton;
    Panel8: TPanel;
    TabSheet3: TTabSheet;
    Panel9: TPanel;
    Panel10: TPanel;
    Label9: TLabel;
    ComboBox4: TComboBox;
    Label10: TLabel;
    ListBox6: TListBox;
    Shape4: TShape;
    Button7: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure ListBox1DrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure Button3Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ComboBox2Change(Sender: TObject);
    procedure ListBox4Click(Sender: TObject);
    procedure ListBox4DrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure Button4Click(Sender: TObject);
    procedure ComboBox3Change(Sender: TObject);
    procedure ListBox5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure ComboBox4Change(Sender: TObject);
    procedure ListBox6DrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure ListBox6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form14: TForm14;

implementation

uses Unit1, Unit12, Unit6;

{$R *.dfm}

procedure TForm14.FormCreate(Sender: TObject);
var Registry: TRegistry;
begin
  Registry := TRegistry.Create;
  Registry.RootKey := hkey_local_machine;
  Registry.OpenKey('system\getinominget',true);
  Shape1.Brush.Color:=StringToColor(Registry.ReadString('getir4'));
  Shape2.Brush.Color:=StringToColor(Registry.ReadString('getir5'));
  Shape3.Brush.Color:=StringToColor(Registry.ReadString('getir6'));
  Shape4.Brush.Color:=StringToColor(Registry.ReadString('getir7'));
  Registry.CloseKey;
  Registry.Free;
  if Shape1.Brush.Color=clBtnFace then
  Begin
  ComboBox1.ItemIndex:=0;
  Combobox1.OnChange(Combobox1);
  end;
  if Shape2.Brush.Color=clBtnFace then
  Begin
  ComboBox2.ItemIndex:=0;
  Combobox2.OnChange(Combobox2);
  end;
  if Shape3.Brush.Color=clBtnFace then
  Begin
  ComboBox3.ItemIndex:=0;
  Combobox3.OnChange(Combobox3);
  end;
  if Shape4.Brush.Color=clBtnFace then
  Begin
  ComboBox4.ItemIndex:=0;
  Combobox4.OnChange(Combobox4);
  end;
end;

procedure TForm14.Button1Click(Sender: TObject);
var a:integer;
Registry: TRegistry;
begin
Registry := TRegistry.Create;
Registry.RootKey := hkey_local_machine;
Registry.OpenKey('system\getinominget',true);
Registry.WriteString('getir4',ColorToString(shape1.Brush.Color));
Registry.WriteString('getir5',ColorToString(shape2.Brush.Color));
Registry.WriteString('getir6',ColorToString(shape3.Brush.Color));
Registry.WriteString('getir7',ColorToString(shape4.Brush.Color));
Registry.CloseKey;
Registry.Free;
Form14.Visible:=false;
Form1.PaneleteVidno(Form1);
Form1.Izmcv(Form1);
Form1.N100.Click;
Form1.IzmcvShapanede(Form1);
end;

procedure TForm14.ListBox1Click(Sender: TObject);
begin
if ComboBox1.Text='����' then
Begin
Shape1.Brush.Color:=StringToColor(ListBox3.Items[ListBox1.ItemIndex]);
if ComboBox2.Text='���� ����' then
Shape2.Brush.Color:=Shape1.Brush.Color;
if ComboBox3.Text='���� ����' then
Shape3.Brush.Color:=Shape1.Brush.Color;
end;
end;

procedure TForm14.Button5Click(Sender: TObject);
begin
ColorDialog1.Color:=Shape1.Brush.Color;
if ColorDialog1.Execute<>false then
Begin
Shape1.Brush.Color:=ColorDialog1.Color;
end;
end;

procedure TForm14.ListBox1DrawItem(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
var
  Bitmap: TBitmap;
  Offset: Integer;
  BMPRect: TRect;
begin
  with (Control as TListBox).Canvas do
  begin
    FillRect(Rect);
    Bitmap := TBitmap.Create;
    Bitmap.LoadFromResourceName(HInstance,ListBox2.Items[index]);
    Offset := 0;
    if Bitmap <> nil then
    begin
      BMPRect := Bounds(Rect.Left+2, Rect.Top+2,
      (Rect.Bottom-Rect.Top-2)*2, Rect.Bottom-Rect.Top-2);
      {StretchDraw(BMPRect, Bitmap); ����� ������ ����������, �� ����� ������� ������ ���}
      BrushCopy(BMPRect,Bitmap, Bounds(0, 0, Bitmap.Width, Bitmap.Height),
      Bitmap.Canvas.Pixels[0, Bitmap.Height-1]);
      Offset := (Rect.Bottom-Rect.Top+1)*2;
    end;
    TextOut(Rect.Left+Offset, Rect.Top, ListBox1.Items[index]);
    Bitmap.Free;
  end;
end;

procedure TForm14.Button3Click(Sender: TObject);
var Registry: TRegistry;
begin
if ComboBox1.ItemIndex<>1 then
Begin
ComboBox1.ItemIndex:=1;
ComboBox1.OnClick(ComboBox1);
end;
Registry := TRegistry.Create;
  Registry.RootKey := hkey_local_machine;
  Registry.OpenKey('system\getinominget',true);
  Shape1.Brush.Color:=StringToColor(Registry.ReadString('getir4'));
  Shape2.Brush.Color:=StringToColor(Registry.ReadString('getir5'));
  Shape3.Brush.Color:=StringToColor(Registry.ReadString('getir6'));
  Shape4.Brush.Color:=StringToColor(Registry.ReadString('getir7'));
  Registry.CloseKey;
  Registry.Free;
  if Shape1.Brush.Color=clBtnFace then
  Begin
  ComboBox1.ItemIndex:=0;
  Combobox1.OnChange(Combobox1);
  end;
  if Shape2.Brush.Color=clBtnFace then
  Begin
  ComboBox2.ItemIndex:=0;
  Combobox2.OnChange(Combobox2);
  end;
  if Shape3.Brush.Color=clBtnFace then
  Begin
  ComboBox3.ItemIndex:=0;
  Combobox3.OnChange(Combobox3);
  end;
  if Shape4.Brush.Color=clBtnFace then
  Begin
  ComboBox4.ItemIndex:=0;
  Combobox4.OnChange(Combobox4);
  end;
end;

procedure TForm14.ComboBox1Change(Sender: TObject);
begin
if ComboBox1.Text='����' then
Begin
Label2.Visible:=true;
Panel1.Visible:=true;
ListBox1.Visible:=true;
Shape1.Visible:=true;
Button5.Visible:=true;
if ComboBox2.Items[2]<>'���� ����' then
ComboBox2.Items.Add('���� ����');
if ComboBox2.Text='���� ����' then
Shape2.Brush.Color:=Shape1.Brush.Color;
if ComboBox3.Items[2]<>'���� ����' then
ComboBox3.Items.Add('���� ����');
if ComboBox3.Text='���� ����' then
Shape3.Brush.Color:=Shape1.Brush.Color;
end;
if ComboBox2.ItemIndex=2 then
Begin
ComboBox2.ItemIndex:=1;
ComboBox2.OnChange(ComboBox2);
end;
if ComboBox3.ItemIndex=2 then
Begin
ComboBox3.ItemIndex:=1;
ComboBox3.OnChange(ComboBox3);
end;
if ComboBox1.Text='���' then
Begin
Label2.Visible:=false;
Panel1.Visible:=false;
ListBox1.Visible:=false;
Shape1.Visible:=false;
Button5.Visible:=false;
Shape1.Brush.Color:=clBtnFace;
ComboBox2.Items.Delete(2);
ComboBox3.Items.Delete(2);
end;
end;

procedure TForm14.Button2Click(Sender: TObject);
begin
Form14.Close;
end;

procedure TForm14.FormClose(Sender: TObject; var Action: TCloseAction);
begin
Button3.Click;
end;

procedure TForm14.ComboBox2Change(Sender: TObject);
begin
if ComboBox2.Text='���' then
Begin
Label3.Visible:=false;
Panel4.Visible:=false;
ListBox4.Visible:=false;
Shape2.Visible:=false;
Button4.Visible:=false;
Shape2.Brush.Color:=clBtnFace;
end;
if ComboBox2.Text='����' then
Begin
Label3.Visible:=true;
Panel4.Visible:=true;
ListBox4.Visible:=true;
Shape2.Visible:=true;
Button4.Visible:=true;
end;
if ComboBox2.Text='���� ����' then
Begin
Label3.Visible:=true;
Panel4.Visible:=true;
ListBox4.Visible:=true;
Shape2.Visible:=true;
Button4.Visible:=true;
Shape2.Brush.Color:=Shape1.Brush.Color;
end;
end;

procedure TForm14.ListBox4Click(Sender: TObject);
begin
if ComboBox2.Text='����' then
Begin
Shape2.Brush.Color:=StringToColor(ListBox3.Items[ListBox4.ItemIndex]);
end;
if ComboBox2.Text='���� ����' then
Begin
Shape2.Brush.Color:=StringToColor(ListBox3.Items[ListBox4.ItemIndex]);
Shape1.Brush.Color:=Shape2.Brush.Color;
if ComboBox3.Text='���� ����' then
Shape3.Brush.Color:=Shape1.Brush.Color;
end;
end;

procedure TForm14.ListBox4DrawItem(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
var
  Bitmap: TBitmap;
  Offset: Integer;
  BMPRect: TRect;
  i:integer;
begin
i:=-1;
 with (Control as TListBox).Canvas do
  begin
    FillRect(Rect);
    Bitmap := TBitmap.Create;
    Bitmap.LoadFromResourceName(HInstance,ListBox2.Items[index]);
    Offset := 0;
    if Bitmap <> nil then
    begin
      BMPRect := Bounds(Rect.Left+2, Rect.Top+2,
      (Rect.Bottom-Rect.Top-2)*2, Rect.Bottom-Rect.Top-2);
      {StretchDraw(BMPRect, Bitmap); ����� ������ ����������, �� ����� ������� ������ ���}
      BrushCopy(BMPRect,Bitmap, Bounds(0, 0, Bitmap.Width, Bitmap.Height),
      Bitmap.Canvas.Pixels[0, Bitmap.Height-1]);
      Offset := (Rect.Bottom-Rect.Top+1)*2;
    end;
    TextOut(Rect.Left+Offset, Rect.Top, ListBox1.Items[index]);
    Bitmap.Free;
  end;
end;

procedure TForm14.Button4Click(Sender: TObject);
begin
ColorDialog1.Color:=Shape2.Brush.Color;
if ColorDialog1.Execute<>false then
Begin
Shape2.Brush.Color:=ColorDialog1.Color;
if ComboBox2.Text='���� ����' then
Begin
Shape1.Brush.Color:=Shape2.Brush.Color;
if ComboBox3.Text='���� ����' then
Shape3.Brush.Color:=Shape2.Brush.Color;
end;
end;
end;

procedure TForm14.ComboBox3Change(Sender: TObject);
begin
if ComboBox3.Text='���' then
Begin
Label7.Visible:=false;
Panel6.Visible:=false;
ListBox5.Visible:=false;
Shape3.Visible:=false;
Button6.Visible:=false;
Shape3.Brush.Color:=clBtnFace;
end;
if ComboBox3.Text='����' then
Begin
Label7.Visible:=true;
Panel6.Visible:=true;
ListBox5.Visible:=true;
Shape3.Visible:=true;
Button6.Visible:=true;
end;
if ComboBox3.Text='���� ����' then
Begin
Label7.Visible:=true;
Panel6.Visible:=true;
ListBox5.Visible:=true;
Shape3.Visible:=true;
Button6.Visible:=true;
Shape3.Brush.Color:=Shape1.Brush.Color;
if ComboBox2.Text='���� ����' then
Shape2.Brush.Color:=Shape1.Brush.Color;
end;
end;

procedure TForm14.ListBox5Click(Sender: TObject);
begin
if ComboBox3.Text='����' then
Begin
Shape3.Brush.Color:=StringToColor(ListBox3.Items[ListBox5.ItemIndex]);
end;
if ComboBox3.Text='���� ����' then
Begin
Shape3.Brush.Color:=StringToColor(ListBox3.Items[ListBox5.ItemIndex]);
Shape1.Brush.Color:=Shape3.Brush.Color;
if ComboBox2.Text='���� ����' then
Shape2.Brush.Color:=Shape1.Brush.Color;
end;
end;

procedure TForm14.Button6Click(Sender: TObject);
begin
ColorDialog1.Color:=Shape3.Brush.Color;
if ColorDialog1.Execute<>false then
Begin
Shape3.Brush.Color:=ColorDialog1.Color;
if ComboBox3.Text='���� ����' then
Begin
Shape1.Brush.Color:=Shape3.Brush.Color;
if ComboBox2.Text='���� ����' then
Shape2.Brush.Color:=Shape3.Brush.Color;
end;
end;
end;

procedure TForm14.ComboBox4Change(Sender: TObject);
begin
if ComboBox4.Text='���' then
Begin
Label10.Visible:=false;
Panel10.Visible:=false;
ListBox6.Visible:=false;
Shape4.Visible:=false;
Button7.Visible:=false;
Shape4.Brush.Color:=clBtnFace;
end;
if ComboBox4.Text='����' then
Begin
Label10.Visible:=true;
Panel10.Visible:=true;
ListBox6.Visible:=true;
Shape4.Visible:=true;
Button7.Visible:=true;
end;
end;

procedure TForm14.ListBox6DrawItem(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
var
  Bitmap: TBitmap;
  Offset: Integer;
  BMPRect: TRect;
  i:integer;
begin
i:=-1;
 with (Control as TListBox).Canvas do
  begin
    FillRect(Rect);
    Bitmap := TBitmap.Create;
    Bitmap.LoadFromResourceName(HInstance,ListBox2.Items[index]);
    Offset := 0;
    if Bitmap <> nil then
    begin
      BMPRect := Bounds(Rect.Left+2, Rect.Top+2,
      (Rect.Bottom-Rect.Top-2)*2, Rect.Bottom-Rect.Top-2);
      {StretchDraw(BMPRect, Bitmap); ����� ������ ����������, �� ����� ������� ������ ���}
      BrushCopy(BMPRect,Bitmap, Bounds(0, 0, Bitmap.Width, Bitmap.Height),
      Bitmap.Canvas.Pixels[0, Bitmap.Height-1]);
      Offset := (Rect.Bottom-Rect.Top+1)*2;
    end;
    TextOut(Rect.Left+Offset, Rect.Top, ListBox1.Items[index]);
    Bitmap.Free;
  end;
end;

procedure TForm14.ListBox6Click(Sender: TObject);
begin
if ComboBox4.Text='����' then
Shape4.Brush.Color:=StringToColor(ListBox3.Items[ListBox6.ItemIndex]);
end;

procedure TForm14.Button7Click(Sender: TObject);
begin
ColorDialog1.Color:=Shape4.Brush.Color;
if ColorDialog1.Execute<>false then
Shape4.Brush.Color:=ColorDialog1.Color;
end;

end.
