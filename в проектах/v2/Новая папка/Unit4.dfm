object Form13: TForm13
  Left = 271
  Top = 118
  Width = 696
  Height = 480
  Caption = 'Form13'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 128
    Top = 16
    Width = 32
    Height = 13
    Caption = 'Label2'
    Visible = False
  end
  object Label1: TLabel
    Left = 136
    Top = 436
    Width = 6
    Height = 13
    Caption = '0'
    Visible = False
  end
  object PageControl1: TPageControl
    Left = 8
    Top = 8
    Width = 673
    Height = 433
    ActivePage = TabSheet2
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1092#1072#1081#1083#1086#1074
      object GroupBox1: TGroupBox
        Left = 8
        Top = 8
        Width = 649
        Height = 393
        Caption = #1048#1084#1103' '#1092#1072#1081#1083#1072
        TabOrder = 0
        object ScrollBar1: TScrollBar
          Left = 631
          Top = 15
          Width = 16
          Height = 376
          Align = alRight
          Kind = sbVertical
          Max = 1
          Min = 1
          PageSize = -15
          Position = 1
          TabOrder = 0
          Visible = False
        end
        object Panel1: TPanel
          Left = 8
          Top = 16
          Width = 529
          Height = 377
          BevelOuter = bvNone
          TabOrder = 1
          object Edit9: TEdit
            Left = 0
            Top = 194
            Width = 529
            Height = 21
            TabOrder = 0
            Visible = False
          end
          object Edit8: TEdit
            Left = 0
            Top = 170
            Width = 529
            Height = 21
            TabOrder = 1
            Visible = False
          end
          object Edit7: TEdit
            Left = 0
            Top = 146
            Width = 529
            Height = 21
            TabOrder = 2
            Visible = False
          end
          object Edit6: TEdit
            Left = 0
            Top = 122
            Width = 529
            Height = 21
            TabOrder = 3
            Visible = False
          end
          object Edit5: TEdit
            Left = 0
            Top = 106
            Width = 529
            Height = 21
            TabOrder = 4
            Visible = False
          end
          object Edit4: TEdit
            Left = 0
            Top = 82
            Width = 529
            Height = 21
            TabOrder = 5
            Visible = False
          end
          object Edit3: TEdit
            Left = 0
            Top = 66
            Width = 529
            Height = 21
            TabOrder = 6
            Visible = False
          end
          object Edit2: TEdit
            Left = 0
            Top = 42
            Width = 529
            Height = 21
            TabOrder = 7
            Visible = False
          end
          object Edit15: TEdit
            Left = 0
            Top = 332
            Width = 529
            Height = 21
            TabOrder = 8
            Visible = False
          end
          object Edit14: TEdit
            Left = 0
            Top = 346
            Width = 529
            Height = 21
            TabOrder = 9
            Visible = False
          end
          object Edit13: TEdit
            Left = 0
            Top = 348
            Width = 529
            Height = 21
            TabOrder = 10
            Visible = False
          end
          object Edit12: TEdit
            Left = 0
            Top = 348
            Width = 529
            Height = 21
            TabOrder = 11
            Visible = False
          end
          object Edit11: TEdit
            Left = 0
            Top = 348
            Width = 529
            Height = 21
            TabOrder = 12
            Visible = False
          end
          object Edit10: TEdit
            Left = 0
            Top = 348
            Width = 529
            Height = 21
            TabOrder = 13
            Visible = False
          end
          object Edit1: TEdit
            Left = 0
            Top = 348
            Width = 529
            Height = 21
            TabOrder = 14
          end
        end
        object Panel2: TPanel
          Left = 538
          Top = 16
          Width = 87
          Height = 393
          BevelOuter = bvNone
          TabOrder = 2
          object Button9: TButton
            Left = 6
            Top = 200
            Width = 75
            Height = 25
            Caption = '&'#1054#1073#1079#1086#1088'...'
            TabOrder = 0
            Visible = False
          end
          object Button8: TButton
            Left = 6
            Top = 176
            Width = 75
            Height = 25
            Caption = '&'#1054#1073#1079#1086#1088'...'
            TabOrder = 1
            Visible = False
          end
          object Button7: TButton
            Left = 6
            Top = 152
            Width = 75
            Height = 25
            Caption = '&'#1054#1073#1079#1086#1088'...'
            TabOrder = 2
            Visible = False
          end
          object Button6: TButton
            Left = 6
            Top = 128
            Width = 75
            Height = 25
            Caption = '&'#1054#1073#1079#1086#1088'...'
            TabOrder = 3
            Visible = False
          end
          object Button5: TButton
            Left = 6
            Top = 112
            Width = 75
            Height = 25
            Caption = '&'#1054#1073#1079#1086#1088'...'
            TabOrder = 4
            Visible = False
          end
          object Button4: TButton
            Left = 6
            Top = 88
            Width = 75
            Height = 25
            Caption = '&'#1054#1073#1079#1086#1088'...'
            TabOrder = 5
            Visible = False
          end
          object Button3: TButton
            Left = 6
            Top = 72
            Width = 75
            Height = 25
            Caption = '&'#1054#1073#1079#1086#1088'...'
            TabOrder = 6
            Visible = False
          end
          object Button2: TButton
            Left = 6
            Top = 48
            Width = 75
            Height = 25
            Caption = '&'#1054#1073#1079#1086#1088'...'
            TabOrder = 7
            Visible = False
          end
          object Button15: TButton
            Left = 6
            Top = 336
            Width = 75
            Height = 25
            Caption = '&'#1054#1073#1079#1086#1088'...'
            TabOrder = 8
            Visible = False
          end
          object Button14: TButton
            Left = 6
            Top = 336
            Width = 75
            Height = 25
            Caption = '&'#1054#1073#1079#1086#1088'...'
            TabOrder = 9
            Visible = False
          end
          object Button13: TButton
            Left = 6
            Top = 336
            Width = 75
            Height = 25
            Caption = '&'#1054#1073#1079#1086#1088'...'
            TabOrder = 10
            Visible = False
          end
          object Button12: TButton
            Left = 6
            Top = 336
            Width = 75
            Height = 25
            Caption = '&'#1054#1073#1079#1086#1088'...'
            TabOrder = 11
            Visible = False
          end
          object Button11: TButton
            Left = 6
            Top = 336
            Width = 75
            Height = 25
            Caption = '&'#1054#1073#1079#1086#1088'...'
            TabOrder = 12
            Visible = False
          end
          object Button10: TButton
            Left = 6
            Top = 336
            Width = 75
            Height = 25
            Caption = '&'#1054#1073#1079#1086#1088'...'
            TabOrder = 13
            Visible = False
          end
          object Button1: TButton
            Left = 6
            Top = 336
            Width = 75
            Height = 25
            Caption = '&'#1054#1073#1079#1086#1088'...'
            TabOrder = 14
          end
        end
      end
      object Edit51: TEdit
        Left = 248
        Top = 8
        Width = 121
        Height = 21
        TabOrder = 1
        Text = 'Edit51'
        Visible = False
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1060#1086#1088#1084#1072#1090' '#1092#1072#1081#1083#1072
      ImageIndex = 1
      object RadioGroup1: TRadioGroup
        Left = 8
        Top = 8
        Width = 313
        Height = 385
        Caption = #1060#1086#1088#1084#1072#1090' '#1092#1072#1081#1083#1072':'
        ItemIndex = 4
        Items.Strings = (
          'Standard/Extended Intel HEX (*.hex)'
          'Motorola S-record (*.hex;*.s;*.mot)'
          'POF (*.pof)'
          'JEDEC (*.jed)'
          'PRG (*.prg)'
          'Holtek OTP (*.otp)'
          'Angstrem SAV (*.sav)'
          'ASCII Hex (*.txt)'
          'ASCII Octal (*.txt)')
        TabOrder = 0
      end
      object GroupBox2: TGroupBox
        Left = 336
        Top = 8
        Width = 321
        Height = 385
        Caption = #1050#1086#1076#1080#1088#1086#1074#1072#1090#1100' '#1074'...'
        TabOrder = 1
        object RadioButton1: TRadioButton
          Left = 8
          Top = 24
          Width = 97
          Height = 17
          Caption = #1044#1074#1086#1080#1095#1085#1099#1081' '#1082#1086#1076
          Checked = True
          TabOrder = 0
          TabStop = True
        end
        object RadioButton2: TRadioButton
          Left = 8
          Top = 48
          Width = 105
          Height = 17
          Caption = #1044#1077#1089#1103#1090#1080#1095#1085#1099#1081' '#1082#1086#1076
          TabOrder = 1
        end
        object RadioButton3: TRadioButton
          Left = 8
          Top = 72
          Width = 145
          Height = 17
          Caption = #1064#1077#1089#1090#1085#1072#1076#1094#1072#1090#1080#1088#1080#1095#1085#1099#1081' '#1082#1086#1076
          TabOrder = 2
        end
      end
    end
  end
  object Button53: TButton
    Left = 408
    Top = 424
    Width = 75
    Height = 25
    Caption = '&'#1042#1086#1089#1089#1090#1072#1085#1086#1074#1080#1090#1100
    TabOrder = 1
  end
  object Button52: TButton
    Left = 328
    Top = 424
    Width = 75
    Height = 25
    Caption = '&'#1054#1090#1084#1077#1085#1080#1090#1100
    TabOrder = 2
  end
  object Button51: TButton
    Left = 248
    Top = 424
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 3
  end
  object Button16: TButton
    Left = 496
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Button16'
    TabOrder = 4
    Visible = False
  end
  object OpenDialog1: TOpenDialog
    Left = 64
    Top = 176
  end
end
