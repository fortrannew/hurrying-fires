unit Forma5;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Registry;

type
  TForm100 = class(TForm)
    Panel1: TPanel;
    Shape1: TShape;
    procedure FormPaint(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
  procedure Sozdat(Sender: TObject);
  procedure Dobavit(Sender: TObject);
  procedure Ydalenie(Sender: TObject);
  procedure Vosproizvedenie(Sender: TObject);
  procedure EffectProsmotra(Sender: TObject);
  procedure WMMoving(var Msg: TWMMoving); message WM_MOVING;
    { Public declarations }
  end;

var
  Form100: TForm100;
  Prosmotr:array[1..15000] of TShape;

implementation

uses Unit1, Unit3;


{$R *.dfm}

{��������� ������� ������������ �� ������ ����������}
function NbrD(b:string):integer;
    external 'NomberDiod.dll';{����� ���������� � ����� ����������}
function NbrS(b:string):integer;
    external 'NomberStroki.dll'; {����� ���������� � ����� ������}
function NbrP(b:string):integer;
    external 'NomberPanel.dll'; {����� ������}
function dis(a,b,c,d:string):integer;
    external 'faight.dll'; {����� ������}

procedure TForm100.FormPaint(Sender: TObject);
begin
Form1.N5.Checked:=true;
end;

procedure TForm100.FormClose(Sender: TObject; var Action: TCloseAction);
begin
Form1.N5.Checked:=false;
end;

procedure TForm100.FormCreate(Sender: TObject);
var i,i1,x,y,i2:integer;
Registry: TRegistry;
begin
x:=0;
y:=0;
i:=0;
For i2:=1 to 15 do
Begin
For i1:=1 to 8 do
Begin
i:=i+1;
Prosmotr[i]:=TShape.Create(Panel1);
Prosmotr[i].Parent:=Panel1;
Prosmotr[i].Shape:=stCircle;
Prosmotr[i].Left:=x;
Prosmotr[i].Top:=y;
Prosmotr[i].Width:=33;
Prosmotr[i].Height:=33;
if i>8 then
Prosmotr[i].Visible:=false;
x:=x+33;
end;
x:=0;
y:=y+33;
end;
Registry := TRegistry.Create;
Registry.RootKey := hkey_local_machine;
Registry.OpenKey('system\getinominget',true);
Shape1.Brush.Color:=StringToColor(Registry.ReadString('getir1'));
For i:=1 to 15*8 do
Begin
Prosmotr[i].Brush.Color:=StringToColor(Registry.ReadString('getir3'));
end;
Registry.CloseKey;
Registry.Free;
end;

procedure TForm100.FormResize(Sender: TObject);
var i1,i2,i,m:integer;
begin
Panel1.Left:=Round(Form100.ClientWidth/2)-Round(Panel1.Width/2);
Panel1.Top:=Round(Form100.ClientHeight/2)-Round(Panel1.Height/2);
m:=0;
For i:=1 to 15 do
Begin
if Prosmotr[i*8].Visible=true then
m:=m+1;
end;
i:=0;
For i1:=1 to 15 do
Begin
For i2:=1 to 8 do
Begin
i:=i+1;
Prosmotr[i].Width:=round(Form100.ClientWidth/8);
Prosmotr[i].Height:=round(Form100.ClientHeight/m);
Prosmotr[i].Top:=Prosmotr[i].Width*(i1-1);
Prosmotr[i].Left:=Prosmotr[i].Width*(i2-1);
end;
end;
end;

procedure TForm100.Sozdat(Sender: TObject);
var i:integer;
begin
For i:=9 to 8*15 do
Begin
if Prosmotr[i].Visible=true then
Prosmotr[i].Visible:=false;
end;
end;

procedure TForm100.Dobavit(Sender: TObject);
var i,m,p,l,b,d:integer;
begin
For l:=1 to 15 do
Begin
if Prosmotr[l*8].Visible=false then
Begin
For i:=1 to 8 do
Begin
m:=(i*15*8+1)-(15*8+1)+l*8-8;
For p:=1 to 8 do
Begin
m:=m+1;
if m<=8*15 then
Prosmotr[m].Visible:=true;
end;
end;
Break;
end;
end;
end;

procedure TForm100.Ydalenie(Sender: TObject);
var i,m,p,l,b,d,k:integer;
Begin
k:=16;
For l:=1 to 15 do
Begin
K:=k-1;
if Prosmotr[k*8].Visible=true then
Begin
For i:=1 to 8 do
Begin
m:=(i*15*8+1)-(15*8+1)+k*8-8;
For p:=1 to 8 do
Begin
m:=m+1;
if m<=8*15 then
Begin
Prosmotr[m].Visible:=false;
end;
end;
end;
Break;
end;
end;
end;

procedure TForm100.Vosproizvedenie(Sender: TObject);
var i:integer;
Registry:TRegistry;
f:char;
begin
Registry := TRegistry.Create;
Registry.RootKey := hkey_local_machine;
Registry.OpenKey('system\getinominget',true);
For i:=1 to 8*15 do
Prosmotr[i].Brush.Color:=StringToColor(Registry.ReadString('getir3'));
Registry.CloseKey;
Registry.Free;
end;

procedure TForm100.EffectProsmotra(Sender: TObject);
var i1,i2,i:integer;
Registry:TRegistry;
begin
i:=0;
Registry := TRegistry.Create;
Registry.RootKey := hkey_local_machine;
Registry.OpenKey('system\getinominget',true);
For i1:=1 to 15 do
Begin
if Prosmotr[i1*8].Visible=true then
Begin
For i2:=1 to 8 do
Begin
i:=i+1;
if copy((Form3.StringGrid1.Cells[NbrS(IntToStr(i)),Strtoint(Form1.Edit3.Text)+NbrP(IntToStr(i))]),NbrD(IntToStr(i)),1)='0' then
Prosmotr[i].Brush.Color:=StringToColor(Registry.ReadString('getir2'))
else
Prosmotr[i].Brush.Color:=StringToColor(Registry.ReadString('getir3'));
end;
end;
end;
Registry.CloseKey;
Registry.Free;
end;

procedure TForm100.WMMoving(var Msg: TWMMoving);
var
workArea: TRect;
begin
workArea := Screen.WorkareaRect;
with Msg.DragRect^ do
begin
if Left < workArea.Left then
OffsetRect(Msg.DragRect^, workArea.Left - Left, 0) ;
if Top < workArea.Top then
OffsetRect(Msg.DragRect^, 0, workArea.Top - Top) ;
if Right > workArea.Right then
OffsetRect(Msg.DragRect^, workArea.Right - Right, 0) ;
if Bottom > workArea.Bottom then
OffsetRect(Msg.DragRect^, 0, workArea.Bottom - Bottom) ;
end;
inherited;
end;

end.
