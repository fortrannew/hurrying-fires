unit Unit21;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Registry, ExtCtrls, sLabel, ComCtrls, acProgressBar,
  RXCtrls, SRLabel;

type
  TZaschita = class(TForm)
    Image1: TImage;
    Timer1: TTimer;
    sLabelFX1: TsLabelFX;
    sProgressBar1: TsProgressBar;
    sLabelFX2: TsLabelFX;
    sLabelFX3: TSRLabel;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
  public
  Label1: string;
  procedure Soobschenie(Sender: TObject);
    { Public declarations }
  end;

var
  Zaschita: TZaschita;

implementation

uses Unit1;

{$R *.dfm}

procedure TZaschita.FormCreate(Sender: TObject);
var i:integer;
Registry:TRegistry;
begin
Registry := TRegistry.Create;
Registry.RootKey := hkey_current_user;
Registry.OpenKey('Software\fyjn',true);
if Registry.ValueExists('salteerhey')=false then
Label1:='������'
else
Label1:='�����';
Registry.CloseKey;
Registry.Free;
end;

procedure TZaschita.Soobschenie(Sender: TObject);
begin
MessageDlg('�� ������� ��������� ������!', mtError, [mbOk] , 0) ;
end;

procedure TZaschita.Timer1Timer(Sender: TObject);
begin
Timer1.Enabled:=false; // ������������� ���������� ���������� �������
if Form1.Visible then Close; // ���� ������� ���� ��� �����, �� ������� ����-��������
end;

end.
