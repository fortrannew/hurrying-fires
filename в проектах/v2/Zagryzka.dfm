object Form20: TForm20
  Left = 205
  Top = 179
  BorderStyle = bsDialog
  Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1092#1072#1081#1083#1086#1074
  ClientHeight = 448
  ClientWidth = 664
  Color = clBtnFace
  DefaultMonitor = dmMainForm
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 649
    Height = 393
    BiDiMode = bdLeftToRight
    Caption = #1048#1084#1103' '#1092#1072#1081#1083#1072
    Ctl3D = True
    ParentBiDiMode = False
    ParentCtl3D = False
    ParentShowHint = False
    ShowHint = False
    TabOrder = 0
  end
  object Button51: TButton
    Left = 224
    Top = 416
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 1
    OnClick = Button51Click
  end
  object Button52: TButton
    Left = 304
    Top = 416
    Width = 75
    Height = 25
    Caption = '&'#1054#1090#1084#1077#1085#1080#1090#1100
    TabOrder = 2
    OnClick = Button52Click
  end
  object Button53: TButton
    Left = 384
    Top = 416
    Width = 75
    Height = 25
    Caption = '&'#1042#1086#1089#1089#1090#1072#1085#1086#1074#1080#1090#1100
    TabOrder = 3
    OnClick = Button53Click
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Binary image (*.bin)|*.bin'
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofEnableSizing]
    Left = 80
    Top = 408
  end
end
