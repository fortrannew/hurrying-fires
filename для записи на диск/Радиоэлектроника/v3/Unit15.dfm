object Form15: TForm15
  Left = 276
  Top = 368
  BorderStyle = bsToolWindow
  Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077
  ClientHeight = 134
  ClientWidth = 447
  Color = clBlack
  TransparentColorValue = clNavy
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Text3D1: TText3D
    Left = 0
    Top = 0
    Width = 447
    Height = 134
    QualityType = qtBest
    RefreshDelay = 10
    Active = False
    Items.Strings = (
      #1048#1084#1103' '#1087#1088#1086#1075#1088#1072#1084#1084#1099': '
      'Hurrying fires'
      #1042#1077#1088#1089#1080#1103' '#1087#1088#1086#1075#1088#1072#1084#1084#1099':'
      '3.0.0.0'
      #1040#1074#1090#1086#1088': '
      #1043#1086#1088#1103#1095#1082#1080#1085' '#1048#1074#1072#1085
      #1057#1076#1077#1083#1072#1085#1086' '#1087#1086' '#1079#1072#1082#1072#1079#1091
      #1088#1072#1076#1080#1086#1090#1077#1093#1085#1080#1095#1077#1089#1082#1086#1075#1086' '#1082#1088#1091#1078#1082#1072
      #1056#1091#1082#1086#1074#1086#1076#1080#1090#1077#1083#1100' '#1088#1072#1076#1080#1086#1082#1088#1091#1078#1082#1072':'
      #1063#1072#1083#1099#1081' '#1043#1077#1085#1085#1072#1076#1080#1081
      #1046#1077#1083#1072#1077#1084' '#1074#1089#1077#1084
      #1091#1089#1087#1077#1093#1086#1074' '#1074' '#1087#1088#1086#1075#1088#1072#1084#1084#1080#1088#1086#1074#1072#1085#1080#1080'!'
      #1057#1087#1072#1089#1080#1073#1086' '#1079#1072' '#1080#1089#1087#1086#1083#1100#1079#1086#1074#1072#1085#1080#1077
      #1076#1072#1085#1085#1086#1081' '#1087#1088#1086#1075#1088#1072#1084#1084#1099
      'FORTRAN NEW Company. 2011.')
    BackColor = clBlack
    NewRowDelay = 6000
    Scale = 1.000000000000000000
    LinesCount = 2
    LiteraDepth = 1.500000000000000000
    VisionAngle = 30
    ChangeStyle = csDiffusion
    RowsGap = 3.000000000000000000
    WaveAmplitude = 0.300000011920929000
    FontName = 'Arial Cyr'
    FontSize = 40
    FontStyle = []
    FontCharset = DEFAULT_CHARSET
    FontColor = clBlue
    LightIntensity = 127
    AnimationSet = [atFixedRotation, atSingleWave, atRotation]
    DefaultRotationPeriod = 3000
    Align = alClient
  end
end
