program Hurrying_fires;

uses
  Forms,
  Unit1 in 'Unit1.pas' {Form1},
  Unit6 in 'Unit6.pas' {Form6},
  Unit12 in 'Unit12.pas' {Form12},
  Unit2 in 'Unit2.pas' {Form2},
  Unit3 in 'Unit3.pas' {Form3},
  Unit14 in 'Unit14.pas' {Form14},
  Unit15 in 'Unit15.pas' {Form15},
  Unit16 in 'Unit16.pas' {Form16},
  Unit17 in 'Unit17.pas' {Form17},
  Forma5 in 'Forma5.pas' {Form100},
  Unit4 in 'Unit4.pas' {Form4},
  Zagryzka in 'Zagryzka.pas' {Form20},
  Unit21 in 'Unit21.pas' {Zaschita},
  Unit37 in 'Unit37.pas' {Form37};

{$R *.res}
begin
Application.Initialize;
Application.CreateForm(TZaschita, Zaschita);
  Application.CreateForm(TForm37, Form37);
  Application.Title := 'Hurrying fires';
  Zaschita.Destroy;
  Zaschita:=TZaschita.Create(Application);
  Zaschita.Show;
  Zaschita.Update;
  Application.CreateForm(TForm1, Form1);
  Zaschita.sProgressBar1.Position:=1;
  Zaschita.sLabelFX3.Caption:='(Загрузка компонентов)';
  Zaschita.sLabelFX3.Update;
  Zaschita.sProgressBar1.Position:=Zaschita.sProgressBar1.Position+1;
  Application.CreateForm(TForm12, Form12);
  Zaschita.sProgressBar1.Position:=Zaschita.sProgressBar1.Position+1;
  Application.CreateForm(TForm2, Form2);
  Zaschita.sProgressBar1.Position:=Zaschita.sProgressBar1.Position+1;
  Application.CreateForm(TForm3, Form3);
  Zaschita.sProgressBar1.Position:=Zaschita.sProgressBar1.Position+1;
  Application.CreateForm(TForm14, Form14);
  Zaschita.sProgressBar1.Position:=Zaschita.sProgressBar1.Position+1;
  Application.CreateForm(TForm15, Form15);
  Zaschita.sProgressBar1.Position:=Zaschita.sProgressBar1.Position+1;
  Application.CreateForm(TForm16, Form16);
  Zaschita.sProgressBar1.Position:=Zaschita.sProgressBar1.Position+1;
  Application.CreateForm(TForm100, Form100);
  Zaschita.sProgressBar1.Position:=Zaschita.sProgressBar1.Position+1;
  Application.CreateForm(TForm4, Form4);
  Zaschita.sProgressBar1.Position:=Zaschita.sProgressBar1.Position+1;
  Application.CreateForm(TForm20, Form20);
  Zaschita.sProgressBar1.Position:=Zaschita.sProgressBar1.Position+1;
  Form1.N10.Click;
  Zaschita.sLabelFX3.Caption:='(Загрузка проекта)';
  Zaschita.sLabelFX3.Update;
  Form1.Formhag(Form1);
  Zaschita.sLabelFX3.Caption:='(Загрузка Hurrying fires)';
  Zaschita.sProgressBar1.Position:=12;
  Zaschita.sLabelFX3.Update;
  Form1.GroupBox1.Visible:=true;
  Form1.GroupBox2.Visible:=true;
  Form1.GroupBox3.Visible:=true;
  Form1.GroupBox4.Visible:=true;
  Form1.sPanel1.visible:=true;
  Form1.sPanel2.visible:=true;
  Form1.sPanel4.Visible:=true;
  Form1.sPanel3.Visible:=true;
  Form1.dobavlen:=false;
  Application.Run;
end.
