object Form6: TForm6
  Left = 156
  Top = 411
  AutoScroll = False
  Caption = 'Form6'
  ClientHeight = 128
  ClientWidth = 256
  Color = clBlack
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnPaint = FormPaint
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 257
    Height = 129
    BevelOuter = bvNone
    Color = clBlack
    TabOrder = 0
    object Shape1: TShape
      Left = 0
      Top = 0
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape2: TShape
      Left = 32
      Top = 0
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape3: TShape
      Left = 64
      Top = 0
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape4: TShape
      Left = 96
      Top = 0
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape5: TShape
      Left = 128
      Top = 0
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape6: TShape
      Left = 160
      Top = 0
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape7: TShape
      Left = 192
      Top = 0
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape8: TShape
      Left = 224
      Top = 0
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape9: TShape
      Left = 0
      Top = 32
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape10: TShape
      Left = 32
      Top = 32
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape11: TShape
      Left = 64
      Top = 32
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape12: TShape
      Left = 96
      Top = 32
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape13: TShape
      Left = 128
      Top = 32
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape14: TShape
      Left = 160
      Top = 32
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape15: TShape
      Left = 192
      Top = 32
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape16: TShape
      Left = 224
      Top = 32
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape17: TShape
      Left = 0
      Top = 64
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape18: TShape
      Left = 32
      Top = 64
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape19: TShape
      Left = 64
      Top = 64
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape20: TShape
      Left = 96
      Top = 64
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape21: TShape
      Left = 128
      Top = 64
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape22: TShape
      Left = 160
      Top = 64
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape23: TShape
      Left = 192
      Top = 64
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape24: TShape
      Left = 224
      Top = 64
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape25: TShape
      Left = 0
      Top = 96
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape26: TShape
      Left = 32
      Top = 96
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape27: TShape
      Left = 64
      Top = 96
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape28: TShape
      Left = 96
      Top = 96
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape29: TShape
      Left = 128
      Top = 96
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape30: TShape
      Left = 160
      Top = 96
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape31: TShape
      Left = 192
      Top = 96
      Width = 33
      Height = 33
      Shape = stCircle
    end
    object Shape32: TShape
      Left = 224
      Top = 96
      Width = 33
      Height = 33
      Shape = stCircle
    end
  end
end
