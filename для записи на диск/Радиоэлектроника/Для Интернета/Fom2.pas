unit Fom2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, sSkinManager, StdCtrls, sButton, sLabel, sEdit, ExtCtrls, Registry;

type
  TFom72 = class(TForm)
    sSkinManager1: TsSkinManager;
    Button1: TButton;
    Button2: TButton;
    Edit1: TEdit;
    Image1: TImage;
    Label1: TLabel;
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fom72: TFom72;

implementation

uses Fom, Unit1;

{$R *.dfm}

procedure TFom72.Button2Click(Sender: TObject);
var Registry :TRegistry;
a,b,c:word;
begin
if Button2.Caption='OK' then
Begin
Fom71.Close;
Registry :=TRegistry.Create;
Registry.RootKey := HKEY_CURRENT_USER;
Registry.OpenKey('Software\Geteroides',true);
DecodeDate(Date,a,b,c);
Registry.WriteString('gatererw',IntToStr(b));
Registry.CloseKey;
Registry.Free;
Registry := TRegistry.Create;
Registry.RootKey := hkey_local_machine;
Registry.OpenKey('Software\texas',true);
if (Registry.ValueExists('sys')=true) then
Registry.DeleteValue('sys');
Registry.CloseKey;
Registry.Free;
end
else
Begin
Fom71.Close;
Form1.Close;
end;
end;

procedure TFom72.Button1Click(Sender: TObject);
var Registry:TRegistry;
f:TextFile;
buf,buf1: String;
a,b,c:word;
begin
assignfile(f,'Obnovlenie.txt');
Reset(f);
readln(f, buf);
CloseFile(f);
Registry := TRegistry.Create;
Registry.RootKey := HKEY_CURRENT_USER;
Registry.OpenKey('Software\Geteroides',true);
buf1:=Registry.ReadString('fatewerq');
Registry.CloseKey;
Registry.Free;
if (Edit1.Text=buf1) and (Edit1.Text=buf) then
Begin
Label1.Caption:='������� �� ������������ �������� Hurrying fires.';
Button2.Caption:='OK';
Button1.Visible:=false;
Button2.Left:=168;
Button2.Top:=136;
Edit1.Visible:=false;
end
else
Begin
Registry := TRegistry.Create;
Registry.RootKey := HKEY_CURRENT_USER;
Registry.OpenKey('Software\Geteroides',true);
if (Registry.ValueExists('fatewerq')=true) then
Registry.DeleteValue('fatewerq');
if (Registry.ValueExists('gatererw')=true) then
Registry.DeleteValue('gatererw');
Registry.CloseKey;
Registry.Free;
Fom71.Close;
Form1.close;
end;

end;

end.
