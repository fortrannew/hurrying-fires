unit Unit12;
{$R System.res}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, IWControl, IWCompButton, XPMan, DBCtrls, DB,
  CustomizeDlg, ExtDlgs, ActnMan, ActnColorMaps, ExtCtrls, Tabs, ColorGrd,
  Spin, Registry;

type
  TForm12 = class(TForm)
    TabSheet: TPageControl;
    Fon: TTabSheet;
    Shape1: TShape;
    Label1: TLabel;
    Label2: TLabel;
    ComboBox1: TComboBox;
    Button5: TButton;
    Panel1: TPanel;
    Panel2: TPanel;
    ListBox1: TListBox;
    ListBox2: TListBox;
    ListBox3: TListBox;
    Lampochki: TTabSheet;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label4: TLabel;
    Label3: TLabel;
    Shape2: TShape;
    Panel3: TPanel;
    ComboBox2: TComboBox;
    Panel4: TPanel;
    ListBox4: TListBox;
    Button4: TButton;
    TabSheet2: TTabSheet;
    Label6: TLabel;
    Label7: TLabel;
    Shape3: TShape;
    Panel5: TPanel;
    ComboBox3: TComboBox;
    Panel6: TPanel;
    ListBox5: TListBox;
    Button6: TButton;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    OpenPictureDialog1: TOpenPictureDialog;
    ColorDialog1: TColorDialog;
    procedure Button2Click(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure ListBox1DrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure ListBox1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ComboBox3Change(Sender: TObject);
    procedure ListBox5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure ComboBox2Change(Sender: TObject);
    procedure ListBox4Click(Sender: TObject);
    procedure ListBox4DrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure Button4Click(Sender: TObject);
  private
  Registry: TRegistry;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form12: TForm12;

implementation

uses Unit1, Unit6, Forma5;

{$R *.dfm}

procedure TForm12.Button2Click(Sender: TObject);
begin
Form12.Close;
end;

procedure TForm12.ComboBox1Change(Sender: TObject);
begin
if ComboBox1.Text='����' then
Begin
Label2.Visible:=true;
Panel1.Visible:=true;
ListBox1.Visible:=true;
Shape1.Visible:=true;
Button5.Visible:=true;
if ComboBox2.Items[2]<>'���� ����' then
ComboBox2.Items.Add('���� ����');
if ComboBox2.Text='���� ����' then
Shape2.Brush.Color:=Shape1.Brush.Color;
if ComboBox3.Items[2]<>'���� ����' then
ComboBox3.Items.Add('���� ����');
if ComboBox3.Text='���� ����' then
Shape3.Brush.Color:=Shape1.Brush.Color;
end;
if ComboBox2.ItemIndex=2 then
Begin
ComboBox2.ItemIndex:=1;
ComboBox2.OnChange(ComboBox2);
end;
if ComboBox3.ItemIndex=2 then
Begin
ComboBox3.ItemIndex:=1;
ComboBox3.OnChange(ComboBox3);
end;
if ComboBox1.Text='���' then
Begin
Label2.Visible:=false;
Panel1.Visible:=false;
ListBox1.Visible:=false;
Shape1.Visible:=false;
Button5.Visible:=false;
Shape1.Brush.Color:=clBtnFace;
ComboBox2.Items.Delete(2);
ComboBox3.Items.Delete(2);
end;
end;

procedure TForm12.Button5Click(Sender: TObject);
begin
ColorDialog1.Color:=Shape1.Brush.Color;
if ColorDialog1.Execute<>false then
Begin
Shape1.Brush.Color:=ColorDialog1.Color;
end;
end;

procedure TForm12.ListBox1DrawItem(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
var
  Bitmap: TBitmap;
  Offset: Integer;
  BMPRect: TRect;
begin
  with (Control as TListBox).Canvas do
  begin
    FillRect(Rect);
    Bitmap := TBitmap.Create;
    Bitmap.LoadFromResourceName(HInstance,ListBox2.Items[index]);
    Offset := 0;
    if Bitmap <> nil then
    begin
      BMPRect := Bounds(Rect.Left+2, Rect.Top+2,
      (Rect.Bottom-Rect.Top-2)*2, Rect.Bottom-Rect.Top-2);
      {StretchDraw(BMPRect, Bitmap); ����� ������ ����������, �� ����� ������� ������ ���}
      BrushCopy(BMPRect,Bitmap, Bounds(0, 0, Bitmap.Width, Bitmap.Height),
      Bitmap.Canvas.Pixels[0, Bitmap.Height-1]);
      Offset := (Rect.Bottom-Rect.Top+1)*2;
    end;
    TextOut(Rect.Left+Offset, Rect.Top, ListBox1.Items[index]);
    Bitmap.Free;
  end;
end;

procedure TForm12.ListBox1Click(Sender: TObject);
begin
if ComboBox1.Text='����' then
Begin
Shape1.Brush.Color:=StringToColor(ListBox3.Items[ListBox1.ItemIndex]);
if ComboBox2.Text='���� ����' then
Shape2.Brush.Color:=Shape1.Brush.Color;
if ComboBox3.Text='���� ����' then
Shape3.Brush.Color:=Shape1.Brush.Color;
end;
end;

procedure TForm12.Button1Click(Sender: TObject);
var a:integer;
begin
Registry := TRegistry.Create;
Registry.RootKey := hkey_local_machine;
Registry.OpenKey('system\getinominget',true);
Registry.WriteString('getir1',ColorToString(shape1.Brush.Color));
Registry.WriteString('getir2',ColorToString(shape2.Brush.Color));
Registry.WriteString('getir3',ColorToString(shape3.Brush.Color));
Form1.Shape1.Brush.Color:=shape1.Brush.Color;
Form100.Shape1.Brush.Color:=Shape1.Brush.Color;
Registry.CloseKey;
Registry.Free;
Form12.Visible:=false;
Form1.getir2:=shape2.Brush.Color;
Form1.getir3:=shape3.Brush.Color;
if Form1.Timer2.Enabled=true then
Form1.Label7Click(Form1);
Form1.EffectProsmotra(Form1);
end;

procedure TForm12.Button3Click(Sender: TObject);
begin
if ComboBox1.ItemIndex<>1 then
ComboBox1.ItemIndex:=1;
Registry := TRegistry.Create;
  Registry.RootKey := hkey_local_machine;
  Registry.OpenKey('system\getinominget',true);
  Shape1.Brush.Color:=StringToColor(Registry.ReadString('getir1'));
  Shape2.Brush.Color:=StringToColor(Registry.ReadString('getir2'));
  Shape3.Brush.Color:=StringToColor(Registry.ReadString('getir3'));
  Registry.CloseKey;
  Registry.Free;
  if Shape1.Brush.Color=clBtnFace then
  Begin
  ComboBox1.ItemIndex:=0;
  Combobox1.OnChange(Combobox1);
  end;
  if Shape2.Brush.Color=clBtnFace then
  Begin
  ComboBox2.ItemIndex:=0;
  Combobox2.OnChange(Combobox2);
  end;
  if Shape3.Brush.Color=clBtnFace then
  Begin
  ComboBox3.ItemIndex:=0;
  Combobox3.OnChange(Combobox3);
  end;
end;

procedure TForm12.FormCreate(Sender: TObject);
begin
  Registry := TRegistry.Create;
  Registry.RootKey := hkey_local_machine;
  Registry.OpenKey('system\getinominget',true);
  Shape1.Brush.Color:=StringToColor(Registry.ReadString('getir1'));
  Shape2.Brush.Color:=StringToColor(Registry.ReadString('getir2'));
  Shape3.Brush.Color:=StringToColor(Registry.ReadString('getir3'));
  Registry.CloseKey;
  Registry.Free;
  if Shape1.Brush.Color=clBtnFace then
  Begin
  ComboBox1.ItemIndex:=0;
  Combobox1.OnChange(Combobox1);
  end;
  if Shape2.Brush.Color=clBtnFace then
  Begin
  ComboBox2.ItemIndex:=0;
  Combobox2.OnChange(Combobox2);
  end;
  if Shape3.Brush.Color=clBtnFace then
  Begin
  ComboBox3.ItemIndex:=0;
  Combobox3.OnChange(Combobox3);
  end;
end;

procedure TForm12.FormClose(Sender: TObject; var Action: TCloseAction);
begin
Button3.Click;
end;

procedure TForm12.ComboBox3Change(Sender: TObject);
begin
if ComboBox3.Text='���' then
Begin
Label7.Visible:=false;
Panel6.Visible:=false;
ListBox5.Visible:=false;
Shape3.Visible:=false;
Button6.Visible:=false;
Shape3.Brush.Color:=clBtnFace;
end;
if ComboBox3.Text='����' then
Begin
Label7.Visible:=true;
Panel6.Visible:=true;
ListBox5.Visible:=true;
Shape3.Visible:=true;
Button6.Visible:=true;
end;
if ComboBox3.Text='���� ����' then
Begin
Label7.Visible:=true;
Panel6.Visible:=true;
ListBox5.Visible:=true;
Shape3.Visible:=true;
Button6.Visible:=true;
Shape3.Brush.Color:=Shape1.Brush.Color;
if ComboBox2.Text='���� ����' then
Shape2.Brush.Color:=Shape1.Brush.Color;
end;
end;

procedure TForm12.ListBox5Click(Sender: TObject);
begin
if ComboBox3.Text='����' then
Begin
Shape3.Brush.Color:=StringToColor(ListBox3.Items[ListBox5.ItemIndex]);
end;
if ComboBox3.Text='���� ����' then
Begin
Shape3.Brush.Color:=StringToColor(ListBox3.Items[ListBox5.ItemIndex]);
Shape1.Brush.Color:=Shape3.Brush.Color;
if ComboBox2.Text='���� ����' then
Shape2.Brush.Color:=Shape1.Brush.Color;
end;
end;

procedure TForm12.Button6Click(Sender: TObject);
begin
ColorDialog1.Color:=Shape3.Brush.Color;
if ColorDialog1.Execute<>false then
Begin
Shape3.Brush.Color:=ColorDialog1.Color;
if ComboBox3.Text='���� ����' then
Begin
Shape1.Brush.Color:=Shape3.Brush.Color;
if ComboBox2.Text='���� ����' then
Shape2.Brush.Color:=Shape3.Brush.Color;
end;
end;
end;

procedure TForm12.ComboBox2Change(Sender: TObject);
begin
if ComboBox2.Text='���' then
Begin
Label3.Visible:=false;
Panel4.Visible:=false;
ListBox4.Visible:=false;
Shape2.Visible:=false;
Button4.Visible:=false;
Shape2.Brush.Color:=clBtnFace;
end;
if ComboBox2.Text='����' then
Begin
Label3.Visible:=true;
Panel4.Visible:=true;
ListBox4.Visible:=true;
Shape2.Visible:=true;
Button4.Visible:=true;
end;
if ComboBox2.Text='���� ����' then
Begin
Label3.Visible:=true;
Panel4.Visible:=true;
ListBox4.Visible:=true;
Shape2.Visible:=true;
Button4.Visible:=true;
Shape2.Brush.Color:=Shape1.Brush.Color;
end;
end;

procedure TForm12.ListBox4Click(Sender: TObject);
begin
if ComboBox2.Text='����' then
Begin
Shape2.Brush.Color:=StringToColor(ListBox3.Items[ListBox4.ItemIndex]);
end;
if ComboBox2.Text='���� ����' then
Begin
Shape2.Brush.Color:=StringToColor(ListBox3.Items[ListBox4.ItemIndex]);
Shape1.Brush.Color:=Shape2.Brush.Color;
if ComboBox3.Text='���� ����' then
Shape3.Brush.Color:=Shape1.Brush.Color;
end;
end;

procedure TForm12.ListBox4DrawItem(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
var
  Bitmap: TBitmap;
  Offset: Integer;
  BMPRect: TRect;
  i:integer;
begin
i:=-1;
 with (Control as TListBox).Canvas do
  begin
    FillRect(Rect);
    Bitmap := TBitmap.Create;
    Bitmap.LoadFromResourceName(HInstance,ListBox2.Items[index]);
    Offset := 0;
    if Bitmap <> nil then
    begin
      BMPRect := Bounds(Rect.Left+2, Rect.Top+2,
      (Rect.Bottom-Rect.Top-2)*2, Rect.Bottom-Rect.Top-2);
      {StretchDraw(BMPRect, Bitmap); ����� ������ ����������, �� ����� ������� ������ ���}
      BrushCopy(BMPRect,Bitmap, Bounds(0, 0, Bitmap.Width, Bitmap.Height),
      Bitmap.Canvas.Pixels[0, Bitmap.Height-1]);
      Offset := (Rect.Bottom-Rect.Top+1)*2;
    end;
    TextOut(Rect.Left+Offset, Rect.Top, ListBox1.Items[index]);
    Bitmap.Free;
  end;
end;

procedure TForm12.Button4Click(Sender: TObject);
begin
ColorDialog1.Color:=Shape2.Brush.Color;
if ColorDialog1.Execute<>false then
Begin
Shape2.Brush.Color:=ColorDialog1.Color;
if ComboBox2.Text='���� ����' then
Begin
Shape1.Brush.Color:=Shape2.Brush.Color;
if ComboBox3.Text='���� ����' then
Shape3.Brush.Color:=Shape2.Brush.Color;
end;
end;
end;

end.
