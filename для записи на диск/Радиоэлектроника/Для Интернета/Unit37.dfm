object Form37: TForm37
  Left = 412
  Top = 255
  BorderStyle = bsDialog
  Caption = #1057#1084#1077#1085#1072' '#1094#1074#1077#1090#1086#1074
  ClientHeight = 302
  ClientWidth = 217
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object sButton1: TsButton
    Left = 24
    Top = 272
    Width = 75
    Height = 25
    Caption = #1054#1050
    TabOrder = 0
    OnClick = sButton1Click
    SkinData.SkinSection = 'BUTTON'
  end
  object sButton2: TsButton
    Left = 112
    Top = 272
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 1
    OnClick = sButton2Click
    SkinData.SkinSection = 'BUTTON'
  end
  object sGroupBox1: TsGroupBox
    Left = 8
    Top = 0
    Width = 201
    Height = 265
    TabOrder = 2
    SkinData.SkinSection = 'GROUPBOX'
    object sLabel24: TsLabel
      Left = 8
      Top = 200
      Width = 45
      Height = 13
      Caption = #1053#1077' '#1075#1086#1088#1080#1090
      ParentFont = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
    end
    object sLabel23: TsLabel
      Left = 8
      Top = 136
      Width = 45
      Height = 13
      Caption = #1062#1074#1077#1090' '#8470'3'
      ParentFont = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
    end
    object sLabel20: TsLabel
      Left = 88
      Top = 8
      Width = 74
      Height = 13
      Caption = #1057#1084#1077#1085#1080#1090#1100' '#1085#1072':'
      ParentFont = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
    end
    object sLabel18: TsLabel
      Left = 8
      Top = 72
      Width = 45
      Height = 13
      Caption = #1062#1074#1077#1090' '#8470'2'
      ParentFont = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
    end
    object sLabel17: TsLabel
      Left = 8
      Top = 8
      Width = 45
      Height = 13
      Caption = #1062#1074#1077#1090' '#8470'1'
      ParentFont = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
    end
    object sPanel8: TsPanel
      Left = 8
      Top = 216
      Width = 41
      Height = 41
      TabOrder = 0
      SkinData.SkinSection = 'PANEL'
      object Shape4: TShape
        Left = 8
        Top = 8
        Width = 25
        Height = 25
        Pen.Style = psClear
      end
      object sBevel4: TsBevel
        Left = 8
        Top = 8
        Width = 25
        Height = 25
      end
    end
    object sPanel7: TsPanel
      Left = 8
      Top = 152
      Width = 41
      Height = 41
      TabOrder = 1
      SkinData.SkinSection = 'PANEL'
      object Shape3: TShape
        Left = 8
        Top = 8
        Width = 25
        Height = 25
        Pen.Style = psClear
      end
      object sBevel3: TsBevel
        Left = 8
        Top = 8
        Width = 25
        Height = 25
      end
    end
    object sPanel6: TsPanel
      Left = 8
      Top = 88
      Width = 41
      Height = 41
      TabOrder = 2
      SkinData.SkinSection = 'PANEL'
      object Shape2: TShape
        Left = 8
        Top = 8
        Width = 25
        Height = 25
        Pen.Style = psClear
      end
      object sBevel2: TsBevel
        Left = 8
        Top = 8
        Width = 25
        Height = 25
      end
    end
    object sPanel5: TsPanel
      Left = 8
      Top = 24
      Width = 41
      Height = 41
      TabOrder = 3
      SkinData.SkinSection = 'PANEL'
      object Shape1: TShape
        Left = 8
        Top = 8
        Width = 25
        Height = 25
        Pen.Style = psClear
      end
      object sBevel1: TsBevel
        Left = 8
        Top = 8
        Width = 25
        Height = 25
      end
    end
    object sPanel4: TsPanel
      Left = 56
      Top = 216
      Width = 137
      Height = 41
      TabOrder = 4
      SkinData.SkinSection = 'PANEL'
      object Shape20: TShape
        Left = 104
        Top = 8
        Width = 25
        Height = 25
        Pen.Style = psClear
      end
      object Shape19: TShape
        Left = 72
        Top = 8
        Width = 25
        Height = 25
        Pen.Style = psClear
      end
      object Shape18: TShape
        Left = 40
        Top = 8
        Width = 25
        Height = 25
        Pen.Style = psClear
      end
      object Shape17: TShape
        Left = 8
        Top = 8
        Width = 25
        Height = 25
        Pen.Style = psClear
      end
      object Bevel13: TBevel
        Left = 8
        Top = 8
        Width = 25
        Height = 25
      end
      object Bevel14: TBevel
        Left = 40
        Top = 8
        Width = 25
        Height = 25
      end
      object Bevel15: TBevel
        Left = 72
        Top = 8
        Width = 25
        Height = 25
      end
      object Bevel16: TBevel
        Left = 104
        Top = 8
        Width = 25
        Height = 25
      end
      object sLabel13: TsLabel
        Left = 8
        Top = 8
        Width = 25
        Height = 25
        AutoSize = False
        ParentFont = False
        OnClick = sLabel13Click
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
      end
      object sLabel14: TsLabel
        Left = 40
        Top = 8
        Width = 25
        Height = 25
        AutoSize = False
        ParentFont = False
        OnClick = sLabel14Click
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
      end
      object sLabel15: TsLabel
        Left = 72
        Top = 8
        Width = 25
        Height = 25
        AutoSize = False
        ParentFont = False
        OnClick = sLabel15Click
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
      end
      object sLabel16: TsLabel
        Left = 104
        Top = 8
        Width = 25
        Height = 25
        AutoSize = False
        ParentFont = False
        OnClick = sLabel16Click
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
      end
    end
    object sPanel3: TsPanel
      Left = 56
      Top = 24
      Width = 137
      Height = 41
      TabOrder = 5
      SkinData.SkinSection = 'PANEL'
      object Shape8: TShape
        Left = 104
        Top = 8
        Width = 25
        Height = 25
        Pen.Style = psClear
      end
      object Shape7: TShape
        Left = 72
        Top = 8
        Width = 25
        Height = 25
        Pen.Style = psClear
      end
      object Shape6: TShape
        Left = 40
        Top = 8
        Width = 25
        Height = 25
        Pen.Style = psClear
      end
      object Shape5: TShape
        Left = 8
        Top = 8
        Width = 25
        Height = 25
        Pen.Style = psClear
      end
      object Bevel1: TBevel
        Left = 8
        Top = 8
        Width = 25
        Height = 25
      end
      object Bevel2: TBevel
        Left = 40
        Top = 8
        Width = 25
        Height = 25
      end
      object Bevel3: TBevel
        Left = 72
        Top = 8
        Width = 25
        Height = 25
      end
      object Bevel4: TBevel
        Left = 104
        Top = 8
        Width = 25
        Height = 25
      end
      object sLabel1: TsLabel
        Left = 8
        Top = 8
        Width = 25
        Height = 25
        AutoSize = False
        ParentFont = False
        OnClick = sLabel1Click
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
      end
      object sLabel2: TsLabel
        Left = 40
        Top = 8
        Width = 25
        Height = 25
        AutoSize = False
        ParentFont = False
        OnClick = sLabel2Click
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
      end
      object sLabel3: TsLabel
        Left = 72
        Top = 8
        Width = 25
        Height = 25
        AutoSize = False
        ParentFont = False
        OnClick = sLabel3Click
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
      end
      object sLabel4: TsLabel
        Left = 104
        Top = 8
        Width = 25
        Height = 25
        AutoSize = False
        ParentFont = False
        OnClick = sLabel4Click
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
      end
    end
    object sPanel2: TsPanel
      Left = 56
      Top = 152
      Width = 137
      Height = 41
      TabOrder = 6
      SkinData.SkinSection = 'PANEL'
      object Shape16: TShape
        Left = 104
        Top = 8
        Width = 25
        Height = 25
        Pen.Style = psClear
      end
      object Shape15: TShape
        Left = 72
        Top = 8
        Width = 25
        Height = 25
        Pen.Style = psClear
      end
      object Shape14: TShape
        Left = 40
        Top = 8
        Width = 25
        Height = 25
        Pen.Style = psClear
      end
      object Shape13: TShape
        Left = 8
        Top = 8
        Width = 25
        Height = 25
        Pen.Style = psClear
      end
      object Bevel9: TBevel
        Left = 8
        Top = 8
        Width = 25
        Height = 25
      end
      object Bevel10: TBevel
        Left = 40
        Top = 8
        Width = 25
        Height = 25
      end
      object Bevel11: TBevel
        Left = 72
        Top = 8
        Width = 25
        Height = 25
      end
      object Bevel12: TBevel
        Left = 104
        Top = 8
        Width = 25
        Height = 25
      end
      object sLabel9: TsLabel
        Left = 8
        Top = 8
        Width = 25
        Height = 25
        AutoSize = False
        ParentFont = False
        OnClick = sLabel9Click
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
      end
      object sLabel10: TsLabel
        Left = 40
        Top = 8
        Width = 25
        Height = 25
        AutoSize = False
        ParentFont = False
        OnClick = sLabel10Click
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
      end
      object sLabel11: TsLabel
        Left = 72
        Top = 8
        Width = 25
        Height = 25
        AutoSize = False
        ParentFont = False
        OnClick = sLabel11Click
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
      end
      object sLabel12: TsLabel
        Left = 104
        Top = 8
        Width = 25
        Height = 25
        AutoSize = False
        ParentFont = False
        OnClick = sLabel12Click
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
      end
    end
    object sPanel1: TsPanel
      Left = 56
      Top = 88
      Width = 137
      Height = 41
      TabOrder = 7
      SkinData.SkinSection = 'PANEL'
      object Shape12: TShape
        Left = 104
        Top = 8
        Width = 25
        Height = 25
        Pen.Style = psClear
      end
      object Shape11: TShape
        Left = 72
        Top = 8
        Width = 25
        Height = 25
        Pen.Style = psClear
      end
      object Shape10: TShape
        Left = 40
        Top = 8
        Width = 25
        Height = 25
        Pen.Style = psClear
      end
      object Shape9: TShape
        Left = 8
        Top = 8
        Width = 25
        Height = 25
        Pen.Style = psClear
      end
      object Bevel5: TBevel
        Left = 8
        Top = 8
        Width = 25
        Height = 25
      end
      object Bevel6: TBevel
        Left = 40
        Top = 8
        Width = 25
        Height = 25
      end
      object Bevel7: TBevel
        Left = 72
        Top = 8
        Width = 25
        Height = 25
      end
      object Bevel8: TBevel
        Left = 104
        Top = 8
        Width = 25
        Height = 25
      end
      object sLabel5: TsLabel
        Left = 8
        Top = 8
        Width = 25
        Height = 25
        AutoSize = False
        ParentFont = False
        OnClick = sLabel5Click
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
      end
      object sLabel6: TsLabel
        Left = 40
        Top = 8
        Width = 25
        Height = 25
        AutoSize = False
        ParentFont = False
        OnClick = sLabel6Click
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
      end
      object sLabel7: TsLabel
        Left = 72
        Top = 8
        Width = 25
        Height = 25
        AutoSize = False
        ParentFont = False
        OnClick = sLabel7Click
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
      end
      object sLabel8: TsLabel
        Left = 104
        Top = 8
        Width = 25
        Height = 25
        AutoSize = False
        ParentFont = False
        OnClick = sLabel8Click
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
      end
    end
  end
end
