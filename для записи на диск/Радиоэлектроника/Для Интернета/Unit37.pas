unit Unit37;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, sLabel, ExtCtrls, sPanel, sBevel, sButton, sGroupBox, Registry;

type
  TForm37 = class(TForm)
    sButton1: TsButton;
    sButton2: TsButton;
    sGroupBox1: TsGroupBox;
    sPanel8: TsPanel;
    Shape4: TShape;
    sBevel4: TsBevel;
    sPanel7: TsPanel;
    Shape3: TShape;
    sBevel3: TsBevel;
    sPanel6: TsPanel;
    Shape2: TShape;
    sBevel2: TsBevel;
    sPanel5: TsPanel;
    Shape1: TShape;
    sBevel1: TsBevel;
    sPanel4: TsPanel;
    Shape20: TShape;
    Shape19: TShape;
    Shape18: TShape;
    Shape17: TShape;
    Bevel13: TBevel;
    Bevel14: TBevel;
    Bevel15: TBevel;
    Bevel16: TBevel;
    sLabel13: TsLabel;
    sLabel14: TsLabel;
    sLabel15: TsLabel;
    sLabel16: TsLabel;
    sPanel3: TsPanel;
    Shape8: TShape;
    Shape7: TShape;
    Shape6: TShape;
    Shape5: TShape;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Bevel3: TBevel;
    Bevel4: TBevel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sLabel3: TsLabel;
    sLabel4: TsLabel;
    sPanel2: TsPanel;
    Shape16: TShape;
    Shape15: TShape;
    Shape14: TShape;
    Shape13: TShape;
    Bevel9: TBevel;
    Bevel10: TBevel;
    Bevel11: TBevel;
    Bevel12: TBevel;
    sLabel9: TsLabel;
    sLabel10: TsLabel;
    sLabel11: TsLabel;
    sLabel12: TsLabel;
    sPanel1: TsPanel;
    Shape12: TShape;
    Shape11: TShape;
    Shape10: TShape;
    Shape9: TShape;
    Bevel5: TBevel;
    Bevel6: TBevel;
    Bevel7: TBevel;
    Bevel8: TBevel;
    sLabel5: TsLabel;
    sLabel6: TsLabel;
    sLabel7: TsLabel;
    sLabel8: TsLabel;
    sLabel24: TsLabel;
    sLabel23: TsLabel;
    sLabel20: TsLabel;
    sLabel18: TsLabel;
    sLabel17: TsLabel;
    procedure sButton2Click(Sender: TObject);
    procedure sLabel1Click(Sender: TObject);
    procedure sLabel2Click(Sender: TObject);
    procedure sLabel3Click(Sender: TObject);
    procedure sLabel4Click(Sender: TObject);
    procedure sLabel5Click(Sender: TObject);
    procedure sLabel6Click(Sender: TObject);
    procedure sLabel7Click(Sender: TObject);
    procedure sLabel8Click(Sender: TObject);
    procedure sLabel9Click(Sender: TObject);
    procedure sLabel10Click(Sender: TObject);
    procedure sLabel11Click(Sender: TObject);
    procedure sLabel12Click(Sender: TObject);
    procedure sLabel13Click(Sender: TObject);
    procedure sLabel14Click(Sender: TObject);
    procedure sLabel15Click(Sender: TObject);
    procedure sLabel16Click(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form37: TForm37;

implementation

uses Unit3, Unit14, Unit1;

{$R *.dfm}

procedure TForm37.sButton2Click(Sender: TObject);
begin
Close;
end;

procedure TForm37.sLabel1Click(Sender: TObject);
begin
Shape1.Brush.Color:=Shape5.Brush.Color;
end;

procedure TForm37.sLabel2Click(Sender: TObject);
begin
Shape1.Brush.Color:=Shape6.Brush.Color;
end;

procedure TForm37.sLabel3Click(Sender: TObject);
begin
Shape1.Brush.Color:=Shape7.Brush.Color;
end;

procedure TForm37.sLabel4Click(Sender: TObject);
begin
Shape1.Brush.Color:=Shape8.Brush.Color;
end;

procedure TForm37.sLabel5Click(Sender: TObject);
begin
Shape2.Brush.Color:=Shape9.Brush.Color;
end;

procedure TForm37.sLabel6Click(Sender: TObject);
begin
Shape2.Brush.Color:=Shape10.Brush.Color;
end;

procedure TForm37.sLabel7Click(Sender: TObject);
begin
Shape2.Brush.Color:=Shape11.Brush.Color;
end;

procedure TForm37.sLabel8Click(Sender: TObject);
begin
Shape2.Brush.Color:=Shape12.Brush.Color;
end;

procedure TForm37.sLabel9Click(Sender: TObject);
begin
Shape3.Brush.Color:=Shape13.Brush.Color;
end;

procedure TForm37.sLabel10Click(Sender: TObject);
begin
Shape3.Brush.Color:=Shape14.Brush.Color;
end;

procedure TForm37.sLabel11Click(Sender: TObject);
begin
Shape3.Brush.Color:=Shape15.Brush.Color;
end;

procedure TForm37.sLabel12Click(Sender: TObject);
begin
Shape3.Brush.Color:=Shape16.Brush.Color;
end;

procedure TForm37.sLabel13Click(Sender: TObject);
begin
Shape4.Brush.Color:=Shape17.Brush.Color;
end;

procedure TForm37.sLabel14Click(Sender: TObject);
begin
Shape4.Brush.Color:=Shape18.Brush.Color;
end;

procedure TForm37.sLabel15Click(Sender: TObject);
begin
Shape4.Brush.Color:=Shape19.Brush.Color;
end;

procedure TForm37.sLabel16Click(Sender: TObject);
begin
Shape4.Brush.Color:=Shape20.Brush.Color;
end;

procedure TForm37.sButton1Click(Sender: TObject);
var i1,i2,i,b:integer;
Registry: TRegistry;
s1,s2:string;
yach:array[1..23] of string;
begin
if Form1.Edit5.Text='' then
Form1.Edit5.Text:='0';
if Form14.sRadioButton1.Checked=true then
Begin
For i:=1 to 8 do
Begin
if (Form1.Panelete[i].BevelInner=bvRaised) and (Form1.Panelete[i].BorderStyle=bsSingle) then
i2:=StrToInt(Form1.Edit5.Text)+i;
end;
b:=11;
For i1:=1 to 11 do
Begin
b:=b+1;
s1:=Form3.StringGrid1.Cells[i1,i2];
s2:=Form3.StringGrid1.Cells[b,i2];
For i:=1 to 8 do
Begin
if shape1.Brush.Color=shape5.Brush.Color then
Begin
if (s1[i]='0') and (s2[i]='1') then
Begin
s1[i]:='0';
s2[i]:='1';
end;
end;
if shape1.Brush.Color=shape6.Brush.Color then
Begin
if (s1[i]='0') and (s2[i]='1') then
Begin
s1[i]:='1';
s2[i]:='0';
end;
end;
if shape1.Brush.Color=shape7.Brush.Color then
Begin
if (s1[i]='0') and (s2[i]='1') then
Begin
s1[i]:='0';
s2[i]:='0';
end;
end;
if shape1.Brush.Color=shape8.Brush.Color then
Begin
if (s1[i]='0') and (s2[i]='1') then
Begin
s1[i]:='1';
s2[i]:='1';
end;
end;
end;
Form1.StringGrid1.Cells[i1,i2]:=s1;
Form1.StringGrid1.Cells[b,i2]:=s2;
end;
b:=11;
For i1:=1 to 11 do
Begin
b:=b+1;
s1:=Form3.StringGrid1.Cells[i1,i2];
s2:=Form3.StringGrid1.Cells[b,i2];
For i:=1 to 8 do
Begin
if shape2.Brush.Color=shape5.Brush.Color then
Begin
if (s1[i]='1') and (s2[i]='0') then
Begin
s1[i]:='0';
s2[i]:='1';
end;
end;
if shape2.Brush.Color=shape6.Brush.Color then
Begin
if (s1[i]='1') and (s2[i]='0') then
Begin
s1[i]:='1';
s2[i]:='0';
end;
end;
if shape2.Brush.Color=shape7.Brush.Color then
Begin
if (s1[i]='1') and (s2[i]='0') then
Begin
s1[i]:='0';
s2[i]:='0';
end;
end;
if shape2.Brush.Color=shape8.Brush.Color then
Begin
if (s1[i]='1') and (s2[i]='0') then
Begin
s1[i]:='1';
s2[i]:='1';
end;
end;
end;
Form1.StringGrid1.Cells[i1,i2]:=s1;
Form1.StringGrid1.Cells[b,i2]:=s2;
end;
For i1:=1 to Form3.StringGrid1.ColCount do
Begin
Form3.StringGrid1.Cells[i1,i2]:=Form1.StringGrid1.Cells[i1,i2];
end;
end;
{if Form14.sRadioButton2.Checked=true then
Begin
Registry := TRegistry.Create;
Registry.RootKey := hkey_local_machine;
Registry.OpenKey('system\getinominget',true);
For b:=0 to adressu do
Begin
if Vudelenie[b]=StringToColor(Registry.ReadString('getir7')) then
Begin
i2:=b+1;
For i1:=1 to 8 do
Begin
Form3.StringGrid1.Cells[i1,i2]:=Form3.StringGrid1.Cells[i1+1,i2];
Form1.StringGrid1.Cells[i1,i2]:=Form1.StringGrid1.Cells[i1+1,i2];
end;
Form3.StringGrid1.Cells[9,i2]:='11111111';
Form1.StringGrid1.Cells[9,i2]:='11111111';
Form3.StringGrid1.Cells[10,i2]:=copy(Form3.StringGrid1.Cells[10,i2],2,7);
Form3.StringGrid1.Cells[10,i2]:=Form3.StringGrid1.Cells[10,i2]+'1';
Form1.StringGrid1.Cells[10,i2]:=copy(Form1.StringGrid1.Cells[10,i2],2,7);
Form1.StringGrid1.Cells[10,i2]:=Form1.StringGrid1.Cells[10,i2]+'1';
s1:=Form1.StringGrid1.Cells[10,i2];
s2:=Form1.StringGrid1.Cells[11,i2];
s1[8]:=s2[1];
Form1.StringGrid1.Cells[10,i2]:=s1;
Form3.StringGrid1.Cells[10,i2]:=s1;
Form3.StringGrid1.Cells[11,i2]:='11111111';
Form1.StringGrid1.Cells[11,i2]:='11111111';
For i1:=12 to 19 do
Begin
Form3.StringGrid1.Cells[i1,i2]:=Form3.StringGrid1.Cells[i1+1,i2];
Form1.StringGrid1.Cells[i1,i2]:=Form1.StringGrid1.Cells[i1+1,i2];
end;
Form3.StringGrid1.Cells[20,i2]:='11111111';
Form1.StringGrid1.Cells[20,i2]:='11111111';
Form3.StringGrid1.Cells[21,i2]:=copy(Form3.StringGrid1.Cells[21,i2],2,7);
Form3.StringGrid1.Cells[21,i2]:=Form3.StringGrid1.Cells[21,i2]+'1';
Form1.StringGrid1.Cells[21,i2]:=copy(Form1.StringGrid1.Cells[21,i2],2,7);
Form1.StringGrid1.Cells[21,i2]:=Form1.StringGrid1.Cells[21,i2]+'1';
s1:=Form1.StringGrid1.Cells[21,i2];
s2:=Form1.StringGrid1.Cells[22,i2];
s1[8]:=s2[1];
Form1.StringGrid1.Cells[21,i2]:=s1;
Form3.StringGrid1.Cells[21,i2]:=s1;
Form3.StringGrid1.Cells[22,i2]:='11111111';
Form1.StringGrid1.Cells[22,i2]:='11111111';
Form1.StringGrid1.Cells[22,i2]:='11111111';
end;
end;
Registry.CloseKey;
Registry.Free;
end;  }
Form1.IzmcvShapanede(Form1);
Form1.IzmenSohran(Form1);
Close;
end;

end.
