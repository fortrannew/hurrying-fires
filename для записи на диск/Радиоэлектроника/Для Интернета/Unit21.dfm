object Zaschita: TZaschita
  Left = 333
  Top = 282
  BorderStyle = bsNone
  Caption = #1047#1072#1089#1090#1072#1074#1082#1072
  ClientHeight = 158
  ClientWidth = 290
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object SRGradient1: TSRGradient
    Left = 0
    Top = 0
    Width = 290
    Height = 158
    Align = alClient
    Buffered = True
    Direction = gdDownRight
    EndColor = clBlack
    StartColor = clBlue
    StepWidth = 1
    Style = gsVertical
  end
  object sLabelFX1: TsLabelFX
    Left = 16
    Top = 96
    Width = 137
    Height = 21
    Caption = #1055#1086#1078#1072#1083#1091#1081#1089#1090#1072' '#1087#1086#1076#1086#1078#1076#1080#1090#1077'...'
    ParentFont = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
  end
  object sLabelFX2: TsLabelFX
    Left = 208
    Top = 137
    Width = 80
    Height = 21
    Caption = '3.0.0.0 '#1074#1077#1088#1089#1080#1103
    ParentFont = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
  end
  object sLabelFX3: TSRLabel
    Left = 16
    Top = 139
    Width = 117
    Height = 13
    BevelStyle = bvNone
    BevelWidth = 1
    BorderWidth = 0
    Caption = '('#1047#1072#1075#1088#1091#1079#1082#1072' Hurrying fires)'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGray
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    HoverColor = clBtnFace
    HoverFontColor = clWindowText
    LinkActive = False
    LinkedAdress = 'http://www.picsoft.de'
    LinkType = ltWWW
    ParentFont = False
    ShortenFilenames = False
    ShowHighlight = False
    ShowShadow = False
    Style = lsCustom
    Transparent = True
  end
  object sLabelFX4: TsLabelFX
    Left = 32
    Top = 24
    Width = 222
    Height = 57
    Alignment = taCenter
    Caption = 'Hurrying fires'
    Color = clBlack
    ParentColor = False
    ParentFont = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -40
    Font.Name = 'Monotype Corsiva'
    Font.Style = [fsBold]
    Kind.KindType = ktCustom
    Kind.Color = clRed
  end
  object sLabelFX5: TsLabelFX
    Left = 72
    Top = 72
    Width = 151
    Height = 23
    Alignment = taCenter
    Caption = #1084#1080#1088' '#1088#1072#1076#1080#1086#1101#1083#1077#1082#1090#1088#1086#1085#1085#1080#1082#1080
    ParentFont = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Reference Sans Serif'
    Font.Style = []
    Kind.KindType = ktCustom
    Kind.Color = clYellow
  end
  object sProgressBar1: TsProgressBar
    Left = 8
    Top = 123
    Width = 273
    Height = 16
    Max = 12
    TabOrder = 0
    SkinData.SkinSection = 'GAUGE'
  end
  object Timer1: TTimer
    Interval = 3000
    OnTimer = Timer1Timer
  end
  object sSkinManager1: TsSkinManager
    InternalSkins = <>
    MenuSupport.IcoLineSkin = 'ICOLINE'
    MenuSupport.ExtraLineFont.Charset = DEFAULT_CHARSET
    MenuSupport.ExtraLineFont.Color = clWindowText
    MenuSupport.ExtraLineFont.Height = -11
    MenuSupport.ExtraLineFont.Name = 'MS Sans Serif'
    MenuSupport.ExtraLineFont.Style = []
    SkinDirectory = 'c:\Skins'
    SkinName = 'Office12Style'
    SkinInfo = '7'
    ThirdParty.ThirdEdits = ' '
    ThirdParty.ThirdButtons = 'TButton'
    ThirdParty.ThirdBitBtns = ' '
    ThirdParty.ThirdCheckBoxes = ' '
    ThirdParty.ThirdGroupBoxes = ' '
    ThirdParty.ThirdListViews = ' '
    ThirdParty.ThirdPanels = ' '
    ThirdParty.ThirdGrids = ' '
    ThirdParty.ThirdTreeViews = ' '
    ThirdParty.ThirdComboBoxes = ' '
    ThirdParty.ThirdWWEdits = ' '
    ThirdParty.ThirdVirtualTrees = ' '
    ThirdParty.ThirdGridEh = ' '
    ThirdParty.ThirdPageControl = ' '
    ThirdParty.ThirdTabControl = ' '
    ThirdParty.ThirdToolBar = ' '
    ThirdParty.ThirdStatusBar = ' '
    ThirdParty.ThirdSpeedButton = ' '
  end
end
